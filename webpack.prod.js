const webpack = require('webpack');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack-plugin').default;

const webpackMerged = {
  mode: 'production',
  devtool: 'source-map',
  stats: {
    warnings: false,
  },
  optimization: {
    minimizer: [
      new OptimizeCssAssetsPlugin({
        cssProcessorOptions: { discardComments: { removeAll: true } },
      }),
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      PROD: true,
    }),
  ],
};

if (process.env.BASE_URL === 'https://vetoptim.com/') {
  webpackMerged.optimization.minimizer.push(
    new UglifyJSPlugin({
      sourceMap: true,
      parallel: true,
      uglifyOptions: {
        ie8: true,
      },
    }),
    new ImageminPlugin({
      test: /\.(jpeg|jpg|png|gif|svg|webp)$/i,
      optipng: {
        enabled: false,
      },
      pngquant: {
        quality: '65-90',
        speed: 4,
      },
    }),
  );
}

module.exports = merge(common, webpackMerged);
