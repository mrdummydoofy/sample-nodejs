const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  entry: {
    'js/bundle': ['babel-polyfill', './assets/js/index.js'],
    sw: ['./assets/js/offline/sw.js'],
    'js/admin': ['babel-polyfill', './assets/js/adminIndex.js'],
  },
  output: {
    path: __dirname + '/.tmp/public',
    filename: '[name].js',
    chunkFilename: '[name].chunk.js',
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        commons: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          chunks: 'all',
        },
      },
    },
  },
  module: {
    rules: [
      {
        use: 'babel-loader',
        test: /\.js$/,
        exclude: /node_modules/,
      },
      {
        test: /\.(scss|css)$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath:
                process.env.NODE_ENV !== 'production'
                  ? 'http://localhost:1337/'
                  : process.env.BASE_URL,
            },
          },
          'css-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: './images/',
            },
          },
        ],
      },
      {
        test: /\.(ttf|eot|svg|woff2|woff)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: './fonts/',
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin([__dirname + '/.tmp'], [{ verbose: true }]),
    new CopyWebpackPlugin([
      { from: './assets/dependencies/sails.io.js', to: __dirname + '/.tmp/public/js' },
      { from: './assets/dependencies/form-wizard.js', to: __dirname + '/.tmp/public/js' },
      { from: './assets/dependencies/map-script.js', to: __dirname + '/.tmp/public/js' },
      { from: './assets/styles/form-wizard.css', to: __dirname + '/.tmp/public/css' },
      { from: './assets/robots.txt', to: __dirname + '/.tmp/public' },
      { from: './assets/manifest.json', to: __dirname + '/.tmp/public' },
      { from: './assets/images', to: __dirname + '/.tmp/public/images' },
      { from: './assets/pdf', to: __dirname + '/.tmp/public/pdf' },
    ]),
    new MiniCssExtractPlugin({
      filename: './css/[name].css',
      chunkFilename: './css/[name].chunk.css',
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
    }),
  ],
};
