#!/bin/bash
set -e
echo "making vetoptim directory if does not exists"
sudo mkdir -p /home/ubuntu/projects/vetoptim
echo "making vetoptim directory if does not exists"
sudo chown -R ubuntu:ubuntu /home/ubuntu/projects/vetoptim
echo "making vetoptim directory if does not exists"
sudo chmod -R 775 /home/ubuntu/projects/vetoptim
echo "copying repo to projects directory"
sudo rsync -rtv --delete --exclude 'node_modules' --exclude '.tmp' ./ /home/ubuntu/projects/vetoptim
echo "going to projects directory"
cd /home/ubuntu/projects/vetoptim
# echo "update packages"
# sudo BUILD_ID=dontKillMe npm update --unsafe-perm
echo "install packages"
sudo BUILD_ID=dontKillMe npm install --unsafe-perm
# echo "audit packages"
# sudo BUILD_ID=dontKillMe npm audit fix --unsafe-perm
echo "stopping old forever process"
sudo npm run sails:prod:stop
echo "migrating new data"
sudo BUILD_ID=dontKillMe npx sails lift --drop &
sleep 60
sudo kill $(ps aux | grep 'npx' | grep 'root' | awk '{print $2}')
# echo "testing vetoptim formulas"
# sudo BUILD_ID=dontKillMe npm run cypress:run
# echo "generating assets"
# sudo BUILD_ID=dontKillMe NODE_ENV=production BASE_URL=https://recette.vetoptim.com/ node --max_old_space_size=1024 node_modules/webpack/bin/webpack.js --progress --hide-modules --config webpack.prod.js || true
# echo "starting vetoptim sails app"
# sudo BUILD_ID=dontKillMe NODE_ENV=production forever start --uid vetoptim -l vetoptim.log --append app.js || true
echo "generating assets and launching vetoptim"
sudo BUILD_ID=dontKillMe npm run lift:staging