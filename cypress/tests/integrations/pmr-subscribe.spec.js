import currencyFormatter from 'currency-formatter';

const currencyFormat = input => {
  return input % 1
    ? currencyFormatter.format(input, {
        symbol: '€',
        decimal: ',',
        thousand: ' ',
        precision: 2,
        format: '%v %s',
      })
    : currencyFormatter.format(input, {
        symbol: '€',
        thousand: ' ',
        precision: 0,
        format: '%v %s',
      });
};

describe('Integration Test PMR Subscription Simulation', () => {
  beforeEach(() => {
    cy.fixture('quotations.json').as('quotationsData');
    cy.fixture('tax.json').as('taxData');
  });
  context('to pmr4-adhesion page', () => {
    it('should go smoothly with urbain and rural activity option', () => {
      cy.visit('localhost:1337');
      cy.get('a.connect').should('not.exist');
      cy.get('a.espace-assure').click();
      cy.get('[ng-model="vm.user.login"]').type('test');
      cy.get('[ng-model="vm.user.password"]').type('test');
      cy.get('div#connexion')
        .find('button[type=submit]')
        .first()
        .click();
      cy.get('a.connect').should('exist');

      cy.wait(2000);

      cy.get('a.button-menu-open').click();
      cy.wait(1000);
      cy.get('[href="/multirisque-professionelles"]')
        .first()
        .click();
      cy.wait(2000);
      cy.get('[href="/pmr"]')
        .first()
        .click();

      cy.wait(2000);
      cy.get('@quotationsData').then(quotationData => {
        cy.get('[ng-model="vm.chiffreAffaires"]')
          .first()
          .eq(0)
          .then(el => {
            if (el.val() !== '') {
              return;
            }
            cy.get('[ng-model="vm.chiffreAffaires"]')
              .first()
              .eq(0)
              .type(quotationData['turnover']);
          });
        cy.get('[ng-model="vm.surfaceLocaux"]')
          .first()
          .eq(0)
          .then(el => {
            if (el.val() !== '') {
              return;
            }
            cy.get('[ng-model="vm.surfaceLocaux"]')
              .first()
              .eq(0)
              .type('1000');
          });
        cy.get('[ng-model="vm.contenuLocaux"]')
          .first()
          .eq(0)
          .then(el => {
            if (el.val() !== '') {
              return;
            }
            cy.get('[ng-model="vm.contenuLocaux"]')
              .first()
              .eq(0)
              .type('50000');
          });
      });

      cy.get(
        'body > div.content-section.ng-scope > div > div > div > div.col-lg-9.col-xl-7 > form > div:nth-child(2) > div > div > div:nth-child(1) > label',
      ).click();

      cy.get('form.steps-form')
        .find('button[type=submit]')
        .first()
        .click();

      cy.wait(2000);
      cy.url().should('include', '/pmr2-menu');

      cy.get('@quotationsData').then(quotationData => {
        cy.get('p.price').should(
          'have.text',
          `Soit ${currencyFormat(
            quotationData['MRP']['500']['urbain']['pmrUrbanQuotation'],
          )}par an`,
        );
      });

      cy.wait(2000);

      cy.visit('localhost:1337/pmr4-adhesion');

      cy.get('@quotationsData').then(quotationData => {
        cy.get('@taxData').then(taxData => {
          cy.get('div.periodicite.franchise')
            .find('.year')
            .eq(0)
            .should(
              'have.text',
              `Soit ${currencyFormat(
                quotationData['MRP']['500']['urbain']['pmrUrbanQuotation'],
              )}/An`,
            );
          cy.get('div.periodicite.franchise')
            .find('.year')
            .eq(1)
            .should(
              'have.text',
              `Soit ${currencyFormat(
                quotationData['MRP']['300']['urbain']['pmrUrbanQuotation'],
              )}/An`,
            );
          cy.get(
            '[ng-click="vm.chosenFraction(1, vm.franchise === \'300\' ? vm.pmrQuotationAnnual : vm.pmrQuotationAnnualPromo, vm.effectDate, vm.franchise)"]',
          ).click();
          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.table-recap > ul > li:nth-child(3) > div.right.ng-scope > h4',
          )
            .eq(0)
            .should(
              'have.text',
              `${currencyFormat(taxData['MRP']['1']['500']['urbain']['PRIM_ANN'])}`,
            );
          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.table-recap > ul > li:nth-child(4) > div.right.ng-scope > p',
          )
            .eq(0)
            .should(
              'have.text',
              `${currencyFormat(taxData['MRP']['1']['500']['urbain']['TOT_HT'])}`,
            );
          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.table-recap > ul > li:nth-child(5) > div.right.ng-scope > p',
          )
            .eq(0)
            .should(
              'have.text',
              `${currencyFormat(taxData['MRP']['1']['500']['urbain']['FRAIS_COURTIER'])}`,
            );
          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.table-recap > ul > li:nth-child(7) > div.right.ng-scope > p',
          )
            .eq(0)
            .should(
              'have.text',
              `${currencyFormat(taxData['MRP']['1']['500']['urbain']['TOT_TAXES'])}`,
            );
          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.offre-table-recap.m-b-30 > div.right > div > ul > li:nth-child(2) > label',
          ).click();
          cy.get(
            '[ng-click="vm.chosenFraction(1, vm.franchise === \'300\' ? vm.pmrQuotationAnnual : vm.pmrQuotationAnnualPromo, vm.effectDate, vm.franchise)"]',
          ).click();

          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.table-recap > ul > li:nth-child(3) > div.right.ng-scope > h4',
          )
            .eq(0)
            .should(
              'have.text',
              `${currencyFormat(taxData['MRP']['1']['300']['urbain']['PRIM_ANN'])}`,
            );
          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.table-recap > ul > li:nth-child(4) > div.right.ng-scope > p',
          )
            .eq(0)
            .should(
              'have.text',
              `${currencyFormat(taxData['MRP']['1']['300']['urbain']['TOT_HT'])}`,
            );
          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.table-recap > ul > li:nth-child(5) > div.right.ng-scope > p',
          )
            .eq(0)
            .should(
              'have.text',
              `${currencyFormat(taxData['MRP']['1']['300']['urbain']['FRAIS_COURTIER'])}`,
            );
          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.table-recap > ul > li:nth-child(7) > div.right.ng-scope > p',
          )
            .eq(0)
            .should(
              'have.text',
              `${currencyFormat(taxData['MRP']['1']['300']['urbain']['TOT_TAXES'])}`,
            );
        });
      });
      cy.get('[href="/pmr"]')
        .first()
        .click();
      cy.get(
        'body > div.content-section.ng-scope > div > div > div > div.col-lg-9.col-xl-7 > form > div:nth-child(2) > div > div > div:nth-child(2) > label',
      ).click();
      cy.wait(2000);
      cy.get('form.steps-form')
        .find('button[type=submit]')
        .first()
        .click();
      cy.get('@quotationsData').then(quotationData => {
        cy.get('p.price').should(
          'have.text',
          `Soit ${currencyFormat(quotationData['MRP']['500']['rural']['pmrRuralQuotation'])}par an`,
        );
      });
      cy.wait(2000);
      cy.visit('localhost:1337/pmr4-adhesion');

      cy.get('@quotationsData').then(quotationData => {
        cy.get('@taxData').then(taxData => {
          cy.get('div.periodicite.franchise')
            .find('.year')
            .eq(0)
            .should(
              'have.text',
              `Soit ${currencyFormat(
                quotationData['MRP']['500']['rural']['pmrRuralQuotation'],
              )}/An`,
            );
          cy.get('div.periodicite.franchise')
            .find('.year')
            .eq(1)
            .should(
              'have.text',
              `Soit ${currencyFormat(
                quotationData['MRP']['300']['rural']['pmrRuralQuotation'],
              )}/An`,
            );
          cy.get(
            '[ng-click="vm.chosenFraction(1, vm.franchise === \'300\' ? vm.pmrQuotationAnnual : vm.pmrQuotationAnnualPromo, vm.effectDate, vm.franchise)"]',
          ).click();
          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.table-recap > ul > li:nth-child(3) > div.right.ng-scope > h4',
          )
            .eq(0)
            .should(
              'have.text',
              `${currencyFormat(taxData['MRP']['1']['500']['rural']['PRIM_ANN'])}`,
            );
          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.table-recap > ul > li:nth-child(4) > div.right.ng-scope > p',
          )
            .eq(0)
            .should(
              'have.text',
              `${currencyFormat(taxData['MRP']['1']['500']['rural']['TOT_HT'])}`,
            );
          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.table-recap > ul > li:nth-child(5) > div.right.ng-scope > p',
          )
            .eq(0)
            .should(
              'have.text',
              `${currencyFormat(taxData['MRP']['1']['500']['rural']['FRAIS_COURTIER'])}`,
            );
          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.table-recap > ul > li:nth-child(7) > div.right.ng-scope > p',
          )
            .eq(0)
            .should(
              'have.text',
              `${currencyFormat(taxData['MRP']['1']['500']['rural']['TOT_TAXES'])}`,
            );

          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.offre-table-recap.m-b-30 > div.right > div > ul > li:nth-child(2) > label',
          ).click();
          cy.get(
            '[ng-click="vm.chosenFraction(1, vm.franchise === \'300\' ? vm.pmrQuotationAnnual : vm.pmrQuotationAnnualPromo, vm.effectDate, vm.franchise)"]',
          ).click();

          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.table-recap > ul > li:nth-child(3) > div.right.ng-scope > h4',
          )
            .eq(0)
            .should(
              'have.text',
              `${currencyFormat(taxData['MRP']['1']['300']['rural']['PRIM_ANN'])}`,
            );
          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.table-recap > ul > li:nth-child(4) > div.right.ng-scope > p',
          )
            .eq(0)
            .should(
              'have.text',
              `${currencyFormat(taxData['MRP']['1']['300']['rural']['TOT_HT'])}`,
            );
          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.table-recap > ul > li:nth-child(5) > div.right.ng-scope > p',
          )
            .eq(0)
            .should(
              'have.text',
              `${currencyFormat(taxData['MRP']['1']['300']['rural']['FRAIS_COURTIER'])}`,
            );
          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.table-recap > ul > li:nth-child(7) > div.right.ng-scope > p',
          )
            .eq(0)
            .should(
              'have.text',
              `${currencyFormat(taxData['MRP']['1']['300']['rural']['TOT_TAXES'])}`,
            );
        });
      });
    });
  });
});
