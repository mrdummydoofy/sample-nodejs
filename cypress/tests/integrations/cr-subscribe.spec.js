import currencyFormatter from 'currency-formatter';

const currencyFormat = input => {
  return input % 1
    ? currencyFormatter.format(input, {
        symbol: '€',
        decimal: ',',
        thousand: ' ',
        precision: 2,
        format: '%v %s',
      })
    : currencyFormatter.format(input, {
        symbol: '€',
        thousand: ' ',
        precision: 0,
        format: '%v %s',
      });
};

describe('Integration Test CR Subscription Simulation', () => {
  beforeEach(() => {
    cy.fixture('quotations.json').as('quotationsData');
    cy.fixture('tax.json').as('taxData');
  });
  context('to cr4-adhesion page', () => {
    it('should go smoothly with urbain and rural activity option', () => {
      cy.visit('localhost:1337');
      cy.get('a.connect').should('not.exist');
      cy.get('a.espace-assure').click();
      cy.get('[ng-model="vm.user.login"]').type('test');
      cy.get('[ng-model="vm.user.password"]').type('test');
      cy.get('div#connexion')
        .find('button[type=submit]')
        .first()
        .click();
      cy.get('a.connect').should('exist');
      cy.wait(2000);

      cy.get('a.button-menu-open').click();
      cy.wait(1000);
      cy.get('[href="/responsabilite-civile"]')
        .first()
        .click();
      cy.wait(2000);
      cy.get('[href="/simulation?offerType=CR"]')
        .first()
        .click();

      cy.wait(2000);
      cy.get('@quotationsData').then(quotationData => {
        cy.get('[ng-model="vm.chiffreAffaires"]')
          .first()
          .eq(0)
          .then(el => {
            if (el.val() !== '') {
              return;
            }
            cy.get('[ng-model="vm.chiffreAffaires"]')
              .first()
              .eq(0)
              .type(quotationData['turnover']);
          });
      });

      cy.get(
        'body > div.content-section.ng-scope > div > div > div > div.col-lg-9.col-xl-7 > form > div:nth-child(2) > div > div > div:nth-child(1) > label',
      ).click();

      cy.get('form.steps-form')
        .find('button[type=submit]')
        .first()
        .click();

      cy.wait(2000);
      cy.url().should('include', '/menu?offerType=CR');

      cy.get('@quotationsData').then(quotationData => {
        cy.get('p.price').should(
          'have.text',
          `Soit ${currencyFormat(quotationData['RC']['500']['urbain']['crUrbanQuotation'])} par an`,
        );
      });

      cy.wait(2000);

      cy.get('[ng-click="vm.chooseMenu(1)"]').click();

      cy.get('[ng-model="vm.form.siren"]')
        .first()
        .eq(0)
        .then(el => {
          if (el.val() !== '') {
            return;
          }
          cy.get('[ng-model="vm.form.siren"]')
            .first()
            .eq(0)
            .type('51489418944444');
        });
      // cy.get('[ng-model="vm.form.orderNumber"]')
      //   .first()
      //   .eq(0)
      //   .then(el => {
      //     if (el.val() !== '') {
      //       return;
      //     }
      //     cy.get('[ng-model="vm.form.orderNumber"]')
      //       .first()
      //       .eq(0)
      //       .type('15814');
      //   });
      cy.get('[ng-model="vm.form.companyName"]')
        .first()
        .eq(0)
        .then(el => {
          if (el.val() !== '') {
            return;
          }
          cy.get('[ng-model="vm.form.companyName"]')
            .first()
            .eq(0)
            .type('oijoiujoià');
        });
      cy.get('[ng-model="vm.form.legalForm"]')
        .first()
        .eq(0)
        .then(el => {
          if (el.val() !== '') {
            return;
          }
          cy.get('[ng-model="vm.form.legalForm"]')
            .first()
            .eq(0)
            .select('SEL');
        });
      cy.get('[ng-model="vm.form.startActivityDateDay"]')
        .first()
        .eq(0)
        .then(el => {
          if (el.val() !== '') {
            return;
          }
          cy.get('[ng-model="vm.form.startActivityDateDay"]')
            .first()
            .eq(0)
            .select('11');
        });
      cy.get('[ng-model="vm.form.startActivityDateMonth"]')
        .first()
        .eq(0)
        .then(el => {
          if (el.val() !== '') {
            return;
          }
          cy.get('[ng-model="vm.form.startActivityDateMonth"]')
            .first()
            .eq(0)
            .select('12');
        });
      cy.get('[ng-model="vm.form.startActivityDateYear"]')
        .first()
        .eq(0)
        .then(el => {
          if (el.val() !== '') {
            return;
          }
          cy.get('[ng-model="vm.form.startActivityDateYear"]')
            .first()
            .eq(0)
            .type('2000');
        });
      cy.get('[ng-model="vm.form.phone"]')
        .first()
        .eq(0)
        .then(el => {
          if (el.val() !== '') {
            return;
          }
          cy.get('[ng-model="vm.form.phone"]')
            .first()
            .eq(0)
            .type('895148948749');
        });
      cy.get('[ng-model="vm.form.address"]')
        .first()
        .eq(0)
        .then(el => {
          if (el.val() !== '') {
            return;
          }
          cy.get('[ng-model="vm.form.address"]')
            .first()
            .eq(0)
            .type('rue des fleurs');
        });
      cy.get('[ng-model="vm.form.postalCode"]')
        .eq(0)
        .then(el => {
          if (el.val() !== '') {
            return;
          }
          cy.get('[ng-model="vm.form.postalCode"]')
            .eq(0)
            .type('20000');
          cy.get('[ng-model="vm.form.city"]')
            .first()
            .eq(0)
            .select('Ajaccio');
        });
      cy.get('[ng-model="vm.form.firstName"]')
        .first()
        .eq(0)
        .then(el => {
          if (el.val() !== '') {
            return;
          }
          cy.get('[ng-model="vm.form.firstName"]')
            .eq(0)
            .type('joijoi');
        });
      cy.get('[ng-model="vm.form.lastName"]')
        .first()
        .eq(0)
        .then(el => {
          if (el.val() !== '') {
            return;
          }
          cy.get('[ng-model="vm.form.lastName"]')
            .eq(0)
            .type('ikjoijoi');
        });
      cy.get('[ng-model="vm.form.mobile"]')
        .first()
        .eq(0)
        .then(el => {
          if (el.val() !== '') {
            return;
          }
          cy.get('[ng-model="vm.form.mobile"]')
            .eq(0)
            .type('89548951489484');
        });
      cy.get('[ng-model="vm.form.endStatementDateDay"]')
        .first()
        .eq(0)
        .then(el => {
          if (el.val() !== '') {
            return;
          }
          cy.get('[ng-model="vm.form.endStatementDateDay"]')
            .eq(0)
            .select('17');
        });
      cy.get('[ng-model="vm.form.endStatementDateMonth"]')
        .first()
        .eq(0)
        .then(el => {
          if (el.val() !== '') {
            return;
          }
          cy.get('[ng-model="vm.form.endStatementDateMonth"]')
            .eq(0)
            .select('04');
        });
      cy.get('div.form-check-validation > label > [ng-model="vm.form.infoTerm1"]')
        .first()
        .eq(0)
        .check({ force: true });
      cy.get('div.form-check-validation > label > [ng-model="vm.form.infoTerm2"]')
        .first()
        .eq(0)
        .check({ force: true });
      cy.wait(2000);
      cy.get('form.steps-form')
        .find('button[type=submit]')
        .first()
        .click();

      cy.get('@quotationsData').then(quotationData => {
        cy.get('@taxData').then(taxData => {
          cy.get('.offre-content')
            .find('.price')
            .eq(0)
            .should(
              'have.text',
              `Soit ${currencyFormat(
                quotationData['RC']['500']['urbain']['crUrbanQuotation'],
              )} /An`,
            );
          cy.get('.offre-content')
            .find('.year')
            .eq(1)
            .should(
              'have.text',
              `Soit ${currencyFormat(
                quotationData['RC']['300']['urbain']['crUrbanQuotation'],
              )} /An`,
            );
          cy.get(
            '[ng-click="vm.chosenFraction(1, vm.franchise === \'300\' ? vm.crQuotationAnnual : vm.crQuotationAnnualPromo, vm.effectDate, vm.franchise)"]',
          ).click();
          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.table-recap > ul > li:nth-child(3) > div.right.ng-scope > h4',
          )
            .eq(0)
            .should(
              'have.text',
              `${currencyFormat(taxData['RC']['1']['500']['urbain']['PRIM_AN'])}`,
            );
          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.table-recap > ul > li:nth-child(4) > div.right.ng-scope > p',
          )
            .eq(0)
            .should(
              'have.text',
              `${currencyFormat(taxData['RC']['1']['500']['urbain']['TOTAL_COT_HT'])}`,
            );
          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.table-recap > ul > li:nth-child(5) > div.right.ng-scope > p',
          )
            .eq(0)
            .should(
              'have.text',
              `${currencyFormat(taxData['RC']['1']['500']['urbain']['FRAIS_COURTIER'])}`,
            );
          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.table-recap > ul > li:nth-child(7) > div.right.ng-scope > p',
          )
            .eq(0)
            .should(
              'have.text',
              `${currencyFormat(taxData['RC']['1']['500']['urbain']['TOTAL_TAX'])}`,
            );

          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.offre-table-recap.m-b-30 > div.right > div > ul > li:nth-child(2) > label',
          ).click();
          cy.get(
            '[ng-click="vm.chosenFraction(1, vm.franchise === \'300\' ? vm.crQuotationAnnual : vm.crQuotationAnnualPromo, vm.effectDate, vm.franchise)"]',
          ).click();

          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.table-recap > ul > li:nth-child(3) > div.right.ng-scope > h4',
          )
            .eq(0)
            .should(
              'have.text',
              `${currencyFormat(taxData['RC']['1']['300']['urbain']['PRIM_AN'])}`,
            );
          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.table-recap > ul > li:nth-child(4) > div.right.ng-scope > p',
          )
            .eq(0)
            .should(
              'have.text',
              `${currencyFormat(taxData['RC']['1']['300']['urbain']['TOTAL_COT_HT'])}`,
            );
          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.table-recap > ul > li:nth-child(5) > div.right.ng-scope > p',
          )
            .eq(0)
            .should(
              'have.text',
              `${currencyFormat(taxData['RC']['1']['300']['urbain']['FRAIS_COURTIER'])}`,
            );
          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.table-recap > ul > li:nth-child(7) > div.right.ng-scope > p',
          )
            .eq(0)
            .should(
              'have.text',
              `${currencyFormat(taxData['RC']['1']['300']['urbain']['TOTAL_TAX'])}`,
            );
        });
      });
      cy.get('[href="/cr"]')
        .first()
        .click();
      cy.get(
        'body > div.content-section.ng-scope > div > div > div > div.col-lg-9.col-xl-7 > form > div:nth-child(2) > div > div > div:nth-child(2) > label',
      ).click();
      cy.wait(2000);
      cy.get('form.steps-form')
        .find('button[type=submit]')
        .first()
        .click();
      cy.get('@quotationsData').then(quotationData => {
        cy.get('p.price').should(
          'have.text',
          `Soit ${currencyFormat(quotationData['RC']['500']['rural']['crRuralQuotation'])}par an`,
        );
      });
      cy.wait(2000);
      cy.visit('localhost:1337/cr4-adhesion');

      cy.get('@quotationsData').then(quotationData => {
        cy.get('@taxData').then(taxData => {
          cy.get('div.periodicite.franchise')
            .find('.year')
            .eq(0)
            .should(
              'have.text',
              `Soit ${currencyFormat(quotationData['RC']['500']['rural']['crRuralQuotation'])}/An`,
            );
          cy.get('div.periodicite.franchise')
            .find('.year')
            .eq(1)
            .should(
              'have.text',
              `Soit ${currencyFormat(quotationData['RC']['300']['rural']['crRuralQuotation'])}/An`,
            );
          cy.get(
            '[ng-click="vm.chosenFraction(1, vm.franchise === \'300\' ? vm.crQuotationAnnual : vm.crQuotationAnnualPromo, vm.effectDate, vm.franchise)"]',
          ).click();
          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.table-recap > ul > li:nth-child(3) > div.right.ng-scope > h4',
          )
            .eq(0)
            .should(
              'have.text',
              `${currencyFormat(taxData['RC']['1']['500']['rural']['PRIM_AN'])}`,
            );
          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.table-recap > ul > li:nth-child(4) > div.right.ng-scope > p',
          )
            .eq(0)
            .should(
              'have.text',
              `${currencyFormat(taxData['RC']['1']['500']['rural']['TOTAL_COT_HT'])}`,
            );
          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.table-recap > ul > li:nth-child(5) > div.right.ng-scope > p',
          )
            .eq(0)
            .should(
              'have.text',
              `${currencyFormat(taxData['RC']['1']['500']['rural']['FRAIS_COURTIER'])}`,
            );
          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.table-recap > ul > li:nth-child(7) > div.right.ng-scope > p',
          )
            .eq(0)
            .should(
              'have.text',
              `${currencyFormat(taxData['RC']['1']['500']['rural']['TOTAL_TAX'])}`,
            );
          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.offre-table-recap.m-b-30 > div.right > div > ul > li:nth-child(2) > label',
          ).click();
          cy.get(
            '[ng-click="vm.chosenFraction(1, vm.franchise === \'300\' ? vm.crQuotationAnnual : vm.crQuotationAnnualPromo, vm.effectDate, vm.franchise)"]',
          ).click();

          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.table-recap > ul > li:nth-child(3) > div.right.ng-scope > h4',
          )
            .eq(0)
            .should(
              'have.text',
              `${currencyFormat(taxData['RC']['1']['300']['rural']['PRIM_AN'])}`,
            );
          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.table-recap > ul > li:nth-child(4) > div.right.ng-scope > p',
          )
            .eq(0)
            .should(
              'have.text',
              `${currencyFormat(taxData['RC']['1']['300']['rural']['TOTAL_COT_HT'])}`,
            );
          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.table-recap > ul > li:nth-child(5) > div.right.ng-scope > p',
          )
            .eq(0)
            .should(
              'have.text',
              `${currencyFormat(taxData['RC']['1']['300']['rural']['FRAIS_COURTIER'])}`,
            );
          cy.get(
            'body > div.content-section > div > div > div > div.col-lg-9 > form > div.table-recap > ul > li:nth-child(7) > div.right.ng-scope > p',
          )
            .eq(0)
            .should(
              'have.text',
              `${currencyFormat(taxData['RC']['1']['300']['rural']['TOTAL_TAX'])}`,
            );
        });
      });
    });
  });
});
