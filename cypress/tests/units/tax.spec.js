import Quotation from '../../../api/models/Quotation';

describe('Unit Test Tax', function() {
  beforeEach(() => {
    cy.fixture('tax.json').as('fixturesData');
  });
  context('RC Offer', () => {
    it('test with fraction, franchise and activity values', () => {
      cy.get('@fixturesData').then(fixturesData => {
        Object.keys(fixturesData['RC']['1']['300']['urbain']).forEach(fixtureKey => {
          expect(parseFloat(fixturesData['RC']['1']['300']['urbain'][fixtureKey]).toFixed(2)).to.eq(
            parseFloat(
              Quotation.rcUrbanCalculateTAX(
                fixturesData['turnover'],
                1,
                300,
                Quotation.crUrbanQuotation(fixturesData['turnover']),
              )[fixtureKey],
            ).toFixed(2),
          );
        });
        Object.keys(fixturesData['RC']['1']['500']['urbain']).forEach(fixtureKey => {
          expect(parseFloat(fixturesData['RC']['1']['500']['urbain'][fixtureKey]).toFixed(2)).to.eq(
            parseFloat(
              Quotation.rcUrbanCalculateTAX(
                fixturesData['turnover'],
                1,
                500,
                Quotation.crUrbanQuotation(fixturesData['turnover']),
              )[fixtureKey],
            ).toFixed(2),
          );
        });
        Object.keys(fixturesData['RC']['1']['300']['rural']).forEach(fixtureKey => {
          expect(parseFloat(fixturesData['RC']['1']['300']['rural'][fixtureKey]).toFixed(2)).to.eq(
            parseFloat(
              Quotation.rcRuralCalculateTAX(
                fixturesData['turnover'],
                1,
                300,
                Quotation.crRuralMixteQuotation(fixturesData['turnover']),
              )[fixtureKey],
            ).toFixed(2),
          );
        });
        Object.keys(fixturesData['RC']['1']['500']['rural']).forEach(fixtureKey => {
          expect(parseFloat(fixturesData['RC']['1']['500']['rural'][fixtureKey]).toFixed(2)).to.eq(
            parseFloat(
              Quotation.rcRuralCalculateTAX(
                fixturesData['turnover'],
                1,
                500,
                Quotation.crRuralMixteQuotation(fixturesData['turnover']),
              )[fixtureKey],
            ).toFixed(2),
          );
        });
      });
    });
  });
  context('MRP Offer', () => {
    it('test with fraction, franchise and activity values', () => {
      cy.get('@fixturesData').then(fixturesData => {
        Object.keys(fixturesData['MRP']['1']['300']['urbain']).forEach(fixtureKey => {
          expect(
            parseFloat(fixturesData['MRP']['1']['300']['urbain'][fixtureKey]).toFixed(2),
          ).to.eq(
            parseFloat(
              Quotation.MrpUrbanCalculateTAX(
                fixturesData['turnover'],
                1,
                300,
                Quotation.mrpUrbanQuotation(fixturesData['turnover']),
              )[fixtureKey],
            ).toFixed(2),
          );
        });
        Object.keys(fixturesData['MRP']['1']['500']['urbain']).forEach(fixtureKey => {
          expect(
            parseFloat(fixturesData['MRP']['1']['500']['urbain'][fixtureKey]).toFixed(2),
          ).to.eq(
            parseFloat(
              Quotation.MrpUrbanCalculateTAX(
                fixturesData['turnover'],
                1,
                500,
                Quotation.mrpUrbanQuotation(fixturesData['turnover']),
              )[fixtureKey],
            ).toFixed(2),
          );
        });
        Object.keys(fixturesData['MRP']['1']['300']['rural']).forEach(fixtureKey => {
          expect(parseFloat(fixturesData['MRP']['1']['300']['rural'][fixtureKey]).toFixed(2)).to.eq(
            parseFloat(
              Quotation.MrpRuralMixteCalculateTAX(
                fixturesData['turnover'],
                1,
                300,
                Quotation.mrpRuralMixteQuotation(fixturesData['turnover']),
              )[fixtureKey],
            ).toFixed(2),
          );
        });
        Object.keys(fixturesData['MRP']['1']['500']['rural']).forEach(fixtureKey => {
          expect(parseFloat(fixturesData['MRP']['1']['500']['rural'][fixtureKey]).toFixed(2)).to.eq(
            parseFloat(
              Quotation.MrpRuralMixteCalculateTAX(
                fixturesData['turnover'],
                1,
                500,
                Quotation.mrpRuralMixteQuotation(fixturesData['turnover']),
              )[fixtureKey],
            ).toFixed(2),
          );
        });
      });
    });
  });
  context('MRP + RC Offer', () => {
    it('test with fraction, franchise and activity values', () => {
      cy.get('@fixturesData').then(fixturesData => {
        Object.keys(fixturesData['MRP + RC']['1']['300']['urbain']).forEach(fixtureKey => {
          expect(
            parseFloat(fixturesData['MRP + RC']['1']['300']['urbain'][fixtureKey]).toFixed(2),
          ).to.eq(
            parseFloat(
              Quotation.MrpCrUrbanCalculateTAX(
                fixturesData['turnover'],
                1,
                300,
                Quotation.mrpRcUrbanQuotation(fixturesData['turnover']),
              )[fixtureKey],
            ).toFixed(2),
          );
        });
        Object.keys(fixturesData['MRP + RC']['1']['500']['urbain']).forEach(fixtureKey => {
          expect(
            parseFloat(fixturesData['MRP + RC']['1']['500']['urbain'][fixtureKey]).toFixed(2),
          ).to.eq(
            parseFloat(
              Quotation.MrpCrUrbanCalculateTAX(
                fixturesData['turnover'],
                1,
                500,
                Quotation.mrpRcUrbanQuotation(fixturesData['turnover']),
              )[fixtureKey],
            ).toFixed(2),
          );
        });
        Object.keys(fixturesData['MRP + RC']['1']['300']['rural']).forEach(fixtureKey => {
          expect(
            parseFloat(fixturesData['MRP + RC']['1']['300']['rural'][fixtureKey]).toFixed(2),
          ).to.eq(
            parseFloat(
              Quotation.MrpCrRuralMixedCalculateTAX(
                fixturesData['turnover'],
                1,
                300,
                Quotation.mrpRcRuralMixteQuotation(fixturesData['turnover']),
              )[fixtureKey],
            ).toFixed(2),
          );
        });
        Object.keys(fixturesData['MRP + RC']['1']['500']['rural']).forEach(fixtureKey => {
          expect(
            parseFloat(fixturesData['MRP + RC']['1']['500']['rural'][fixtureKey]).toFixed(2),
          ).to.eq(
            parseFloat(
              Quotation.MrpCrRuralMixedCalculateTAX(
                fixturesData['turnover'],
                1,
                500,
                Quotation.mrpRcRuralMixteQuotation(fixturesData['turnover']),
              )[fixtureKey],
            ).toFixed(2),
          );
        });
      });
    });
  });
});
