import Quotation from '../../../api/models/Quotation';

describe('Unit Test Quotations', function() {
  beforeEach(() => {
    cy.fixture('quotations.json').as('fixturesData');
  });
  context('RC Offer', () => {
    it('test with franchise and activity values', () => {
      cy.get('@fixturesData').then(fixturesData => {
        expect(Quotation.crUrbanQuotation(fixturesData['turnover'])).to.have.property(
          'crUrbanQuotation',
          fixturesData['RC']['300']['urbain']['crUrbanQuotation'],
        );
        expect(Quotation.crUrbanQuotation(fixturesData['turnover'])).to.have.property(
          'crUrbanQuotationPromo',
          fixturesData['RC']['500']['urbain']['crUrbanQuotation'],
        );
        expect(Quotation.crRuralMixteQuotation(fixturesData['turnover'])).to.have.property(
          'crRuralMixteQuotation',
          fixturesData['RC']['300']['rural']['crRuralQuotation'],
        );
        expect(Quotation.crRuralMixteQuotation(fixturesData['turnover'])).to.have.property(
          'crRuralMixteQuotationPromo',
          fixturesData['RC']['500']['rural']['crRuralQuotation'],
        );
      });
    });
  });
  context('MRP Offer', () => {
    it('test with franchise and activity values', () => {
      cy.get('@fixturesData').then(fixturesData => {
        expect(Quotation.mrpUrbanQuotation(fixturesData['turnover'])).to.have.property(
          'mrpUrbanQuotation',
          fixturesData['MRP']['300']['urbain']['pmrUrbanQuotation'],
        );
        expect(Quotation.mrpUrbanQuotation(fixturesData['turnover'])).to.have.property(
          'mrpUrbanQuotationPromo',
          fixturesData['MRP']['500']['urbain']['pmrUrbanQuotation'],
        );
        expect(Quotation.mrpRuralMixteQuotation(fixturesData['turnover'])).to.have.property(
          'mrpRuralMixteQuotation',
          fixturesData['MRP']['300']['rural']['pmrRuralQuotation'],
        );
        expect(Quotation.mrpRuralMixteQuotation(fixturesData['turnover'])).to.have.property(
          'mrpRuralMixteQuotationPromo',
          fixturesData['MRP']['500']['rural']['pmrRuralQuotation'],
        );
      });
    });
  });
  context('MRP + RC Offer', () => {
    it('test with franchise and activity values', () => {
      cy.get('@fixturesData').then(fixturesData => {
        expect(Quotation.mrpRcUrbanQuotation(fixturesData['turnover'])).to.have.property(
          'mrpRcUrbanQuotation',
          fixturesData['MRP + RC']['300']['urbain']['pmrcrUrbanQuotation'],
        );
        expect(Quotation.mrpRcUrbanQuotation(fixturesData['turnover'])).to.have.property(
          'mrpRcUrbanQuotationPromo',
          fixturesData['MRP + RC']['500']['urbain']['pmrcrUrbanQuotation'],
        );
        expect(Quotation.mrpRcRuralMixteQuotation(fixturesData['turnover'])).to.have.property(
          'mrpRcRuralMixteQuotation',
          fixturesData['MRP + RC']['300']['rural']['pmrcrRuralQuotation'],
        );
        expect(Quotation.mrpRcRuralMixteQuotation(fixturesData['turnover'])).to.have.property(
          'mrpRcRuralMixteQuotationPromo',
          fixturesData['MRP + RC']['500']['rural']['pmrcrRuralQuotation'],
        );
      });
    });
  });
});
