const webpack = require('webpack');
const merge = require('webpack-merge');
const LiveReloadPlugin = require('webpack-livereload-plugin');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  mode: 'development',
  devtool: 'source-map',
  watch: true,
  plugins: [
    new LiveReloadPlugin(),
    new webpack.DefinePlugin({
      DEV: true,
    }),
  ],
});
