import { app } from '../app';

app.service('WS', ['$http', function($http) {
  const service = {};
  let headers = { 'Content-Type':'application/json'};
  let apiUrl = window.location.protocol+'//'+window.location.host+'/';

  service.get = function(route, data, cache = false) {

    let url = apiUrl + route;
    if (data) {
      url += '?' + data;
    }
    if (cache) {
      url += '?' + Date.now();
    }

    return new Promise((resolve, reject) => {
      $http({
        method: 'GET',
        url: url,
        headers:headers
      }).then((res) => {
        resolve(res);
      }).catch((err) => {
        reject(err);
      });
    });
  };

  service.post = function(route, data, headerType) {

    let url = apiUrl + route;

    if(headerType || headerType === undefined ){
      headers['Content-Type'] = headerType;
    }
    return new Promise((resolve, reject) => {
      $http({
        method: 'POST',
        url: url,
        headers: headers,
        data: data
      }).then((res) => {
        resolve(res);
      }).catch((err) => {
        reject(err);
      });
    });
  };

  service.put = function(route, data) {

    let url = apiUrl + route;
    return new Promise((resolve, reject) => {
      $http({
        method: 'PUT',
        url: url,
        headers: headers,
        data: data
      }).then((res) => {
        resolve(res);
      }).catch((err) => {
        reject(err);
      });
    });
  };

  service.delete = function(route, data) {

    let url = apiUrl + route;
    return new Promise((resolve, reject) => {
      $http({
        method: 'DELETE',
        url: url,
        headers: headers,
        data: data
      }).then((res) => {
        resolve(res);
      }).catch((err) => {
        reject(err);
      });

    });
  };
  return service;
} ]);
