const CACHE_NAME = 'vetoptim-cache-v1';

// TODO if connected add the following routes
  // '/welcome',
  // '/mail-check',
  // '/formula-choice',
  // '/cr',
  // '/cr2-menu',
  // '/cr3-subscribe',
  // '/cr3a-subscribe',
  // '/cr3b-subscribe',
  // '/cr3c-subscribe',
  // '/cr3d-subscribe',
  // '/cr3e-subscribe',
  // '/cr4-quote',
  // '/cr5-save',
  // '/cr6-help',

const assetToCache = [
  '/',
  '/register',
  '/js/bundle.js',
  '/js/sails.io.js',
  '/sw.js',
  '/css/bundle.css',
  '/fonts/GalanoGrotesque-Bold.woff',
  '/fonts/GalanoGrotesque-Bold.woff2',
  '/fonts/GalanoGrotesque-Light.woff',
  '/fonts/GalanoGrotesque-Light.woff2',
  '/fonts/GalanoGrotesque-Medium.woff',
  '/fonts/GalanoGrotesque-Medium.woff2',
  '/fonts/icomoon.eot',
  '/fonts/icomoon.svg',
  '/fonts/icomoon.ttf',
  '/fonts/icomoon.woff',
  '/images/slider-bg.png',
  '/images/owl.video.play.png',
  '/images/externe/slide1.png',
  '/images/externe/slider-bg.png',
  '/images/icons/icon-72x72.png',
  '/images/icons/icon-72x72.png',
  '/images/icons/icon-96x96.png',
  '/images/icons/icon-128x128.png',
  '/images/icons/icon-144x144.png',
  '/images/icons/icon-152x152.png',
  '/images/icons/icon-192x192.png',
  '/images/icons/icon-384x384.png',
  '/images/icons/icon-512x512.png',
  '/favicon.ico',
  '/robots.txt',
  '/manifest.json',
];

self.addEventListener('install', (event) => {
  console.log('installing');
  self.skipWaiting();
  event.waitUntil(
    caches.open(CACHE_NAME).then((cache) => {
      return cache.addAll(assetToCache);
    })
  );
});

self.addEventListener('fetch', (event) => {
  event.respondWith(
    caches.match(event.request).then((response) => {
        // DEV ONLY
        return fetch(event.request) || response ;
        // PROD ONLY
        //return response || fetch(event.request) ;
    })
  );
});

self.addEventListener('activate', (event) => {
  const cacheWhitelist = [CACHE_NAME];
  event.waitUntil(
    caches.keys().then((keyList) => {
      return Promise.all(keyList.map((key) => {
        if (cacheWhitelist.indexOf(key) === -1) {
          return caches.delete(key);
        }
      }));
    }).then(() => self.clients.claim())
  );
});
