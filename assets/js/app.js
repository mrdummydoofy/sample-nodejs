import angular from 'angular';
import 'angular-touch';
import 'angular-froala/src/angular-froala';
import 'angular-utils-pagination';

const app = angular.module('app', ['ngTouch', 'froala', 'angularUtils.directives.dirPagination']);

app.config([
  '$compileProvider',
  '$sceDelegateProvider',
  function($compileProvider, $sceDelegateProvider) {
    $compileProvider.aHrefSanitizationWhitelist(
      /^\s*(https?|ftp|mailto|chrome-extension|javascript|tel):/,
    );
    $sceDelegateProvider.resourceUrlWhitelist([
      'self',
      'https://sherlock.staging.spicy.digital/**',
    ]);
  },
]);

app.config([
  '$httpProvider',
  function($httpProvider) {
    $httpProvider.interceptors.push('csrfInjector');
  },
]);

export { app };
