import { app } from '../app';
import moment from 'moment';

moment.locale('fr');

app.filter('dateFormat', function() {
    return function(input) {
        return moment(new Date(input)).format('DD MMMM YYYY');
    };
});

app.filter('effectDateFormat', function() {
    return function(input) {
        return moment(new Date(input.split('-').reverse().join())).format('DD MMMM YYYY');
    };
});
