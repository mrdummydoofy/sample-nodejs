import { app } from '../app';
import currencyFormatter from 'currency-formatter';


app.filter('currencyFormat', function() {
    return function(input) {
        return input % 1 ? currencyFormatter.format(input, {
            symbol: '€',
            decimal: ',',
            thousand: ' ',
            precision: 2,
            format: '%v %s'
        }) : currencyFormatter.format(input, {
            symbol: '€',
            thousand: ' ',
            precision: 0,
            format: '%v %s'
        }) ;
    };
});

app.filter('currencyFormatInput', function() {
    return function(input) {
        return input === undefined ? input : currencyFormatter.format(input, {
            symbol: '',
            decimal: '',
            thousand: ' ',
            precision: 0,
            format: '%v'
        });
    };
});
