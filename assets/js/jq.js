import $ from 'jquery';

$(document).ready(() => {
  $('#loader-app').fadeOut();
  // if ('serviceWorker' in navigator) {
  //   // register the offline sw
  //   navigator.serviceWorker
  //   .register('sw.js')
  //   .then()
  //   .catch(console.error);
  // }
});

$(() => {
  $('[data-toggle="tooltip"]').tooltip();
});

/* Définir page accueil */
try {
  if (window.location.pathname === '/') {
    $('body').addClass('front');
  } else {
    $('body').addClass('not-front');
  }
} catch (err) {}

/* Formulaire Login et mot de passe oublié swtich */
try {
  $('.toggle-form').click(() => {
    $('.div-form').slideToggle('fast');
  });
} catch (err) {}

/* Menu trigger */
try {
  $('.button-menu').click(e => {
    $('body').toggleClass('menu-open');
    $('.blocker').toggle();
    e.stopPropagation();
  });
  $('.menu').click(e => {
    e.stopPropagation();
  });
  $(document).click(event => {
    $('body').removeClass('menu-open');
    $('.blocker').hide();
  });
} catch (err) {}

/* Popup : VOUS AVEZ UNE DEMANDE SAUVEGRADÉE */
/*try{
  $(document).ready(() => {
    setTimeout(function(){ $('#demande-sauvegarder').modal(); }, 3000);
  });
}
catch(err){
}*/

/* Navigation accordion */
try {
  $('.nav-content .sub-menu span.btn-accordion').click(function(e) {
    if (!$(this).hasClass('active')) {
      $('.nav-content .sub-menu span.btn-accordion.active').removeClass('active');
    }
    $(this).toggleClass('active');
    $('.nav-content ul ul').slideUp(),
      $(this)
        .parents('li.sub-menu')
        .find('ul')
        .is(':visible') ||
        $(this)
          .parents('li.sub-menu')
          .find('ul')
          .slideDown(),
      e.stopPropagation();
  });
} catch (err) {}

/* Domaine d'intervention accordion */
try {
  $('.panel-intervention-content .btn-accordion').click(function(e) {
    if (!$(this).hasClass('active')) {
      $('.panel-intervention-content .btn-accordion.active').removeClass('active');
    }
    $(this).toggleClass('active');
    $('.panel-intervention-content ul').slideUp(),
      $(this)
        .parents('.panel-intervention-content')
        .find('ul')
        .is(':visible') ||
        $(this)
          .parents('.panel-intervention-content')
          .find('ul')
          .slideDown(),
      e.stopPropagation();
  });
} catch (err) {}

/* Responsabilité civile pro accordion */
try {
  $('.responsabilite-content .tab-pane .tab-header a').click(function(e) {
    if (!$(this).hasClass('active')) {
      $('.responsabilite-content .tab-pane .tab-header a.active').removeClass('active');
    }
    $(this).toggleClass('active');
    $('.responsabilite-content .tab-pane .tab-body').slideUp(),
      $(this)
        .parents('.tab-pane')
        .find('.tab-body')
        .is(':visible') ||
        $(this)
          .parents('.tab-pane')
          .find('.tab-body')
          .slideDown(),
      e.stopPropagation();
  });
  $(window).resize(() => {
    $('.responsabilite-content .tab-pane .tab-body').each(function() {
      if ($(window).width() > 767) {
        $(this).show();
      } else {
        $(this).hide();
      }
    });
  });
} catch (err) {}

/* Condition général et et particulières accordion */
try {
  $(window).resize(() => {
    if ($(window).width() > 480) {
      $('.mobile-accordion .accordion-content').css('display', 'block');
    } else {
      $('.mobile-accordion .accordion-content').css('display', 'none');
      $('.mobile-accordion h4.active').removeClass('active');
    }
  });
  $('.mobile-accordion h4').click(function(e) {
    if ($(window).width() <= 480) {
      if (!$(this).hasClass('active')) {
        $('.mobile-accordion h4.active').removeClass('active');
      }
      $(this).toggleClass('active');
      $('.mobile-accordion .accordion-content').slideUp(),
        $(this)
          .next()
          .is(':visible') ||
          $(this)
            .next()
            .slideDown(),
        e.stopPropagation();
    }
  });
} catch (err) {}

/* Profile Menu accordion */
try {
  $(document).ready(() => {
    if ($(window).width() > 991) {
      $('.profile-menu li a.sub-menu.active')
        .next()
        .slideDown();
    }
  });
  $('.profile-menu li a.sub-menu').click(function(e) {
    if ($(window).width() > 991) {
      if (!$(this).hasClass('active')) {
        $('.profile-menu li a.sub-menu.active').removeClass('active');
      }
      $(this).toggleClass('active');
      $('.profile-menu li .sub-liste').slideUp(),
        $(this)
          .next()
          .is(':visible') ||
          $(this)
            .next()
            .slideDown(),
        e.stopPropagation();
    } else {
      if (e.target.className !== 'profile-menu') {
        $('.profile-menu li .sub-liste').hide();
      }
    }
  });
  $('body').click(e => {
    if ($(window).width() <= 991 && e.target.className !== 'profile-menu') {
      $('.profile-menu li .sub-liste').hide();
    }
  });
} catch (err) {}

/* NOS DOMAINES D’INTERVENTIONS Height equal */
try {
  function equalFunction() {
    if ($(window).width() > 480) {
      var maxHeight = 0;
      $('div.equal-height').each(function() {
        if ($(this).height() > maxHeight) {
          maxHeight = $(this).height();
        }
      });
      $('div.equal-height').height(maxHeight);
    } else {
      $('div.equal-height').css('height', 'auto');
    }
  }
  $(document).ready(() => {
    equalFunction();
  });
  $(window).resize(() => {
    equalFunction();
  });
} catch (err) {}

try {
  var owl = $('.home-slider-carousel');
  owl.owlCarousel({
    autoplay: true,
    autoplayTimeout: 4000,
    loop: true,
    autoplayHoverPause: true,
    margin: 10,
    nav: false,
    pagination: true,
    center: true,
    items: 1,
  });
  // add animate.css class(es) to the elements to be animated
  function setAnimation(_elem, _InOut) {
    // Store all animationend event name in a string.
    // cf animate.css documentation
    var animationEndEvent =
      'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

    _elem.each(function() {
      var $elem = $(this);
      var $animationType = 'animated ' + $elem.data('animation-' + _InOut);

      $elem.addClass($animationType).one(animationEndEvent, () => {
        $elem.removeClass($animationType); // remove animate.css Class at the end of the animations
      });
    });
  }

  // Fired before current slide change
  owl.on('change.owl.carousel', event => {
    var $currentItem = $('.owl-item', owl).eq(event.item.index);
    var $elemsToanim = $currentItem.find('[data-animation-out]');
    setAnimation($elemsToanim, 'out');
  });

  // Fired after current slide has been changed
  var round = 0;
  owl.on('changed.owl.carousel', event => {
    var $currentItem = $('.owl-item', owl).eq(event.item.index);
    var $elemsToanim = $currentItem.find('[data-animation-in]');

    setAnimation($elemsToanim, 'in');
  });

  owl.on('translated.owl.carousel', event => {
    if (event.item.index === event.page.count - 1) {
      if (round < 1) {
        round++;
      } else {
        owl.trigger('stop.owl.autoplay');
        var owlData = owl.data('owl.carousel');
        owlData.settings.autoplay = false; //don't know if both are necessary
        owlData.options.autoplay = false;
        owl.trigger('refresh.owl.carousel');
      }
    }
  });
} catch (err) {}

try {
  var TIMEOUT = 6000;
  var interval = setInterval(handleNext, TIMEOUT);
  function handleNext() {
    var $radios = $('input[class*="slide-radio"]');
    var $activeRadio = $('input[class*="slide-radio"]:checked');
    var currentIndex = $activeRadio.index();
    var radiosLength = $radios.length;
    $radios.attr('checked', false);
    if (currentIndex >= radiosLength - 1) {
      $radios.first().attr('checked', true);
    } else {
      $activeRadio.next('input[class*="slide-radio"]').attr('checked', true);
    }
  }
} catch (err) {}

/* Découvrez nos offres carousel */
try {
  $('.offres-carousel').owlCarousel({
    autoplayHoverPause: true,
    margin: 10,
    nav: false,
    pagination: true,
    responsive: {
      0: {
        autoplay: true,
        autoplayTimeout: 8000,
        loop: true,
        items: 1,
      },
      600: {
        autoplay: true,
        autoplayTimeout: 8000,
        loop: true,
        items: 2,
      },
      1000: {
        items: 3,
      },
    },
  });
} catch (err) {
  //
}
/* Responsabilté caroussel */
try {
  $('.responsabilite-carousel').owlCarousel({
    autoplay: true,
    autoplayTimeout: 8000,
    loop: true,
    autoplayHoverPause: true,
    margin: 10,
    nav: false,
    pagination: true,
    responsive: {
      0: {
        items: 1,
      },
      600: {
        items: 2,
      },
      1000: {
        items: 2,
      },
    },
  });
} catch (err) {
  //
}

try {
  $('.advantage-carousel').owlCarousel({
    autoplayHoverPause: true,
    margin: 10,
    nav: false,
    pagination: true,
    responsive: {
      0: {
        autoplay: true,
        autoplayTimeout: 8000,
        loop: true,
        items: 1,
      },
      600: {
        autoplay: true,
        autoplayTimeout: 8000,
        loop: true,
        items: 3,
      },
      1000: {
        items: 5,
      },
    },
  });
} catch (err) {
  //
}
try {
  $('.testimony-carousel').owlCarousel({
    autoplayHoverPause: true,
    margin: 10,
    nav: false,
    pagination: true,
    responsive: {
      0: {
        autoplay: true,
        autoplayTimeout: 8000,
        loop: true,
        items: 1,
      },
      600: {
        autoplay: true,
        autoplayTimeout: 8000,
        loop: true,
        items: 1,
      },
      1000: {
        items: 2,
      },
    },
  });
} catch (err) {}

try {
  $('.partner-carousel').owlCarousel({
    autoplay: true,
    autoplayTimeout: 4000,
    loop: true,
    autoplayHoverPause: true,
    margin: 10,
    nav: false,
    pagination: true,
    responsive: {
      0: {
        autoplay: true,
        autoplayTimeout: 5000,
        loop: true,
        items: 2,
      },
      600: {
        autoplay: true,
        autoplayTimeout: 5000,
        loop: true,
        items: 3,
      },
      1000: {
        items: 6,
      },
    },
  });
} catch (err) {
  //
}

/* Dirigeants caroussel */
try {
  $('.dirigeants-carousel').owlCarousel({
    autoplay: true,
    autoplayTimeout: 8000,
    loop: true,
    autoplayHoverPause: true,
    margin: 10,
    nav: false,
    pagination: true,
    items: 1,
  });
} catch (err) {
  //
}

try {
  (function() {
    $(() => {
      return $('.sidebar').stick_in_parent({
        parent: '.sticky-content',
        offset_top: 30,
      });
    });
  }.call(this));
} catch (err) {
  //
}

try {
  $(document).ready(() => {
    $('.franchise input[type="radio"]:checked')
      .parents('li')
      .addClass('bg-pink');
  });
  $('.franchise input[type="radio"]').on('change', function(e) {
    $('.franchise input[type="radio"]').each(function() {
      $(this)
        .parents('li')
        .removeClass('bg-pink');
    });
    $(this)
      .parents('li')
      .addClass('bg-pink');
  });
} catch (err) {
  //
}

try {
  $(document).ready(() => {
    $('.periode input[type="radio"]:checked')
      .parents('li')
      .addClass('bg-pink');
  });
  $('.periode input[type="radio"]').on('change', function(e) {
    $('.periode input[type="radio"]').each(function() {
      $(this)
        .parents('li')
        .removeClass('bg-pink');
    });
    $(this)
      .parents('li')
      .addClass('bg-pink');
  });
} catch (err) {
  //
}

/* Radio box checked civilité */
try {
  $(document).ready(() => {
    $('.form-wizard .select-mobile input[type="radio"]:checked')
      .parents('.label-radio')
      .addClass('bg-pink');
  });
  $('.form-wizard .select-mobile input[type="radio"]').on('change', function(e) {
    $('.form-wizard .select-mobile input[type="radio"]').each(function() {
      $(this)
        .parents('.label-radio')
        .removeClass('bg-pink');
    });
    $(this)
      .parents('.label-radio')
      .addClass('bg-pink');
  });
} catch (err) {
  //
}

try {
  (function() {
    'use strict';
    $('.input-file').each(function() {
      var $input = $(this);
      var $label = $input.next('.js-labelFile');
      var labelVal = $label.html();

      $input.on('change', element => {
        var fileName = '';
        if (element.target.value) {
          fileName = element.target.value.split('\\').pop();
        }
        fileName
          ? $label
              .addClass('has-file')
              .find('.js-fileName')
              .html(fileName)
          : $label.removeClass('has-file').html(labelVal);
      });
    });
  })();
} catch (err) {
  //
}

/* On change show hide element */
try {
  $('.change').change(function() {
    if ($(this).val() === 'oui') {
      $(this)
        .parents('.form-group')
        .find('.hide-element')
        .slideDown();
    } else {
      $(this)
        .parents('.form-group')
        .find('.hide-element')
        .slideUp();
    }
  });
} catch (err) {
  //
}

/* Cookies */
try {
  $(document).ready(() => {
    if (!localStorage.getItem('vetoptim-cookies')) {
      setTimeout(() => {
        $('.cookies').slideDown();
        localStorage.setItem('vetoptim-cookies', true);
      }, 2000);
    }
  });
  $('.cookies .accept-cookies').click(() => {
    $('.cookies').slideUp();
  });
  $('.cookies .close').click(() => {
    $('.cookies').slideUp();
  });
} catch (err) {}

// $('.number-format').keyup(function() {
//   var n = $(this).val().replace(/\s/g, '').toString();
//   $(this).val(parseInt(n).toLocaleString('fr') !== 'NaN' ? parseInt(n).toLocaleString('fr') : '');
// });/* Définir page accueil */
try {
  if (window.location.pathname === '/') {
    $('body').addClass('front');
  } else {
    $('body').addClass('not-front');
  }
} catch (err) {}

/* Définir page accueil */
try {
  if (window.location.pathname === '/protection-activite') {
    localStorage.removeItem('type');
  }
} catch (err) {}

/* défilement jusqu'à domaines d'interventions */
try {
  $('a[name=gotointerventions]').click(function() {
    $('html,body').animate(
      {
        scrollTop: $('#interventions').offset().top - 100,
      },
      500,
      'linear',
    );
  });
} catch (error) {}

/* Tableau garantie responsabilité civile */
/*try {
  function garantieRC(){
    var nature = $( "ul.right-liste.header li:first-child" );
    console.log(nature);
  }
  $(window).resize(() => {
    if ($(window).width() < 768) {
      garantieRC();
    }
  });
  $(document).ready(() => {
    if ($(window).width() < 768) {
      garantieRC();
    }
  });
}catch (error) { }*/

/* défilement jusqu'à Liste des offres à combiner */
try {
  $('.link-combine a').click(function() {
    $('html,body').animate(
      {
        scrollTop: $('.section-combine').offset().top,
      },
      500,
      'linear',
    );
  });
} catch (error) {}

/* Adhésion: Franchise change */
try {
  $('.periodicite.franchise input[name="franchise"]').change(function() {
    $('.offre-table-recap .left .reduction').toggleClass('bottom');
  });
} catch (error) {}

// Chiffre d'affaire change valeur
try {
  $('#chiffreAffaires').on('keypress keyup', function() {
    if ($(this).val().length == 1 && $(this).val() == 0) {
      $(this).val('');
    }
  });
} catch (error) {}

// Fixer le width de l'entête tableau dans le popup
try {
  function fixedHead() {
    $('.modal.modal-fullscreen ul.liste-garantie li.fixed-head').width(
      $('.modal.modal-fullscreen ul.liste-garantie').width(),
    );
  }
  $('.modal-fullscreen').on('shown.bs.modal', function() {
    $(window).resize(() => {
      fixedHead();
    });
    fixedHead();
  });
} catch (error) {}

try {
  function copyFixedHead(t1, t2, t3) {
    $('.modal.modal-fullscreen .garanties.garanties-rc .content-modal ul.liste-garantie li').each(
      function() {
        if (!$(this).hasClass('fixed-head')) {
          $(this)
            .find('.right .right-liste li:nth-child(1)')
            .prepend(t1);
          $(this)
            .find('.right .right-liste li:nth-child(2)')
            .prepend(t2);
          $(this)
            .find('.right .right-liste li:nth-child(3)')
            .prepend(t3);
        }
      },
    );
  }
  $('.modal-fullscreen').on('shown.bs.modal', function() {
    var t1 = $(
      '.modal.modal-fullscreen .garanties.garanties-rc .content-modal ul.liste-garantie li.fixed-head .right .right-liste li:nth-child(1)',
    ).html();
    var t2 = $(
      '.modal.modal-fullscreen .garanties.garanties-rc .content-modal ul.liste-garantie li.fixed-head .right .right-liste li:nth-child(2)',
    ).html();
    var t3 = $(
      '.modal.modal-fullscreen .garanties.garanties-rc .content-modal ul.liste-garantie li.fixed-head .right .right-liste li:nth-child(3)',
    ).html();
    var resize = 0;
    $(window).resize(() => {
      if ($(window).width() < 768 && resize == 0) {
        resize = 1;
        copyFixedHead(t1, t2, t3);
      }
    });
    if ($(window).width() < 768) {
      resize = 1;
      copyFixedHead(t1, t2, t3);
    }
  });
} catch (error) {}
