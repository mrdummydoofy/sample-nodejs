import moment from 'moment';

export function calculateDeadline(fraction, effectDate) {
  let deadlineDate = null;
  let afterDeadlineDate = null;
  effectDate = effectDate
    .split('-')
    .reverse()
    .join('-');

  var effectMonth = moment(effectDate).month() + 1;
  if (parseInt(fraction) === 4) {
    if (effectMonth <= 3) {
      deadlineDate = moment()
        .set({ month: 2, date: 31, year: moment(effectDate).year() })
        .format('DD-MM-YYYY');
    }
    if (effectMonth > 3 && effectMonth <= 6) {
      deadlineDate = moment()
        .set({ month: 5, date: 30, year: moment(effectDate).year() })
        .format('DD-MM-YYYY');
    }
    if (effectMonth > 6 && effectMonth <= 9) {
      deadlineDate = moment()
        .set({ month: 8, date: 30, year: moment(effectDate).year() })
        .format('DD-MM-YYYY');
    }
    if (effectMonth > 9 && effectMonth <= 12) {
      deadlineDate = moment()
        .set({ month: 11, date: 31, year: moment(effectDate).year() })
        .format('DD-MM-YYYY');
    }
  }
  if (parseInt(fraction) === 2) {
    if (effectMonth <= 6) {
      deadlineDate = moment()
        .set({ month: 5, date: 30, year: moment(effectDate).year() })
        .format('DD-MM-YYYY');
    }
    if (effectMonth > 6 && effectMonth <= 12) {
      deadlineDate = moment()
        .set({ month: 11, date: 31, year: moment(effectDate).year() })
        .format('DD-MM-YYYY');
    }
  }
  if (parseInt(fraction) === 1) {
    deadlineDate = moment()
      .set({ month: 11, date: 31, year: moment(effectDate).year() })
      .format('DD-MM-YYYY');
  }
  if (parseInt(fraction) === 12) {
    deadlineDate = moment(effectDate)
      .endOf('month')
      .format('DD-MM-YYYY');
  }

  const parsedDeadlineDate = deadlineDate.split('-');
  afterDeadlineDate = moment(parsedDeadlineDate[2] + parsedDeadlineDate[1] + parsedDeadlineDate[0])
    .add(1, 'days')
    .format('DD-MM-YYYY');

  return {
    deadlineDate,
    afterDeadlineDate,
  };
}

export function postUpdateAdhesion(
  WS,
  offerType,
  fraction,
  effectDate,
  chosenQuotation,
  deadlineDate,
  afterDeadlineDate,
  cashQuotation,
  franchise,
  chiffreAffaires,
  errorOutput,
  $scope,
  formula,
) {
  WS.post(
    'updateAdhesion',
    JSON.stringify({
      offerType,
      fraction,
      effectDate,
      chosenQuotation,
      deadlineDate,
      afterDeadlineDate,
      cashQuotation,
      franchise,
      chiffreAffaires,
      formula,
    }),
  )
    .then(() => {})
    .then(null, error => {
      errorOutput = error.data.error;
      $scope.$applyAsync();
    });
}

export function getChosenPeriod(fraction) {
  if (fraction === 1) {
    return 'Annuelle';
  } else if (fraction === 2) {
    return 'Semestrielle';
  } else if (fraction === 4) {
    return 'Trimestrielle';
  } else {
    return 'Mensuelle';
  }
}

export function getChosenPeriodSliced(fraction) {
  if (fraction === 1) {
    return 'An';
  } else if (fraction === 2) {
    return 'Sem';
  } else if (fraction === 4) {
    return 'Trim';
  } else {
    return 'Mois';
  }
}

export function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if (new Date().getTime() - start > milliseconds) {
      break;
    }
  }
}

export function postToggleContinueLater(WS, continueLater, $window, path, offerType) {
  WS.post(
    'toggleContinueLater',
    JSON.stringify({
      continueLater,
      offerType,
    }),
  )
    .then(data => {
      if (data.data.message) {
        if (path) {
          $window.location.href = path;
        }
      }
    })
    .then(null, error => {
      console.log(error.data.error);
    });
}

export function postToggleContinueLaterPopup(WS, continueLaterShowPopup, offerType) {
  WS.post(
    'toggleContinueLaterPopup',
    JSON.stringify({
      continueLaterShowPopup,
      offerType,
    }),
  )
    .then(() => {})
    .then(null, error => {
      console.log(error.data.error);
    });
}

export async function checkRedirectOnConnect($window, $http) {
  if (localStorage.getItem('chosenMenu')) {
    $window.location.href = await nextPageAfterLogin(
      localStorage.getItem('chosenMenu'),
      localStorage.getItem('type'),
      $http,
      $window,
    );
  } else {
    if ($window.location.pathname === '/resetPassword') {
      $window.location.href = '/';
    } else {
      $window.location.reload();
    }
  }
}

export async function getIpAddress($http) {
  const query = new Promise((resolve, reject) => {
    $http({
      method: 'GET',
      url: 'https://api.ipify.org/?format=json',
    })
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
  const res = await query;
  return res.data.ip;
}

async function nextPageAfterLogin(value, type, $http, $window) {
  const query = new Promise((resolve, reject) => {
    $http({
      method: 'GET',
      url: $window.location.protocol + '//' + $window.location.host + '/authenticated',
    })
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
  const res = await query;

  if ((parseInt(value) === 1 || parseInt(value) === 2 || parseInt(value) === 3) && type === 'CR') {
    if (
      res.data.user.customer.length > 0 &&
      (res.data.user.customer[0].turnover === 0 || res.data.user.customer[0].activity === '')
    ) {
      return 'simulation?offerType=CR';
    }
    if (res.data.user.customer.length > 0 && res.data.user.customer[0].siren === '') {
      return 'info?offerType=CR';
    }
    return 'adhesion-summary?offerType=CR';
  } else if (parseInt(value) === 4 && type === 'CR') {
    if (
      res.data.user.customer.length > 0 &&
      (res.data.user.customer[0].turnover === 0 || res.data.user.customer[0].activity === '')
    ) {
      return 'simulation?offerType=CR';
    }
    if (res.data.user.customer.length > 0 && res.data.user.customer[0].siren === '') {
      return 'info?offerType=CR';
    }
    return 'rappeler';
  } else if (
    (parseInt(value) === 1 || parseInt(value) === 2 || parseInt(value) === 3) &&
    type === 'PMR'
  ) {
    if (
      res.data.user.customer.length > 0 &&
      (res.data.user.customer[0].turnover === 0 || res.data.user.customer[0].activity === '')
    ) {
      return 'simulation?offerType=PMR';
    }
    if (res.data.user.customer.length > 0 && res.data.user.customer[0].siren === '') {
      return 'info?offerType=PMR';
    }
    return 'adhesion-summary?offerType=PMR';
  } else if (parseInt(value) === 4 && type === 'PMR') {
    if (
      res.data.user.customer.length > 0 &&
      (res.data.user.customer[0].turnover === 0 || res.data.user.customer[0].activity === '')
    ) {
      return 'simulation?offerType=PMR';
    }
    if (res.data.user.customer.length > 0 && res.data.user.customer[0].siren === '') {
      return 'info?offerType=PMR';
    }
    return 'rappeler';
  } else if (
    (parseInt(value) === 1 || parseInt(value) === 2 || parseInt(value) === 3) &&
    type === 'PMRCR'
  ) {
    if (
      res.data.user.customer.length > 0 &&
      (res.data.user.customer[0].turnover === 0 || res.data.user.customer[0].activity === '')
    ) {
      return 'simulation?offerType=PMRCR';
    }
    if (res.data.user.customer.length > 0 && res.data.user.customer[0].siren === '') {
      return 'info?offerType=PMRCR';
    }
    return 'adhesion-summary?offerType=PMRCR';
  } else if (parseInt(value) === 4 && type === 'PMRCR') {
    if (
      res.data.user.customer.length > 0 &&
      (res.data.user.customer[0].turnover === 0 || res.data.user.customer[0].activity === '')
    ) {
      return 'simulation?offerType=PMRCR';
    }
    if (res.data.user.customer.length > 0 && res.data.user.customer[0].siren === '') {
      return 'info?offerType=PMRCR';
    }
    return 'rappeler';
  } else if (
    (parseInt(value) === 1 || parseInt(value) === 2 || parseInt(value) === 3) &&
    type === 'MD'
  ) {
    if (res.data.user.leader.length > 0 && res.data.user.leader[0].leaderBirthdate === '') {
      return 'simulation?offerType=MD';
    }
    if (res.data.user.leader.length > 0 && res.data.user.leader[0].siren === '') {
      return 'info?offerType=MD';
    }
    return 'adhesion-summary?offerType=MD';
  } else if (parseInt(value) === 4 && type === 'MD') {
    if (res.data.user.leader.length > 0 && res.data.user.leader[0].leaderBirthdate === '') {
      return 'simulation?offerType=MD';
    }
    if (res.data.user.leader.length > 0 && res.data.user.leader[0].siren === '') {
      return 'info?offerType=MD';
    }
    return 'rappeler';
  } else if (
    (parseInt(value) === 1 || parseInt(value) === 2 || parseInt(value) === 3) &&
    type === 'RCMS'
  ) {
    if (res.data.user.leader.length > 0 && res.data.user.leader[0].turnover === 0) {
      return 'simulation?offerType=RCMS';
    }
    if (res.data.user.leader.length > 0 && res.data.user.leader[0].siren === '') {
      return 'info?offerType=RCMS';
    }
    return 'adhesion-summary?offerType=RCMS';
  } else if (parseInt(value) === 4 && type === 'RCMS') {
    if (res.data.user.leader.length > 0 && res.data.user.customer[0].turnover === 0) {
      return 'simulation?offerType=RCMS';
    }
    if (res.data.user.leader.length > 0 && res.data.user.leader[0].siren === '') {
      return 'info?offerType=RCMS';
    }
    return 'rappeler';
  }
}

export async function calculateTax(
  WS,
  chiffreAffaires,
  activite,
  fraction,
  franchise,
  effectDate,
  alarm,
  bdm,
) {
  const query = new Promise((resolve, reject) => {
    if (activite === 'urbain') {
      WS.post(
        'pmrUrbanTax',
        Object.assign(
          {},
          {
            chiffreAffaires: chiffreAffaires,
            fraction: fraction,
            franchise: franchise,
            effectDate,
            alarm,
            bdm: bdm,
          },
        ),
      )
        .then(data => {
          resolve(data.data);
        })
        .then(null, error => {
          reject(error);
        });
    } else {
      WS.post(
        'pmrRuralMixedTax',
        Object.assign(
          {},
          {
            chiffreAffaires: chiffreAffaires,
            fraction: fraction,
            franchise: franchise,
            effectDate,
            alarm,
            bdm: bdm,
          },
        ),
      )
        .then(data => {
          resolve(data.data);
        })
        .then(null, error => {
          reject(error);
        });
    }
  });

  const res = await query;
  return res;
}

export async function calculateMD(
  WS,
  leaderBirthdate,
  conjointBirthdate,
  formula,
  childrenNumbers,
  haveConjoint,
  haveChildren,
  indivAccidentChoix,
  indivEnfants,
  rapatriementChoix,
  quotation,
  fraction,
  effectDate,
  ChildrenBirthdates,
) {
  const query = new Promise((resolve, reject) => {
    WS.post(
      'mdQuotation',
      Object.assign(
        {},
        {
          leaderBirthdate,
          conjointBirthdate,
          formula,
          childrenNumbers,
          haveConjoint,
          haveChildren,
          indivAccidentChoix,
          indivEnfants,
          rapatriementChoix,
          quotation,
          fraction,
          effectDate,
          ChildrenBirthdates,
        },
      ),
    )
      .then(data => {
        resolve(data.data);
      })
      .then(null, error => {
        reject(error);
      });
  });
  const res = await query;
  return res;
}

export async function calculatePmrCrTax(
  WS,
  chiffreAffaires,
  activite,
  fraction,
  franchise,
  effectDate,
  alarm,
  bdm,
) {
  const query = new Promise((resolve, reject) => {
    if (activite === 'urbain') {
      WS.post(
        'pmrCrUrbanTax',
        Object.assign(
          {},
          {
            chiffreAffaires: chiffreAffaires,
            fraction: fraction,
            franchise: franchise,
            effectDate,
            alarm,
            bdm: bdm,
          },
        ),
      )
        .then(data => {
          resolve(data.data);
        })
        .then(null, error => {
          reject(error);
        });
    } else {
      WS.post(
        'pmrCrRuralMixedTax',
        Object.assign(
          {},
          {
            chiffreAffaires: chiffreAffaires,
            fraction: fraction,
            franchise: franchise,
            effectDate,
            alarm,
            bdm: bdm,
          },
        ),
      )
        .then(data => {
          resolve(data.data);
        })
        .then(null, error => {
          reject(error);
        });
    }
  });

  const res = await query;
  return res;
}

export async function calculateCrTax(WS, chiffreAffaires, activite, fraction, effectDate) {
  const query = new Promise((resolve, reject) => {
    if (activite === 'urbain') {
      WS.post(
        'crUrbanTax',
        Object.assign(
          {},
          {
            chiffreAffaires: chiffreAffaires,
            fraction: fraction,
            effectDate: effectDate,
          },
        ),
      )
        .then(data => {
          resolve(data.data);
        })
        .then(null, error => {
          reject(error);
        });
    } else {
      WS.post(
        'crRuralTax',
        Object.assign(
          {},
          {
            chiffreAffaires: chiffreAffaires,
            fraction: fraction,
            effectDate: effectDate,
          },
        ),
      )
        .then(data => {
          resolve(data.data);
        })
        .then(null, error => {
          reject(error);
        });
    }
  });

  const res = await query;
  return res;
}

export async function calculateRCMS(WS, fraction, quotation, effectDate) {
  const query = new Promise((resolve, reject) => {
    WS.post(
      'rcmsQuotation',
      Object.assign(
        {},
        {
          quotation: quotation,
          fraction: fraction,
          effectDate: effectDate,
        },
      ),
    )
      .then(data => {
        resolve(data.data);
      })
      .then(null, error => {
        reject(error);
      });
  });
  const res = await query;
  return res;
}

export async function calculateRcms(WS, quotation, fraction, effectDate) {
  const query = new Promise((resolve, reject) => {
    WS.post(
      'rcmsQuotation',
      Object.assign(
        {},
        {
          quotation: quotation,
          fraction: fraction,
          effectDate: effectDate,
        },
      ),
    )
      .then(data => {
        resolve(data.data);
      })
      .then(null, error => {
        reject(error);
      });
  });

  const res = await query;
  return res;
}

export async function calculateMd(
  WS,
  leaderBirthdate,
  conjointBirthdate,
  ChildrenBirthdates,
  formula,
  childrenNumbers,
  haveConjoint,
  haveChildren,
  indivAccidentChoix,
  indivEnfants,
  rapatriementChoix,
  quotation,
  fraction,
  effectDate,
) {
  const query = new Promise((resolve, reject) => {
    WS.post(
      'mdQuotation',
      Object.assign(
        {},
        {
          leaderBirthdate,
          conjointBirthdate,
          ChildrenBirthdates,
          formula,
          childrenNumbers,
          haveConjoint,
          haveChildren,
          indivAccidentChoix,
          indivEnfants,
          rapatriementChoix,
          quotation,
          fraction,
          effectDate,
        },
      ),
    )
      .then(data => {
        resolve(data.data);
      })
      .then(null, error => {
        reject(error);
      });
  });

  const res = await query;
  return res;
}

export function padNumbers(value) {
  if (value.toString().length === 1) return '0' + value;
  return value.toString();
}

export function correctMonthlyCashQuotation(deadlineDate, effectDate, fraction, fractQuotation) {
  let monthlyQuotation = null;
  let remainingMonths = 0;

  const effectDateMonth =
    moment(
      new Date(
        `${effectDate.split('-')[2]}-${effectDate.split('-')[1]}-${effectDate.split('-')[0]}`,
      ),
    ).month() + 1;

  if (fraction === 12) {
    monthlyQuotation = fractQuotation;
  } else if (fraction === 4) {
    monthlyQuotation = fractQuotation / 3;
    if (
      effectDateMonth === 1 ||
      effectDateMonth === 4 ||
      effectDateMonth === 7 ||
      effectDateMonth === 10
    ) {
      remainingMonths = 2;
    } else if (
      effectDateMonth === 2 ||
      effectDateMonth === 5 ||
      effectDateMonth === 8 ||
      effectDateMonth === 11
    ) {
      remainingMonths = 1;
    } else {
      remainingMonths = 0;
    }
  } else if (fraction === 2) {
    monthlyQuotation = fractQuotation / 6;
    if (effectDateMonth <= 6) {
      remainingMonths = 6 - effectDateMonth;
    } else {
      remainingMonths = 12 - effectDateMonth;
    }
  } else if (fraction === 1) {
    monthlyQuotation = fractQuotation / 12;
    remainingMonths = 12 - effectDateMonth;
  }

  const daysInMonth = moment(
    new Date(`${effectDate.split('-')[2]}-${effectDate.split('-')[1]}-${effectDate.split('-')[0]}`),
  ).daysInMonth();

  const remainingDays =
    1 +
    daysInMonth -
    moment(
      new Date(
        `${effectDate.split('-')[2]}-${effectDate.split('-')[1]}-${effectDate.split('-')[0]}`,
      ),
    ).date();

  const remaningQuotation = (monthlyQuotation / daysInMonth) * remainingDays;
  const remainingMonthsQuotation = monthlyQuotation * remainingMonths;

  const cashQuotation = remaningQuotation + remainingMonthsQuotation;

  return {
    cashQuotation,
  };
}

export function getOfferTypeFr(offerType) {
  if (offerType === 'CR') {
    return 'RC';
  } else if (offerType === 'PMR') {
    return 'MRP';
  } else if (offerType === 'PMRCR') {
    return 'MRP + RC';
  } else if (offerType === 'MD') {
    return 'MD';
  } else if (offerType === 'RCMS') {
    return 'RCMS';
  }
}

export function getOfferTypeEng(offerType) {
  if (offerType === 'RC') {
    return 'CR';
  } else if (offerType === 'MRP') {
    return 'PMR';
  } else if (offerType === 'MRP + RC') {
    return 'PMRCR';
  } else if (offerType === 'MD') {
    return 'MD';
  } else if (offerType === 'RCMS') {
    return 'RCMS';
  }
}
