import { app } from '../app';

app.factory('csrfInjector', [
  function() {
    const csrfInjector = {
      request: function(config) {
        if (config.url !== 'http://ip-api.com/json') {
          config.headers['X-CSRF-Token'] = localStorage.getItem('vetoptim-csrf-token');
        }
        return config;
      },
    };
    return csrfInjector;
  },
]);
