import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-sweetalert/dist/sweetalert.css';
import 'font-awesome/css/font-awesome.min.css';
import 'froala-editor/css/froala_editor.pkgd.min.css';
import 'froala-editor/css/froala_style.min.css';
import '../styles/admin/themify-icons.css';
import '../styles/admin/component.css';
import '../styles/admin/style.css';
import '../styles/admin/linearicons.css';
import '../styles/admin/simple-line-icons.css';
import '../styles/admin/ionicons.css';
import '../styles/admin/jquery.mCustomScrollbar.css';
import '../styles/loading.css';
import '../styles/admin/style.scss';

import 'jquery';
import 'jquery-ui';
import 'popper.js/dist/popper.min.js';
import 'bootstrap/dist/js/bootstrap.min.js';
import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import 'bootstrap-sweetalert/dist/sweetalert.min.js';
import 'jquery-slimscroll/jquery.slimscroll.min.js';
import 'classie/lib/classie.js';
import 'froala-editor/js/froala_editor.pkgd.min.js';
import 'bootstrap-validator';

import './app';
import './services/WS';
import './interceptors/csrf';
import './controllers/admin';

import './adminjq';
import './admin/pcoded.min.js';
import './admin/demo-12.js';
import './admin/jquery.mCustomScrollbar.concat.min.js';
import './admin/jquery.mousewheel.min.js';
