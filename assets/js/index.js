import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import 'froala-editor/css/froala_editor.pkgd.min.css';
import 'froala-editor/css/froala_style.min.css';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'animate.css';
import '../styles/loading.css';
import '../styles/icon.css';
import '../styles/style.scss';

import 'jquery';
import 'popper.js/dist/popper.min.js';
import 'bootstrap/dist/js/bootstrap.min.js';
import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import 'owl.carousel';
import 'froala-editor/js/froala_editor.pkgd.min.js';
import 'sticky-kit/dist/sticky-kit.min.js';
import 'bootstrap-validator';

import './app';
import './services/WS';
import './filters/currency';
import './filters/date';
import './interceptors/csrf';
import './controllers/auth';
import './controllers/offerType';
import './controllers/simulationCR';
import './controllers/simulationPMR';
import './controllers/simulationPMRCR';
import './controllers/simulationMD';
import './controllers/simulationRCMS';

import './jq';
