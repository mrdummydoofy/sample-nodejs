import { app } from '../app';
import moment from 'moment';
import {
  padNumbers,
  calculateDeadline,
  postToggleContinueLater,
  postToggleContinueLaterPopup,
  calculateMd,
  calculateMD,
  correctMonthlyCashQuotation,
  postUpdateAdhesion,
  getChosenPeriod,
  getChosenPeriodSliced,
} from '../utils';

app.controller('SimulationMDController', [
  '$scope',
  '$window',
  '$http',
  'WS',
  function($scope, $window, $http, WS) {
    const vm = this;
    vm.form = {};
    vm.user = {};

    vm.form.haveConjoint = vm.form.haveConjoint ? vm.form.haveConjoint : 'non';
    vm.form.haveChildren = vm.form.haveChildren ? vm.form.haveChildren : 'non';
    vm.form.childrenNumbers = '';
    vm.form.ChildrenBirthdateDay = {};
    vm.form.ChildrenBirthdateMonth = {};
    vm.form.ChildrenBirthdateYear = {};
    vm.form.ChildrenName = {};
    vm.form.ChildrenLastname = {};
    vm.form.indivAccident = vm.form.indivAccident ? vm.form.indivAccident : 'non';
    vm.form.indivEnfants = vm.form.indivEnfants ? vm.form.indivEnfants : 'non';
    vm.form.rapatriement = vm.form.rapatriement ? vm.form.rapatriement : 'non';
    vm.form.civility = vm.form.civility ? vm.form.civility : 'monsieur';
    vm.form.type = vm.form.type ? vm.form.type : 'MD';
    vm.form.chosenMenu = localStorage.getItem('chosenMenu');
    vm.fraction = vm.fraction ? vm.fraction : 12;
    vm.formula = vm.formula ? vm.formula : 1;
    vm.effectDate = vm.effectDate
      ? vm.effectDate
      : moment()
          .utc()
          .utcOffset(2)
          .year(
            moment()
              .utc()
              .utcOffset(2)
              .year(),
          )
          .month(
            moment()
              .utc()
              .utcOffset(2)
              .month() + 1,
          )
          .date(1)
          .format('DD-MM-YYYY');
    vm.effectDateTmp = vm.effectDateTmp
      ? vm.effectDateTmp
      : moment()
          .add(1, 'days')
          .format('DD-MM-YYYY');
    vm.effectDateDay = vm.effectDateDay ? vm.effectDateDay : '01';
    vm.effectDateMonth = vm.effectDateMonth
      ? vm.effectDateMonth
      : (
          moment()
            .utc()
            .utcOffset(2)
            .year(
              moment()
                .utc()
                .utcOffset(2)
                .year(),
            )
            .month(
              moment()
                .utc()
                .utcOffset(2)
                .month() + 1,
            )
            .date(1)
            .month() + 1
        ).toString().length === 1
        ? `0${moment()
            .utc()
            .utcOffset(2)
            .year(
              moment()
                .utc()
                .utcOffset(2)
                .year(),
            )
            .month(
              moment()
                .utc()
                .utcOffset(2)
                .month() + 1,
            )
            .date(1)
            .month() + 1}`
        : (
            moment()
              .utc()
              .utcOffset(2)
              .year(
                moment()
                  .utc()
                  .utcOffset(2)
                  .year(),
              )
              .month(
                moment()
                  .utc()
                  .utcOffset(2)
                  .month() + 1,
              )
              .date(1)
              .month() + 1
          ).toString();
    vm.effectDateYear = vm.effectDateYear
      ? vm.effectDateYear
      : moment()
          .utc()
          .utcOffset(2)
          .year(
            moment()
              .utc()
              .utcOffset(2)
              .year(),
          )
          .month(
            moment()
              .utc()
              .utcOffset(2)
              .month() + 1,
          )
          .date(1)
          .year();
    vm.effectDates = [
      moment()
        .utc()
        .utcOffset(2)
        .date(1)
        .add(1, 'M')
        .format('DD-MM-YYYY'),
      moment()
        .utc()
        .utcOffset(2)
        .date(1)
        .add(2, 'M')
        .format('DD-MM-YYYY'),
      moment()
        .utc()
        .utcOffset(2)
        .date(1)
        .add(3, 'M')
        .format('DD-MM-YYYY'),
    ];
    vm.effectDates = vm.effectDates.map(e => {
      return { month: e.split('-')[1], year: e.split('-')[2] };
    });
    vm.deadlineDate = vm.deadlineDate
      ? vm.deadlineDate
      : calculateDeadline(vm.fraction, vm.effectDate).deadlineDate;
    vm.afterDeadlineDate = vm.afterDeadlineDate
      ? vm.afterDeadlineDate
      : calculateDeadline(vm.fraction, vm.effectDate).afterDeadlineDate;
    vm.showCityInput = vm.showCityInput ? vm.showCityInput : false;
    vm.form.type = vm.form.type ? vm.form.type : 'MD';

    $('#demande-sauvegarder').on('hidden.bs.modal', () => {
      postToggleContinueLaterPopup(WS, false, 'MD');
    });

    vm.authenticated = function() {
      WS.get('csrftoken').then(data => {
        localStorage.setItem('vetoptim-csrf-token', data.data._csrf);
      });

      WS.get('authenticated')
        .then(async data => {
          if (data.data.user) {
            //save md infos when connected
            if (
              localStorage.getItem('leaderBirthdateTmp') &&
              localStorage.getItem('haveConjointTmp') &&
              localStorage.getItem('haveChildrenTmp')
            ) {
              WS.post(
                'md',
                Object.assign(
                  {},
                  {
                    leaderBirthdate: localStorage.getItem('leaderBirthdateTmp')
                      ? localStorage.getItem('leaderBirthdateTmp')
                      : '',
                    haveConjoint: localStorage.getItem('haveConjointTmp') === 'oui' ? true : false,
                    haveChildren: localStorage.getItem('haveChildrenTmp') === 'oui' ? true : false,
                    conjointBirthdate: localStorage.getItem('conjointBirthdateTmp')
                      ? localStorage.getItem('conjointBirthdateTmp')
                      : '',
                    conjointLastname: localStorage.getItem('conjointLastnameTmp')
                      ? localStorage.getItem('conjointLastnameTmp')
                      : '',
                    conjointName: localStorage.getItem('conjointNameTmp')
                      ? localStorage.getItem('conjointNameTmp')
                      : '',
                    childrenNumber: localStorage.getItem('childrenNumberTmp')
                      ? localStorage.getItem('childrenNumberTmp')
                      : 0,
                    ChildrenBirthdateDay: localStorage.getItem('ChildrenBirthdateDayTmp')
                      ? localStorage.getItem('ChildrenBirthdateDayTmp')
                      : '',
                    ChildrenBirthdateMonth: localStorage.getItem('ChildrenBirthdateMonthTmp')
                      ? localStorage.getItem('ChildrenBirthdateMonthTmp')
                      : '',
                    ChildrenBirthdateYear: localStorage.getItem('ChildrenBirthdateYearTmp')
                      ? localStorage.getItem('ChildrenBirthdateYearTmp')
                      : '',
                    ChildrenName: localStorage.getItem('ChildrenNameTmp')
                      ? localStorage.getItem('ChildrenNameTmp')
                      : '',
                    ChildrenLastname: localStorage.getItem('ChildrenLastnameTmp')
                      ? localStorage.getItem('ChildrenLastnameTmp')
                      : '',
                    type: 'MD',
                  },
                ),
              )
                .then(data => {
                  if (data.data.message) {
                    localStorage.removeItem('leaderBirthdateTmp');
                    localStorage.removeItem('haveConjointTmp');
                    localStorage.removeItem('haveChildrenTmp');
                    localStorage.removeItem('conjointBirthdateTmp');
                    localStorage.removeItem('conjointNameTmp');
                    localStorage.removeItem('conjointLastnameTmp');
                    localStorage.removeItem('childrenNumberTmp');
                    localStorage.removeItem('ChildrenBirthdateDayTmp');
                    localStorage.removeItem('ChildrenBirthdateMonthTmp');
                    localStorage.removeItem('ChildrenBirthdateYearTmp');
                    localStorage.removeItem('ChildrenNameTmp');
                    localStorage.removeItem('ChildrenLastnameTmp');
                    vm.authenticated();
                  }
                })
                .then(null, error => {
                  vm.error = error.data.error;
                  $scope.$applyAsync();
                });
            }

            vm.user = data.data;
            vm.email = data.data.user.email;

            // menu and info form

            vm.continuesLater = [];
            vm.rates = [];

            if (data.data.user.adhesion.length > 0) {
              //my rate page
              vm.rates = data.data.user.adhesion.filter(rate => rate.rateFileName !== '');
              vm.continuesLater = data.data.user.adhesion.filter(
                save => save.continueLater === true,
              );

              if (vm.continuesLater.length > 0) {
                let lastContinueLaterDates = vm.continuesLater.map(last => last.updatedAt);
                vm.lastContinueLaterDate = lastContinueLaterDates.reduce((last, value) =>
                  moment.max(moment(value), moment(last)),
                );
              }

              if (data.data.user.adhesion.find(e => e.offerType === 'MD')) {
                vm.chosenQuotation = data.data.user.adhesion.find(
                  e => e.offerType === 'MD',
                ).chosenQuotation;
                vm.cashQuotation = data.data.user.adhesion.find(
                  e => e.offerType === 'MD',
                ).cashQuotation;
                vm.firstPaymentType = data.data.user.adhesion.find(
                  e => e.offerType === 'MD',
                ).firstPaymentType;
                vm.bcUrl = `https://sherlock.staging.spicy.digital/call_request.php?chosenQuotation=${Math.round(
                  vm.chosenQuotation,
                ) * 100}&user=${data.data.user.id}&offer=MD`;
                vm.chosenPeriod = getChosenPeriod(
                  data.data.user.adhesion.find(e => e.offerType === 'MD').fraction,
                );
                vm.chosenPeriodSliced = getChosenPeriodSliced(
                  data.data.user.adhesion.find(e => e.offerType === 'MD').fraction,
                );
                vm.fraction =
                  data.data.user.adhesion.find(e => e.offerType === 'MD').fraction === 0
                    ? 12
                    : data.data.user.adhesion.find(e => e.offerType === 'MD').fraction;
                vm.deadlineDate =
                  data.data.user.adhesion.find(e => e.offerType === 'MD').deadlineDate === ''
                    ? calculateDeadline(12, vm.effectDate).deadlineDate
                    : data.data.user.adhesion.find(e => e.offerType === 'MD').deadlineDate;
                vm.afterDeadlineDate =
                  data.data.user.adhesion.find(e => e.offerType === 'MD').deadlineDate === ''
                    ? calculateDeadline(12, vm.effectDate).afterDeadlineDate
                    : data.data.user.adhesion.find(e => e.offerType === 'MD').afterDeadlineDate;
                vm.effectDate =
                  data.data.user.adhesion.find(e => e.offerType === 'MD').effectDate === ''
                    ? vm.effectDate
                    : data.data.user.adhesion.find(e => e.offerType === 'MD').effectDate;
                vm.effectDateDay = padNumbers(
                  moment(
                    new Date(
                      `${vm.effectDate.split('-')[2]}-${vm.effectDate.split('-')[1]}-${
                        vm.effectDate.split('-')[0]
                      }`,
                    ),
                  ).date(),
                );
                vm.effectDateMonth = padNumbers(
                  moment(
                    new Date(
                      `${vm.effectDate.split('-')[2]}-${vm.effectDate.split('-')[1]}-${
                        vm.effectDate.split('-')[0]
                      }`,
                    ),
                  ).month() + 1,
                );
                vm.effectDateYear = padNumbers(
                  moment(
                    new Date(
                      `${vm.effectDate.split('-')[2]}-${vm.effectDate.split('-')[1]}-${
                        vm.effectDate.split('-')[0]
                      }`,
                    ),
                  ).year(),
                );

                const isAfterEffectDate = moment(
                  new Date(`${vm.effectDateYear}-${vm.effectDateMonth}-${vm.effectDateDay}`),
                ).isAfter(moment());
                if (!isAfterEffectDate) {
                  vm.validateEffectDateRes = false;
                } else {
                  vm.validateEffectDateRes = true;
                }
                const isValidEffectDate = moment(
                  `${vm.effectDateYear}-${vm.effectDateMonth}-${vm.effectDateDay}`,
                ).isValid();
                if (!isValidEffectDate) {
                  vm.validateEffectDateRes = false;
                } else {
                  vm.validateEffectDateRes = true;
                }

                if (
                  data.data.user.adhesion.find(e => e.offerType === 'MD').continueLater &&
                  data.data.user.adhesion.find(e => e.offerType === 'MD').continueLaterShowPopup
                ) {
                  setTimeout(() => {
                    $('#demande-sauvegarder').modal();
                  }, 3000);
                }
              }
            }

            if (data.data.user.leader.length > 0) {
              localStorage.setItem('leaderBirthdate', data.data.user.leader[0].leaderBirthdate);
              localStorage.setItem('conjointBirthdate', data.data.user.leader[0].conjointBirthdate);
              localStorage.setItem('conjointName', data.data.user.leader[0].conjointName);
              localStorage.setItem('conjointLastname', data.data.user.leader[0].conjointLastname);
              localStorage.setItem(
                'haveConjoint',
                data.data.user.leader[0].haveConjoint ? 'oui' : 'non',
              );
              localStorage.setItem(
                'haveChildren',
                data.data.user.leader[0].haveChildren ? 'oui' : 'non',
              );
              localStorage.setItem('childrenNumber', data.data.user.leader[0].childrenNumber);
              vm.form.siren = data.data.user.leader[0].siren;
              vm.form.companyName = data.data.user.leader[0].companyName;
              vm.leaderRef = data.data.user.leader[0].reference;
              if (data.data.user.leader[0].startActivityDate) {
                vm.form.startActivityDateDay = moment(
                  new Date(data.data.user.leader[0].startActivityDate),
                ).date();
                vm.form.startActivityDateMonth =
                  moment(new Date(data.data.user.leader[0].startActivityDate)).month() + 1;
                vm.form.startActivityDateYear = moment(
                  new Date(data.data.user.leader[0].startActivityDate),
                ).year();
              }
              vm.form.legalForm = data.data.user.leader[0].legalForm;
              vm.form.indivAccident = data.data.user.leader[0].indivAccident ? 'oui' : 'non';
              vm.form.indivAccidentChoix = data.data.user.leader[0].indivAccidentChoix;
              vm.form.indivEnfants = data.data.user.leader[0].indivEnfants ? 'oui' : 'non';
              vm.form.rapatriement = data.data.user.leader[0].rapatriement ? 'oui' : 'non';
              vm.form.rapatriementChoix = data.data.user.leader[0].rapatriementChoix;

              for (let i = 0; i < data.data.user.leader[0].children.length; i++) {
                vm.form.ChildrenBirthdateDay[i] = moment(
                  new Date(
                    `${data.data.user.leader[0].children[i].birthdate.split('-')[2]}-${
                      data.data.user.leader[0].children[i].birthdate.split('-')[1]
                    }-${data.data.user.leader[0].children[i].birthdate.split('-')[0]}`,
                  ),
                ).date();
                vm.form.ChildrenBirthdateMonth[i] =
                  moment(
                    new Date(
                      `${data.data.user.leader[0].children[i].birthdate.split('-')[2]}-${
                        data.data.user.leader[0].children[i].birthdate.split('-')[1]
                      }-${data.data.user.leader[0].children[i].birthdate.split('-')[0]}`,
                    ),
                  ).month() + 1;
                vm.form.ChildrenBirthdateYear[i] = moment(
                  new Date(
                    `${data.data.user.leader[0].children[i].birthdate.split('-')[2]}-${
                      data.data.user.leader[0].children[i].birthdate.split('-')[1]
                    }-${data.data.user.leader[0].children[i].birthdate.split('-')[0]}`,
                  ),
                ).year();
                vm.form.ChildrenName[i] = data.data.user.leader[0].children[i].name;
                vm.form.ChildrenLastname[i] = data.data.user.leader[0].children[i].lastname;
              }

              localStorage.setItem(
                'ChildrenBirthdateDay',
                JSON.stringify(vm.form.ChildrenBirthdateDay),
              );
              localStorage.setItem(
                'ChildrenBirthdateMonth',
                JSON.stringify(vm.form.ChildrenBirthdateMonth),
              );
              localStorage.setItem(
                'ChildrenBirthdateYear',
                JSON.stringify(vm.form.ChildrenBirthdateYear),
              );
              localStorage.setItem('ChildrenName', JSON.stringify(vm.form.ChildrenName));
              localStorage.setItem('ChildrenLastname', JSON.stringify(vm.form.ChildrenLastname));

              vm.form.haveConjoint = localStorage.getItem('haveConjoint');
              vm.form.haveChildren = localStorage.getItem('haveChildren');
              vm.form.childrenNumbers = localStorage.getItem('childrenNumber')
                ? localStorage.getItem('childrenNumber') === '0'
                  ? ''
                  : localStorage.getItem('childrenNumber')
                : '';

              vm.form.leaderBirthdate = localStorage.getItem('leaderBirthdate')
                ? localStorage.getItem('leaderBirthdate')
                : '';
              vm.form.conjointBirthdate = localStorage.getItem('conjointBirthdate')
                ? localStorage.getItem('conjointBirthdate')
                : '';
              vm.form.conjointName = localStorage.getItem('conjointName')
                ? localStorage.getItem('conjointName')
                : '';
              vm.form.conjointLastname = localStorage.getItem('conjointLastname')
                ? localStorage.getItem('conjointLastname')
                : '';
              vm.form.leaderAge = moment().diff(
                moment(
                  new Date(
                    `${vm.form.leaderBirthdate.split('-')[2]}-${
                      vm.form.leaderBirthdate.split('-')[1]
                    }-${vm.form.leaderBirthdate.split('-')[0]}`,
                  ),
                ),
                'year',
              );

              vm.form.conjointAge = moment().diff(
                moment(
                  new Date(
                    `${vm.form.conjointBirthdate.split('-')[2]}-${
                      vm.form.conjointBirthdate.split('-')[1]
                    }-${vm.form.conjointBirthdate.split('-')[0]}`,
                  ),
                ),
                'year',
              );

              if (data.data.user.leader[0].leaderBirthdate) {
                vm.form.leaderBirthdateDay = data.data.user.leader[0].leaderBirthdate.split('-')[0];
                vm.form.leaderBirthdateMonth = data.data.user.leader[0].leaderBirthdate.split(
                  '-',
                )[1];
                vm.form.leaderBirthdateYear = data.data.user.leader[0].leaderBirthdate.split(
                  '-',
                )[2];
              }

              if (data.data.user.leader[0].conjointBirthdate) {
                vm.form.conjointBirthdateDay = data.data.user.leader[0].conjointBirthdate.split(
                  '-',
                )[0];
                vm.form.conjointBirthdateMonth = data.data.user.leader[0].conjointBirthdate.split(
                  '-',
                )[1];
                vm.form.conjointBirthdateYear = data.data.user.leader[0].conjointBirthdate.split(
                  '-',
                )[2];
              }

              vm.childrenArray = data.data.user.leader[0].children;
              vm.childrenNumberInteger = data.data.user.leader[0].children.length;

              if (data.data.user.leader[0].children.length > 0) {
                if (vm.childrenArray.length > 0) {
                  for (let i = 0; i < vm.childrenArray.length; i++) {
                    vm.form.ChildrenBirthdateDay[`${i}`] = padNumbers(
                      moment(
                        new Date(
                          `${vm.childrenArray[i].birthdate.split('-')[2]}-${
                            vm.childrenArray[i].birthdate.split('-')[1]
                          }-${vm.childrenArray[i].birthdate.split('-')[0]}`,
                        ),
                      ).date(),
                    );
                    vm.form.ChildrenBirthdateMonth[`${i}`] = padNumbers(
                      moment(
                        new Date(
                          `${vm.childrenArray[i].birthdate.split('-')[2]}-${
                            vm.childrenArray[i].birthdate.split('-')[1]
                          }-${vm.childrenArray[i].birthdate.split('-')[0]}`,
                        ),
                      ).month() + 1,
                    );
                    vm.form.ChildrenBirthdateYear[`${i}`] = padNumbers(
                      moment(
                        new Date(
                          `${vm.childrenArray[i].birthdate.split('-')[2]}-${
                            vm.childrenArray[i].birthdate.split('-')[1]
                          }-${vm.childrenArray[i].birthdate.split('-')[0]}`,
                        ),
                      ).year(),
                    );
                    vm.form.ChildrenName[`${i}`] = padNumbers(vm.childrenArray[i].name);
                    vm.form.ChildrenLastname[`${i}`] = padNumbers(vm.childrenArray[i].lastname);
                  }
                }
                // generate ChildrenBirthdates
                const ChildrenBirthdates = [];

                for (let i = 0; i < Object.keys(vm.form.ChildrenBirthdateDay).length; i++) {
                  ChildrenBirthdates.push(
                    moment(
                      new Date(
                        `${vm.form.ChildrenBirthdateYear[`${i}`]}-${
                          vm.form.ChildrenBirthdateMonth[`${i}`]
                        }-${vm.form.ChildrenBirthdateDay[`${i}`]}`,
                      ),
                    ).format('DD-MM-YYYY'),
                  );
                }

                vm.form.ChildrenBirthdates = ChildrenBirthdates;
                vm.childrenNumbersLessThan26 = data.data.user.leader[0].children
                  .map(e =>
                    moment(
                      new Date(
                        `${e.birthdate.split('-')[2]}-${e.birthdate.split('-')[1]}-${
                          e.birthdate.split('-')[0]
                        }`,
                      ),
                    ),
                  )
                  .reduce(
                    (acc, current) => (moment().diff(current, 'year') < 26 ? (acc += 1) : acc),
                    0,
                  );
              }

              vm.formula = data.data.user.leader[0].formula;
            }

            if (data.data.user.address.length > 0) {
              vm.form.address = data.data.user.address[0].address;
              vm.form.postalCode = data.data.user.address[0].postalCode;
              vm.form.city = data.data.user.address[0].city;
              vm.showCityInput = true;
            }

            vm.form.civility = data.data.user.title === '' ? 'monsieur' : data.data.user.title;
            vm.form.firstName = data.data.user.name;
            vm.form.lastName = data.data.user.lastname;
            vm.form.phone = data.data.user.phone === '' ? undefined : data.data.user.phone;
            vm.form.mobile = data.data.user.mobile;
            vm.form.email = data.data.user.email;

            vm.chosenMenu = parseInt(localStorage.getItem('chosenMenu'));

            //paymentMethod form
            vm.paymentDay = 10;
            vm.paymentMethod = 'sepa';

            if (data.data.user.payment.length > 0) {
              vm.paymentDay = data.data.user.payment[0].pickingDate;
              vm.bic = data.data.user.payment[0].BIC;
              vm.iban = data.data.user.payment[0].IBAN;
              vm.holder = data.data.user.payment[0].holder;
              vm.exactTitle = data.data.user.payment[0].exactTitle;
              vm.ribImg = data.data.user.payment[0].ribImg;
              vm.isPayed = true;
            }

            if (data.data.user.firstPaymentCb.length > 0) {
              // vm.bic = data.data.user.firstPaymentCb[0].BIC;
              // vm.iban = data.data.user.firstPaymentCb[0].IBAN;
              // vm.holder = data.data.user.firstPaymentCb[0].holder;
              // vm.exactTitle = data.data.user.firstPaymentCb[0].exactTitle;
              vm.firstPaymentResponseCode = data.data.user.firstPaymentCb[0].responseCode;
              vm.firstPaymentOfferType = data.data.user.firstPaymentCb[0].offerType;
              if (vm.firstPaymentResponseCode === '00' && vm.firstPaymentOfferType === 'MD') {
                vm.paymentMethod = 'cb';
                vm.isPayed = true;
              }
            }

            vm.contracts = [];

            //transaction success
            if (data.data.user.contract.length > 0) {
              vm.contractRef =
                data.data.user.contract[data.data.user.contract.length - 1].reference;
              vm.mandatRef = data.data.user.contract[data.data.user.contract.length - 1].sepaMndRef;
            }

            //information success page
            vm.mySpaceUpdatedTarget = localStorage.getItem('mySpaceUpdatedTarget');

            //help form
            if (data.data.user.help.length > 0) {
              vm.phone = data.data.user.help[0].phone;
              vm.timing = data.data.user.help[0].choice;
              vm.title = data.data.user.help[0].title;
              vm.name = data.data.user.help[0].name;
              vm.lastname = data.data.user.help[0].lastname;
              vm.helpEmail = data.data.user.help[0].email;
              vm.requestDateDay =
                moment(new Date(data.data.user.help[0].date)).date() >= 0 &&
                moment(new Date(data.data.user.help[0].date)).date() <= 9
                  ? '0' + moment(new Date(data.data.user.help[0].date)).date()
                  : moment(new Date(data.data.user.help[0].date))
                      .date()
                      .toString();
              vm.requestDateMonth =
                moment(new Date(data.data.user.help[0].date))
                  .add(1, 'months')
                  .month() >= 0 &&
                moment(new Date(data.data.user.help[0].date))
                  .add(1, 'months')
                  .month() <= 9
                  ? '0' +
                    moment(new Date(data.data.user.help[0].date))
                      .add(1, 'months')
                      .month()
                  : moment(new Date(data.data.user.help[0].date))
                      .add(1, 'months')
                      .month()
                      .toString();
              vm.requestDateYear = moment(new Date(data.data.user.help[0].date)).year();
              vm.requestDateHour = data.data.user.help[0].hour.split(':')[0];
              vm.requestDateMinutes = data.data.user.help[0].hour.split(':')[1];
            }

            //Contact form
            if (data.data.user.contact.length > 0) {
              vm.customer = data.data.user.contact[0].customer;
              vm.message = data.data.user.contact[0].message;
              vm.subject = data.data.user.contact[0].subject;
              vm.mobile = data.data.user.contact[0].mobile;
              vm.firstname = data.data.user.contact[0].firstname;
              vm.lastname = data.data.user.contact[0].lastname;
              vm.contactEmail = data.data.user.contact[0].email;
            }

            if (vm.form.ChildrenName && Object.keys(vm.form.ChildrenName).length === 0) {
              vm.form.ChildrenName = undefined;
            }

            if (vm.form.ChildrenLastname && Object.keys(vm.form.ChildrenLastname).length === 0) {
              vm.form.ChildrenLastname = undefined;
            }

            if (
              vm.form.ChildrenBirthdateDay &&
              Object.keys(vm.form.ChildrenBirthdateDay).length === 0
            ) {
              vm.form.ChildrenBirthdateDay = undefined;
            }

            if (
              vm.form.ChildrenBirthdateMonth &&
              Object.keys(vm.form.ChildrenBirthdateMonth).length === 0
            ) {
              vm.form.ChildrenBirthdateMonth = undefined;
            }

            if (
              vm.form.ChildrenBirthdateYear &&
              Object.keys(vm.form.ChildrenBirthdateYear).length === 0
            ) {
              vm.form.ChildrenBirthdateYear = undefined;
            }
            if (
              $window.location.pathname === '/menu' &&
              $window.location.search.split('?')[1].split('=')[1] === 'MD' &&
              vm.form.leaderBirthdate === ''
            ) {
              $window.location.href = 'simulation?offerType=MD';
            }
          } else {
            vm.form.leaderBirthdateDay = localStorage.getItem('leaderBirthdateTmp')
              ? localStorage.getItem('leaderBirthdateTmp').split('-')[0]
              : undefined;
            vm.form.leaderBirthdateMonth = localStorage.getItem('leaderBirthdateTmp')
              ? localStorage.getItem('leaderBirthdateTmp').split('-')[1]
              : undefined;
            vm.form.leaderBirthdateYear = localStorage.getItem('leaderBirthdateTmp')
              ? localStorage.getItem('leaderBirthdateTmp').split('-')[2]
              : undefined;
            if (
              $window.location.pathname === '/menu' &&
              $window.location.search.split('?')[1].split('=')[1] === 'MD' &&
              vm.form.leaderBirthdateDay === undefined &&
              vm.form.leaderBirthdateMonth === undefined &&
              vm.form.leaderBirthdateYear === undefined
            ) {
              $window.location.href = 'simulation?offerType=MD';
            }
            vm.form.haveConjoint = localStorage.getItem('haveConjointTmp')
              ? localStorage.getItem('haveConjointTmp')
              : 'non';
            vm.form.haveChildren = localStorage.getItem('haveChildrenTmp')
              ? localStorage.getItem('haveChildrenTmp')
              : 'non';
            vm.form.childrenNumbers = localStorage.getItem('childrenNumberTmp')
              ? localStorage.getItem('childrenNumberTmp')
              : '';
            vm.childrenNumberInteger = parseInt(localStorage.getItem('childrenNumberTmp'));
            vm.childrenArray = localStorage.getItem('childrenNumberTmp')
              ? [...Array(vm.childrenNumberInteger).keys()]
              : 0;
            vm.form.conjointBirthdateDay = localStorage.getItem('conjointBirthdateTmp')
              ? localStorage.getItem('conjointBirthdateTmp').split('-')[0]
              : undefined;
            vm.form.conjointBirthdateMonth = localStorage.getItem('conjointBirthdateTmp')
              ? localStorage.getItem('conjointBirthdateTmp').split('-')[1]
              : undefined;
            vm.form.conjointBirthdateYear = localStorage.getItem('conjointBirthdateTmp')
              ? localStorage.getItem('conjointBirthdateTmp').split('-')[2]
              : undefined;
            vm.form.conjointName = localStorage.getItem('conjointNameTmp')
              ? localStorage.getItem('conjointNameTmp')
              : '';
            vm.form.conjointLastname = localStorage.getItem('conjointLastnameTmp')
              ? localStorage.getItem('conjointLastnameTmp')
              : '';
            vm.form.ChildrenBirthdateDay = localStorage.getItem('ChildrenBirthdateDayTmp')
              ? JSON.parse(localStorage.getItem('ChildrenBirthdateDayTmp'))
              : undefined;
            vm.form.ChildrenBirthdateMonth = localStorage.getItem('ChildrenBirthdateMonthTmp')
              ? JSON.parse(localStorage.getItem('ChildrenBirthdateMonthTmp'))
              : undefined;
            vm.form.ChildrenBirthdateYear = localStorage.getItem('ChildrenBirthdateYearTmp')
              ? JSON.parse(localStorage.getItem('ChildrenBirthdateYearTmp'))
              : undefined;

            vm.form.ChildrenName = localStorage.getItem('ChildrenNameTmp')
              ? JSON.parse(localStorage.getItem('ChildrenNameTmp'))
              : undefined;
            vm.form.ChildrenLastname = localStorage.getItem('ChildrenLastnameTmp')
              ? JSON.parse(localStorage.getItem('ChildrenLastnameTmp'))
              : undefined;

            vm.chosenMenu = parseInt(localStorage.getItem('chosenMenu'));
            vm.email = localStorage.getItem('email');
            vm.registrationEmail = localStorage.getItem('registrationEmail');
          }
        })
        .then(null, error => {
          console.log(error);
          vm.error = error.data.error;
          $scope.$applyAsync();
        });
    };

    vm.authenticated();

    vm.generateChildrenArray = function(value) {
      let generatedArray = [];
      if (value) {
        if (parseInt(value) <= 0) {
          vm.childrenArray = generatedArray;
          return;
        }
        for (let i = 0; i < parseInt(value); i++) {
          generatedArray.push(i);
        }
        vm.childrenNumberInteger = parseInt(value);
        vm.childrenArray = generatedArray;
        $scope.$applyAsync();
      } else {
        vm.childrenArray = generatedArray;
        $scope.$applyAsync();
      }
    };

    vm.store = function() {
      const leaderBirthdate = moment(
        new Date(
          `${vm.form.leaderBirthdateYear}-${vm.form.leaderBirthdateMonth}-${
            vm.form.leaderBirthdateDay
          }`,
        ),
      );
      const conjointBirthdate = moment(
        new Date(
          `${vm.form.conjointBirthdateYear}-${vm.form.conjointBirthdateMonth}-${
            vm.form.conjointBirthdateDay
          }`,
        ),
      );
      if (vm.user.user) {
        if (document.getElementById('sim1Form').checkValidity()) {
          if (
            leaderBirthdate.isValid() &&
            vm.form.haveConjoint !== undefined &&
            vm.form.haveChildren !== undefined
          ) {
            WS.post(
              'md',
              Object.assign(
                {},
                {
                  leaderBirthdate: leaderBirthdate.format('DD-MM-YYYY'),
                  haveConjoint: vm.form.haveConjoint === 'oui' ? true : false,
                  haveChildren: vm.form.haveChildren === 'oui' ? true : false,
                  conjointBirthdate: conjointBirthdate.format('DD-MM-YYYY'),
                  conjointName: vm.form.conjointName,
                  conjointLastname: vm.form.conjointLastname,
                  childrenNumber: vm.form.childrenNumbers,
                  ChildrenBirthdateDay: JSON.stringify(vm.form.ChildrenBirthdateDay),
                  ChildrenBirthdateMonth: JSON.stringify(vm.form.ChildrenBirthdateMonth),
                  ChildrenBirthdateYear: JSON.stringify(vm.form.ChildrenBirthdateYear),
                  ChildrenName: JSON.stringify(vm.form.ChildrenName),
                  ChildrenLastname: JSON.stringify(vm.form.ChildrenLastname),
                  type: 'MD',
                },
              ),
            )
              .then(data => {
                if (data.data.message) {
                  $window.location.href = 'menu?offerType=MD';
                }
              })
              .then(null, error => {
                vm.error = error.data.error;
                $scope.$applyAsync();
              });
          }
        }
      } else {
        if (document.getElementById('sim1Form').checkValidity()) {
          localStorage.setItem('leaderBirthdateTmp', leaderBirthdate.format('DD-MM-YYYY'));
          localStorage.setItem('haveConjointTmp', vm.form.haveConjoint);
          localStorage.setItem('haveChildrenTmp', vm.form.haveChildren);
          localStorage.setItem(
            'conjointBirthdateTmp',
            vm.form.haveConjoint === 'oui' ? conjointBirthdate.format('DD-MM-YYYY') : '',
          );
          localStorage.setItem(
            'conjointNameTmp',
            vm.form.haveConjoint === 'oui' ? vm.form.conjointName : '',
          );
          localStorage.setItem(
            'conjointLastnameTmp',
            vm.form.haveConjoint === 'oui' ? vm.form.conjointLastname : '',
          );
          localStorage.setItem(
            'childrenNumberTmp',
            vm.form.haveChildren === 'oui' ? vm.form.childrenNumbers : '',
          );
          if (vm.form.haveChildren === 'oui') {
            localStorage.setItem(
              'ChildrenBirthdateDayTmp',
              JSON.stringify(vm.form.ChildrenBirthdateDay),
            );
            localStorage.setItem(
              'ChildrenBirthdateMonthTmp',
              JSON.stringify(vm.form.ChildrenBirthdateMonth),
            );
            localStorage.setItem(
              'ChildrenBirthdateYearTmp',
              JSON.stringify(vm.form.ChildrenBirthdateYear),
            );
            localStorage.setItem('ChildrenNameTmp', JSON.stringify(vm.form.ChildrenName));
            localStorage.setItem('ChildrenLastnameTmp', JSON.stringify(vm.form.ChildrenLastname));
          }
          $window.location.href = 'menu?offerType=MD';
        }
      }
    };

    // Promo MD Quotation (menu)
    vm.mdQuotationPromo = function() {
      $scope.$watch('vm.form.leaderBirthdateDay', (newVal, oldVal) => {
        if (newVal !== oldVal) {
          const ChildrenBirthdates = [];
          if (vm.form.ChildrenBirthdateDay) {
            for (
              let i = 0;
              i <
              Object.keys(
                JSON.parse(
                  localStorage.getItem('ChildrenBirthdateDayTmp')
                    ? localStorage.getItem('ChildrenBirthdateDayTmp')
                    : localStorage.getItem('ChildrenBirthdateDay'),
                ),
              ).length;
              i++
            ) {
              ChildrenBirthdates.push(
                moment(
                  new Date(
                    `${
                      JSON.parse(
                        localStorage.getItem('ChildrenBirthdateYearTmp')
                          ? localStorage.getItem('ChildrenBirthdateYearTmp')
                          : localStorage.getItem('ChildrenBirthdateYear'),
                      )[i]
                    }-${
                      JSON.parse(
                        localStorage.getItem('ChildrenBirthdateMonthTmp')
                          ? localStorage.getItem('ChildrenBirthdateMonthTmp')
                          : localStorage.getItem('ChildrenBirthdateMonth'),
                      )[i]
                    }-${
                      JSON.parse(
                        localStorage.getItem('ChildrenBirthdateDayTmp')
                          ? localStorage.getItem('ChildrenBirthdateDayTmp')
                          : localStorage.getItem('ChildrenBirthdateDay'),
                      )[i]
                    }`,
                  ),
                ).format('DD-MM-YYYY'),
              );
            }
          }
          WS.post(
            'mdQuotationPromo',
            Object.assign(
              {},
              {
                leaderBirthdate: localStorage.getItem('leaderBirthdateTmp')
                  ? localStorage.getItem('leaderBirthdateTmp')
                  : localStorage.getItem('leaderBirthdate'),
                haveConjoint: localStorage.getItem('haveConjointTmp')
                  ? localStorage.getItem('haveConjointTmp')
                  : localStorage.getItem('haveConjoint'),
                haveChildren: localStorage.getItem('haveChildrenTmp')
                  ? localStorage.getItem('haveChildrenTmp')
                  : localStorage.getItem('haveChildren'),
                conjointBirthdate: localStorage.getItem('conjointBirthdateTmp')
                  ? localStorage.getItem('conjointBirthdateTmp')
                  : localStorage.getItem('conjointBirthdate'),
                ChildrenBirthdates: ChildrenBirthdates,
                childrenNumbers: localStorage.getItem('childrenNumberTmp')
                  ? localStorage.getItem('childrenNumberTmp')
                  : localStorage.getItem('childrenNumber'),
              },
            ),
          )
            .then(data => {
              vm.mdPromoQuotationMonthly = parseFloat(data.data.mdPromoQuotation).toFixed(2);
              vm.mdPromoQuotationAnnual = (parseFloat(vm.mdPromoQuotationMonthly) * 12).toFixed(2);
              vm.authenticated();
              $scope.$applyAsync();
            })
            .then(null, error => {
              vm.error = error.data.error;
              $scope.$applyAsync();
            });
        }
      });
    };

    // Fractionnal MD Quotation
    vm.fractMdQuotation = function() {
      $scope.$watch('vm.form.leaderAge', (newVal, oldVal) => {
        if (newVal !== oldVal) {
          (async () => {
            try {
              const value1 = await calculateMd(
                WS,
                vm.form.leaderBirthdate,
                vm.form.conjointBirthdate ? vm.form.conjointBirthdate : undefined,
                vm.form.ChildrenBirthdates ? vm.form.ChildrenBirthdates : undefined,
                1,
                vm.form.childrenNumbers,
                vm.form.haveConjoint,
                vm.form.haveChildren,
                vm.form.indivAccidentChoix,
                vm.form.indivEnfants,
                vm.form.rapatriementChoix,
                undefined,
                12,
                vm.effectDate,
              );
              vm.fractQuotationMonthly1 = parseFloat(value1.fractQuotation);
              vm.fractQuotation = vm.fractQuotationMonthly1;
              vm.cashQuotation = vm.cashQuotation
                ? vm.cashQuotation
                : correctMonthlyCashQuotation(
                    vm.deadlineDate,
                    vm.effectDate,
                    vm.fraction,
                    vm.fractQuotation,
                  ).cashQuotation;
              const value2 = await calculateMd(
                WS,
                vm.form.leaderBirthdate,
                vm.form.conjointBirthdate ? vm.form.conjointBirthdate : undefined,
                vm.form.ChildrenBirthdates ? vm.form.ChildrenBirthdates : undefined,
                2,
                vm.form.childrenNumbers,
                vm.form.haveConjoint,
                vm.form.haveChildren,
                vm.form.indivAccidentChoix,
                vm.form.indivEnfants,
                vm.form.rapatriementChoix,
                undefined,
                12,
                vm.effectDate,
              );
              const value3 = await calculateMd(
                WS,
                vm.form.leaderBirthdate,
                vm.form.conjointBirthdate ? vm.form.conjointBirthdate : undefined,
                vm.form.ChildrenBirthdates ? vm.form.ChildrenBirthdates : undefined,
                3,
                vm.form.childrenNumbers,
                vm.form.haveConjoint,
                vm.form.haveChildren,
                vm.form.indivAccidentChoix,
                vm.form.indivEnfants,
                vm.form.rapatriementChoix,
                undefined,
                12,
                vm.effectDate,
              );
              const value4 = await calculateMd(
                WS,
                vm.form.leaderBirthdate,
                vm.form.conjointBirthdate ? vm.form.conjointBirthdate : undefined,
                vm.form.ChildrenBirthdates ? vm.form.ChildrenBirthdates : undefined,
                4,
                vm.form.childrenNumbers,
                vm.form.haveConjoint,
                vm.form.haveChildren,
                vm.form.indivAccidentChoix,
                vm.form.indivEnfants,
                vm.form.rapatriementChoix,
                undefined,
                12,
                vm.effectDate,
              );
              const value5 = await calculateMd(
                WS,
                vm.form.leaderBirthdate,
                vm.form.conjointBirthdate ? vm.form.conjointBirthdate : undefined,
                vm.form.ChildrenBirthdates ? vm.form.ChildrenBirthdates : undefined,
                5,
                vm.form.childrenNumbers,
                vm.form.haveConjoint,
                vm.form.haveChildren,
                vm.form.indivAccidentChoix,
                vm.form.indivEnfants,
                vm.form.rapatriementChoix,
                undefined,
                12,
                vm.effectDate,
              );
              const value6 = await calculateMd(
                WS,
                vm.form.leaderBirthdate,
                vm.form.conjointBirthdate ? vm.form.conjointBirthdate : undefined,
                vm.form.ChildrenBirthdates ? vm.form.ChildrenBirthdates : undefined,
                6,
                vm.form.childrenNumbers,
                vm.form.haveConjoint,
                vm.form.haveChildren,
                vm.form.indivAccidentChoix,
                vm.form.indivEnfants,
                vm.form.rapatriementChoix,
                undefined,
                12,
                vm.effectDate,
              );

              vm.fractQuotationAnnual1 = parseFloat(value1.fractQuotation * 12);
              vm.fractQuotationMonthly2 = parseFloat(value2.fractQuotation);
              vm.fractQuotationAnnual2 = parseFloat(value2.fractQuotation * 12);
              vm.fractQuotationMonthly3 = parseFloat(value3.fractQuotation);
              vm.fractQuotationAnnual3 = parseFloat(value3.fractQuotation * 12);
              vm.fractQuotationMonthly4 = parseFloat(value4.fractQuotation);
              vm.fractQuotationAnnual4 = parseFloat(value4.fractQuotation * 12);
              vm.fractQuotationMonthly5 = parseFloat(value5.fractQuotation);
              vm.fractQuotationAnnual5 = parseFloat(value5.fractQuotation * 12);
              vm.fractQuotationMonthly6 = parseFloat(value6.fractQuotation);
              vm.fractQuotationAnnual6 = parseFloat(value6.fractQuotation * 12);

              if (vm.formula === 1) {
                if (vm.fraction === 1) {
                  vm.fractQuotation = parseFloat(vm.fractQuotationMonthly1 * 12);
                } else if (vm.fraction === 2) {
                  vm.fractQuotation = parseFloat(vm.fractQuotationMonthly1 * 6);
                } else if (vm.fraction === 4) {
                  vm.fractQuotation = parseFloat(vm.fractQuotationMonthly1 * 3);
                } else {
                  vm.fractQuotation = vm.fractQuotationMonthly1;
                }
              } else if (vm.formula === 2) {
                if (vm.fraction === 1) {
                  vm.fractQuotation = parseFloat(vm.fractQuotationMonthly2 * 12);
                } else if (vm.fraction === 2) {
                  vm.fractQuotation = parseFloat(vm.fractQuotationMonthly2 * 6);
                } else if (vm.fraction === 4) {
                  vm.fractQuotation = parseFloat(vm.fractQuotationMonthly2 * 3);
                } else {
                  vm.fractQuotation = vm.fractQuotationMonthly2;
                }
              } else if (vm.formula === 3) {
                if (vm.fraction === 1) {
                  vm.fractQuotation = parseFloat(vm.fractQuotationMonthly3 * 12);
                } else if (vm.fraction === 2) {
                  vm.fractQuotation = parseFloat(vm.fractQuotationMonthly3 * 6);
                } else if (vm.fraction === 4) {
                  vm.fractQuotation = parseFloat(vm.fractQuotationMonthly3 * 3);
                } else {
                  vm.fractQuotation = vm.fractQuotationMonthly3;
                }
              } else if (vm.formula === 4) {
                if (vm.fraction === 1) {
                  vm.fractQuotation = parseFloat(vm.fractQuotationMonthly4 * 12);
                } else if (vm.fraction === 2) {
                  vm.fractQuotation = parseFloat(vm.fractQuotationMonthly4 * 6);
                } else if (vm.fraction === 4) {
                  vm.fractQuotation = parseFloat(vm.fractQuotationMonthly4 * 3);
                } else {
                  vm.fractQuotation = vm.fractQuotationMonthly4;
                }
              } else if (vm.formula === 5) {
                if (vm.fraction === 1) {
                  vm.fractQuotation = parseFloat(vm.fractQuotationMonthly5 * 12);
                } else if (vm.fraction === 2) {
                  vm.fractQuotation = parseFloat(vm.fractQuotationMonthly5 * 6);
                } else if (vm.fraction === 4) {
                  vm.fractQuotation = parseFloat(vm.fractQuotationMonthly5 * 3);
                } else {
                  vm.fractQuotation = vm.fractQuotationMonthly5;
                }
              } else if (vm.formula === 6) {
                if (vm.fraction === 1) {
                  vm.fractQuotation = parseFloat(vm.fractQuotationMonthly6 * 12);
                } else if (vm.fraction === 2) {
                  vm.fractQuotation = parseFloat(vm.fractQuotationMonthly6 * 6);
                } else if (vm.fraction === 4) {
                  vm.fractQuotation = parseFloat(vm.fractQuotationMonthly6 * 3);
                } else {
                  vm.fractQuotation = vm.fractQuotationMonthly6;
                }
              }
              $scope.$applyAsync();
            } catch (error) {
              console.log(error);
            }
          })();
        }
      });
    };

    // update adhesion summary MD quotation
    vm.chosenFraction = function(
      leaderBirthdate,
      conjointBirthdate,
      formula,
      childrenNumbers,
      haveConjoint,
      haveChildren,
      indivAccidentChoix,
      indivEnfants,
      rapatriementChoix,
      quotation,
      fraction,
      effectDate,
      ChildrenBirthdates,
    ) {
      if (formula) {
        vm.formula = formula;
      }
      if (indivAccidentChoix) {
        vm.form.indivAccidentChoix = indivAccidentChoix;
      }
      if (indivEnfants) {
        indivEnfants = vm.form.indivEnfants;
      }
      if (rapatriementChoix) {
        rapatriementChoix = vm.form.rapatriementChoix;
      }
      if (conjointBirthdate) {
        conjointBirthdate = vm.form.conjointBirthdate;
      }
      if (ChildrenBirthdates) {
        ChildrenBirthdates = vm.form.ChildrenBirthdates;
      }

      (async () => {
        try {
          const taxValues = await calculateMD(
            WS,
            leaderBirthdate,
            conjointBirthdate,
            formula,
            childrenNumbers,
            haveConjoint,
            haveChildren,
            indivAccidentChoix,
            indivEnfants,
            rapatriementChoix,
            quotation,
            fraction,
            effectDate,
            ChildrenBirthdates,
          );
          vm.fractQuotation = parseFloat(quotation);
          vm.deadlineDate = calculateDeadline(fraction, effectDate).deadlineDate;
          vm.afterDeadlineDate = calculateDeadline(fraction, effectDate).afterDeadlineDate;
          vm.fraction = parseInt(fraction);
          vm.cashQuotation = correctMonthlyCashQuotation(
            vm.deadlineDate,
            effectDate,
            vm.fraction,
            vm.fractQuotation,
          ).cashQuotation;
          $scope.$applyAsync();
        } catch (error) {
          console.log(error);
        }
      })();
    };

    // update chosenMenu value
    vm.chooseMenu = function(value) {
      localStorage.setItem('chosenMenu', value);
      localStorage.setItem('type', 'MD');
      if (localStorage.getItem('chosenMenu') === '4') {
        $window.location.href = 'rappeler';
      } else {
        $window.location.href = 'info?offerType=MD';
      }
    };

    vm.getCityName = function() {
      vm.postalCodeError = undefined;
      if (vm.form.postalCode && vm.form.postalCode.length === 5) {
        WS.post('getCityName', JSON.stringify({ postalCode: vm.form.postalCode }))
          .then(data => {
            if (data.data.message) {
              vm.form.cities = data.data.message;
              vm.postalCodeError = undefined;
              vm.cityError = undefined;
              vm.showCityInput = false;
            }
            $scope.$applyAsync();
          })
          .then(null, error => {
            vm.postalCodeError = error.data.error;
            vm.form.city = undefined;
            vm.cityError = 'Veuillez saisir votre ville';
            $scope.$applyAsync();
          });
      }
    };

    vm.getSirenCoordinates = function() {
      if (vm.form.siren && vm.form.siren.length === 9) {
        WS.post(
          'getSirenCoordinates',
          JSON.stringify({
            siren:
              vm.form.siren[0] +
              vm.form.siren[1] +
              vm.form.siren[2] +
              ' ' +
              vm.form.siren[3] +
              vm.form.siren[4] +
              vm.form.siren[5] +
              ' ' +
              vm.form.siren[6] +
              vm.form.siren[7] +
              vm.form.siren[8],
          }),
        )
          .then(data => {
            if (data.data.message) {
              vm.form.siren = data.data.message.siren.replace(/ /g, '');
              vm.form.companyName = data.data.message.legalEntity;
              vm.form.address = data.data.message.address;
              vm.form.postalCode = data.data.message.postalCode;
              vm.showCityInput = true;
              vm.form.city = data.data.message.city;
              vm.form.phone = data.data.message.telephone;
              vm.form.email = data.data.message.email;
            }
            $scope.$applyAsync();
          })
          .then(null, error => {});
      }
    };

    // save form infos
    vm.saveInfos = function() {
      if (vm.form.infoTerm1 && vm.form.infoTerm2) {
        vm.error = undefined;
        if (document.getElementById('saveInfos').checkValidity()) {
          WS.post('subscriptionInfos', JSON.stringify(vm.form))
            .then(data => {
              if (data.data.redirect) {
                localStorage.setItem('email', vm.form.email);
                $window.location.href = data.data.redirect;
              }
            })
            .then(null, error => {
              vm.error = error.data.error;
              $scope.$applyAsync();
            });
        }
      } else {
        vm.error = 'Veuillez Accepter les termes';
      }
    };

    // get continue later email
    vm.getContinueLaterEmail = function() {
      WS.get('continueEmail', `offerType=${encodeURIComponent('MD')}`)
        .then(() => {})
        .then(null, error => {
          vm.error = error.data.error;
          $scope.$applyAsync();
        });
    };

    // continue subscription
    vm.continueSubscription = function(value) {
      if (value) {
        postToggleContinueLater(WS, false, $window, 'menu?offerType=MD', 'MD');
        postToggleContinueLaterPopup(WS, false, 'MD');
      } else {
        postToggleContinueLater(WS, false, $window, 'MD');
        postToggleContinueLaterPopup(WS, false, 'MD');
      }
    };

    // validate effectDate in adhesion page
    vm.validateEffectDate = function() {
      if (vm.effectDateDay && vm.effectDateMonth && vm.effectDateYear) {
        const isAfterEffectDate = moment(
          new Date(`${vm.effectDateYear}-${vm.effectDateMonth}-${vm.effectDateDay}`),
        ).isAfter(moment());
        vm.effectDate = moment()
          .add(1, 'days')
          .format('DD-MM-YYYY');
        const isValidEffectDate = moment(
          `${vm.effectDateYear}-${vm.effectDateMonth}-${vm.effectDateDay}`,
        ).isValid();
        if (!isAfterEffectDate) {
          document
            .getElementById('effectDateDay')
            .setCustomValidity(`La date d'effet doit être supérieur ou égale à ${vm.effectDate}`);
          document
            .getElementById('effectDateMonth')
            .setCustomValidity(`La date d'effet doit être supérieur ou égale à ${vm.effectDate}`);
          document
            .getElementById('effectDateYear')
            .setCustomValidity(`La date d'effet doit être supérieur ou égale à ${vm.effectDate}`);
          vm.validateEffectDateRes = false;
          return `La date d'effet doit être supérieur ou égale à ${vm.effectDate}`;
        } else if (!isValidEffectDate) {
          document
            .getElementById('effectDateDay')
            .setCustomValidity(`La date d'effet n'est pas valide`);
          document
            .getElementById('effectDateMonth')
            .setCustomValidity(`La date d'effet n'est pas valide`);
          document
            .getElementById('effectDateYear')
            .setCustomValidity(`La date d'effet n'est pas valide`);
          vm.validateEffectDateRes = false;
          return `La date d'effet n'est pas valide`;
        } else {
          document.getElementById('effectDateDay').setCustomValidity('');
          document.getElementById('effectDateMonth').setCustomValidity('');
          document.getElementById('effectDateYear').setCustomValidity('');
        }
        vm.effectDate = moment(
          new Date(`${vm.effectDateYear}-${vm.effectDateMonth}-${vm.effectDateDay}`),
        ).format('DD-MM-YYYY');
        vm.deadlineDate = calculateDeadline(vm.fraction, vm.effectDate).deadlineDate;
        vm.afterDeadlineDate = calculateDeadline(vm.fraction, vm.effectDate).afterDeadlineDate;
        vm.validateEffectDateRes = true;
        $scope.$applyAsync();
      }
    };

    // get adhesion summary
    vm.submitAdhesion = function() {
      if (vm.fraction) {
        $('#errorPeriod').remove();
        if (vm.chosenMenu === 1) {
          if (vm.form.infoTerm1 && vm.form.infoTerm2) {
            if (document.forms['adhesionForm'].checkValidity()) {
              postUpdateAdhesion(
                WS,
                'MD',
                vm.fraction,
                vm.effectDate,
                vm.fractQuotation,
                vm.deadlineDate,
                vm.afterDeadlineDate,
                vm.cashQuotation,
                vm.franchise,
                vm.error,
                $scope,
                vm.formula,
              );
              if (vm.chosenMenu === 1) {
                $window.location.href = 'terms?offerType=MD';
              } else if (vm.chosenMenu === 2) {
                $window.location.href = 'quote?offerType=MD';
              } else if (vm.chosenMenu === 3) {
                $window.location.href = 'save?offerType=MD';
              } else {
                $window.location.href = 'rappeler';
              }
              localStorage.setItem('currentUrlPath', $window.location.pathname);
            }
          } else {
            vm.error = 'Veuillez Accepter les termes';
          }
        } else {
          if (document.forms['adhesionForm'].checkValidity()) {
            postUpdateAdhesion(
              WS,
              'MD',
              vm.fraction,
              vm.effectDate,
              vm.fractQuotation,
              vm.deadlineDate,
              vm.afterDeadlineDate,
              vm.cashQuotation,
              vm.franchise,
              vm.error,
              $scope,
              vm.formula,
            );
            if (vm.chosenMenu === 1) {
              $window.location.href = 'terms?offerType=MD';
            } else if (vm.chosenMenu === 2) {
              $window.location.href = 'quote?offerType=MD';
            } else if (vm.chosenMenu === 3) {
              $window.location.href = 'save?offerType=MD';
            } else {
              $window.location.href = 'rappeler';
            }
            localStorage.setItem('currentUrlPath', $window.location.pathname);
          }
        }
      } else {
        $('html, body').animate(
          {
            scrollTop: $('div.periodicite').offset().top - 100,
          },
          500,
        );
        vm.errorPeriod = 'Veuillez choisir la périodicité';
      }
    };

    //save paymentMethod
    vm.savePaymentMethod = function() {
      const formdata = new FormData(document.getElementById('paymentForm'));
      if (
        document.getElementById('file').files.length > 0 &&
        document.getElementById('file').files[0].name &&
        vm.ribImg
      ) {
        formdata.append('updateRibFile', true);
      }
      (async () => {
        try {
          const { data } = await $http.post('/paymentInfos', formdata, {
            headers: { 'Content-Type': undefined },
            transformResponse: angular.identity,
          });
          if (JSON.parse(data).redirect) {
            $window.location.href = JSON.parse(data).redirect;
          }
        } catch (error) {
          vm.error = JSON.parse(error.data).error;
          $scope.$applyAsync();
        }
      })();
    };

    //download ribFile
    vm.downloadRibFile = function(ribFileName) {
      $window.open(`/downloadRib?filename=${ribFileName}`);
    };

    // go to transaction success page
    vm.goToTXSuccessPage = function() {
      if (vm.form.infoTerm1 && vm.form.infoTerm2) {
        $window.location.href = 'payment?offerType=MD';
      } else {
        vm.error = 'Veuillez Accepter les termes';
      }
    };

    // go to universign
    vm.signContract = function() {
      WS.get('getUniversignTransaction', 'type=MD')
        .then(data => {
          if (data.data.url) {
            $window.location.href = data.data.url;
          }
        })
        .then(null, error => {
          vm.error = error.data.error;
          $scope.$applyAsync();
        });
    };

    // --------------------------------------------------------------

    // rate path
    // get rate email
    vm.getRateEmail = function() {
      if (
        localStorage.getItem('currentUrlPath') &&
        localStorage.getItem('currentUrlPath') !== $window.location.pathname
      ) {
        WS.get('rateEmail', 'type=MD')
          .then(data => {
            if (data.data.rateFileName) {
              vm.rateFileNameGenerated = data.data.rateFileName;
              localStorage.setItem('rateFileNameGenerated', vm.rateFileNameGenerated);
              $scope.$applyAsync();
            }
          })
          .then(null, error => {
            vm.error = error.data.error;
            $scope.$applyAsync();
          });
      } else {
        vm.rateFileNameGenerated = localStorage.getItem('rateFileNameGenerated');
      }
      localStorage.setItem('currentUrlPath', $window.location.pathname);
    };

    // downloadRate
    vm.downloadRate = function(filename) {
      $window.open(`/downloadRate?filename=${filename}`);
    };

    vm.continueSubscriptionFromRate = function() {
      localStorage.setItem('chosenMenu', 1);
      $window.location.href = 'adhesion-summary?offerType=MD';
    };
  },
]);
