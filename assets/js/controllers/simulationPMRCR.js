import { app } from '../app';
import {
  calculateDeadline,
  postUpdateAdhesion,
  getChosenPeriod,
  getChosenPeriodSliced,
  postToggleContinueLater,
  postToggleContinueLaterPopup,
  calculatePmrCrTax,
  padNumbers,
  correctMonthlyCashQuotation,
} from '../utils';
import moment from 'moment';
import $ from 'jquery';

moment.suppressDeprecationWarnings = true;

// Simulation Controller
app.controller('SimulationPMRCRController', [
  '$scope',
  'WS',
  '$window',
  '$http',
  'currencyFormatFilter',
  function($scope, WS, $window, $http) {
    const vm = this;
    vm.user = {};
    vm.form = {};
    vm.commercial = {};
    vm.form.sinisterType = {};
    vm.form.sinisterAmountCR = {};
    vm.form.sinisterDateDayCR = {};
    vm.form.sinisterDateMonthCR = {};
    vm.form.sinisterDateYearCR = {};
    vm.form.sinisterAmountPMR = {};
    vm.form.sinisterDateDayPMR = {};
    vm.form.sinisterDateMonthPMR = {};
    vm.form.sinisterDateYearPMR = {};

    vm.form.sinistreConnaissance5Years = vm.form.sinistreConnaissance5Years
      ? vm.form.sinistreConnaissance5Years
      : 'non';
    vm.form.sinistreConnaissance = vm.form.sinistreConnaissance
      ? vm.form.sinistreConnaissance
      : 'non';
    vm.form.quotationsUpdated = vm.form.quotationsUpdated ? vm.form.quotationsUpdated : 'oui';
    vm.form.bdmSup = vm.form.bdmSup ? vm.form.bdmSup : 'non';
    vm.form.animalsValue = vm.form.animalsValue ? vm.form.animalsValue : 'non';
    vm.form.insuranceCancel = vm.form.insuranceCancel ? vm.form.insuranceCancel : 'non';
    vm.form.centralRef = vm.form.centralRef ? vm.form.centralRef : false;
    vm.form.individualCabinet = vm.form.individualCabinet ? vm.form.individualCabinet : false;
    vm.form.structure = vm.form.structure ? vm.form.structure : false;
    vm.form.liberalCollaborator = vm.form.liberalCollaborator ? vm.form.liberalCollaborator : false;
    vm.form.alarm = vm.form.alarm ? vm.form.alarm : 'non';
    vm.form.approvedAlarm = vm.form.approvedAlarm ? vm.form.approvedAlarm : 'non';
    vm.form.currentContract = vm.form.currentContract ? vm.form.currentContract : 'non';
    vm.form.currentContractCancel = vm.form.currentContractCancel
      ? vm.form.currentContractCancel
      : 'non';
    vm.form.tenant = vm.form.tenant ? vm.form.tenant : 'non';
    vm.form.endStatement = vm.form.endStatement ? vm.form.endStatement : false;
    vm.form.type = vm.form.type ? vm.form.type : 'PMRCR';
    vm.form.civility = vm.form.civility ? vm.form.civility : 'monsieur';
    vm.form.chosenMenu = localStorage.getItem('chosenMenu');
    vm.franchise = vm.franchise ? vm.franchise : '500';
    vm.fraction = vm.fraction ? vm.fraction : 12;
    vm.effectDate = vm.effectDate
      ? vm.effectDate
      : moment()
          .utc()
          .utcOffset(2)
          .year(
            moment()
              .utc()
              .utcOffset(2)
              .year(),
          )
          .month(
            moment()
              .utc()
              .utcOffset(2)
              .month() + 1,
          )
          .date(1)
          .format('DD-MM-YYYY');
    vm.effectDateTmp = vm.effectDateTmp
      ? vm.effectDateTmp
      : moment()
          .add(1, 'days')
          .format('DD-MM-YYYY');
    vm.effectDateDay = vm.effectDateDay ? vm.effectDateDay : '01';
    vm.effectDateMonth = vm.effectDateMonth
      ? vm.effectDateMonth
      : (
          moment()
            .utc()
            .utcOffset(2)
            .year(
              moment()
                .utc()
                .utcOffset(2)
                .year(),
            )
            .month(
              moment()
                .utc()
                .utcOffset(2)
                .month() + 1,
            )
            .date(1)
            .month() + 1
        ).toString().length === 1
        ? `0${moment()
            .utc()
            .utcOffset(2)
            .year(
              moment()
                .utc()
                .utcOffset(2)
                .year(),
            )
            .month(
              moment()
                .utc()
                .utcOffset(2)
                .month() + 1,
            )
            .date(1)
            .month() + 1}`
        : (
            moment()
              .utc()
              .utcOffset(2)
              .year(
                moment()
                  .utc()
                  .utcOffset(2)
                  .year(),
              )
              .month(
                moment()
                  .utc()
                  .utcOffset(2)
                  .month() + 1,
              )
              .date(1)
              .month() + 1
          ).toString();
    vm.effectDateYear = vm.effectDateYear
      ? vm.effectDateYear
      : moment()
          .utc()
          .utcOffset(2)
          .year(
            moment()
              .utc()
              .utcOffset(2)
              .year(),
          )
          .month(
            moment()
              .utc()
              .utcOffset(2)
              .month() + 1,
          )
          .date(1)
          .year();
    vm.effectDates = [
      moment()
        .utc()
        .utcOffset(2)
        .date(1)
        .add(1, 'M')
        .format('DD-MM-YYYY'),
      moment()
        .utc()
        .utcOffset(2)
        .date(1)
        .add(2, 'M')
        .format('DD-MM-YYYY'),
      moment()
        .utc()
        .utcOffset(2)
        .date(1)
        .add(3, 'M')
        .format('DD-MM-YYYY'),
    ];
    vm.effectDates = vm.effectDates.map(e => {
      return { month: e.split('-')[1], year: e.split('-')[2] };
    });
    vm.deadlineDate = vm.deadlineDate
      ? vm.deadlineDate
      : calculateDeadline(vm.fraction, vm.effectDate).deadlineDate;
    vm.afterDeadlineDate = vm.afterDeadlineDate
      ? vm.afterDeadlineDate
      : calculateDeadline(vm.fraction, vm.effectDate).afterDeadlineDate;
    vm.showCityInput = vm.showCityInput ? vm.showCityInput : false;
    vm.timing = vm.timing ? vm.timing : 'immediately';
    vm.title = vm.title ? vm.title : 'monsieur';
    vm.commercial.civility = vm.commercial.civility ? vm.commercial.civility : 'monsieur';
    vm.commercial.centralRef = vm.commercial.centralRef ? vm.commercial.centralRef : false;
    vm.commercial.individualCabinet = vm.commercial.individualCabinet
      ? vm.commercial.individualCabinet
      : false;
    vm.commercial.structure = vm.commercial.structure ? vm.commercial.structure : false;
    vm.commercial.liberalCollaborator = vm.commercial.liberalCollaborator
      ? vm.commercial.liberalCollaborator
      : false;
    vm.commercial.currentContract = vm.commercial.currentContract
      ? vm.commercial.currentContract
      : false;
    vm.commercial.currentContractCancel = vm.commercial.currentContractCancel
      ? vm.commercial.currentContractCancel
      : false;
    vm.commercial.endStatement = vm.commercial.endStatement ? vm.commercial.endStatement : false;
    vm.commercial.franchise = vm.commercial.franchise ? vm.commercial.franchise : '500';
    vm.commercial.activite = vm.commercial.activite ? vm.commercial.activite : 'urbain';
    vm.commercial.effectDateDay = vm.commercial.effectDateDay ? vm.commercial.effectDateDay : '01';
    vm.customer = vm.customer ? vm.customer : 'oui';
    // pagination
    vm.currentPage = 1;
    vm.itemsPerPage = 5;
    vm.loading = true;

    $('#demande-sauvegarder').on('hidden.bs.modal', () => {
      postToggleContinueLaterPopup(WS, false, 'MRP + RC');
    });

    vm.authenticated = function() {
      WS.get('csrftoken').then(data => {
        localStorage.setItem('vetoptim-csrf-token', data.data._csrf);
      });

      WS.get('authenticated')
        .then(async data => {
          // registration form
          if (data.data.user) {
            //save pmrcr infos when connected
            if (localStorage.getItem('chiffreAffairesTmp') && localStorage.getItem('activiteTmp')) {
              if (localStorage.getItem('tmpError')) {
                vm.error = localStorage.getItem('tmpError');
                localStorage.removeItem('chiffreAffairesTmp');
                localStorage.removeItem('activiteTmp');
                localStorage.removeItem('tmpError');
              } else {
                if (parseInt(localStorage.getItem('chiffreAffairesTmp')) <= 1500000) {
                  WS.post(
                    'pmr',
                    Object.assign(
                      {},
                      {
                        chiffreAffaires: localStorage.getItem('chiffreAffairesTmp'),
                        activite: localStorage.getItem('activiteTmp')
                          ? localStorage.getItem('activiteTmp')
                          : 'rurale',
                        type: 'PMRCR',
                      },
                    ),
                  )
                    .then(data => {
                      if (data.data.message) {
                        localStorage.removeItem('chiffreAffairesTmp');
                        localStorage.removeItem('activiteTmp');
                        vm.authenticated();
                      }
                    })
                    .then(null, error => {
                      vm.error = error.data.error;
                      $scope.$applyAsync();
                    });
                }
              }
            }

            vm.user = data.data;
            vm.email = data.data.user.email;

            // menu and info form
            vm.continuesLater = [];
            vm.rates = [];

            if (data.data.user.adhesion.length > 0) {
              if (data.data.user.adhesion.find(e => e.offerType === 'MRP + RC')) {
                vm.rates = data.data.user.adhesion.filter(rate => rate.rateFileName !== '');
                // generate sinsitersArrays
                vm.form.sinisterNumbersCR = data.data.user.adhesion.find(
                  e => e.offerType === 'MRP + RC',
                ).CRSinisterStatement.length;
                vm.sinsitersNumberIntegerCR = data.data.user.adhesion.find(
                  e => e.offerType === 'MRP + RC',
                ).CRSinisterStatement.length;
                vm.sinsitersArrayCR = data.data.user.adhesion.find(
                  e => e.offerType === 'MRP + RC',
                ).CRSinisterStatement;
                vm.form.sinisterNumbersPMR = data.data.user.adhesion.find(
                  e => e.offerType === 'MRP + RC',
                ).PMRSinisterStatement.length;
                vm.sinsitersNumberIntegerPMR = data.data.user.adhesion.find(
                  e => e.offerType === 'MRP + RC',
                ).PMRSinisterStatement.length;
                vm.sinsitersArrayPMR = data.data.user.adhesion.find(
                  e => e.offerType === 'MRP + RC',
                ).PMRSinisterStatement;

                if (vm.sinsitersArrayPMR.length > 0) {
                  for (let i = 0; i < vm.sinsitersArrayPMR.length; i++) {
                    vm.form.sinisterType[i] = vm.sinsitersArrayPMR[i].type;
                    vm.form.sinisterAmountPMR[i] = vm.sinsitersArrayPMR[i].amount;
                    vm.form.sinisterDateDayPMR[i] = padNumbers(
                      moment(
                        new Date(
                          `${vm.sinsitersArrayPMR[i].date.split('-')[2]}-${
                            vm.sinsitersArrayPMR[i].date.split('-')[1]
                          }-${vm.sinsitersArrayPMR[i].date.split('-')[0]}`,
                        ),
                      ).date(),
                    );
                    vm.form.sinisterDateMonthPMR[i] = padNumbers(
                      moment(
                        new Date(
                          `${vm.sinsitersArrayPMR[i].date.split('-')[2]}-${
                            vm.sinsitersArrayPMR[i].date.split('-')[1]
                          }-${vm.sinsitersArrayPMR[i].date.split('-')[0]}`,
                        ),
                      ).month() + 1,
                    );
                    vm.form.sinisterDateYearPMR[i] = moment(
                      new Date(
                        `${vm.sinsitersArrayPMR[i].date.split('-')[2]}-${
                          vm.sinsitersArrayPMR[i].date.split('-')[1]
                        }-${vm.sinsitersArrayPMR[i].date.split('-')[0]}`,
                      ),
                    ).year();
                  }
                }

                if (vm.sinsitersArrayCR.length > 0) {
                  for (let i = 0; i < vm.sinsitersArrayCR.length; i++) {
                    vm.form.sinisterAmountCR[i] = vm.sinsitersArrayCR[i].amount;
                    vm.form.sinisterDateDayCR[i] = padNumbers(
                      moment(
                        new Date(
                          `${vm.sinsitersArrayCR[i].date.split('-')[2]}-${
                            vm.sinsitersArrayCR[i].date.split('-')[1]
                          }-${vm.sinsitersArrayCR[i].date.split('-')[0]}`,
                        ),
                      ).date(),
                    );
                    vm.form.sinisterDateMonthCR[i] = padNumbers(
                      moment(
                        new Date(
                          `${vm.sinsitersArrayCR[i].date.split('-')[2]}-${
                            vm.sinsitersArrayCR[i].date.split('-')[1]
                          }-${vm.sinsitersArrayCR[i].date.split('-')[0]}`,
                        ),
                      ).month() + 1,
                    );
                    vm.form.sinisterDateYearCR[i] = moment(
                      new Date(
                        `${vm.sinsitersArrayCR[i].date.split('-')[2]}-${
                          vm.sinsitersArrayCR[i].date.split('-')[1]
                        }-${vm.sinsitersArrayCR[i].date.split('-')[0]}`,
                      ),
                    ).year();
                  }
                }

                vm.chosenQuotation = data.data.user.adhesion.find(
                  e => e.offerType === 'MRP + RC',
                ).chosenQuotation;
                vm.cashQuotation = data.data.user.adhesion.find(
                  e => e.offerType === 'MRP + RC',
                ).cashQuotation;
                vm.firstPaymentType = data.data.user.adhesion.find(
                  e => e.offerType === 'MRP + RC',
                ).firstPaymentType;
                vm.bcUrl = `https://sherlock.staging.spicy.digital/call_request.php?chosenQuotation=${Math.round(
                  vm.chosenQuotation,
                ) * 100}&user=${data.data.user.id}&offer=PMRCR`;
                vm.chosenPeriod = getChosenPeriod(
                  data.data.user.adhesion.find(e => e.offerType === 'MRP + RC').fraction,
                );
                vm.chosenPeriodSliced = getChosenPeriodSliced(
                  data.data.user.adhesion.find(e => e.offerType === 'MRP + RC').fraction,
                );
                vm.fraction =
                  data.data.user.adhesion.find(e => e.offerType === 'MRP + RC').fraction === 0
                    ? 12
                    : data.data.user.adhesion.find(e => e.offerType === 'MRP + RC').fraction;
                vm.deadlineDate =
                  data.data.user.adhesion.find(e => e.offerType === 'MRP + RC').deadlineDate === ''
                    ? calculateDeadline(12, vm.effectDate).deadlineDate
                    : data.data.user.adhesion.find(e => e.offerType === 'MRP + RC').deadlineDate;
                vm.afterDeadlineDate =
                  data.data.user.adhesion.find(e => e.offerType === 'MRP + RC').deadlineDate === ''
                    ? calculateDeadline(12, vm.effectDate).afterDeadlineDate
                    : data.data.user.adhesion.find(e => e.offerType === 'MRP + RC')
                        .afterDeadlineDate;

                vm.effectDate =
                  data.data.user.adhesion.find(e => e.offerType === 'MRP + RC').effectDate === ''
                    ? vm.effectDate
                    : data.data.user.adhesion.find(e => e.offerType === 'MRP + RC').effectDate;
                vm.effectDateDay = padNumbers(
                  moment(
                    new Date(
                      `${vm.effectDate.split('-')[2]}-${vm.effectDate.split('-')[1]}-${
                        vm.effectDate.split('-')[0]
                      }`,
                    ),
                  ).date(),
                );
                vm.effectDateMonth = padNumbers(
                  moment(
                    new Date(
                      `${vm.effectDate.split('-')[2]}-${vm.effectDate.split('-')[1]}-${
                        vm.effectDate.split('-')[0]
                      }`,
                    ),
                  ).month() + 1,
                );
                vm.effectDateYear = padNumbers(
                  moment(
                    new Date(
                      `${vm.effectDate.split('-')[2]}-${vm.effectDate.split('-')[1]}-${
                        vm.effectDate.split('-')[0]
                      }`,
                    ),
                  ).year(),
                );

                const isAfterEffectDate = moment(
                  new Date(`${vm.effectDateYear}-${vm.effectDateMonth}-${vm.effectDateDay}`),
                ).isAfter(moment());
                if (!isAfterEffectDate) {
                  vm.validateEffectDateRes = false;
                } else {
                  vm.validateEffectDateRes = true;
                }
                const isValidEffectDate = moment(
                  `${vm.effectDateYear}-${vm.effectDateMonth}-${vm.effectDateDay}`,
                ).isValid();
                if (!isValidEffectDate) {
                  vm.validateEffectDateRes = false;
                } else {
                  vm.validateEffectDateRes = true;
                }

                if (
                  data.data.user.adhesion.find(e => e.offerType === 'MRP + RC').continueLater &&
                  data.data.user.adhesion.find(e => e.offerType === 'MRP + RC')
                    .continueLaterShowPopup
                ) {
                  setTimeout(() => {
                    $('#demande-sauvegarder').modal();
                  }, 3000);
                }
              }
            }

            if (data.data.user.customer.length > 0) {
              localStorage.setItem(
                'chiffreAffaires',
                parseInt(data.data.user.customer[0].turnover),
              );
              localStorage.setItem('activite', data.data.user.customer[0].activity);
              localStorage.setItem('surfaceLocaux', data.data.user.customer[0].localArea);
              localStorage.setItem(
                'contenuLocaux',
                parseInt(data.data.user.customer[0].localContent),
              );
              vm.chiffreAffaires =
                parseInt(localStorage.getItem('chiffreAffaires')) === 0
                  ? undefined
                  : parseInt(localStorage.getItem('chiffreAffaires'));
              vm.activite = localStorage.getItem('activite')
                ? localStorage.getItem('activite')
                : 'urbain';
              vm.surfaceLocaux =
                parseInt(localStorage.getItem('surfaceLocaux')) === 0
                  ? undefined
                  : parseInt(localStorage.getItem('surfaceLocaux'));
              vm.contenuLocaux =
                parseInt(localStorage.getItem('contenuLocaux')) === 0
                  ? undefined
                  : parseInt(localStorage.getItem('contenuLocaux'));
              vm.form.siren = data.data.user.customer[0].siren;
              vm.form.surfaceLocaux =
                data.data.user.customer[0].localArea === 0
                  ? undefined
                  : data.data.user.customer[0].localArea;
              vm.form.contenuLocaux =
                data.data.user.customer[0].localContent === 0
                  ? undefined
                  : data.data.user.customer[0].localContent;
              vm.form.legalForm = data.data.user.customer[0].legalForm;

              if (data.data.user.customer[0].startActivityDate) {
                vm.form.startActivityDateDay = moment(
                  new Date(data.data.user.customer[0].startActivityDate),
                ).date();
                vm.form.startActivityDateMonth =
                  moment(new Date(data.data.user.customer[0].startActivityDate)).month() + 1;
                vm.form.startActivityDateYear = moment(
                  new Date(data.data.user.customer[0].startActivityDate),
                ).year();
              }

              if (data.data.user.customer[0].endStatementDate) {
                vm.form.endStatementDateDay = data.data.user.customer[0].endStatementDate.split(
                  '-',
                )[1];
                vm.form.endStatementDateMonth = data.data.user.customer[0].endStatementDate.split(
                  '-',
                )[0];
              }

              if (data.data.user.customer[0].currentContractDate) {
                vm.effectDate = moment(data.data.user.customer[0].currentContractDate)
                  .add(1, 'days')
                  .format('DD-MM-YYYY');
                vm.deadlineDate = calculateDeadline(vm.fraction, vm.effectDate).deadlineDate;
                vm.currentContractDate = moment(
                  data.data.user.customer[0].currentContractDate,
                ).format('DD-MM-YYYY');
                vm.afterDeadlineDate = calculateDeadline(
                  vm.fraction,
                  vm.effectDate,
                ).afterDeadlineDate;
                vm.form.currentContractDateDay = moment(
                  new Date(data.data.user.customer[0].currentContractDate),
                ).date();
                vm.form.currentContractDateMonth =
                  moment(new Date(data.data.user.customer[0].currentContractDate)).month() + 1;
                vm.form.currentContractDateYear = moment(
                  new Date(data.data.user.customer[0].currentContractDate),
                ).year();
                vm.form.currentContractDateMinus2Months =
                  moment(new Date(data.data.user.customer[0].currentContractDate)).diff(
                    moment().add(2, 'months'),
                    'months',
                    true,
                  ) <= 0;
                if (!vm.form.currentContractDateMinus2Months) {
                  vm.currentContractInfo = `Pensez à résilier votre contrat avant le ${moment(
                    new Date(
                      `${vm.form.currentContractDateYear}-${vm.form.currentContractDateMonth}-${
                        vm.form.currentContractDateDay
                      }`,
                    ),
                  )
                    .subtract(2, 'months')
                    .format('DD-MM-YYYY')}`;
                }
              }

              vm.form.currentContract = data.data.user.customer[0].currentContract ? 'oui' : 'non';
              vm.form.currentContractCancel = data.data.user.customer[0].currentContractCancel
                ? 'oui'
                : 'non';
              vm.form.companyName = data.data.user.customer[0].companyName;
              vm.form.structure = data.data.user.customer[0].structure;
              vm.form.alarm = data.data.user.customer[0].alarm ? 'oui' : 'non';
              vm.form.tenant = data.data.user.customer[0].tenant ? 'oui' : 'non';
              if (vm.form.alarm !== false) {
                vm.form.approvedAlarm = data.data.user.customer[0].approvedAlarm ? 'oui' : 'non';
              }
              vm.form.bdmSup = data.data.user.customer[0].bdmSup ? 'oui' : 'non';
              if (vm.form.bdmSup) {
                vm.form.bdm = data.data.user.customer[0].bdm
                  ? data.data.user.customer[0].bdm
                  : 'limite20000';
              }
              vm.form.centralRef = data.data.user.customer[0].centralRef;
              if (vm.form.centralRef !== false) {
                vm.form.centralRefName = data.data.user.customer[0].centralRefName;
              }
              vm.form.insuranceCancel = data.data.user.customer[0].insuranceCancel ? 'oui' : 'non';
              if (vm.form.insuranceCancel !== false) {
                vm.form.cancelReasonsSinister = data.data.user.customer[0].cancelReasonsSinister;
                vm.form.cancelReasonsPayment = data.data.user.customer[0].cancelReasonsPayment;
              }
              if (vm.form.cancelReasonsPayment) {
                vm.form.quotationsUpdated = data.data.user.customer[0].quotationsUpdated
                  ? 'oui'
                  : 'non';
              }
              vm.form.sinistreConnaissance5Years = data.data.user.customer[0]
                .sinistreConnaissance5Years
                ? 'oui'
                : 'non';
              vm.form.sinistreConnaissance = data.data.user.customer[0].sinistreConnaissance
                ? 'oui'
                : 'non';
              vm.form.animalsValue = data.data.user.customer[0].animalsValue ? 'oui' : 'non';
              vm.form.endStatement = data.data.user.customer[0].endStatement;
              vm.form.individualCabinet = data.data.user.customer[0].individualCabinet;
              vm.form.liberalCollaborator = data.data.user.customer[0].liberalCollaborator;
              vm.franchise = data.data.user.customer[0].franchise.toString();
            }
            if (data.data.user.address.length > 0) {
              vm.form.address = data.data.user.address[0].address;
              vm.form.postalCode = data.data.user.address[0].postalCode;
              vm.form.city = data.data.user.address[0].city;
              vm.showCityInput = true;
            }
            vm.form.civility = data.data.user.title === '' ? 'monsieur' : data.data.user.title;
            vm.form.firstName = data.data.user.name;
            vm.form.lastName = data.data.user.lastname;
            vm.form.phone = data.data.user.phone;
            vm.form.mobile = data.data.user.mobile;
            vm.form.email = data.data.user.email;

            //adhesion summary form

            vm.endOfEffectMonth = moment()
              .utc()
              .utcOffset(2)
              .year(
                moment()
                  .utc()
                  .utcOffset(2)
                  .year(),
              )
              .month(
                moment()
                  .utc()
                  .utcOffset(2)
                  .month() + 1,
              )
              .date(1)
              .endOf('month')
              .format('DD-MM-YYYY');

            vm.chosenMenu = parseInt(localStorage.getItem('chosenMenu'));
            //paymentMethod form
            vm.paymentDay = 10;
            vm.paymentMethod = 'sepa';

            if (data.data.user.payment.length > 0) {
              vm.paymentDay = data.data.user.payment[0].pickingDate;
              vm.bic = data.data.user.payment[0].BIC;
              vm.iban = data.data.user.payment[0].IBAN;
              vm.holder = data.data.user.payment[0].holder;
              vm.exactTitle = data.data.user.payment[0].exactTitle;
              vm.ribImg = data.data.user.payment[0].ribImg;
              vm.isPayed = true;
            }

            if (data.data.user.firstPaymentCb.length > 0) {
              vm.firstPaymentResponseCode = data.data.user.firstPaymentCb[0].responseCode;
              vm.firstPaymentOfferType = data.data.user.firstPaymentCb[0].offerType;
              if (vm.firstPaymentResponseCode === '00' && vm.firstPaymentOfferType === 'PMR') {
                vm.paymentMethod = 'cb';
                vm.isPayed = true;
              }
            }

            //transaction success
            if (data.data.user.contract.length > 0) {
              vm.contractRef =
                data.data.user.contract[data.data.user.contract.length - 1].reference;
              vm.customerRef = data.data.user.customer[0].reference;
              vm.mandatRef = data.data.user.contract[data.data.user.contract.length - 1].sepaMndRef;
            }

            //help form
            if (data.data.user.help.length > 0) {
              vm.phone = data.data.user.help[0].phone;
              vm.timing = data.data.user.help[0].choice;
              vm.title = data.data.user.help[0].title;
              vm.name = data.data.user.help[0].name;
              vm.lastname = data.data.user.help[0].lastname;
              vm.helpEmail = data.data.user.help[0].email;
              vm.requestDateDay =
                moment(new Date(data.data.user.help[0].date)).date() >= 0 &&
                moment(new Date(data.data.user.help[0].date)).date() <= 9
                  ? '0' + moment(new Date(data.data.user.help[0].date)).date()
                  : moment(new Date(data.data.user.help[0].date))
                      .date()
                      .toString();
              vm.requestDateMonth =
                moment(new Date(data.data.user.help[0].date))
                  .add(1, 'months')
                  .month() >= 0 &&
                moment(new Date(data.data.user.help[0].date))
                  .add(1, 'months')
                  .month() <= 9
                  ? '0' +
                    moment(new Date(data.data.user.help[0].date))
                      .add(1, 'months')
                      .month()
                  : moment(new Date(data.data.user.help[0].date))
                      .add(1, 'months')
                      .month()
                      .toString();
              vm.requestDateYear = moment(new Date(data.data.user.help[0].date)).year();
              vm.requestDateHour = data.data.user.help[0].hour.split(':')[0];
              vm.requestDateMinutes = data.data.user.help[0].hour.split(':')[1];
            }

            //custom Rate form
            if (data.data.user.customRate.length > 0) {
              vm.chiffreAffaires = data.data.user.customRate[0].turnover;
              vm.firstname = data.data.user.customRate[0].firstname;
              vm.lastname = data.data.user.customRate[0].lastname;
              vm.phone = data.data.user.customRate[0].phone;
              vm.requestEmail = data.data.user.customRate[0].email;
            }

            //contact form
            if (data.data.user.contact.length > 0) {
              vm.customer = data.data.user.contact[0].customer;
              vm.message = data.data.user.contact[0].message;
              vm.subject = data.data.user.contact[0].subject;
              vm.mobile = data.data.user.contact[0].mobile;
              vm.firstname = data.data.user.contact[0].firstname;
              vm.lastname = data.data.user.contact[0].lastname;
              vm.contactEmail = data.data.user.contact[0].email;
            }
            if (
              $window.location.pathname === '/menu' &&
              $window.location.search.split('?')[1].split('=')[1] === 'PMRCR' &&
              vm.chiffreAffaires === undefined
            ) {
              $window.location.href = 'simulation?offerType=PMRCR';
            }
          } else {
            vm.chiffreAffaires =
              localStorage.getItem('chiffreAffairesTmp') === null
                ? undefined
                : parseInt(localStorage.getItem('chiffreAffairesTmp'));
            if (
              $window.location.pathname === '/menu' &&
              $window.location.search.split('?')[1].split('=')[1] === 'PMRCR' &&
              vm.chiffreAffaires === undefined
            ) {
              $window.location.href = 'simulation?offerType=PMRCR';
            }
            vm.activite = localStorage.getItem('activiteTmp')
              ? localStorage.getItem('activiteTmp')
              : 'urbain';
            vm.surfaceLocaux =
              localStorage.getItem('surfaceLocaux') === null
                ? undefined
                : parseInt(localStorage.getItem('surfaceLocaux'));
            vm.contenuLocaux =
              localStorage.getItem('contenuLocaux') === null
                ? undefined
                : parseInt(localStorage.getItem('contenuLocaux'));
            vm.chosenMenu = parseInt(localStorage.getItem('chosenMenu'));
            vm.email = localStorage.getItem('email');
          }

          $scope.$applyAsync();
        })
        .then(null, error => {
          vm.error = error.data.error;
          $scope.$applyAsync();
        });
    };

    vm.authenticated();

    // save professional multi risk and restricting criteria for simulation
    vm.store = function() {
      if (vm.user.user) {
        if (
          vm.chiffreAffaires &&
          parseInt(vm.chiffreAffaires.toString().replace(/\s/g, '')) <= 1500000
        ) {
          WS.post(
            'pmr',
            Object.assign(
              {},
              {
                chiffreAffaires: parseInt(vm.chiffreAffaires.toString().replace(/\s/g, '')),
                activite: vm.activite ? vm.activite : 'urbain',
                type: 'PMRCR',
              },
            ),
          )
            .then(data => {
              if (data.data.message) {
                $window.location.href = 'menu?offerType=PMRCR';
              }
            })
            .then(null, error => {
              vm.error = error.data.error;
              $scope.$applyAsync();
            });
        } else {
          if ($scope.sim1Form.$valid) {
            $window.location.href = 'custom-rate?offerType=PMRCR';
          }
        }
      } else {
        if (
          vm.chiffreAffaires &&
          parseInt(vm.chiffreAffaires.toString().replace(/\s/g, '')) <= 1500000
        ) {
          localStorage.setItem(
            'chiffreAffairesTmp',
            vm.chiffreAffaires.toString().replace(/\s/g, ''),
          );
          localStorage.setItem('activiteTmp', vm.activite ? vm.activite : 'urbain');
          $window.location.href = 'menu?offerType=PMRCR';
        } else {
          if ($scope.sim1Form.$valid) {
            localStorage.setItem(
              'chiffreAffairesTmp',
              vm.chiffreAffaires.toString().replace(/\s/g, ''),
            );
            $window.location.href = 'custom-rate?offerType=PMRCR';
          }
        }
      }
    };

    // get pmr warranties in the simulation
    vm.pmrCrWarranty = function() {
      $scope.$watch('vm.chiffreAffaires', (newVal, oldVal) => {
        if (newVal !== oldVal) {
          WS.post(
            'pmrCrWarranty',
            Object.assign(
              {},
              {
                chiffreAffaires: vm.user.user
                  ? localStorage.getItem('chiffreAffaires')
                  : localStorage.getItem('chiffreAffairesTmp'),
                bdm: vm.form.bdm,
              },
            ),
          )
            .then(data => {
              vm.perteRecettes = parseInt(data.data.perteRecettes);
              vm.perteValeurVenale = parseInt(data.data.perteValeurVenale);
              vm.incendieReconstitution = parseInt(data.data.incendieReconstitution);
              vm.tempeteReconstitution = parseInt(data.data.tempeteReconstitution);
              vm.vandalismeReconstitution = parseInt(data.data.vandalismeReconstitution);
              vm.brisMachine = parseInt(data.data.brisMachine);
              vm.fraisSupplementaire = parseInt(data.data.fraisSupplementaire);
              $scope.$applyAsync();
            })
            .then(null, error => {
              vm.error = error.data.error;
              $scope.$applyAsync();
            });
        }
      });
    };

    // get pmr + cr quotation in the simulation
    vm.pmrCrQuotationPromo = function() {
      $scope.$watch('vm.chiffreAffaires', (newVal, oldVal) => {
        if (newVal !== oldVal) {
          WS.post(
            'pmrCrQuotationPromo',
            Object.assign(
              {},
              {
                chiffreAffaires: vm.chiffreAffaires,
                activite: vm.activite,
                surfaceLocaux: vm.surfaceLocaux,
                contenuLocaux: vm.contenuLocaux,
              },
            ),
          )
            .then(data => {
              if (vm.activite === 'urbain') {
                vm.pmrcrQuotationAnnualPromo = parseFloat(data.data.mrpRcUrbanQuotation);
                vm.pmrcrQuotationMonthlyPromo = parseFloat(vm.pmrcrQuotationAnnualPromo) / 12;
                vm.pmrcrQuotationSemestrialPromo = parseFloat(vm.pmrcrQuotationAnnualPromo) / 2;
                vm.pmrcrQuotationTrimestrialPromo = parseFloat(vm.pmrcrQuotationAnnualPromo) / 4;
                if (vm.franchise === '500') {
                  vm.chosenQuotation = parseFloat(vm.pmrcrQuotationMonthlyPromo);
                }
                vm.authenticated();
              } else {
                vm.pmrcrQuotationAnnualPromo = parseFloat(data.data.mrpRcRuralMixteQuotation);
                vm.pmrcrQuotationMonthlyPromo = parseFloat(vm.pmrcrQuotationAnnualPromo) / 12;
                vm.pmrcrQuotationSemestrialPromo = parseFloat(vm.pmrcrQuotationAnnualPromo) / 2;
                vm.pmrcrQuotationTrimestrialPromo = parseFloat(vm.pmrcrQuotationAnnualPromo) / 4;
                if (vm.franchise === '500') {
                  vm.chosenQuotation = parseFloat(vm.pmrcrQuotationMonthlyPromo);
                }
                vm.authenticated();
              }
              $scope.$applyAsync();
            })
            .then(null, error => {
              vm.error = error.data.error;
              $scope.$applyAsync();
            });
        }
      });
    };

    // get pmr + cr quotation infos
    vm.pmrCrQuotation = function() {
      $scope.$watch('vm.chiffreAffaires', (newVal, oldVal) => {
        if (newVal !== oldVal) {
          WS.post(
            'pmrCrQuotation',
            Object.assign(
              {},
              {
                chiffreAffaires: localStorage.getItem('chiffreAffaires'),
                activite: localStorage.getItem('activite'),
                surfaceLocaux: localStorage.getItem('surfaceLocaux'),
                contenuLocaux: localStorage.getItem('contenuLocaux'),
              },
            ),
          )
            .then(data => {
              if (localStorage.getItem('activite') === 'urbain') {
                vm.pmrcrQuotationAnnual = parseFloat(data.data.mrpRcUrbanQuotation);
                vm.pmrcrQuotationMonthly = parseFloat(vm.pmrcrQuotationAnnual) / 12;
                vm.pmrcrQuotationSemestrial = parseFloat(vm.pmrcrQuotationAnnual) / 2;
                vm.pmrcrQuotationTrimestrial = parseFloat(vm.pmrcrQuotationAnnual) / 4;
                if (vm.franchise === '300') {
                  vm.chosenQuotation = parseFloat(vm.pmrcrQuotationMonthly);
                }
                vm.authenticated();
              } else {
                vm.pmrcrQuotationAnnual = parseFloat(data.data.mrpRcRuralMixteQuotation);
                vm.pmrcrQuotationMonthly = parseFloat(vm.pmrcrQuotationAnnual / 12);
                vm.pmrcrQuotationSemestrial = parseFloat(vm.pmrcrQuotationAnnual) / 2;
                vm.pmrcrQuotationTrimestrial = parseFloat(vm.pmrcrQuotationAnnual) / 4;
                if (vm.franchise === '300') {
                  vm.chosenQuotation = parseFloat(vm.pmrcrQuotationMonthly);
                }
                vm.authenticated();
              }
              $scope.$applyAsync();
            })
            .then(null, error => {
              vm.error = error.data.error;
              $scope.$applyAsync();
            });
        }
      });
    };

    //saveCustomRate request
    vm.saveCustomRate = function() {
      if ($scope.customRateForm.$valid) {
        WS.post(
          'saveCustomRate',
          JSON.stringify({
            // turnover: parseInt(vm.chiffreAffaires.toString().replace(/\s/g, '')),
            firstname: vm.firstname,
            lastname: vm.lastname,
            phone: vm.phone,
            email: vm.requestEmail,
          }),
        ).then(data => {
          if (data.data.message) {
            $window.location.href = 'custom-rate-check?offerType=PMRCR';
          }
        });
      }
    };

    // update chosenMenu value
    vm.chooseMenu = function(value) {
      localStorage.setItem('chosenMenu', value);
      localStorage.setItem('type', 'PMRCR');
      if (localStorage.getItem('chosenMenu') === '4') {
        $window.location.href = 'rappeler';
      } else {
        $window.location.href = 'info?offerType=PMRCR';
      }
    };

    // save form infos
    vm.saveInfos = function() {
      // const formValid = Object.keys(vm.form)
      //   .map(e => vm.form[e])
      //   .filter(e => e === undefined || e === null || e === '' || e === 'undefined');
      vm.form.surfaceLocaux = vm.form.surfaceLocaux
        ? parseInt(vm.form.surfaceLocaux.toString().replace(/\s/g, ''))
        : undefined;
      vm.form.contenuLocaux = vm.form.contenuLocaux
        ? parseInt(vm.form.contenuLocaux.toString().replace(/\s/g, ''))
        : undefined;
      if ($window.location.pathname === '/informations') {
        vm.form.type = 'COMMON';
        if (document.getElementById('saveInfos').checkValidity()) {
          WS.post('subscriptionInfos', JSON.stringify(vm.form))
            .then(data => {
              if (data.data.redirect) {
                localStorage.setItem('mySpaceUpdatedTarget', 'DES INFORMATIONS');
                $window.location.href = data.data.redirect;
              }
            })
            .then(null, error => {
              vm.error = error.data.error;
              $scope.$applyAsync();
            });
        }
      }
      if (vm.form.infoTerm1 && vm.form.infoTerm2) {
        if (
          (vm.form.insuranceCancel === 'oui' && vm.form.cancelReasonsPayment) ||
          (vm.form.insuranceCancel === 'oui' && vm.form.cancelReasonsSinister) ||
          vm.form.insuranceCancel === 'non'
        ) {
          vm.error = undefined;
          if (document.getElementById('saveInfos').checkValidity()) {
            WS.post('subscriptionInfos', JSON.stringify(vm.form))
              .then(data => {
                if (data.data.redirect) {
                  localStorage.setItem('email', vm.form.email);

                  $window.location.href = data.data.redirect;
                }
              })
              .then(null, error => {
                vm.error = error.data.error;
                $scope.$applyAsync();
              });
          }
        } else {
          vm.cancelReasonsError = 'Veuillez choisir un motif';
          $scope.$applyAsync();
        }
      } else {
        vm.error = 'Veuillez Accepter les termes';
      }
    };

    // set Current Contract Date Minus 2 Months
    vm.setCurrentContractDateMinus2Months = function() {
      if (
        vm.form.currentContractDateYear &&
        vm.form.currentContractDateMonth &&
        vm.form.currentContractDateDay
      ) {
        const isAfterCurrentDate = moment(
          new Date(
            `${vm.form.currentContractDateYear}-${vm.form.currentContractDateMonth}-${
              vm.form.currentContractDateDay
            }`,
          ),
        ).isAfter(moment());
        if (!isAfterCurrentDate) {
          document
            .getElementById('currentContractDateYear')
            .setCustomValidity('merci de vérifier la date');
          document
            .getElementById('currentContractDateDay')
            .setCustomValidity('merci de vérifier la date');
          document
            .getElementById('currentContractDateMonth')
            .setCustomValidity('merci de vérifier la date');
          return;
        } else {
          document.getElementById('currentContractDateYear').setCustomValidity('');
          document.getElementById('currentContractDateDay').setCustomValidity('');
          document.getElementById('currentContractDateMonth').setCustomValidity('');
        }
        vm.form.currentContractDateMinus2Months =
          moment(
            new Date(
              `${vm.form.currentContractDateYear}-${vm.form.currentContractDateMonth}-${
                vm.form.currentContractDateDay
              }`,
            ),
          ).diff(moment().add(2, 'months'), 'months', true) <= 0;
        if (!vm.form.currentContractDateMinus2Months) {
          vm.currentContractInfo = `Pensez à résilier votre contrat avant le ${moment(
            new Date(
              `${vm.form.currentContractDateYear}-${vm.form.currentContractDateMonth}-${
                vm.form.currentContractDateDay
              }`,
            ),
          )
            .subtract(2, 'months')
            .format('DD-MM-YYYY')}`;
        }
      }
    };

    // validate effectDate in adhesion page
    vm.validateEffectDate = function() {
      if (vm.effectDateDay && vm.effectDateMonth && vm.effectDateYear) {
        const isAfterEffectDate = moment(
          new Date(`${vm.effectDateYear}-${vm.effectDateMonth}-${vm.effectDateDay}`),
        ).isAfter(moment());
        vm.effectDate = moment()
          .add(1, 'days')
          .format('DD-MM-YYYY');
        const isValidEffectDate = moment(
          `${vm.effectDateYear}-${vm.effectDateMonth}-${vm.effectDateDay}`,
        ).isValid();
        if (!isAfterEffectDate) {
          document
            .getElementById('effectDateDay')
            .setCustomValidity(`La date d'effet doit être supérieur ou égale à ${vm.effectDate}`);
          document
            .getElementById('effectDateMonth')
            .setCustomValidity(`La date d'effet doit être supérieur ou égale à ${vm.effectDate}`);
          document
            .getElementById('effectDateYear')
            .setCustomValidity(`La date d'effet doit être supérieur ou égale à ${vm.effectDate}`);
          vm.validateEffectDateRes = false;
          return `La date d'effet doit être supérieur ou égale à ${vm.effectDate}`;
        } else if (!isValidEffectDate) {
          document
            .getElementById('effectDateDay')
            .setCustomValidity(`La date d'effet n'est pas valide`);
          document
            .getElementById('effectDateMonth')
            .setCustomValidity(`La date d'effet n'est pas valide`);
          document
            .getElementById('effectDateYear')
            .setCustomValidity(`La date d'effet n'est pas valide`);
          vm.validateEffectDateRes = false;
          return `La date d'effet n'est pas valide`;
        } else {
          document.getElementById('effectDateDay').setCustomValidity('');
          document.getElementById('effectDateMonth').setCustomValidity('');
          document.getElementById('effectDateYear').setCustomValidity('');
        }
        vm.effectDate = moment(
          new Date(`${vm.effectDateYear}-${vm.effectDateMonth}-${vm.effectDateDay}`),
        ).format('DD-MM-YYYY');
        vm.deadlineDate = calculateDeadline(vm.fraction, vm.effectDate).deadlineDate;
        vm.afterDeadlineDate = calculateDeadline(vm.fraction, vm.effectDate).afterDeadlineDate;
        vm.validateEffectDateRes = true;
        $scope.$applyAsync();
      }
    };

    vm.getCityName = function() {
      vm.postalCodeError = undefined;
      if (vm.form.postalCode && vm.form.postalCode.length === 5) {
        WS.post('getCityName', JSON.stringify({ postalCode: vm.form.postalCode }))
          .then(data => {
            if (data.data.message) {
              vm.form.cities = data.data.message;
              vm.postalCodeError = undefined;
              vm.cityError = undefined;
              vm.showCityInput = false;
            }
            $scope.$applyAsync();
          })
          .then(null, error => {
            vm.postalCodeError = error.data.error;
            vm.form.city = undefined;
            vm.cityError = 'Veuillez saisir votre ville';
            $scope.$applyAsync();
          });
      }
    };

    vm.getSirenCoordinates = function() {
      if (vm.form.siren && vm.form.siren.length === 9) {
        WS.post(
          'getSirenCoordinates',
          JSON.stringify({
            siren:
              vm.form.siren[0] +
              vm.form.siren[1] +
              vm.form.siren[2] +
              ' ' +
              vm.form.siren[3] +
              vm.form.siren[4] +
              vm.form.siren[5] +
              ' ' +
              vm.form.siren[6] +
              vm.form.siren[7] +
              vm.form.siren[8],
          }),
        )
          .then(data => {
            if (data.data.message) {
              vm.form.siren = data.data.message.siren.replace(/ /g, '');
              vm.form.companyName = data.data.message.legalEntity;
              vm.form.address = data.data.message.address;
              vm.form.postalCode = data.data.message.postalCode;
              vm.showCityInput = true;
              vm.form.city = data.data.message.city;
              vm.form.phone = data.data.message.telephone;
              vm.form.email = data.data.message.email;
            }
            $scope.$applyAsync();
          })
          .then(null, error => {});
      }
    };

    // get (pmr + cr) Urban tax
    vm.pmrCrTax = function() {
      $scope.$watch('vm.chiffreAffaires', (newVal, oldVal) => {
        if (newVal !== oldVal) {
          if (localStorage.getItem('activite') === 'urbain') {
            WS.post(
              'pmrCrUrbanTax',
              Object.assign(
                {},
                {
                  chiffreAffaires: localStorage.getItem('chiffreAffaires'),
                  fraction: vm.fraction,
                  franchise: vm.franchise,
                  effectDate: vm.effectDate,
                  alarm: vm.form.approvedAlarm,
                  bdm: vm.form.bdm,
                },
              ),
            )
              .then(data => {
                vm.fractQuotation = parseFloat(data.data.fractQuotation);
                vm.cashQuotation = correctMonthlyCashQuotation(
                  vm.deadlineDate,
                  vm.effectDate,
                  vm.fraction,
                  vm.fractQuotation,
                ).cashQuotation;
                vm.totalTaxFraisContr = parseFloat(data.data.totalTaxFraisContr);
                vm.totalTax = parseFloat(data.data.totalTax);
                vm.contrAttentat = parseFloat(data.data.contrAttentat);
                vm.fraisAssistance = parseFloat(data.data.fraisAssistance);
                vm.fraisCourtier = parseFloat(data.data.fraisCourtier);
                vm.totalTaxFraisContrSem = parseFloat(data.data.totalTaxFraisContrSem);
                vm.totalTaxFraisContrTrim = parseFloat(data.data.totalTaxFraisContrTrim);
                vm.totalTaxFraisContrMens = parseFloat(data.data.totalTaxFraisContrMens);
                vm.totalTaxSem = parseFloat(data.data.totalTaxSem);
                vm.totalTaxTrim = parseFloat(data.data.totalTaxTrim);
                vm.totalTaxMens = parseFloat(data.data.totalTaxMens);
                vm.contrAttentatSem = parseFloat(data.data.contrAttentatSem);
                vm.contrAttentatTrim = parseFloat(data.data.contrAttentatTrim);
                vm.contrAttentatMens = parseFloat(data.data.contrAttentatMens);
                vm.fraisCourtierSem = parseFloat(data.data.fraisCourtierSem);
                vm.fraisCourtierTrim = parseFloat(data.data.fraisCourtierTrim);
                vm.fraisCourtierMens = parseFloat(data.data.fraisCourtierMens);
                vm.TotalHT = parseFloat(data.data.TotalHT);
                vm.TotalHTMens = parseFloat(data.data.TotalHTMens);
                vm.TotalHTSem = parseFloat(data.data.TotalHTSem);
                vm.TotalHTTrim = parseFloat(data.data.TotalHTTrim);
                vm.RedVolHT = parseFloat(data.data.RedVolHT);
                vm.RedATT = parseFloat(data.data.RedATT);
                vm.RedCATNAT = parseFloat(data.data.RedCATNAT);
                vm.RedTAX = parseFloat(data.data.RedTAX);
                vm.RedVOLTTC = parseFloat(data.data.RedVOLTTC);
                vm.RedVOLTTCSEM = parseFloat(data.data.RedVOLTTCSEM);
                vm.RedVOLTTCTRIM = parseFloat(data.data.RedVOLTTCTRIM);
                vm.RedVOLTTCMENS = parseFloat(data.data.RedVOLTTCMENS);
                $scope.$applyAsync();
              })
              .then(null, error => {
                vm.error = error.data.error;
                $scope.$applyAsync();
              });
          } else {
            WS.post(
              'pmrCrRuralMixedTax',
              Object.assign(
                {},
                {
                  chiffreAffaires: localStorage.getItem('chiffreAffaires'),
                  fraction: vm.fraction,
                  franchise: vm.franchise,
                  effectDate: vm.effectDate,
                  alarm: vm.form.approvedAlarm,
                  bdm: vm.form.bdm,
                },
              ),
            )
              .then(data => {
                vm.fractQuotation = parseFloat(data.data.fractQuotation);
                vm.cashQuotation = correctMonthlyCashQuotation(
                  vm.deadlineDate,
                  vm.effectDate,
                  vm.fraction,
                  vm.fractQuotation,
                ).cashQuotation;
                vm.totalTaxFraisContr = parseFloat(data.data.totalTaxFraisContr);
                vm.totalTax = parseFloat(data.data.totalTax);
                vm.contrAttentat = parseFloat(data.data.contrAttentat);
                vm.fraisAssistance = parseFloat(data.data.fraisAssistance);
                vm.fraisCourtier = parseFloat(data.data.fraisCourtier);
                vm.totalTaxFraisContrSem = parseFloat(data.data.totalTaxFraisContrSem);
                vm.totalTaxFraisContrTrim = parseFloat(data.data.totalTaxFraisContrTrim);
                vm.totalTaxFraisContrMens = parseFloat(data.data.totalTaxFraisContrMens);
                vm.totalTaxSem = parseFloat(data.data.totalTaxSem);
                vm.totalTaxTrim = parseFloat(data.data.totalTaxTrim);
                vm.totalTaxMens = parseFloat(data.data.totalTaxMens);
                vm.contrAttentatSem = parseFloat(data.data.contrAttentatSem);
                vm.contrAttentatTrim = parseFloat(data.data.contrAttentatTrim);
                vm.contrAttentatMens = parseFloat(data.data.contrAttentatMens);
                vm.fraisCourtierSem = parseFloat(data.data.fraisCourtierSem);
                vm.fraisCourtierTrim = parseFloat(data.data.fraisCourtierTrim);
                vm.fraisCourtierMens = parseFloat(data.data.fraisCourtierMens);
                vm.TotalHT = parseFloat(data.data.TotalHT);
                vm.TotalHTMens = parseFloat(data.data.TotalHTMens);
                vm.TotalHTSem = parseFloat(data.data.TotalHTSem);
                vm.TotalHTTrim = parseFloat(data.data.TotalHTTrim);
                vm.RedVolHT = parseFloat(data.data.RedVolHT);
                vm.RedATT = parseFloat(data.data.RedATT);
                vm.RedCATNAT = parseFloat(data.data.RedCATNAT);
                vm.RedTAX = parseFloat(data.data.RedTAX);
                vm.RedVOLTTC = parseFloat(data.data.RedVOLTTC);
                vm.RedVOLTTCSEM = parseFloat(data.data.RedVOLTTCSEM);
                vm.RedVOLTTCTRIM = parseFloat(data.data.RedVOLTTCTRIM);
                vm.RedVOLTTCMENS = parseFloat(data.data.RedVOLTTCMENS);
                $scope.$applyAsync();
              })
              .then(null, error => {
                vm.error = error.data.error;
                $scope.$applyAsync();
              });
          }
        }
      });
    };

    // get adhesion summary
    vm.submitAdhesion = function() {
      if (vm.fraction) {
        $('#errorPeriod').remove();
        if (vm.chosenMenu === 1) {
          if (vm.form.infoTerm1 && vm.form.infoTerm2) {
            if (document.forms['adhesionForm'].checkValidity()) {
              postUpdateAdhesion(
                WS,
                'MRP + RC',
                vm.fraction,
                vm.effectDate,
                vm.fractQuotation,
                vm.deadlineDate,
                vm.afterDeadlineDate,
                vm.cashQuotation,
                vm.franchise,
                vm.chiffreAffaires,
                vm.error,
                $scope,
              );
              if (vm.chosenMenu === 1) {
                $window.location.href = 'terms?offerType=PMRCR';
              } else if (vm.chosenMenu === 2) {
                $window.location.href = 'quote?offerType=PMRCR';
              } else if (vm.chosenMenu === 3) {
                $window.location.href = 'save?offerType=PMRCR';
              } else {
                $window.location.href = 'rappeler';
              }
              localStorage.setItem('currentUrlPath', $window.location.pathname);
            }
          } else {
            vm.error = 'Veuillez Accepter les termes';
          }
        } else {
          if (document.forms['adhesionForm'].checkValidity()) {
            postUpdateAdhesion(
              WS,
              'MRP + RC',
              vm.fraction,
              vm.effectDate,
              vm.fractQuotation,
              vm.deadlineDate,
              vm.afterDeadlineDate,
              vm.cashQuotation,
              vm.franchise,
              vm.chiffreAffaires,
              vm.error,
              $scope,
            );
            if (vm.chosenMenu === 1) {
              $window.location.href = 'terms?offerType=PMRCR';
            } else if (vm.chosenMenu === 2) {
              $window.location.href = 'quote?offerType=PMRCR';
            } else if (vm.chosenMenu === 3) {
              $window.location.href = 'save?offerType=PMRCR';
            } else {
              $window.location.href = 'rappeler';
            }
            localStorage.setItem('currentUrlPath', $window.location.pathname);
          }
        }
      } else {
        $('html, body').animate(
          {
            scrollTop: $('div.periodicite').offset().top - 100,
          },
          500,
        );
        vm.errorPeriod = 'Veuillez choisir la périodicité';
      }
    };

    // update adhesion summary quotation
    vm.chosenFraction = function(fraction, quotation, effectDate, franchise, alarm, bdm) {
      if (franchise) {
        vm.franchise = franchise;
      }
      if (alarm) {
        vm.form.approvedAlarm = alarm;
      }
      bdm = vm.form.bdm;
      (async () => {
        try {
          const taxValues = await calculatePmrCrTax(
            WS,
            localStorage.getItem('chiffreAffaires'),
            localStorage.getItem('activite'),
            fraction,
            franchise,
            effectDate,
            alarm,
            bdm,
          );
          vm.totalTax = parseFloat(taxValues.totalTax);
          vm.totalTaxSem = parseFloat(taxValues.totalTaxSem);
          vm.totalTaxTrim = parseFloat(taxValues.totalTaxTrim);
          vm.totalTaxMens = parseFloat(taxValues.totalTaxMens);
          vm.contrAttentat = parseFloat(taxValues.contrAttentat);
          vm.contrAttentatSem = parseFloat(taxValues.contrAttentatSem);
          vm.contrAttentatTrim = parseFloat(taxValues.contrAttentatTrim);
          vm.contrAttentatMens = parseFloat(taxValues.contrAttentatMens);
          vm.fraisAssistance = parseFloat(taxValues.fraisAssistance);
          vm.fraisCourtier = parseFloat(taxValues.fraisCourtier);
          vm.fraisCourtierSem = parseFloat(taxValues.fraisCourtierSem);
          vm.fraisCourtierTrim = parseFloat(taxValues.fraisCourtierTrim);
          vm.fraisCourtierMens = parseFloat(taxValues.fraisCourtierMens);
          vm.totalTaxFraisContr = parseFloat(taxValues.totalTaxFraisContr);
          vm.totalTaxFraisContrSem = parseFloat(taxValues.totalTaxFraisContrSem);
          vm.totalTaxFraisContrTrim = parseFloat(taxValues.totalTaxFraisContrTrim);
          vm.totalTaxFraisContrMens = parseFloat(taxValues.totalTaxFraisContrMens);
          vm.fractQuotation = parseFloat(quotation);
          vm.TotalHT = parseFloat(taxValues.TotalHT);
          vm.TotalHTMens = parseFloat(taxValues.TotalHTMens);
          vm.TotalHTSem = parseFloat(taxValues.TotalHTSem);
          vm.TotalHTTrim = parseFloat(taxValues.TotalHTTrim);
          vm.fraction = parseInt(vm.fraction);
          vm.RedVolHT = parseFloat(taxValues.RedVolHT);
          vm.RedATT = parseFloat(taxValues.RedATT);
          vm.RedCATNAT = parseFloat(taxValues.RedCATNAT);
          vm.RedTAX = parseFloat(taxValues.RedTAX);
          vm.RedVOLTTC = parseFloat(taxValues.RedVOLTTC);
          vm.RedVOLTTCSEM = parseFloat(taxValues.RedVOLTTCSEM);
          vm.RedVOLTTCTRIM = parseFloat(taxValues.RedVOLTTCTRIM);
          vm.RedVOLTTCMENS = parseFloat(taxValues.RedVOLTTCMENS);
          vm.chosenQuotation = parseFloat(quotation);
          vm.deadlineDate = calculateDeadline(fraction, effectDate).deadlineDate;
          vm.afterDeadlineDate = calculateDeadline(fraction, effectDate).afterDeadlineDate;
          vm.cashQuotation = correctMonthlyCashQuotation(
            vm.deadlineDate,
            effectDate,
            vm.fraction,
            vm.fractQuotation,
          ).cashQuotation;
          $scope.$applyAsync();
        } catch (error) {
          console.log(error);
        }
      })();
    };

    //save paymentMethod
    vm.savePaymentMethod = function() {
      const formdata = new FormData(document.getElementById('paymentForm'));
      if (
        document.getElementById('file').files.length > 0 &&
        document.getElementById('file').files[0].name &&
        vm.ribImg
      ) {
        formdata.append('updateRibFile', true);
      }
      (async () => {
        try {
          const { data } = await $http.post('/paymentInfos', formdata, {
            headers: { 'Content-Type': undefined },
            transformResponse: angular.identity,
          });
          if (JSON.parse(data).redirect) {
            $window.location.href = JSON.parse(data).redirect;
          }
        } catch (error) {
          vm.error = JSON.parse(error.data).error;
          $scope.$applyAsync();
        }
      })();
    };

    //download ribFile
    vm.downloadRibFile = function(ribFileName) {
      $window.open(`/downloadRib?filename=${ribFileName}`);
    };

    // go to transaction success page
    vm.goToTXSuccessPage = function() {
      if (vm.form.infoTerm1 && vm.form.infoTerm2) {
        $window.location.href = 'payment?offerType=PMRCR';
      } else {
        vm.error = 'Veuillez Accepter les termes';
      }
    };

    // go to universign
    vm.signContract = function() {
      WS.get('getUniversignTransaction', `type=${encodeURIComponent('MRP + RC')}`)
        .then(data => {
          if (data.data.url) {
            $window.location.href = data.data.url;
          }
        })
        .then(null, error => {
          vm.error = error.data.error;
          $scope.$applyAsync();
        });
    };

    // --------------------------------------------------------------

    // rate path
    // get rate email
    vm.getRateEmail = function() {
      if (
        localStorage.getItem('currentUrlPath') &&
        localStorage.getItem('currentUrlPath') !== $window.location.pathname
      ) {
        WS.get('rateEmail', `type=${encodeURIComponent('MRP + RC')}`)
          .then(data => {
            if (data.data.rateFileName) {
              vm.rateFileNameGenerated = data.data.rateFileName;
              localStorage.setItem('rateFileNameGenerated', vm.rateFileNameGenerated);
              $scope.$applyAsync();
            }
          })
          .then(null, error => {
            vm.error = error.data.error;
            $scope.$applyAsync();
          });
      } else {
        vm.rateFileNameGenerated = localStorage.getItem('rateFileNameGenerated');
      }
      localStorage.setItem('currentUrlPath', $window.location.pathname);
    };

    // ----------------------------------------------------------
    // continue subscription
    vm.continueSubscription = function(value) {
      if (value) {
        postToggleContinueLater(WS, false, $window, 'menu?offerType=PMRCR', 'MRP + RC');
        postToggleContinueLaterPopup(WS, false, 'MRP + RC');
      } else {
        postToggleContinueLater(WS, false, $window, 'MRP + RC');
        postToggleContinueLaterPopup(WS, false, 'MRP + RC');
      }
    };

    // save request path
    // get continue later email
    vm.getContinueLaterEmail = function() {
      WS.get('continueEmail', `offerType=${encodeURIComponent('MRP + RC')}`)
        .then(() => {})
        .then(null, error => {
          vm.error = error.data.error;
          $scope.$applyAsync();
        });
    };

    vm.generateSinistersArrayCR = function(value) {
      let generatedArray = null;
      generatedArray = [];
      if (value) {
        if (parseInt(value) <= 0) {
          vm.sinsitersArrayCR = generatedArray;
          return;
        }
        for (let i = 0; i < parseInt(value); i++) {
          generatedArray.push(i);
        }
        vm.sinsitersNumberIntegerCR = parseInt(value);
        vm.sinsitersArrayCR = generatedArray;
        $scope.$applyAsync();
      } else {
        vm.sinsitersArrayCR = generatedArray;
        $scope.$applyAsync();
      }
    };

    vm.generateSinistersArrayPMR = function(value) {
      let generatedArray = null;
      generatedArray = [];
      if (value) {
        if (parseInt(value) <= 0) {
          vm.sinsitersArrayPMR = generatedArray;
          return;
        }
        for (let i = 0; i < parseInt(value); i++) {
          generatedArray.push(i);
        }
        vm.sinsitersNumberIntegerPMR = parseInt(value);
        vm.sinsitersArrayPMR = generatedArray;
        $scope.$applyAsync();
      } else {
        vm.sinsitersArrayPMR = generatedArray;
        $scope.$applyAsync();
      }
    };

    // downloadRate
    vm.downloadRate = function(filename) {
      $window.open(`/downloadRate?filename=${filename}`);
    };

    vm.fractPmrCrQuotation = function() {
      $scope.$watch('vm.chiffreAffaires', (newVal, oldVal) => {
        if (newVal !== oldVal) {
          (async () => {
            try {
              const taxValues500 = await calculatePmrCrTax(
                WS,
                localStorage.getItem('chiffreAffairesTmp')
                  ? localStorage.getItem('chiffreAffairesTmp')
                  : localStorage.getItem('chiffreAffaires'),
                localStorage.getItem('activiteTmp')
                  ? localStorage.getItem('activiteTmp')
                  : localStorage.getItem('activite'),
                12,
                '500',
                vm.effectDate,
                vm.form.approvedAlarm,
                vm.form.bdm,
              );
              const taxValues300 = await calculatePmrCrTax(
                WS,
                localStorage.getItem('chiffreAffairesTmp')
                  ? localStorage.getItem('chiffreAffairesTmp')
                  : localStorage.getItem('chiffreAffaires'),
                localStorage.getItem('activiteTmp')
                  ? localStorage.getItem('activiteTmp')
                  : localStorage.getItem('activite'),
                12,
                '300',
                vm.effectDate,
                vm.form.approvedAlarm,
                vm.form.bdm,
              );
              vm.fractQuotationMonthly500 = parseFloat(taxValues500.fractQuotation);
              vm.fractQuotationAnnual500 = parseFloat(taxValues500.fractQuotation * 12);
              vm.fractQuotationMonthly300 = parseFloat(taxValues300.fractQuotation);
              vm.fractQuotationAnnual300 = parseFloat(taxValues300.fractQuotation * 12);
              if (vm.franchise === '500') {
                if (vm.fraction === 1) {
                  vm.fractQuotation = parseFloat(vm.fractQuotationMonthly500 * 12);
                } else if (vm.fraction === 2) {
                  vm.fractQuotation = parseFloat(vm.fractQuotationMonthly500 * 6);
                } else if (vm.fraction === 4) {
                  vm.fractQuotation = parseFloat(vm.fractQuotationMonthly500 * 3);
                } else {
                  vm.fractQuotation = vm.fractQuotationMonthly500;
                }
              } else {
                if (vm.fraction === 1) {
                  vm.fractQuotation = parseFloat(vm.fractQuotationMonthly300 * 12);
                } else if (vm.fraction === 2) {
                  vm.fractQuotation = parseFloat(vm.fractQuotationMonthly300 * 6);
                } else if (vm.fraction === 4) {
                  vm.fractQuotation = parseFloat(vm.fractQuotationMonthly300 * 3);
                } else {
                  vm.fractQuotation = vm.fractQuotationMonthly300;
                }
              }
              vm.chiffreAffaires = localStorage.getItem('chiffreAffairesTmp')
                ? localStorage.getItem('chiffreAffairesTmp')
                : localStorage.getItem('chiffreAffaires');
              vm.activite = localStorage.getItem('activiteTmp')
                ? localStorage.getItem('activiteTmp')
                : localStorage.getItem('activite');
              $scope.$applyAsync();
            } catch (error) {
              console.log(error);
            }
          })();
        }
      });
    };

    vm.setCancelReasonsError = function() {
      if (!vm.form.cancelReasonsSinister && !vm.form.cancelReasonsPayment) {
        vm.cancelReasonsError = 'Veuillez choisir un motif';
      } else {
        vm.cancelReasonsError = undefined;
      }
      $scope.$applyAsync();
    };

    vm.continueSubscriptionFromRate = function() {
      localStorage.setItem('chosenMenu', 1);
      $window.location.href = 'adhesion-summary?offerType=PMRCR';
    };
  },
]);
