import { app } from '../app';

// Simulation Controller
app.controller('OfferTypeChooser', [
  '$window',
  'WS',
  function($window) {
    const vm = this;

    vm.setOfferType = function(value) {
      localStorage.setItem('vetoptim-offer-type', value);
      $window.location.href = `simulation?offerType=${value}`;
    };

    vm.setCommercialOfferType = function(value) {
      localStorage.setItem('commercial-offer-type', value);
      $window.location.href = `ajouter-contrat`;
    };
  },
]);
