import { app } from '../app';

// Administration Controller
app.controller('AdminController', ['$scope', 'WS', '$window', function($scope, WS, $window) {
  const vm = this;
  vm.user = {};
  vm.title = 'monsieur';

  vm.authenticated = function(){

    WS.get('csrftoken')
      .then((data) => {
        localStorage.setItem('vetoptim-csrf-token', data.data._csrf);
      });

    WS.get('authenticated')
      .then((data) => {
        vm.user = data.data.user;
        $scope.$applyAsync();
      })
      .then(null, (error) => {
        vm.error = error.data.error;
        $scope.$applyAsync();
      });
  };

  vm.authenticated();

  vm.loginAdmin = function() {
    WS.post('loginAdmin',vm.user)
        .then(async (res) => {
          if(res.data.success) {
            $window.location.href = `${window.location.protocol}//${window.location.host}/admin/index`;
          }
        })
        .then(null, (error) => {
          vm.loginError = error.data.message;
          $scope.$applyAsync();
        });
  };

  vm.getResetPassword = function() {
    WS.get('resetPassword', 'email='+vm.user.resetemail+'&admin=true')
        .then((data) => {
          if (data.data.message) {
            vm.resetSuccess = data.data.message;
            $scope.$apply();
          }
        }).then(null, (error) => {
          vm.resetError = error.data.error;
          $scope.$applyAsync();
        });
  };

  // save user
  vm.saveUser = function() {
    WS.post('saveUser', JSON.stringify({
      login: vm.email,
      password: vm.password,
      email: vm.email,
      mobile: vm.mobile,
      title: vm.title,
      name: vm.name,
      lastname: vm.lastname,
    })).then((data) => {
      if (data.data.message) {
        vm.success = data.data.message;
      }
      $scope.$applyAsync();
    }).then(null, (error) => {
      vm.error = error.data.error;
      $scope.$applyAsync();
    });
  };

  // update user
  vm.updateUser = function() {
    WS.post('updateUser', JSON.stringify({
      mobile: vm.mobile,
      title: vm.title,
      name: vm.name,
      lastname: vm.lastname,
      user: $window.location.search.split('=')[1]
    })).then((data) => {
      if (data.data.message) {
        vm.success = data.data.message;
      }
      $scope.$applyAsync();
    }).then(null, (error) => {
      vm.error = error.data.error;
      $scope.$applyAsync();
    });
  };

  // listing users
  vm.listUsers = function() {
    WS.get('listUsers')
      .then((data) => {
        vm.users = data.data.users;
        $scope.$applyAsync();
      })
      .then(null, (error) => {
        vm.error = error.data.error;
        $scope.$applyAsync();
      });
  };

  // listing users
  vm.getUserById = function() {
    if($window.location.search.split('=')[1] !== undefined) {
      WS.get('listUsers')
      .then((data) => {
        vm.users = data.data.users;
        const currentUser = vm.users.find((e) => e.id === parseInt($window.location.search.split('=')[1]));
        vm.email = currentUser.email;
        vm.mobile = currentUser.mobile;
        vm.title = currentUser.title;
        vm.name = currentUser.name;
        vm.lastname = currentUser.lastname;
        $scope.$applyAsync();
      })
      .then(null, (error) => {
        vm.error = error.data.error;
        $scope.$applyAsync();
      });
    }
  };


}]);
