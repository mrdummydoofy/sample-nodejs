import { app } from '../app';
import {
  calculateDeadline,
  postUpdateAdhesion,
  getChosenPeriod,
  getChosenPeriodSliced,
  postToggleContinueLater,
  postToggleContinueLaterPopup,
  calculateCrTax,
  padNumbers,
  correctMonthlyCashQuotation,
  getOfferTypeFr,
} from '../utils';
import moment from 'moment';
import $ from 'jquery';

moment.suppressDeprecationWarnings = true;

// Simulation Controller
app.controller('SimulationCRController', [
  '$scope',
  'WS',
  '$window',
  '$http',
  'currencyFormatInputFilter',
  function($scope, WS, $window, $http, currencyFormatInputFilter) {
    const vm = this;
    vm.user = {};
    vm.form = {};
    vm.newsletter = {};
    vm.partnership = {};
    vm.commercial = {};
    vm.customQuote = {};
    vm.form.sinisterAmountCR = {};
    vm.form.sinisterDateDayCR = {};
    vm.form.sinisterDateMonthCR = {};
    vm.form.sinisterDateYearCR = {};

    vm.form.sinistreConnaissance5Years = vm.form.sinistreConnaissance5Years
      ? vm.form.sinistreConnaissance5Years
      : 'non';
    vm.form.quotationsUpdated = vm.form.quotationsUpdated ? vm.form.quotationsUpdated : 'oui';
    vm.form.animalsValue = vm.form.animalsValue ? vm.form.animalsValue : 'non';
    vm.form.insuranceCancel = vm.form.insuranceCancel ? vm.form.insuranceCancel : 'non';
    vm.form.centralRef = vm.form.centralRef ? vm.form.centralRef : false;
    vm.form.individualCabinet = vm.form.individualCabinet ? vm.form.individualCabinet : false;
    vm.form.structure = vm.form.structure ? vm.form.structure : false;
    vm.form.liberalCollaborator = vm.form.liberalCollaborator ? vm.form.liberalCollaborator : false;
    vm.form.currentContract = vm.form.currentContract ? vm.form.currentContract : 'non';
    vm.form.currentContractCancel = vm.form.currentContractCancel
      ? vm.form.currentContractCancel
      : 'non';
    vm.form.endStatement = vm.form.endStatement ? vm.form.endStatement : false;
    vm.form.type = vm.form.type ? vm.form.type : 'CR';
    vm.form.civility = vm.form.civility ? vm.form.civility : 'monsieur';
    vm.form.chosenMenu = localStorage.getItem('chosenMenu');
    vm.franchise = vm.franchise ? vm.franchise : '500';
    vm.fraction = vm.fraction ? vm.fraction : 12;
    vm.effectDate = vm.effectDate
      ? vm.effectDate
      : moment()
          .utc()
          .utcOffset(2)
          .year(
            moment()
              .utc()
              .utcOffset(2)
              .year(),
          )
          .month(
            moment()
              .utc()
              .utcOffset(2)
              .month() + 1,
          )
          .date(1)
          .format('DD-MM-YYYY');
    vm.effectDateTmp = vm.effectDateTmp
      ? vm.effectDateTmp
      : moment()
          .add(1, 'days')
          .format('DD-MM-YYYY');
    vm.effectDateDay = vm.effectDateDay ? vm.effectDateDay : '01';
    vm.effectDateMonth = vm.effectDateMonth
      ? vm.effectDateMonth
      : (
          moment()
            .utc()
            .utcOffset(2)
            .year(
              moment()
                .utc()
                .utcOffset(2)
                .year(),
            )
            .month(
              moment()
                .utc()
                .utcOffset(2)
                .month() + 1,
            )
            .date(1)
            .month() + 1
        ).toString().length === 1
        ? `0${moment()
            .utc()
            .utcOffset(2)
            .year(
              moment()
                .utc()
                .utcOffset(2)
                .year(),
            )
            .month(
              moment()
                .utc()
                .utcOffset(2)
                .month() + 1,
            )
            .date(1)
            .month() + 1}`
        : (
            moment()
              .utc()
              .utcOffset(2)
              .year(
                moment()
                  .utc()
                  .utcOffset(2)
                  .year(),
              )
              .month(
                moment()
                  .utc()
                  .utcOffset(2)
                  .month() + 1,
              )
              .date(1)
              .month() + 1
          ).toString();
    vm.effectDateYear = vm.effectDateYear
      ? vm.effectDateYear
      : moment()
          .utc()
          .utcOffset(2)
          .year(
            moment()
              .utc()
              .utcOffset(2)
              .year(),
          )
          .month(
            moment()
              .utc()
              .utcOffset(2)
              .month() + 1,
          )
          .date(1)
          .year();
    vm.effectDates = [
      moment()
        .utc()
        .utcOffset(2)
        .date(1)
        .add(1, 'M')
        .format('DD-MM-YYYY'),
      moment()
        .utc()
        .utcOffset(2)
        .date(1)
        .add(2, 'M')
        .format('DD-MM-YYYY'),
      moment()
        .utc()
        .utcOffset(2)
        .date(1)
        .add(3, 'M')
        .format('DD-MM-YYYY'),
    ];
    vm.effectDates = vm.effectDates.map(e => {
      return { month: e.split('-')[1], year: e.split('-')[2] };
    });
    vm.deadlineDate = vm.deadlineDate
      ? vm.deadlineDate
      : calculateDeadline(vm.fraction, vm.effectDate).deadlineDate;
    vm.afterDeadlineDate = vm.afterDeadlineDate
      ? vm.afterDeadlineDate
      : calculateDeadline(vm.fraction, vm.effectDate).afterDeadlineDate;
    vm.showCityInput = vm.showCityInput ? vm.showCityInput : false;
    vm.timing = vm.timing ? vm.timing : 'immediately';
    vm.title = vm.title ? vm.title : 'monsieur';
    vm.commercial.civility = vm.commercial.civility ? vm.commercial.civility : 'monsieur';
    vm.commercial.centralRef = vm.commercial.centralRef ? vm.commercial.centralRef : false;
    vm.commercial.individualCabinet = vm.commercial.individualCabinet
      ? vm.commercial.individualCabinet
      : false;
    vm.commercial.structure = vm.commercial.structure ? vm.commercial.structure : false;
    vm.commercial.liberalCollaborator = vm.commercial.liberalCollaborator
      ? vm.commercial.liberalCollaborator
      : false;
    vm.commercial.currentContract = vm.commercial.currentContract
      ? vm.commercial.currentContract
      : false;
    vm.commercial.currentContractCancel = vm.commercial.currentContractCancel
      ? vm.commercial.currentContractCancel
      : false;
    vm.commercial.endStatement = vm.commercial.endStatement ? vm.commercial.endStatement : false;
    vm.commercial.franchise = vm.commercial.franchise ? vm.commercial.franchise : '500';
    vm.commercial.activite = vm.commercial.activite ? vm.commercial.activite : 'urbain';
    vm.commercial.effectDateDay = vm.commercial.effectDateDay ? vm.commercial.effectDateDay : '01';
    vm.commercial.brokerFee = vm.commercial.brokerFee ? vm.commercial.brokerFee : '0.30';

    vm.commercial.haveConjoint = vm.commercial.haveConjoint ? vm.commercial.haveConjoint : 'non';
    vm.commercial.haveChildren = vm.commercial.haveChildren ? vm.commercial.haveChildren : 'non';
    vm.commercial.indivAccident = vm.commercial.indivAccident ? vm.commercial.indivAccident : 'non';
    vm.commercial.indivEnfants = vm.commercial.indivEnfants ? vm.commercial.indivEnfants : 'non';
    vm.commercial.rapatriement = vm.commercial.rapatriement ? vm.commercial.rapatriement : 'non';
    vm.commercial.indivAccidentChoix = vm.commercial.indivAccidentChoix
      ? vm.commercial.indivAccidentChoix
      : 'individuel';
    vm.commercial.rapatriementChoix = vm.commercial.rapatriementChoix
      ? vm.commercial.rapatriementChoix
      : '1adulte';
    vm.commercial.formula = vm.commercial.formula ? vm.commercial.formula : '1';
    vm.commercial.alarm = vm.commercial.alarm ? vm.commercial.alarm : 'non';
    vm.commercial.approvedAlarm = vm.commercial.approvedAlarm ? vm.commercial.approvedAlarm : 'non';
    vm.commercial.bdmSup = vm.commercial.bdmSup ? vm.commercial.bdmSup : 'non';
    vm.commercial.bdm = vm.commercial.bdm ? vm.commercial.bdm : 'limite20000';
    vm.commercial.capitauxPropres = vm.commercial.capitauxPropres
      ? vm.commercial.capitauxPropres
      : 'non';
    vm.commercial.resultatNet = vm.commercial.resultatNet ? vm.commercial.resultatNet : 'non';
    if (localStorage.getItem('commercial-offer-type')) {
      vm.commercial.offer = getOfferTypeFr(localStorage.getItem('commercial-offer-type'));
      localStorage.removeItem('commercial-offer-type');
    }

    vm.customer = vm.customer ? vm.customer : 'oui';
    vm.customQuote.offer = vm.customQuote.offer
      ? vm.customQuote.offer
      : localStorage.getItem('chosenOfferIndex');
    // pagination
    vm.currentPage = 1;
    vm.itemsPerPage = 5;
    vm.loading = true;
    vm.commercial.loading = false;

    $('#demande-sauvegarder').on('hidden.bs.modal', () => {
      postToggleContinueLaterPopup(WS, false, getOfferTypeFr(localStorage.getItem('type')));
    });

    vm.authenticated = function() {
      WS.get('csrftoken').then(data => {
        localStorage.setItem('vetoptim-csrf-token', data.data._csrf);
      });

      WS.get('authenticated')
        .then(async data => {
          // registration form
          if (data.data.user) {
            //save cr infos when connected
            if (localStorage.getItem('chiffreAffairesTmp') && localStorage.getItem('activiteTmp')) {
              if (localStorage.getItem('tmpError')) {
                vm.error = localStorage.getItem('tmpError');
                localStorage.removeItem('chiffreAffairesTmp');
                localStorage.removeItem('activiteTmp');
                localStorage.removeItem('tmpError');
              } else {
                if (parseInt(localStorage.getItem('chiffreAffairesTmp')) <= 1500000) {
                  WS.post(
                    'cr',
                    Object.assign(
                      {},
                      {
                        chiffreAffaires: localStorage.getItem('chiffreAffairesTmp'),
                        activite: localStorage.getItem('activiteTmp')
                          ? localStorage.getItem('activiteTmp')
                          : 'rurale',
                      },
                    ),
                  )
                    .then(data => {
                      if (data.data.message) {
                        localStorage.removeItem('chiffreAffairesTmp');
                        localStorage.removeItem('activiteTmp');
                        vm.authenticated();
                      }
                    })
                    .then(null, error => {
                      vm.error = error.data.error;
                      $scope.$applyAsync();
                    });
                }
              }
            }

            vm.user = data.data;
            vm.email = data.data.user.email;
            vm.isCommercial = data.data.user.isCommercial;

            // menu and info form

            vm.continuesLater = [];
            vm.rates = [];

            if (data.data.user.leader.length > 0) {
              vm.turnover = data.data.user.leader[0].turnover;
            }

            if (data.data.user.adhesion.length > 0) {
              //my rate page
              vm.rates = data.data.user.adhesion.filter(rate => rate.rateFileName !== '');
              vm.continuesLater = data.data.user.adhesion.filter(
                save => save.continueLater === true,
              );

              if (vm.continuesLater.length > 0) {
                let lastContinueLaterDates = vm.continuesLater.map(last => last.updatedAt);
                vm.lastContinueLaterDate = lastContinueLaterDates.reduce((last, value) =>
                  moment.max(moment(value), moment(last)),
                );
              }

              if (data.data.user.adhesion.find(e => e.offerType === 'RC')) {
                // generate sinsitersArrays
                vm.form.sinisterNumbersCR = data.data.user.adhesion.find(
                  e => e.offerType === 'RC',
                ).CRSinisterStatement.length;
                vm.sinsitersNumberIntegerCR = data.data.user.adhesion.find(
                  e => e.offerType === 'RC',
                ).CRSinisterStatement.length;
                vm.sinsitersArrayCR = data.data.user.adhesion.find(
                  e => e.offerType === 'RC',
                ).CRSinisterStatement;

                if (vm.sinsitersArrayCR.length > 0) {
                  for (let i = 0; i < vm.sinsitersArrayCR.length; i++) {
                    vm.form.sinisterAmountCR[i] = vm.sinsitersArrayCR[i].amount;
                    vm.form.sinisterDateDayCR[i] = padNumbers(
                      moment(
                        new Date(
                          `${vm.sinsitersArrayCR[i].date.split('-')[2]}-${
                            vm.sinsitersArrayCR[i].date.split('-')[1]
                          }-${vm.sinsitersArrayCR[i].date.split('-')[0]}`,
                        ),
                      ).date(),
                    );
                    vm.form.sinisterDateMonthCR[i] = padNumbers(
                      moment(
                        new Date(
                          `${vm.sinsitersArrayCR[i].date.split('-')[2]}-${
                            vm.sinsitersArrayCR[i].date.split('-')[1]
                          }-${vm.sinsitersArrayCR[i].date.split('-')[0]}`,
                        ),
                      ).month() + 1,
                    );
                    vm.form.sinisterDateYearCR[i] = moment(
                      new Date(
                        `${vm.sinsitersArrayCR[i].date.split('-')[2]}-${
                          vm.sinsitersArrayCR[i].date.split('-')[1]
                        }-${vm.sinsitersArrayCR[i].date.split('-')[0]}`,
                      ),
                    ).year();
                  }
                }

                vm.chosenQuotation = data.data.user.adhesion.find(
                  e => e.offerType === 'RC',
                ).chosenQuotation;
                vm.cashQuotation = data.data.user.adhesion.find(
                  e => e.offerType === 'RC',
                ).cashQuotation;
                vm.firstPaymentType = data.data.user.adhesion.find(
                  e => e.offerType === 'RC',
                ).firstPaymentType;
                vm.bcUrl = `https://sherlock.staging.spicy.digital/call_request.php?chosenQuotation=${Math.round(
                  vm.chosenQuotation,
                ) * 100}&user=${data.data.user.id}&offer=CR`;
                vm.chosenPeriod = getChosenPeriod(
                  data.data.user.adhesion.find(e => e.offerType === 'RC').fraction,
                );
                vm.chosenPeriodSliced = getChosenPeriodSliced(
                  data.data.user.adhesion.find(e => e.offerType === 'RC').fraction,
                );
                vm.fraction =
                  data.data.user.adhesion.find(e => e.offerType === 'RC').fraction === 0
                    ? 12
                    : data.data.user.adhesion.find(e => e.offerType === 'RC').fraction;
                vm.deadlineDate =
                  data.data.user.adhesion.find(e => e.offerType === 'RC').deadlineDate === ''
                    ? calculateDeadline(12, vm.effectDate).deadlineDate
                    : data.data.user.adhesion.find(e => e.offerType === 'RC').deadlineDate;
                vm.afterDeadlineDate =
                  data.data.user.adhesion.find(e => e.offerType === 'RC').deadlineDate === ''
                    ? calculateDeadline(12, vm.effectDate).afterDeadlineDate
                    : data.data.user.adhesion.find(e => e.offerType === 'RC').afterDeadlineDate;
                vm.effectDate =
                  data.data.user.adhesion.find(e => e.offerType === 'RC').effectDate === ''
                    ? vm.effectDate
                    : data.data.user.adhesion.find(e => e.offerType === 'RC').effectDate;
                vm.effectDateDay = padNumbers(
                  moment(
                    new Date(
                      `${vm.effectDate.split('-')[2]}-${vm.effectDate.split('-')[1]}-${
                        vm.effectDate.split('-')[0]
                      }`,
                    ),
                  ).date(),
                );
                vm.effectDateMonth = padNumbers(
                  moment(
                    new Date(
                      `${vm.effectDate.split('-')[2]}-${vm.effectDate.split('-')[1]}-${
                        vm.effectDate.split('-')[0]
                      }`,
                    ),
                  ).month() + 1,
                );
                vm.effectDateYear = padNumbers(
                  moment(
                    new Date(
                      `${vm.effectDate.split('-')[2]}-${vm.effectDate.split('-')[1]}-${
                        vm.effectDate.split('-')[0]
                      }`,
                    ),
                  ).year(),
                );

                const isAfterEffectDate = moment(
                  new Date(`${vm.effectDateYear}-${vm.effectDateMonth}-${vm.effectDateDay}`),
                ).isAfter(moment());
                if (!isAfterEffectDate) {
                  vm.validateEffectDateRes = false;
                } else {
                  vm.validateEffectDateRes = true;
                }
                const isValidEffectDate = moment(
                  `${vm.effectDateYear}-${vm.effectDateMonth}-${vm.effectDateDay}`,
                ).isValid();
                if (!isValidEffectDate) {
                  vm.validateEffectDateRes = false;
                } else {
                  vm.validateEffectDateRes = true;
                }

                if (
                  data.data.user.adhesion.find(e => e.offerType === 'RC').continueLater &&
                  data.data.user.adhesion.find(e => e.offerType === 'RC').continueLaterShowPopup
                ) {
                  setTimeout(() => {
                    $('#demande-sauvegarder').modal();
                  }, 3000);
                }
              }
            }

            if (data.data.user.customer.length > 0) {
              localStorage.setItem(
                'chiffreAffaires',
                parseInt(data.data.user.customer[0].turnover),
              );
              localStorage.setItem('activite', data.data.user.customer[0].activity);
              vm.chiffreAffaires =
                parseInt(localStorage.getItem('chiffreAffaires')) === 0
                  ? undefined
                  : parseInt(localStorage.getItem('chiffreAffaires'));
              vm.activite = localStorage.getItem('activite')
                ? localStorage.getItem('activite')
                : 'urbain';
              vm.form.siren = data.data.user.customer[0].siren;
              vm.form.legalForm = data.data.user.customer[0].legalForm;
              vm.customerRef = data.data.user.customer[0].reference;

              if (data.data.user.customer[0].startActivityDate) {
                vm.form.startActivityDateDay = moment(
                  new Date(data.data.user.customer[0].startActivityDate),
                ).date();
                vm.form.startActivityDateMonth =
                  moment(new Date(data.data.user.customer[0].startActivityDate)).month() + 1;
                vm.form.startActivityDateYear = moment(
                  new Date(data.data.user.customer[0].startActivityDate),
                ).year();
              }

              if (data.data.user.customer[0].endStatementDate) {
                vm.form.endStatementDateDay = data.data.user.customer[0].endStatementDate.split(
                  '-',
                )[1];
                vm.form.endStatementDateMonth = data.data.user.customer[0].endStatementDate.split(
                  '-',
                )[0];
              }

              if (data.data.user.customer[0].currentContractDate) {
                vm.effectDate = moment(data.data.user.customer[0].currentContractDate)
                  .add(1, 'days')
                  .format('DD-MM-YYYY');
                vm.deadlineDate = calculateDeadline(vm.fraction, vm.effectDate).deadlineDate;
                vm.currentContractDate = moment(
                  data.data.user.customer[0].currentContractDate,
                ).format('DD-MM-YYYY');
                vm.afterDeadlineDate = calculateDeadline(
                  vm.fraction,
                  vm.effectDate,
                ).afterDeadlineDate;
                vm.form.currentContractDateDay = moment(
                  new Date(data.data.user.customer[0].currentContractDate),
                ).date();
                vm.form.currentContractDateMonth =
                  moment(new Date(data.data.user.customer[0].currentContractDate)).month() + 1;
                vm.form.currentContractDateYear = moment(
                  new Date(data.data.user.customer[0].currentContractDate),
                ).year();
                vm.form.currentContractDateMinus2Months =
                  moment(new Date(data.data.user.customer[0].currentContractDate)).diff(
                    moment().add(2, 'months'),
                    'months',
                    true,
                  ) <= 0;
                if (!vm.form.currentContractDateMinus2Months) {
                  vm.currentContractInfo = `Pensez à résilier votre contrat avant le ${moment(
                    new Date(
                      `${vm.form.currentContractDateYear}-${vm.form.currentContractDateMonth}-${
                        vm.form.currentContractDateDay
                      }`,
                    ),
                  )
                    .subtract(2, 'months')
                    .format('DD-MM-YYYY')}`;
                }
              }

              vm.form.currentContract = data.data.user.customer[0].currentContract ? 'oui' : 'non';
              vm.form.currentContractCancel = data.data.user.customer[0].currentContractCancel
                ? 'oui'
                : 'non';
              vm.form.companyName = data.data.user.customer[0].companyName;
              vm.form.structure = data.data.user.customer[0].structure;
              vm.form.centralRef = data.data.user.customer[0].centralRef;
              if (vm.form.centralRef !== false) {
                vm.form.centralRefName = data.data.user.customer[0].centralRefName;
              }
              vm.form.insuranceCancel = data.data.user.customer[0].insuranceCancel ? 'oui' : 'non';
              vm.form.sinistreConnaissance5Years = data.data.user.customer[0]
                .sinistreConnaissance5Years
                ? 'oui'
                : 'non';
              if (vm.form.insuranceCancel !== false) {
                vm.form.cancelReasonsSinister = data.data.user.customer[0].cancelReasonsSinister;
                vm.form.cancelReasonsPayment = data.data.user.customer[0].cancelReasonsPayment;
              }
              if (vm.form.cancelReasonsPayment) {
                vm.form.quotationsUpdated = data.data.user.customer[0].quotationsUpdated
                  ? 'oui'
                  : 'non';
              }
              vm.form.animalsValue = data.data.user.customer[0].animalsValue ? 'oui' : 'non';
              vm.form.endStatement = data.data.user.customer[0].endStatement;
              vm.form.individualCabinet = data.data.user.customer[0].individualCabinet;
              vm.form.liberalCollaborator = data.data.user.customer[0].liberalCollaborator;
              vm.franchise = data.data.user.customer[0].franchise.toString();
            }
            if (data.data.user.address.length > 0) {
              vm.form.address = data.data.user.address[0].address;
              vm.form.postalCode = data.data.user.address[0].postalCode;
              vm.form.city = data.data.user.address[0].city;
              vm.showCityInput = true;
            }
            vm.form.civility = data.data.user.title === '' ? 'monsieur' : data.data.user.title;
            vm.form.firstName = data.data.user.name;
            vm.form.lastName = data.data.user.lastname;
            vm.form.phone = data.data.user.phone === '' ? undefined : data.data.user.phone;
            vm.form.mobile = data.data.user.mobile;
            vm.form.email = data.data.user.email;

            vm.chosenMenu = parseInt(localStorage.getItem('chosenMenu'));

            //paymentMethod form
            vm.paymentDay = 10;
            vm.paymentMethod = 'sepa';

            if (data.data.user.payment.length > 0) {
              vm.paymentDay = data.data.user.payment[0].pickingDate;
              vm.bic = data.data.user.payment[0].BIC;
              vm.iban = data.data.user.payment[0].IBAN;
              vm.holder = data.data.user.payment[0].holder;
              vm.exactTitle = data.data.user.payment[0].exactTitle;
              vm.ribImg = data.data.user.payment[0].ribImg;
              vm.isPayed = true;
            }

            if (data.data.user.firstPaymentCb.length > 0) {
              // vm.bic = data.data.user.firstPaymentCb[0].BIC;
              // vm.iban = data.data.user.firstPaymentCb[0].IBAN;
              // vm.holder = data.data.user.firstPaymentCb[0].holder;
              // vm.exactTitle = data.data.user.firstPaymentCb[0].exactTitle;
              vm.firstPaymentResponseCode = data.data.user.firstPaymentCb[0].responseCode;
              vm.firstPaymentOfferType = data.data.user.firstPaymentCb[0].offerType;
              if (vm.firstPaymentResponseCode === '00' && vm.firstPaymentOfferType === 'CR') {
                vm.paymentMethod = 'cb';
                vm.isPayed = true;
              }
            }

            vm.contracts = [];

            //transaction success
            if (data.data.user.contract.length > 0) {
              vm.contracts = data.data.user.contract;
              vm.contracts.forEach(e => {
                e.afterDeadlineDate = moment(
                  new Date(
                    `${e.afterDeadlineDate.split('-')[2]}-${e.afterDeadlineDate.split('-')[1]}-${
                      e.afterDeadlineDate.split('-')[0]
                    }`,
                  ),
                )
                  .date(10)
                  .format('DD-MM-YYYY');
              });
              if (vm.contracts.length > 0) {
                let lastContractsDates = vm.contracts.map(last => last.updatedAt);
                vm.lastContractDate = lastContractsDates.reduce((last, value) =>
                  moment.max(moment(value), moment(last)),
                );
              }
              vm.contractRef =
                data.data.user.contract[data.data.user.contract.length - 1].reference;
              vm.mandatRef = data.data.user.contract[data.data.user.contract.length - 1].sepaMndRef;
            }

            if (data.data.user.leader.length > 0) {
              vm.leaderRef = data.data.user.leader[0].reference;
            }

            //information success page
            vm.mySpaceUpdatedTarget = localStorage.getItem('mySpaceUpdatedTarget');

            vm.helps = [];
            //help form
            if (data.data.user.help.length > 0) {
              vm.helps = data.data.user.help;
              vm.phone = data.data.user.help[0].phone;
              vm.timing = data.data.user.help[0].choice;
              vm.title = data.data.user.help[0].title;
              vm.name = data.data.user.help[0].name;
              vm.lastname = data.data.user.help[0].lastname;
              vm.helpEmail = data.data.user.help[0].email;
              vm.requestDateDay =
                moment(new Date(data.data.user.help[0].date)).date() >= 0 &&
                moment(new Date(data.data.user.help[0].date)).date() <= 9
                  ? '0' + moment(new Date(data.data.user.help[0].date)).date()
                  : moment(new Date(data.data.user.help[0].date))
                      .date()
                      .toString();
              vm.requestDateMonth =
                moment(new Date(data.data.user.help[0].date))
                  .add(1, 'months')
                  .month() >= 0 &&
                moment(new Date(data.data.user.help[0].date))
                  .add(1, 'months')
                  .month() <= 9
                  ? '0' +
                    moment(new Date(data.data.user.help[0].date))
                      .add(1, 'months')
                      .month()
                  : moment(new Date(data.data.user.help[0].date))
                      .add(1, 'months')
                      .month()
                      .toString();
              vm.requestDateYear = moment(new Date(data.data.user.help[0].date)).year();
              vm.requestDateHour = data.data.user.help[0].hour.split(':')[0];
              vm.requestDateMinutes = data.data.user.help[0].hour.split(':')[1];
              if (vm.helps.length > 0) {
                let lastHelpDates = vm.helps.map(last => last.updatedAt);
                vm.lastHelpDate = lastHelpDates.reduce((last, value) =>
                  moment.max(moment(value), moment(last)),
                );
              }
            }

            //custom Rate form
            if (data.data.user.customRate.length > 0) {
              vm.chiffreAffaires = data.data.user.customRate[0].turnover;
              vm.firstname = data.data.user.customRate[0].firstname;
              vm.lastname = data.data.user.customRate[0].lastname;
              vm.phone = data.data.user.customRate[0].phone;
              vm.requestEmail = data.data.user.customRate[0].email;
            }

            //contact form
            if (data.data.user.contact.length > 0) {
              vm.customer = data.data.user.contact[0].customer;
              vm.message = data.data.user.contact[0].message;
              vm.subject = data.data.user.contact[0].subject;
              vm.mobile = data.data.user.contact[0].mobile;
              vm.firstname = data.data.user.contact[0].firstname;
              vm.lastname = data.data.user.contact[0].lastname;
              vm.contactEmail = data.data.user.contact[0].email;
            }

            vm.treatmentSinisters = [];

            if (data.data.user.sinister.length > 0) {
              vm.treatmentSinisters = data.data.user.sinister.filter(
                e => e.status === 'in treatment',
              );
              if (vm.treatmentSinisters.length > 0) {
                let lastSinisterDates = vm.treatmentSinisters.map(last => last.updatedAt);
                vm.lastSinisterDate = lastSinisterDates.reduce((last, value) =>
                  moment.max(moment(value), moment(last)),
                );
              }
            }
            vm.commercialContracts = [];

            if (data.data.user.commercialContracts.length > 0) {
              vm.commercialContracts = data.data.user.commercialContracts;
              vm.commercialContracts.forEach(e => {
                e.afterDeadlineDate = moment(
                  new Date(
                    `${e.afterDeadlineDate.split('-')[2]}-${e.afterDeadlineDate.split('-')[1]}-${
                      e.afterDeadlineDate.split('-')[0]
                    }`,
                  ),
                )
                  .date(10)
                  .format('DD-MM-YYYY');
              });
            }

            vm.adminContracts = [];

            if (data.data.user.adminContracts && data.data.user.adminContracts.length > 0) {
              vm.adminContracts = data.data.user.adminContracts;
            }

            vm.adminRates = [];

            if (data.data.user.adminRates && data.data.user.adminRates.length > 0) {
              vm.adminRates = data.data.user.adminRates;
            }
            if (
              $window.location.pathname === '/menu' &&
              $window.location.search.split('?')[1].split('=')[1] === 'CR' &&
              vm.chiffreAffaires === undefined
            ) {
              $window.location.href = 'simulation?offerType=CR';
            }
          } else {
            vm.chiffreAffaires =
              localStorage.getItem('chiffreAffairesTmp') === null
                ? undefined
                : parseInt(localStorage.getItem('chiffreAffairesTmp'));
            vm.activite = localStorage.getItem('activiteTmp')
              ? localStorage.getItem('activiteTmp')
              : 'urbain';
            if (
              $window.location.pathname === '/menu' &&
              $window.location.search.split('?')[1].split('=')[1] === 'CR' &&
              vm.chiffreAffaires === undefined
            ) {
              $window.location.href = 'simulation?offerType=CR';
            }
            vm.chosenMenu = parseInt(localStorage.getItem('chosenMenu'));
            vm.email = localStorage.getItem('email');
            vm.registrationEmail = localStorage.getItem('registrationEmail');
          }
          vm.loading = false;
          $scope.$applyAsync();
        })
        .then(null, error => {
          console.log(error);
          vm.error = error.data.error;
          $scope.$applyAsync();
        });
    };

    vm.authenticated();

    // subscription path
    // save civil responsibility
    vm.store = function() {
      if (vm.user.user) {
        if (
          vm.chiffreAffaires &&
          parseInt(vm.chiffreAffaires.toString().replace(/\s/g, '')) <= 1500000
        ) {
          WS.post(
            'cr',
            Object.assign(
              {},
              {
                chiffreAffaires: parseInt(vm.chiffreAffaires.toString().replace(/\s/g, '')),
                activite: vm.activite ? vm.activite : 'urbain',
                type: 'CR',
              },
            ),
          )
            .then(data => {
              if (data.data.message) {
                $window.location.href = 'menu?offerType=CR';
              }
            })
            .then(null, error => {
              vm.error = error.data.error;
              $scope.$applyAsync();
            });
        } else {
          if ($scope.sim1Form.$valid) {
            $window.location.href = 'custom-rate?offerType=CR';
          }
        }
      } else {
        if (
          vm.chiffreAffaires &&
          parseInt(vm.chiffreAffaires.toString().replace(/\s/g, '')) <= 1500000
        ) {
          localStorage.setItem(
            'chiffreAffairesTmp',
            parseInt(vm.chiffreAffaires.toString().replace(/\s/g, '')),
          );
          localStorage.setItem('activiteTmp', vm.activite ? vm.activite : 'urbain');
          $window.location.href = 'menu?offerType=CR';
        } else {
          if ($scope.sim1Form.$valid) {
            localStorage.setItem(
              'chiffreAffairesTmp',
              vm.chiffreAffaires.toString().replace(/\s/g, ''),
            );
            $window.location.href = 'custom-rate?offerType=CR';
          }
        }
      }
    };

    // get cr quotation infos
    vm.crQuotation = function() {
      $scope.$watch('vm.chiffreAffaires', (newVal, oldVal) => {
        if (newVal !== oldVal) {
          WS.post(
            'crQuotation',
            Object.assign(
              {},
              {
                chiffreAffaires: localStorage.getItem('chiffreAffairesTmp')
                  ? localStorage.getItem('chiffreAffairesTmp')
                  : localStorage.getItem('chiffreAffaires'),
                activite: localStorage.getItem('activiteTmp')
                  ? localStorage.getItem('activiteTmp')
                  : localStorage.getItem('activite'),
              },
            ),
          )
            .then(data => {
              const activite = localStorage.getItem('activiteTmp')
                ? localStorage.getItem('activiteTmp')
                : localStorage.getItem('activite');
              if (activite === 'urbain') {
                vm.crQuotationAnnual = parseFloat(data.data.crUrbanQuotation);
                vm.crQuotationMonthly = parseFloat(vm.crQuotationAnnual) / 12;
                vm.crQuotationSemestrial = parseFloat(vm.crQuotationAnnual) / 2;
                vm.crQuotationTrimestrial = parseFloat(vm.crQuotationAnnual) / 4;
                if (vm.franchise === '300') {
                  vm.chosenQuotation = parseFloat(vm.crQuotationMonthly);
                }
                vm.authenticated();
              } else {
                vm.crQuotationAnnual = parseFloat(data.data.crRuralMixteQuotation);
                vm.crQuotationMonthly = parseFloat(vm.crQuotationAnnual / 12);
                vm.crQuotationSemestrial = parseFloat(vm.crQuotationAnnual) / 2;
                vm.crQuotationTrimestrial = parseFloat(vm.crQuotationAnnual) / 4;
                if (vm.franchise === '300') {
                  vm.chosenQuotation = parseFloat(vm.crQuotationMonthly);
                }
                vm.authenticated();
              }
              $scope.$applyAsync();
            })
            .then(null, error => {
              vm.error = error.data.error;
              $scope.$applyAsync();
            });
        }
      });
    };

    //saveCustomRate request
    vm.saveCustomRate = function() {
      if ($scope.customRateForm.$valid) {
        WS.post(
          'saveCustomRate',
          JSON.stringify({
            // turnover: parseInt(vm.chiffreAffaires.toString().replace(/\s/g, '')),
            firstname: vm.firstname,
            lastname: vm.lastname,
            phone: vm.phone,
            email: vm.requestEmail,
          }),
        ).then(data => {
          if (data.data.message) {
            $window.location.href = 'custom-rate-check?offerType=CR';
          }
        });
      }
    };

    // update chosenMenu value
    vm.chooseMenu = function(value) {
      localStorage.setItem('chosenMenu', value);
      localStorage.setItem('type', 'CR');
      if (localStorage.getItem('chosenMenu') === '4') {
        $window.location.href = 'rappeler';
      } else {
        $window.location.href = 'info?offerType=CR';
      }
    };

    // save form infos
    vm.saveInfos = function() {
      // const formValid = Object.keys(vm.form)
      //   .map(e => vm.form[e])
      //   .filter(e => e === undefined || e === null || e === '' || e === 'undefined');
      if ($window.location.pathname === '/informations') {
        vm.form.type = 'COMMON';
        if (document.getElementById('saveInfos').checkValidity()) {
          WS.post('subscriptionInfos', JSON.stringify(vm.form))
            .then(data => {
              if (data.data.redirect) {
                localStorage.setItem('mySpaceUpdatedTarget', 'DES INFORMATIONS');
                $window.location.href = data.data.redirect;
              }
            })
            .then(null, error => {
              vm.error = error.data.error;
              $scope.$applyAsync();
            });
        }
      }
      if (vm.form.infoTerm1 && vm.form.infoTerm2) {
        if (
          (vm.form.insuranceCancel === 'oui' && vm.form.cancelReasonsPayment) ||
          (vm.form.insuranceCancel === 'oui' && vm.form.cancelReasonsSinister) ||
          vm.form.insuranceCancel === 'non'
        ) {
          vm.error = undefined;
          if (document.getElementById('saveInfos').checkValidity()) {
            WS.post('subscriptionInfos', JSON.stringify(vm.form))
              .then(data => {
                if (data.data.redirect) {
                  localStorage.setItem('email', vm.form.email);
                  $window.location.href = data.data.redirect;
                }
              })
              .then(null, error => {
                vm.error = error.data.error;
                $scope.$applyAsync();
              });
          }
        } else {
          vm.cancelReasonsError = 'Veuillez choisir un motif';
          $scope.$applyAsync();
        }
      } else {
        vm.error = 'Veuillez Accepter les termes';
      }
    };

    vm.getCityName = function() {
      vm.postalCodeError = undefined;
      if (vm.form.postalCode && vm.form.postalCode.length === 5) {
        WS.post('getCityName', JSON.stringify({ postalCode: vm.form.postalCode }))
          .then(data => {
            if (data.data.message) {
              vm.form.cities = data.data.message;
              vm.postalCodeError = undefined;
              vm.cityError = undefined;
              vm.showCityInput = false;
            }
            $scope.$applyAsync();
          })
          .then(null, error => {
            vm.postalCodeError = error.data.error;
            vm.form.city = undefined;
            vm.cityError = 'Veuillez saisir votre ville';
            $scope.$applyAsync();
          });
      }
    };

    vm.getSirenCoordinates = function() {
      if (vm.form.siren && vm.form.siren.length === 9) {
        WS.post(
          'getSirenCoordinates',
          JSON.stringify({
            siren:
              vm.form.siren[0] +
              vm.form.siren[1] +
              vm.form.siren[2] +
              ' ' +
              vm.form.siren[3] +
              vm.form.siren[4] +
              vm.form.siren[5] +
              ' ' +
              vm.form.siren[6] +
              vm.form.siren[7] +
              vm.form.siren[8],
          }),
        )
          .then(data => {
            if (data.data.message) {
              vm.form.siren = data.data.message.siren.replace(/ /g, '');
              vm.form.companyName = data.data.message.legalEntity;
              vm.form.address = data.data.message.address;
              vm.form.postalCode = data.data.message.postalCode;
              vm.showCityInput = true;
              vm.form.city = data.data.message.city;
              vm.form.phone = data.data.message.telephone;
              vm.form.email = data.data.message.email;
            }
            $scope.$applyAsync();
          })
          .then(null, error => {});
      }
    };

    // set Current Contract Date Minus 2 Months
    vm.setCurrentContractDateMinus2Months = function() {
      if (
        vm.form.currentContractDateYear &&
        vm.form.currentContractDateMonth &&
        vm.form.currentContractDateDay
      ) {
        const isAfterCurrentDate = moment(
          new Date(
            `${vm.form.currentContractDateYear}-${vm.form.currentContractDateMonth}-${
              vm.form.currentContractDateDay
            }`,
          ),
        ).isAfter(moment());
        if (!isAfterCurrentDate) {
          document
            .getElementById('currentContractDateYear')
            .setCustomValidity('merci de vérifier la date');
          document
            .getElementById('currentContractDateDay')
            .setCustomValidity('merci de vérifier la date');
          document
            .getElementById('currentContractDateMonth')
            .setCustomValidity('merci de vérifier la date');
          return;
        } else {
          document.getElementById('currentContractDateYear').setCustomValidity('');
          document.getElementById('currentContractDateDay').setCustomValidity('');
          document.getElementById('currentContractDateMonth').setCustomValidity('');
        }
        vm.form.currentContractDateMinus2Months =
          moment(
            new Date(
              `${vm.form.currentContractDateYear}-${vm.form.currentContractDateMonth}-${
                vm.form.currentContractDateDay
              }`,
            ),
          ).diff(moment().add(2, 'months'), 'months', true) <= 0;
        if (!vm.form.currentContractDateMinus2Months) {
          vm.currentContractInfo = `Pensez à résilier votre contrat avant le ${moment(
            new Date(
              `${vm.form.currentContractDateYear}-${vm.form.currentContractDateMonth}-${
                vm.form.currentContractDateDay
              }`,
            ),
          )
            .subtract(2, 'months')
            .format('DD-MM-YYYY')}`;
        }
      }
    };

    // validate effectDate in adhesion page
    vm.validateEffectDate = function() {
      if (vm.effectDateDay && vm.effectDateMonth && vm.effectDateYear) {
        const isAfterEffectDate = moment(
          new Date(`${vm.effectDateYear}-${vm.effectDateMonth}-${vm.effectDateDay}`),
        ).isAfter(moment());
        vm.effectDate = moment()
          .add(1, 'days')
          .format('DD-MM-YYYY');
        const isValidEffectDate = moment(
          `${vm.effectDateYear}-${vm.effectDateMonth}-${vm.effectDateDay}`,
        ).isValid();
        if (!isAfterEffectDate) {
          document
            .getElementById('effectDateDay')
            .setCustomValidity(`La date d'effet doit être supérieur ou égale à ${vm.effectDate}`);
          document
            .getElementById('effectDateMonth')
            .setCustomValidity(`La date d'effet doit être supérieur ou égale à ${vm.effectDate}`);
          document
            .getElementById('effectDateYear')
            .setCustomValidity(`La date d'effet doit être supérieur ou égale à ${vm.effectDate}`);
          vm.validateEffectDateRes = false;
          return `La date d'effet doit être supérieur ou égale à ${vm.effectDate}`;
        } else if (!isValidEffectDate) {
          document
            .getElementById('effectDateDay')
            .setCustomValidity(`La date d'effet n'est pas valide`);
          document
            .getElementById('effectDateMonth')
            .setCustomValidity(`La date d'effet n'est pas valide`);
          document
            .getElementById('effectDateYear')
            .setCustomValidity(`La date d'effet n'est pas valide`);
          vm.validateEffectDateRes = false;
          return `La date d'effet n'est pas valide`;
        } else {
          document.getElementById('effectDateDay').setCustomValidity('');
          document.getElementById('effectDateMonth').setCustomValidity('');
          document.getElementById('effectDateYear').setCustomValidity('');
        }
        vm.effectDate = moment(
          new Date(`${vm.effectDateYear}-${vm.effectDateMonth}-${vm.effectDateDay}`),
        ).format('DD-MM-YYYY');
        vm.deadlineDate = calculateDeadline(vm.fraction, vm.effectDate).deadlineDate;
        vm.afterDeadlineDate = calculateDeadline(vm.fraction, vm.effectDate).afterDeadlineDate;
        vm.validateEffectDateRes = true;
        $scope.$applyAsync();
      }
      if (
        vm.commercial.effectDateDay &&
        vm.commercial.effectDateMonth &&
        vm.commercial.effectDateYear
      ) {
        const isAfterEffectDate = moment(
          new Date(
            `${vm.commercial.effectDateYear}-${vm.commercial.effectDateMonth}-${
              vm.commercial.effectDateDay
            }`,
          ),
        ).isAfter(moment());
        const isValidEffectDate = moment(
          `${vm.commercial.effectDateYear}-${vm.commercial.effectDateMonth}-${
            vm.commercial.effectDateDay
          }`,
        ).isValid();
        vm.effectDate = moment()
          .add(1, 'days')
          .format('DD-MM-YYYY');
        if (!isAfterEffectDate) {
          document
            .getElementById('effectDateDay')
            .setCustomValidity(`La date d'effet doit être supérieur ou égale à ${vm.effectDate}`);
          document
            .getElementById('effectDateMonth')
            .setCustomValidity(`La date d'effet doit être supérieur ou égale à ${vm.effectDate}`);
          document
            .getElementById('effectDateYear')
            .setCustomValidity(`La date d'effet doit être supérieur ou égale à ${vm.effectDate}`);
          vm.validateEffectDateResult = false;
          return `La date d'effet doit être supérieur ou égale à ${vm.effectDate}`;
        } else if (!isValidEffectDate) {
          document
            .getElementById('effectDateDay')
            .setCustomValidity(`La date d'effet n'est pas valide`);
          document
            .getElementById('effectDateMonth')
            .setCustomValidity(`La date d'effet n'est pas valide`);
          document
            .getElementById('effectDateYear')
            .setCustomValidity(`La date d'effet n'est pas valide`);
          vm.validateEffectDateResult = false;
          return `La date d'effet n'est pas valide`;
        } else {
          document.getElementById('effectDateDay').setCustomValidity('');
          document.getElementById('effectDateMonth').setCustomValidity('');
          document.getElementById('effectDateYear').setCustomValidity('');
        }
        vm.effectDate = moment(
          new Date(
            `${vm.commercial.effectDateYear}-${vm.commercial.effectDateMonth}-${
              vm.commercial.effectDateDay
            }`,
          ),
        ).format('DD-MM-YYYY');
        vm.validateEffectDateResult = true;
        $scope.$applyAsync();
        return true;
      }
    };

    // get resend activation email
    vm.getResendActivationEmail = function() {
      WS.get('resendActivationMail')
        .then(data => {
          if (data.data.message) {
            vm.success = data.data.message;
          }
          $scope.$applyAsync();
        })
        .then(null, error => {
          vm.error = error.data.error;
          $scope.$applyAsync();
        });
    };

    // get resend process activation email
    vm.getResendProcessActivationEmail = function() {
      WS.get('resendProcessActivationMail')
        .then(data => {
          if (data.data.message) {
            vm.success = data.data.message;
          }
          $scope.$applyAsync();
        })
        .then(null, error => {
          vm.error = error.data.error;
          $scope.$applyAsync();
        });
    };

    // get adhesion summary
    vm.submitAdhesion = function() {
      if (vm.fraction) {
        $('#errorPeriod').remove();
        if (vm.chosenMenu === 1) {
          if (vm.form.infoTerm1 && vm.form.infoTerm2) {
            if (document.forms['adhesionForm'].checkValidity()) {
              postUpdateAdhesion(
                WS,
                'RC',
                vm.fraction,
                vm.effectDate,
                vm.fractQuotation,
                vm.deadlineDate,
                vm.afterDeadlineDate,
                vm.cashQuotation,
                vm.franchise,
                vm.chiffreAffaires,
                vm.error,
                $scope,
              );
              if (vm.chosenMenu === 1) {
                $window.location.href = 'terms?offerType=CR';
              } else if (vm.chosenMenu === 2) {
                $window.location.href = 'quote?offerType=CR';
              } else if (vm.chosenMenu === 3) {
                $window.location.href = 'save?offerType=CR';
              } else {
                $window.location.href = 'rappeler';
              }
              localStorage.setItem('currentUrlPath', $window.location.pathname);
            }
          } else {
            vm.error = 'Veuillez Accepter les termes';
          }
        } else {
          if (document.forms['adhesionForm'].checkValidity()) {
            postUpdateAdhesion(
              WS,
              'RC',
              vm.fraction,
              vm.effectDate,
              vm.fractQuotation,
              vm.deadlineDate,
              vm.afterDeadlineDate,
              vm.cashQuotation,
              vm.franchise,
              vm.chiffreAffaires,
              vm.error,
              $scope,
            );
            if (vm.chosenMenu === 1) {
              $window.location.href = 'terms?offerType=CR';
            } else if (vm.chosenMenu === 2) {
              $window.location.href = 'quote?offerType=CR';
            } else if (vm.chosenMenu === 3) {
              $window.location.href = 'save?offerType=CR';
            } else {
              $window.location.href = 'rappeler';
            }
            localStorage.setItem('currentUrlPath', $window.location.pathname);
          }
        }
      } else {
        $('html, body').animate(
          {
            scrollTop: $('div.periodicite').offset().top - 100,
          },
          500,
        );
        vm.errorPeriod = 'Veuillez choisir la périodicité';
      }
    };

    // update adhesion summary quotation
    vm.chosenFraction = function(fraction, fractQuotation, effectDate) {
      (async () => {
        try {
          const taxValues = await calculateCrTax(
            WS,
            localStorage.getItem('chiffreAffaires'),
            localStorage.getItem('activite'),
            fraction,
            effectDate,
          );
          vm.totalTax = parseFloat(taxValues.totalTax);
          vm.fraisCourtier = parseFloat(taxValues.fraisCourtier);
          vm.totalTaxFraisContr = parseFloat(taxValues.totalTaxFraisContr);
          vm.fractQuotation = parseFloat(fractQuotation);
          vm.totalTaxSem = parseFloat(taxValues.totalTaxSem);
          vm.fraisCourtierSem = parseFloat(taxValues.fraisCourtierSem);
          vm.totalTaxFraisContrSem = parseFloat(taxValues.totalTaxFraisContrSem);
          vm.totalTaxTrim = parseFloat(taxValues.totalTaxTrim);
          vm.fraisCourtierTrim = parseFloat(taxValues.fraisCourtierTrim);
          vm.totalTaxFraisContrTrim = parseFloat(taxValues.totalTaxFraisContrTrim);
          vm.totalTaxMens = parseFloat(taxValues.totalTaxMens);
          vm.fraisCourtierMens = parseFloat(taxValues.fraisCourtierMens);
          vm.totalTaxFraisContrMens = parseFloat(taxValues.totalTaxFraisContrMens);
          vm.totalHT = parseFloat(taxValues.totalHT);
          vm.totalHTSem = parseFloat(taxValues.totalHTSem);
          vm.totalHTTrim = parseFloat(taxValues.totalHTTrim);
          vm.totalHTMens = parseFloat(taxValues.totalHTMens);
          vm.fraction = parseInt(vm.fraction);
          vm.deadlineDate = calculateDeadline(fraction, effectDate).deadlineDate;
          vm.afterDeadlineDate = calculateDeadline(fraction, effectDate).afterDeadlineDate;
          vm.cashQuotation = correctMonthlyCashQuotation(
            vm.deadlineDate,
            effectDate,
            vm.fraction,
            vm.fractQuotation,
          ).cashQuotation;
          $scope.$applyAsync();
        } catch (error) {
          console.log(error);
        }
      })();
    };

    // get cr tax
    vm.crTax = function() {
      $scope.$watch('vm.chiffreAffaires', (newVal, oldVal) => {
        if (newVal !== oldVal) {
          if (localStorage.getItem('activite') === 'urbain') {
            WS.post(
              'crUrbanTax',
              Object.assign(
                {},
                {
                  chiffreAffaires: localStorage.getItem('chiffreAffaires'),
                  fraction: vm.fraction,
                  effectDate: vm.effectDate,
                },
              ),
            )
              .then(data => {
                vm.fractQuotation = parseFloat(data.data.fractQuotation);
                vm.cashQuotation = correctMonthlyCashQuotation(
                  vm.deadlineDate,
                  vm.effectDate,
                  vm.fraction,
                  vm.fractQuotation,
                ).cashQuotation;
                vm.totalTaxFraisContr = parseFloat(data.data.totalTaxFraisContr);
                vm.totalTaxFraisContrSem = parseFloat(data.data.totalTaxFraisContrSem);
                vm.totalTaxFraisContrTrim = parseFloat(data.data.totalTaxFraisContrTrim);
                vm.totalTaxFraisContrMens = parseFloat(data.data.totalTaxFraisContrMens);
                vm.totalTax = parseFloat(data.data.totalTax);
                vm.totalTaxSem = parseFloat(data.data.totalTaxSem);
                vm.totalTaxTrim = parseFloat(data.data.totalTaxTrim);
                vm.totalTaxMens = parseFloat(data.data.totalTaxMens);
                vm.fraisCourtier = parseFloat(data.data.fraisCourtier);
                vm.fraisCourtierSem = parseFloat(data.data.fraisCourtierSem);
                vm.fraisCourtierTrim = parseFloat(data.data.fraisCourtierTrim);
                vm.fraisCourtierMens = parseFloat(data.data.fraisCourtierMens);
                vm.totalHT = parseFloat(data.data.totalHT);
                vm.totalHTSem = parseFloat(data.data.totalHTSem);
                vm.totalHTTrim = parseFloat(data.data.totalHTTrim);
                vm.totalHTMens = parseFloat(data.data.totalHTMens);
                $scope.$applyAsync();
              })
              .then(null, error => {
                vm.error = error.data.error;
                $scope.$applyAsync();
              });
          } else {
            WS.post(
              'crRuralTax',
              Object.assign(
                {},
                {
                  chiffreAffaires: localStorage.getItem('chiffreAffaires'),
                  fraction: vm.fraction,
                  effectDate: vm.effectDate,
                },
              ),
            )
              .then(data => {
                vm.fractQuotation = parseFloat(data.data.fractQuotation);
                vm.cashQuotation = correctMonthlyCashQuotation(
                  vm.deadlineDate,
                  vm.effectDate,
                  vm.fraction,
                  vm.fractQuotation,
                ).cashQuotation;
                vm.totalTaxFraisContr = parseFloat(data.data.totalTaxFraisContr);
                vm.totalTaxFraisContrSem = parseFloat(data.data.totalTaxFraisContrSem);
                vm.totalTaxFraisContrTrim = parseFloat(data.data.totalTaxFraisContrTrim);
                vm.totalTaxFraisContrMens = parseFloat(data.data.totalTaxFraisContrMens);
                vm.totalTax = parseFloat(data.data.totalTax);
                vm.totalTaxSem = parseFloat(data.data.totalTaxSem);
                vm.totalTaxTrim = parseFloat(data.data.totalTaxTrim);
                vm.totalTaxMens = parseFloat(data.data.totalTaxMens);
                vm.fraisCourtier = parseFloat(data.data.fraisCourtier);
                vm.fraisCourtierSem = parseFloat(data.data.fraisCourtierSem);
                vm.fraisCourtierTrim = parseFloat(data.data.fraisCourtierTrim);
                vm.fraisCourtierMens = parseFloat(data.data.fraisCourtierMens);
                vm.totalHT = parseFloat(data.data.totalHT);
                vm.totalHTSem = parseFloat(data.data.totalHTSem);
                vm.totalHTTrim = parseFloat(data.data.totalHTTrim);
                vm.totalHTMens = parseFloat(data.data.totalHTMens);
                $scope.$applyAsync();
              })
              .then(null, error => {
                vm.error = error.data.error;
                $scope.$applyAsync();
              });
          }
        }
      });
    };

    //save paymentMethod
    vm.savePaymentMethod = function() {
      const formdata = new FormData(document.getElementById('paymentForm'));
      if (
        document.getElementById('file').files.length > 0 &&
        document.getElementById('file').files[0].name &&
        vm.ribImg
      ) {
        formdata.append('updateRibFile', true);
      }
      (async () => {
        try {
          const { data } = await $http.post('/paymentInfos', formdata, {
            headers: { 'Content-Type': undefined },
            transformResponse: angular.identity,
          });
          if (JSON.parse(data).redirect) {
            $window.location.href = JSON.parse(data).redirect;
          }
        } catch (error) {
          vm.error = JSON.parse(error.data).error;
          $scope.$applyAsync();
        }
      })();
    };

    //download ribFile
    vm.downloadRibFile = function(ribFileName) {
      $window.open(`/downloadRib?filename=${ribFileName}`);
    };

    // go to transaction success page
    vm.goToTXSuccessPage = function() {
      if (vm.form.infoTerm1 && vm.form.infoTerm2) {
        $window.location.href = 'payment?offerType=CR';
      } else {
        vm.error = 'Veuillez Accepter les termes';
      }
    };

    // go to universign
    vm.signContract = function() {
      WS.get('getUniversignTransaction', 'type=RC')
        .then(data => {
          if (data.data.url) {
            $window.location.href = data.data.url;
          }
        })
        .then(null, error => {
          vm.error = error.data.error;
          $scope.$applyAsync();
        });
    };

    // --------------------------------------------------------------

    // rate path
    // get rate email
    vm.getRateEmail = function() {
      if (
        localStorage.getItem('currentUrlPath') &&
        localStorage.getItem('currentUrlPath') !== $window.location.pathname
      ) {
        WS.get('rateEmail', 'type=RC')
          .then(data => {
            if (data.data.rateFileName) {
              vm.rateFileNameGenerated = data.data.rateFileName;
              localStorage.setItem('rateFileNameGenerated', vm.rateFileNameGenerated);
              $scope.$applyAsync();
            }
          })
          .then(null, error => {
            vm.error = error.data.error;
            $scope.$applyAsync();
          });
      } else {
        vm.rateFileNameGenerated = localStorage.getItem('rateFileNameGenerated');
      }
      localStorage.setItem('currentUrlPath', $window.location.pathname);
    };

    // ---------------------------------------------------------

    // help path
    // save request path
    vm.saveHelp = function() {
      WS.post(
        'saveHelp',
        JSON.stringify({
          phone: vm.phone,
          timing: vm.timing,
          requestDate:
            vm.requestDateYear && vm.requestDateMonth && vm.requestDateDay
              ? moment(
                  new Date(`${vm.requestDateYear}-${vm.requestDateMonth}-${vm.requestDateDay}`),
                ).format('YYYY-MM-DD')
              : null,
          hour:
            vm.requestDateHour && vm.requestDateMinutes
              ? `${vm.requestDateHour}:${vm.requestDateMinutes}`
              : null,
          title: vm.title,
          name: vm.name,
          lastname: vm.lastname,
          email: vm.helpEmail,
          offerType: localStorage.getItem('type'),
        }),
      )
        .then(data => {
          if (data.data.message) {
            localStorage.setItem('email', vm.helpEmail);
            $window.location.href = 'succes-rappeler';
          }
        })
        .then(null, error => {
          vm.error = error.data.error;
          $scope.$applyAsync();
        });
    };

    // ----------------------------------------------------------
    // get continue later email
    vm.getContinueLaterEmail = function() {
      WS.get('continueEmail', `offerType=${encodeURIComponent('RC')}`)
        .then(() => {})
        .then(null, error => {
          vm.error = error.data.error;
          $scope.$applyAsync();
        });
    };

    // ----------------------------------------------------------

    // common logic
    // check signed contract in delete account
    vm.checkSignedContract = function() {
      WS.get('checkSignedContract')
        .then(data => {
          if (data.data.message) {
          }
        })
        .then(null, error => {
          vm.error = error.data.error;
          $scope.$applyAsync();
        });
    };

    // continue subscription
    vm.continueSubscription = function(value) {
      if (value) {
        postToggleContinueLater(
          WS,
          false,
          $window,
          `menu?offerType=${localStorage.getItem('type')}`,
          getOfferTypeFr(localStorage.getItem('type')),
        );
        postToggleContinueLaterPopup(WS, false, getOfferTypeFr(localStorage.getItem('type')));
      } else {
        postToggleContinueLater(WS, false, $window, getOfferTypeFr(localStorage.getItem('type')));
        postToggleContinueLaterPopup(WS, false, getOfferTypeFr(localStorage.getItem('type')));
      }
    };

    // change Password
    vm.changePassword = function() {
      if (
        $scope.changePasswordForm.$valid &&
        (vm.password && vm.password.length >= 4) &&
        (vm.newPassword && vm.newPassword.length >= 4) &&
        vm.newPassword === vm.renewPassword
      ) {
        WS.post(
          'changePassword',
          JSON.stringify({
            password: vm.password,
            newPassword: vm.newPassword,
          }),
        )
          .then(data => {
            if (data.data.message) {
              localStorage.setItem('mySpaceUpdatedTarget', 'DU MOT DE PASSE');
              $window.location.href = 'informations-success';
            }
            $scope.$applyAsync();
          })
          .then(null, error => {
            vm.error = error.data.error;
            $scope.$applyAsync();
          });
      }
    };

    // downloadRate
    vm.downloadRate = function(filename) {
      $window.open(`/downloadRate?filename=${filename}`);
    };

    // downloadContract
    vm.downloadContract = function(filename, user) {
      $window.open(`/downloadContract?filename=${filename}&user=${user}`);
    };

    //deleteAccount
    vm.deleteAccount = function() {
      if ($scope.supprimerCompteForm.$valid) {
        WS.post(
          'deleteAccount',
          JSON.stringify({
            password: vm.password,
            confirmPassword: vm.confirmPassword,
          }),
        )
          .then(data => {
            if (data.data.message) {
              $window.location.href = 'supprimer-succes';
            }
            $scope.$apply();
          })
          .then(null, error => {
            vm.error = error.data.error;
            $scope.$apply();
          });
      }
    };

    // save contact
    vm.saveContact = function() {
      if ($scope.contactForm.$valid) {
        if (vm.rappeler) {
          WS.post(
            'saveContact',
            JSON.stringify({
              customer: vm.customer === 'oui' ? true : false,
              message: vm.message,
              mobile: vm.mobile,
              firstname: vm.firstname,
              lastname: vm.lastname,
              email: vm.contactEmail,
              objet: vm.objet,
            }),
          )
            .then(data => {
              if (data.data.message) {
                vm.success = data.data.message;
              }
              $scope.$applyAsync();
            })
            .then(null, error => {
              vm.error = error.data.error;
              $scope.$applyAsync();
            });
        } else {
          vm.error = 'Veuillez Accepter ce terme';
        }
      }
    };

    // save partnership
    vm.savePartnership = function() {
      if ($scope.partnershipForm.$valid) {
        if (vm.traitement && vm.offres) {
          WS.post('savePartnership', JSON.stringify(vm.partnership))
            .then(data => {
              if (data.data.message) {
                $window.location.href = 'partenariat-bourgelat-send';
              }
              $scope.$applyAsync();
            })
            .then(null, error => {
              vm.error = error.data.error;
              $scope.$applyAsync();
            });
        } else {
          vm.error = 'Veuillez Accepter les termes';
        }
      }
    };

    // save sinister
    vm.saveSinister = function() {
      if ($scope.sinisterForm.$valid) {
        if (vm.infoTerm1 && vm.infoTerm2) {
          WS.post(
            'saveSinister',
            JSON.stringify({
              provisionAmount: parseInt(vm.provisionAmount.toString().replace(/\s/g, '')),
              regulationAmount: parseInt(vm.regulationAmount.toString().replace(/\s/g, '')),
              reportingDate:
                vm.reportingDateYear && vm.reportingDateMonth && vm.reportingDateDay
                  ? moment(
                      new Date(
                        `${vm.reportingDateYear}-${vm.reportingDateMonth}-${vm.reportingDateDay}`,
                      ),
                    ).format('YYYY-MM-DD')
                  : null,
              status: vm.status,
              franchise: parseInt(vm.form.franchise.toString().replace(/\s/g, '')),
              statementScan: vm.statementScan,
              challengeScan: vm.challengeScan,
              contract: vm.contractId,
            }),
          )
            .then(data => {
              if (data.data.redirect) {
                $window.location.href = 'success-sinistre';
              }
              $scope.$applyAsync();
            })
            .then(null, error => {
              vm.error = error.data.error;
              $scope.$applyAsync();
            });
        } else {
          vm.error = 'Veuillez Accepter les termes';
        }
      }
    };

    vm.backHelp = function() {
      if (localStorage.getItem('type')) {
        $window.location.href = `menu?offerType=${localStorage.getItem('type')}`;
      } else {
        $window.location.href = 'protection-activite';
      }
    };

    vm.togglePeriodicity = function() {
      if (
        vm.commercial.turnover !== undefined &&
        vm.commercial.surfaceLocaux !== undefined &&
        vm.commercial.contenuLocaux !== undefined &&
        vm.commercial.endStatementDateDay !== undefined &&
        vm.commercial.endStatementDateMonth !== undefined &&
        vm.commercial.alarm !== undefined &&
        vm.commercial.approvedAlarm !== undefined &&
        vm.commercial.bdmSup !== undefined &&
        vm.commercial.bdm !== undefined &&
        (vm.commercial.offer === 'MRP' || vm.commercial.offer === 'MRP + RC')
      ) {
        vm.commercial.showPeridicity = true;
      } else if (
        vm.commercial.turnover !== undefined &&
        vm.commercial.endStatementDateDay !== undefined &&
        vm.commercial.endStatementDateMonth !== undefined &&
        vm.commercial.offer === 'RC'
      ) {
        vm.commercial.showPeridicity = true;
      } else if (
        vm.commercial.leaderBirthdateDay !== undefined &&
        vm.commercial.leaderBirthdateMonth !== undefined &&
        vm.commercial.leaderBirthdateYear !== undefined &&
        vm.commercial.haveConjoint !== undefined &&
        vm.commercial.haveChildren !== undefined &&
        vm.commercial.formula &&
        vm.commercial.offer === 'MD'
      ) {
        vm.commercial.showPeridicity = true;
      } else if (vm.commercial.turnover !== undefined && vm.commercial.offer === 'RCMS') {
        vm.commercial.showPeridicity = true;
      } else {
        vm.commercial.showPeridicity = false;
      }
      $scope.$applyAsync();
    };

    vm.getCommercialQuotation = function(
      offer,
      turnover,
      activity,
      fraction,
      franchise,
      brokerFee,
    ) {
      if (
        vm.commercial.leaderBirthdateDay &&
        vm.commercial.leaderBirthdateMonth &&
        vm.commercial.leaderBirthdateYear
      ) {
        vm.commercial.leaderBirthdate = moment(
          `${vm.commercial.leaderBirthdateYear}-${vm.commercial.leaderBirthdateMonth}-${
            vm.commercial.leaderBirthdateDay
          }`,
        ).format('DD-MM-YYYY');
      }
      if (
        vm.commercial.haveConjoint === 'oui' &&
        vm.commercial.conjointBirthdateDay &&
        vm.commercial.conjointBirthdateMonth &&
        vm.commercial.conjointBirthdateYear
      ) {
        vm.commercial.conjointBirthdate = moment(
          `${vm.commercial.conjointBirthdateYear}-${vm.commercial.conjointBirthdateMonth}-${
            vm.commercial.conjointBirthdateDay
          }`,
        ).format('DD-MM-YYYY');
      }
      if (vm.commercial.ChildrenBirthdateYear) {
        // generate ChildrenBirthdates
        const ChildrenBirthdates = [];

        for (let i = 0; i < Object.keys(vm.commercial.ChildrenBirthdateDay).length; i++) {
          ChildrenBirthdates.push(
            moment(
              new Date(
                `${vm.commercial.ChildrenBirthdateYear[`${i}`]}-${
                  vm.commercial.ChildrenBirthdateMonth[`${i}`]
                }-${vm.commercial.ChildrenBirthdateDay[`${i}`]}`,
              ),
            ).format('DD-MM-YYYY'),
          );
        }

        vm.commercial.ChildrenBirthdates = ChildrenBirthdates;
      }

      if (
        vm.commercial.effectDateDay &&
        vm.commercial.effectDateMonth &&
        vm.commercial.effectDateYear
      ) {
        vm.commercial.effectDate = moment(
          new Date(
            `${vm.commercial.effectDateYear}-${vm.commercial.effectDateMonth}-${
              vm.commercial.effectDateDay
            }`,
          ),
        ).format('DD-MM-YYYY');
      }

      if (fraction && vm.commercial.effectDate) {
        WS.get(
          'quotation',
          `offer=${encodeURIComponent(offer)}&turnover=${
            turnover ? turnover.replace(/\s/g, '') : undefined
          }&activity=${activity}&franchise=${franchise}&fraction=${fraction}&brokerFee=${parseFloat(
            brokerFee,
          ).toFixed(2)}&effectDate=${vm.commercial.effectDate}&formula=${
            vm.commercial.formula
          }&leaderBirthdate=${vm.commercial.leaderBirthdate}&conjointBirthdate=${
            vm.commercial.conjointBirthdate
          }&kidBirthdate=${vm.commercial.ChildrenBirthdates}&kidsNumber=${
            vm.commercial.childrenNumbers
          }&haveConjoint=${vm.commercial.haveConjoint === 'oui' ? true : false}&haveKids=${
            vm.commercial.haveChildren === 'oui' ? true : false
          }&alarm=${vm.commercial.alarm === 'oui' ? vm.commercial.approvedAlarm : false}&bdm=${
            vm.commercial.bdm
          }&indivAccidentChoix=${
            vm.commercial.indivAccident === 'oui' ? vm.commercial.indivAccidentChoix : undefined
          }&indivEnfants=${vm.commercial.indivEnfants === 'oui' ? true : false}&rapatriementChoix=${
            vm.commercial.rapatriement === 'oui' ? vm.commercial.rapatriementChoix : undefined
          }`,
        )
          .then(data => {
            vm.commercial.fractQuotation = data.data.quotation.fractQuotation;
            vm.commercial.cashQuotation = correctMonthlyCashQuotation(
              undefined,
              vm.commercial.effectDate,
              parseInt(fraction),
              vm.commercial.fractQuotation,
            ).cashQuotation;
            $scope.$applyAsync();
          })
          .then(null, error => {
            vm.error = error.data.error;
            $scope.$applyAsync();
          });
      }
    };

    vm.createCommercialAdhesion = function() {
      //check all form elements validity
      // let inputs = document.forms['addContract'].getElementsByTagName('input');
      // let selects = document.forms['addContract'].getElementsByTagName('select');

      // for (let input of inputs) {
      //   console.log('Input: ', input.name);
      //   console.log('Validates: ', input.validity.valid);
      //   console.log('Value missing: ', input.validity.valueMissing);
      // }

      // for (let input of selects) {
      //   console.log('select: ', input.name);
      //   console.log('Validates: ', input.validity.valid);
      //   console.log('Value missing: ', input.validity.valueMissing);
      // }
      if (vm.commercial.infoTerm1 && vm.commercial.infoTerm2 && !vm.commercialCheckValidity()) {
        let query = JSON.parse(JSON.stringify(vm.commercial));

        query.effectDate = moment(
          new Date(`${query.effectDateYear}-${query.effectDateMonth}-${query.effectDateDay}`),
        ).format('DD-MM-YYYY');
        query.startActivityDate = moment(
          new Date(
            `${query.startActivityDateYear}-${query.startActivityDateMonth}-${
              query.startActivityDateDay
            }`,
          ),
        ).format('YYYY-MM-DD');

        if (query.leaderBirthdateDay && query.leaderBirthdateMonth && query.leaderBirthdateYear) {
          query.leaderBirthdate = moment(
            `${query.leaderBirthdateYear}-${query.leaderBirthdateMonth}-${
              query.leaderBirthdateDay
            }`,
          ).format('DD-MM-YYYY');
        }
        if (
          query.haveConjoint === 'oui' &&
          query.conjointBirthdateDay &&
          query.conjointBirthdateMonth &&
          query.conjointBirthdateYear
        ) {
          query.conjointBirthdate = moment(
            `${query.conjointBirthdateYear}-${query.conjointBirthdateMonth}-${
              query.conjointBirthdateDay
            }`,
          ).format('DD-MM-YYYY');
        }
        if (query.ChildrenBirthdateYear) {
          // generate ChildrenBirthdates
          const ChildrenBirthdates = [];

          for (let i = 0; i < Object.keys(query.ChildrenBirthdateDay).length; i++) {
            ChildrenBirthdates.push(
              moment(
                new Date(
                  `${query.ChildrenBirthdateYear[`${i}`]}-${query.ChildrenBirthdateMonth[`${i}`]}-${
                    query.ChildrenBirthdateDay[`${i}`]
                  }`,
                ),
              ).format('DD-MM-YYYY'),
            );
          }

          query.ChildrenBirthdates = ChildrenBirthdates;
        }

        $('html, body').animate(
          {
            scrollTop: 0,
          },
          500,
        );
        vm.commercial.loading = true;
        WS.post(
          'commercialCreateAdhesion',
          JSON.stringify(
            Object.assign(query, {
              brokerFee: parseFloat(vm.commercial.brokerFee).toFixed(2),
            }),
          ),
        )
          .then(data => {
            if (data.data.url) {
              $window.location.href = data.data.url;
            }
            $scope.$applyAsync();
          })
          .then(null, error => {
            vm.error = error.data.error;
            if (error.data.redirect) {
              $window.location.href = error.data.redirect;
              return;
            }
            vm.commercial.loading = false;
            $('html, body').animate(
              {
                scrollTop: $('footer').offset().top + 900,
              },
              500,
            );
            $scope.$applyAsync();
          });
      } else {
        $('#commercialForm').validator('validate');
        vm.error = 'Veuillez Accepter les termes et compléter le formulaire';
        $scope.$applyAsync();
      }
    };

    vm.getCommercialCityName = function() {
      if (vm.commercial.postalCode && vm.commercial.postalCode.length === 5) {
        WS.post('getCityName', JSON.stringify({ postalCode: vm.commercial.postalCode }))
          .then(data => {
            if (data.data.message) {
              vm.commercial.cities = data.data.message;
              vm.postalCodeError = undefined;
              vm.cityError = undefined;
              vm.showCityInput = false;
            }
            $scope.$applyAsync();
          })
          .then(null, error => {
            vm.postalCodeError = error.data.error;
            vm.commercial.city = undefined;
            vm.cityError = 'Veuillez saisir votre ville';
            $scope.$applyAsync();
          });
      }
    };

    vm.commercialCheckValidity = function() {
      if (vm.commercial.offer === 'MRP' || vm.commercial.offer === 'MRP + RC') {
        if (vm.commercial.surfaceLocaux !== undefined) {
          vm.commercial.surfaceLocaux = currencyFormatInputFilter(
            document.getElementById('surfaceInput').value,
          );
          if (parseInt(vm.commercial.surfaceLocaux.replace(/ /g, '')) > 1000) {
            vm.surfaceLocauxError = true;
          } else {
            vm.surfaceLocauxError = false;
          }
        } else {
          vm.surfaceLocauxError = true;
        }

        if (vm.commercial.contenuLocaux !== undefined) {
          vm.commercial.contenuLocaux = currencyFormatInputFilter(
            document.getElementById('contenuInput').value,
          );
          if (parseInt(vm.commercial.contenuLocaux.replace(/ /g, '')) > 400000) {
            vm.contenuLocauxError = true;
          } else {
            vm.contenuLocauxError = false;
          }
        } else {
          vm.contenuLocauxError = true;
        }
        if (vm.commercial.turnover !== undefined) {
          vm.commercial.turnover = currencyFormatInputFilter(
            document.getElementById('turnoverInput').value,
          );
          if (parseInt(vm.commercial.turnover.replace(/ /g, '')) > 1500000) {
            vm.turnoverError = true;
          } else {
            vm.turnoverError = false;
          }
        } else {
          vm.turnoverError = true;
        }
        if (
          vm.commercial.endStatementDateDay !== undefined &&
          vm.commercial.endStatementDateMonth !== undefined
        ) {
          vm.endStatementDateError = false;
        } else {
          vm.endStatementDateError = true;
        }

        if (vm.commercial.bdm !== undefined && vm.commercial.bdm === 'reserveSiege') {
          vm.bdmError = true;
        } else {
          vm.bdmError = false;
        }
      }

      if (vm.commercial.offer === 'RC') {
        if (vm.commercial.turnover !== undefined) {
          vm.commercial.turnover = currencyFormatInputFilter(
            document.getElementById('turnoverInput').value,
          );
          if (parseInt(vm.commercial.turnover.replace(/ /g, '')) > 1500000) {
            vm.turnoverError = true;
          } else {
            vm.turnoverError = false;
          }
        } else {
          vm.turnoverError = true;
        }
        if (
          vm.commercial.endStatementDateDay !== undefined &&
          vm.commercial.endStatementDateMonth !== undefined
        ) {
          vm.endStatementDateError = false;
        } else {
          vm.endStatementDateError = true;
        }
      }

      if (vm.commercial.offer === 'MD') {
        if (
          vm.commercial.leaderBirthdateDay !== undefined &&
          vm.commercial.leaderBirthdateMonth !== undefined &&
          vm.commercial.leaderBirthdateYear !== undefined
        ) {
          vm.leaderBirthdateError = false;
        } else {
          vm.leaderBirthdateError = true;
        }

        if (
          (vm.commercial.haveConjoint === 'oui' && vm.commercial.conjointName !== undefined) ||
          vm.commercial.haveConjoint === 'non'
        ) {
          vm.conjointNameError = false;
        } else {
          vm.conjointNameError = true;
        }
        if (
          (vm.commercial.haveConjoint === 'oui' && vm.commercial.conjointLastname !== undefined) ||
          vm.commercial.haveConjoint === 'non'
        ) {
          vm.conjointLastnameError = false;
        } else {
          vm.conjointLastnameError = true;
        }
        if (
          vm.commercial.haveConjoint === 'oui' &&
          vm.commercial.conjointBirthdateDay !== undefined &&
          vm.commercial.conjointBirthdateMonth !== undefined &&
          vm.commercial.conjointBirthdateYear !== undefined
        ) {
          vm.conjointBirthdateError = false;
        } else if (vm.commercial.haveConjoint === 'non') {
          vm.conjointBirthdateError = false;
        } else {
          vm.conjointBirthdateError = true;
        }
        if (
          (vm.commercial.haveChildren === 'oui' && vm.commercial.childrenNumbers !== undefined) ||
          vm.commercial.haveChildren === 'non'
        ) {
          vm.childrenNumbersError = false;
        } else {
          vm.childrenNumbersError = true;
        }
      }

      if (vm.commercial.offer === 'RCMS') {
        if (vm.commercial.turnover !== undefined) {
          vm.commercial.turnover = currencyFormatInputFilter(
            document.getElementById('turnoverInput').value,
          );
          if (parseInt(vm.commercial.turnover.replace(/ /g, '')) > 1500000) {
            vm.turnoverError = true;
          } else {
            vm.turnoverError = false;
          }
        } else {
          vm.turnoverError = true;
        }

        if (
          vm.commercial.capitauxPropres !== undefined &&
          vm.commercial.capitauxPropres === 'non'
        ) {
          vm.capitauxPropresError = true;
        } else {
          vm.capitauxPropresError = false;
        }

        if (vm.commercial.resultatNet !== undefined && vm.commercial.resultatNet === 'non') {
          vm.resultatNetError = true;
        } else {
          vm.resultatNetError = false;
        }

        if (
          vm.commercial.legalForm !== undefined &&
          (vm.commercial.legalForm === 'EURL' ||
            vm.commercial.legalForm === 'SARL' ||
            vm.commercial.legalForm === 'SAS' ||
            vm.commercial.legalForm === 'SA' ||
            vm.commercial.legalForm === 'SELARL' ||
            vm.commercial.legalForm === 'SELAS' ||
            vm.commercial.legalForm === 'SELAFA' ||
            vm.commercial.legalForm === 'SELCA')
        ) {
          vm.legalFormError = false;
        } else {
          vm.legalFormError = true;
        }

        if (vm.commercial.chiffreAffaireCours !== undefined) {
          vm.chiffreAffaireCoursError = false;
        } else {
          vm.chiffreAffaireCoursError = true;
        }
        if (vm.commercial.chiffreAffairePrev !== undefined) {
          vm.chiffreAffairePrevError = false;
        } else {
          vm.chiffreAffairePrevError = true;
        }
        if (vm.commercial.totalActifAnnee !== undefined) {
          vm.totalActifAnneeError = false;
        } else {
          vm.totalActifAnneeError = true;
        }
        if (vm.commercial.totalActifCours !== undefined) {
          vm.totalActifCoursError = false;
        } else {
          vm.totalActifCoursError = true;
        }
        if (vm.commercial.totalActifPrev !== undefined) {
          vm.totalActifPrevError = false;
        } else {
          vm.totalActifPrevError = true;
        }
        if (vm.commercial.capitauxAnnee !== undefined) {
          vm.capitauxAnneeError = false;
        } else {
          vm.capitauxAnneeError = true;
        }
        if (vm.commercial.capitauxCours !== undefined) {
          vm.capitauxCoursError = false;
        } else {
          vm.capitauxCoursError = true;
        }
        if (vm.commercial.capitauxPrev !== undefined) {
          vm.capitauxPrevError = false;
        } else {
          vm.capitauxPrevError = true;
        }
        if (vm.commercial.totalAnnee !== undefined) {
          vm.totalAnneeError = false;
        } else {
          vm.totalAnneeError = true;
        }
        if (vm.commercial.totalCours !== undefined) {
          vm.totalCoursError = false;
        } else {
          vm.totalCoursError = true;
        }
        if (vm.commercial.totalPrev !== undefined) {
          vm.totalPrevError = false;
        } else {
          vm.totalPrevError = true;
        }
      }

      $scope.$applyAsync();

      if (
        vm.turnoverError ||
        vm.surfaceLocauxError ||
        vm.contenuLocauxError ||
        vm.leaderBirthdateError ||
        vm.endStatementDateError ||
        vm.chiffreAffaireCoursError ||
        vm.chiffreAffairePrevError ||
        vm.totalActifAnneeError ||
        vm.totalActifCoursError ||
        vm.totalActifPrevError ||
        vm.capitauxAnneeError ||
        vm.capitauxCoursError ||
        vm.capitauxPrevError ||
        vm.totalAnneeError ||
        vm.totalCoursError ||
        vm.totalPrevError ||
        vm.conjointNameError ||
        vm.conjointBirthdateError ||
        vm.conjointLastnameError ||
        vm.childrenNumbersError ||
        vm.capitauxPropresError ||
        vm.resultatNetError ||
        vm.legalFormError ||
        vm.bdmError
      ) {
        return true;
      }
    };

    // send verification code to login
    vm.verifyActivationCode = function() {
      if (vm.verifCode && vm.verifCode.length === 6) {
        WS.post('activateAccount', JSON.stringify({ token: vm.verifCode.replace(/ /g, '') }))
          .then(data => {
            if (data.data.redirect) {
              $window.location.href = data.data.redirect;
            }
          })
          .then(null, error => {
            vm.error = error.data.error;
            $scope.$applyAsync();
          });
      }
    };

    // save customQuote
    vm.saveCustomQuote = function() {
      if (document.forms['customQuoteForm'].checkValidity()) {
        WS.post('saveCustomQuote', JSON.stringify(vm.customQuote))
          .then(data => {
            if (data.data.message) {
              $window.location.href = 'custom-quote-check';
            }
          })
          .then(null, error => {
            vm.error = error.data.error;
            $scope.$applyAsync();
          });
      }
    };

    // save customQuote
    vm.chosenOfferIndex = function(value) {
      localStorage.setItem('chosenOfferIndex', value);
      $window.location.href = 'custom-quote';
    };

    vm.generateSinistersArrayCR = function(value) {
      let generatedArray = [];
      if (value) {
        if (parseInt(value) <= 0) {
          vm.sinsitersArrayCR = generatedArray;
          return;
        }
        for (let i = 0; i < parseInt(value); i++) {
          generatedArray.push(i);
        }
        vm.sinsitersNumberIntegerCR = parseInt(value);
        vm.sinsitersArrayCR = generatedArray;
        $scope.$applyAsync();
      } else {
        vm.sinsitersArrayCR = generatedArray;
        $scope.$applyAsync();
      }
    };

    vm.fractCrQuotation = function() {
      $scope.$watch('vm.chiffreAffaires', (newVal, oldVal) => {
        if (newVal !== oldVal) {
          (async () => {
            try {
              const taxValues = await calculateCrTax(
                WS,
                localStorage.getItem('chiffreAffairesTmp')
                  ? localStorage.getItem('chiffreAffairesTmp')
                  : localStorage.getItem('chiffreAffaires'),
                localStorage.getItem('activiteTmp')
                  ? localStorage.getItem('activiteTmp')
                  : localStorage.getItem('activite'),
                12,
                vm.effectDate,
              );
              vm.fractQuotationMonthly = parseFloat(taxValues.fractQuotation);
              vm.fractQuotationAnnual = parseFloat(taxValues.fractQuotation * 12);
              vm.chiffreAffaires = localStorage.getItem('chiffreAffairesTmp')
                ? localStorage.getItem('chiffreAffairesTmp')
                : localStorage.getItem('chiffreAffaires');
              if (vm.fraction === 1) {
                vm.fractQuotation = parseFloat(vm.fractQuotationMonthly * 12);
              } else if (vm.fraction === 2) {
                vm.fractQuotation = parseFloat(vm.fractQuotationMonthly * 6);
              } else if (vm.fraction === 4) {
                vm.fractQuotation = parseFloat(vm.fractQuotationMonthly * 3);
              } else {
                vm.fractQuotation = vm.fractQuotationMonthly;
              }

              vm.activite = localStorage.getItem('activiteTmp')
                ? localStorage.getItem('activiteTmp')
                : localStorage.getItem('activite');
              $scope.$applyAsync();
            } catch (error) {
              console.log(error);
            }
          })();
        }
      });
    };

    vm.simulationRedirect = function() {
      if (vm.offerTypePMRCR) {
        $window.location.href = '/simulation?offerType=PMRCR';
      } else if (vm.offerTypeCR) {
        $window.location.href = '/simulation?offerType=CR';
      } else if (vm.offerTypePMR) {
        $window.location.href = 'simulation?offerType=PMR';
      } else if (vm.offerTypeMD) {
        $window.location.href = '/simulation?offerType=MD';
      } else if (vm.offerTypeRCMS) {
        $window.location.href = '/simulation?offerType=RCMS';
      } else {
        vm.error = 'Vous devez choisir vos offres';
        $scope.$applyAsync();
      }
    };

    vm.setCancelReasonsError = function() {
      if (!vm.form.cancelReasonsSinister && !vm.form.cancelReasonsPayment) {
        vm.cancelReasonsError = 'Veuillez choisir un motif';
      } else {
        vm.cancelReasonsError = undefined;
      }
      $scope.$applyAsync();
    };

    vm.subscribeToNewsLetter = function() {
      WS.post('subscribeToNewsletter', { email: vm.newsletter.email })
        .then(data => {
          if (data.data.message) {
            localStorage.setItem('newsletterEmail', vm.newsletter.email);
            $('#popup-newsletter').modal('show');
          }
        })
        .then(null, error => {
          vm.error = error.data.error;
          $scope.$applyAsync();
        });
    };

    vm.unsubscribeToNewsLetter = function() {
      WS.post('unsubscribeFromNewsletter', { email: localStorage.getItem('newsletterEmail') })
        .then(data => {
          if (data.data.message) {
            vm.success = data.data.message;
            $scope.$applyAsync();
            setTimeout(() => {
              $('#popup-newsletter').modal('hide');
              vm.success = undefined;
            }, 2000);
          }
        })
        .then(null, error => {
          vm.error = error.data.error;
          $scope.$applyAsync();
        });
    };

    vm.continueSubscriptionFromRate = function() {
      localStorage.setItem('chosenMenu', 1);
      $window.location.href = 'adhesion-summary?offerType=CR';
    };

    vm.continueSubscriptionFromRateProfile = function(offerType) {
      localStorage.setItem('chosenMenu', 1);
      if (offerType === 'RC') {
        $window.location.href = 'adhesion-summary?offerType=CR';
      } else if (offerType === 'MRP') {
        $window.location.href = 'adhesion-summary?offerType=PMR';
      } else if (offerType === 'MRP + RC') {
        $window.location.href = 'adhesion-summary?offerType=PMRCR';
      } else if (offerType === 'MD') {
        $window.location.href = 'adhesion-summary?offerType=MD';
      } else if (offerType === 'RCMS') {
        $window.location.href = 'adhesion-summary?offerType=RCMS';
      }
    };

    vm.generateChildrenArray = function(value) {
      let generatedArray = [];
      if (value) {
        if (parseInt(value) <= 0) {
          vm.childrenArray = generatedArray;
          return;
        }
        for (let i = 0; i < parseInt(value); i++) {
          generatedArray.push(i);
        }
        vm.childrenNumberInteger = parseInt(value);
        vm.childrenArray = generatedArray;
        $scope.$applyAsync();
      } else {
        vm.childrenArray = generatedArray;
        $scope.$applyAsync();
      }
    };

    vm.partenariatParseInt = function(value) {
      return parseInt(value);
    };

    vm.isPartnershipFromHourSelected = function() {
      return vm.partnership.fromHour !== undefined;
    };

    vm.isPartnershipToHourSelected = function() {
      return vm.partnership.toHour !== undefined;
    };
  },
]);
