import { app } from '../app';
import moment from 'moment';
import {
  padNumbers,
  calculateDeadline,
  postToggleContinueLater,
  postToggleContinueLaterPopup,
  correctMonthlyCashQuotation,
  postUpdateAdhesion,
  getChosenPeriod,
  getChosenPeriodSliced,
  calculateRCMS,
  calculateRcms,
} from '../utils';

app.controller('SimulationRCMSController', [
  '$scope',
  '$window',
  '$http',
  'WS',
  function($scope, $window, $http, WS) {
    const vm = this;
    vm.form = {};
    vm.user = {};

    vm.form.civility = vm.form.civility ? vm.form.civility : 'monsieur';
    vm.form.capitauxPropres = vm.form.capitauxPropres ? vm.form.capitauxPropres : 'non';
    vm.form.resultatNet = vm.form.resultatNet ? vm.form.resultatNet : 'non';
    vm.form.type = vm.form.type ? vm.form.type : 'RCMS';
    vm.form.chosenMenu = localStorage.getItem('chosenMenu');
    vm.fraction = vm.fraction ? vm.fraction : 12;
    vm.effectDate = vm.effectDate
      ? vm.effectDate
      : moment()
          .utc()
          .utcOffset(2)
          .year(
            moment()
              .utc()
              .utcOffset(2)
              .year(),
          )
          .month(
            moment()
              .utc()
              .utcOffset(2)
              .month() + 1,
          )
          .date(1)
          .format('DD-MM-YYYY');
    vm.effectDateTmp = vm.effectDateTmp
      ? vm.effectDateTmp
      : moment()
          .add(1, 'days')
          .format('DD-MM-YYYY');
    vm.effectDateDay = vm.effectDateDay ? vm.effectDateDay : '01';
    vm.effectDateMonth = vm.effectDateMonth
      ? vm.effectDateMonth
      : (
          moment()
            .utc()
            .utcOffset(2)
            .year(
              moment()
                .utc()
                .utcOffset(2)
                .year(),
            )
            .month(
              moment()
                .utc()
                .utcOffset(2)
                .month() + 1,
            )
            .date(1)
            .month() + 1
        ).toString().length === 1
        ? `0${moment()
            .utc()
            .utcOffset(2)
            .year(
              moment()
                .utc()
                .utcOffset(2)
                .year(),
            )
            .month(
              moment()
                .utc()
                .utcOffset(2)
                .month() + 1,
            )
            .date(1)
            .month() + 1}`
        : (
            moment()
              .utc()
              .utcOffset(2)
              .year(
                moment()
                  .utc()
                  .utcOffset(2)
                  .year(),
              )
              .month(
                moment()
                  .utc()
                  .utcOffset(2)
                  .month() + 1,
              )
              .date(1)
              .month() + 1
          ).toString();
    vm.effectDateYear = vm.effectDateYear
      ? vm.effectDateYear
      : moment()
          .utc()
          .utcOffset(2)
          .year(
            moment()
              .utc()
              .utcOffset(2)
              .year(),
          )
          .month(
            moment()
              .utc()
              .utcOffset(2)
              .month() + 1,
          )
          .date(1)
          .year();
    vm.effectDates = [
      moment()
        .utc()
        .utcOffset(2)
        .date(1)
        .add(1, 'M')
        .format('DD-MM-YYYY'),
      moment()
        .utc()
        .utcOffset(2)
        .date(1)
        .add(2, 'M')
        .format('DD-MM-YYYY'),
      moment()
        .utc()
        .utcOffset(2)
        .date(1)
        .add(3, 'M')
        .format('DD-MM-YYYY'),
    ];
    vm.effectDates = vm.effectDates.map(e => {
      return { month: e.split('-')[1], year: e.split('-')[2] };
    });
    vm.deadlineDate = vm.deadlineDate
      ? vm.deadlineDate
      : calculateDeadline(vm.fraction, vm.effectDate).deadlineDate;
    vm.afterDeadlineDate = vm.afterDeadlineDate
      ? vm.afterDeadlineDate
      : calculateDeadline(vm.fraction, vm.effectDate).afterDeadlineDate;
    vm.showCityInput = vm.showCityInput ? vm.showCityInput : false;
    vm.form.type = vm.form.type ? vm.form.type : 'RCMS';

    $('#demande-sauvegarder').on('hidden.bs.modal', () => {
      postToggleContinueLaterPopup(WS, false, 'RCMS');
    });

    vm.authenticated = function() {
      WS.get('csrftoken').then(data => {
        localStorage.setItem('vetoptim-csrf-token', data.data._csrf);
      });

      WS.get('authenticated')
        .then(async data => {
          if (data.data.user) {
            //save rcms infos when connected
            if (localStorage.getItem('turnoverTmp')) {
              if (localStorage.getItem('tmpError')) {
                vm.error = localStorage.getItem('tmpError');
                localStorage.removeItem('turnoverTmp');
                localStorage.removeItem('tmpError');
              } else {
                if (parseInt(localStorage.getItem('turnoverTmp')) <= 1500000) {
                  WS.post(
                    'rcms',
                    Object.assign(
                      {},
                      {
                        turnover:
                          localStorage.getItem('turnoverTmp') === 0
                            ? undefined
                            : parseInt(localStorage.getItem('turnoverTmp')),
                        type: 'RCMS',
                      },
                    ),
                  )
                    .then(data => {
                      if (data.data.message) {
                        localStorage.removeItem('turnoverTmp');
                        vm.authenticated();
                      }
                    })
                    .then(null, error => {
                      vm.error = error.data.error;
                      $scope.$applyAsync();
                    });
                }
              }
            }

            vm.user = data.data;
            vm.email = data.data.user.email;
            // menu and info form

            vm.continuesLater = [];
            vm.rates = [];

            if (data.data.user.adhesion.length > 0) {
              if (data.data.user.adhesion.find(e => e.offerType === 'RCMS')) {
                vm.rates = data.data.user.adhesion.filter(rate => rate.rateFileName !== '');
                vm.continuesLater = data.data.user.adhesion.filter(
                  save => save.continueLater === true,
                );

                if (vm.continuesLater.length > 0) {
                  let lastContinueLaterDates = vm.continuesLater.map(last => last.updatedAt);
                  vm.lastContinueLaterDate = lastContinueLaterDates.reduce((last, value) =>
                    moment.max(moment(value), moment(last)),
                  );
                }

                if (data.data.user.adhesion.find(e => e.offerType === 'RCMS')) {
                  vm.chosenQuotation = data.data.user.adhesion.find(
                    e => e.offerType === 'RCMS',
                  ).chosenQuotation;
                  vm.cashQuotation = data.data.user.adhesion.find(
                    e => e.offerType === 'RCMS',
                  ).cashQuotation;
                  vm.firstPaymentType = data.data.user.adhesion.find(
                    e => e.offerType === 'RCMS',
                  ).firstPaymentType;
                  vm.bcUrl = `https://sherlock.staging.spicy.digital/call_request.php?chosenQuotation=${Math.round(
                    vm.chosenQuotation,
                  ) * 100}&user=${data.data.user.id}&offer=RCMS`;
                  vm.chosenPeriod = getChosenPeriod(
                    data.data.user.adhesion.find(e => e.offerType === 'RCMS').fraction,
                  );
                  vm.chosenPeriodSliced = getChosenPeriodSliced(
                    data.data.user.adhesion.find(e => e.offerType === 'RCMS').fraction,
                  );
                  vm.fraction =
                    data.data.user.adhesion.find(e => e.offerType === 'RCMS').fraction === 0
                      ? 12
                      : data.data.user.adhesion.find(e => e.offerType === 'RCMS').fraction;
                  vm.deadlineDate =
                    data.data.user.adhesion.find(e => e.offerType === 'RCMS').deadlineDate === ''
                      ? calculateDeadline(12, vm.effectDate).deadlineDate
                      : data.data.user.adhesion.find(e => e.offerType === 'RCMS').deadlineDate;
                  vm.afterDeadlineDate =
                    data.data.user.adhesion.find(e => e.offerType === 'RCMS').deadlineDate === ''
                      ? calculateDeadline(12, vm.effectDate).afterDeadlineDate
                      : data.data.user.adhesion.find(e => e.offerType === 'RCMS').afterDeadlineDate;
                  vm.effectDate =
                    data.data.user.adhesion.find(e => e.offerType === 'RCMS').effectDate === ''
                      ? vm.effectDate
                      : data.data.user.adhesion.find(e => e.offerType === 'RCMS').effectDate;
                  vm.effectDateDay = padNumbers(
                    moment(
                      new Date(
                        `${vm.effectDate.split('-')[2]}-${vm.effectDate.split('-')[1]}-${
                          vm.effectDate.split('-')[0]
                        }`,
                      ),
                    ).date(),
                  );
                  vm.effectDateMonth = padNumbers(
                    moment(
                      new Date(
                        `${vm.effectDate.split('-')[2]}-${vm.effectDate.split('-')[1]}-${
                          vm.effectDate.split('-')[0]
                        }`,
                      ),
                    ).month() + 1,
                  );
                  vm.effectDateYear = padNumbers(
                    moment(
                      new Date(
                        `${vm.effectDate.split('-')[2]}-${vm.effectDate.split('-')[1]}-${
                          vm.effectDate.split('-')[0]
                        }`,
                      ),
                    ).year(),
                  );

                  const isAfterEffectDate = moment(
                    new Date(`${vm.effectDateYear}-${vm.effectDateMonth}-${vm.effectDateDay}`),
                  ).isAfter(moment());
                  if (!isAfterEffectDate) {
                    vm.validateEffectDateRes = false;
                  } else {
                    vm.validateEffectDateRes = true;
                  }
                  const isValidEffectDate = moment(
                    `${vm.effectDateYear}-${vm.effectDateMonth}-${vm.effectDateDay}`,
                  ).isValid();
                  if (!isValidEffectDate) {
                    vm.validateEffectDateRes = false;
                  } else {
                    vm.validateEffectDateRes = true;
                  }

                  if (
                    data.data.user.adhesion.find(e => e.offerType === 'RCMS').continueLater &&
                    data.data.user.adhesion.find(e => e.offerType === 'RCMS').continueLaterShowPopup
                  ) {
                    setTimeout(() => {
                      $('#demande-sauvegarder').modal();
                    }, 3000);
                  }
                }
              }
            }

            if (data.data.user.leader.length > 0) {
              localStorage.setItem('turnover', parseInt(data.data.user.leader[0].turnover));

              vm.turnover =
                parseInt(localStorage.getItem('turnover')) === 0
                  ? undefined
                  : parseInt(localStorage.getItem('turnover'));
              vm.form.siren = data.data.user.leader[0].siren;
              vm.form.companyName = data.data.user.leader[0].companyName;
              if (data.data.user.leader[0].startActivityDate) {
                vm.form.startActivityDateDay = moment(
                  new Date(data.data.user.leader[0].startActivityDate),
                ).date();
                vm.form.startActivityDateMonth =
                  moment(new Date(data.data.user.leader[0].startActivityDate)).month() + 1;
                vm.form.startActivityDateYear = moment(
                  new Date(data.data.user.leader[0].startActivityDate),
                ).year();
              }
              vm.form.legalForm = data.data.user.leader[0].legalForm;
              vm.form.capitauxPropres = data.data.user.leader[0].capitauxPropres ? 'oui' : 'non';
              vm.form.resultatNet = data.data.user.leader[0].resultatNet ? 'oui' : 'non';

              vm.form.chiffreAffaireCours =
                data.data.user.leader[0].chiffreAffaireCours === 0
                  ? undefined
                  : data.data.user.leader[0].chiffreAffaireCours;
              vm.form.chiffreAffairePrev =
                data.data.user.leader[0].chiffreAffairePrev === 0
                  ? undefined
                  : data.data.user.leader[0].chiffreAffairePrev;
              vm.form.totalActifAnnee =
                data.data.user.leader[0].totalActifAnnee === 0
                  ? undefined
                  : data.data.user.leader[0].totalActifAnnee;
              vm.form.totalActifCours =
                data.data.user.leader[0].totalActifCours === 0
                  ? undefined
                  : data.data.user.leader[0].totalActifCours;
              vm.form.totalActifPrev =
                data.data.user.leader[0].totalActifPrev === 0
                  ? undefined
                  : data.data.user.leader[0].totalActifPrev;
              vm.form.capitauxAnnee =
                data.data.user.leader[0].capitauxAnnee === 0
                  ? undefined
                  : data.data.user.leader[0].capitauxAnnee;
              vm.form.capitauxCours =
                data.data.user.leader[0].capitauxCours === 0
                  ? undefined
                  : data.data.user.leader[0].capitauxCours;
              vm.form.capitauxPrev =
                data.data.user.leader[0].capitauxPrev === 0
                  ? undefined
                  : data.data.user.leader[0].capitauxPrev;
              vm.form.totalAnnee =
                data.data.user.leader[0].totalAnnee === 0
                  ? undefined
                  : data.data.user.leader[0].totalAnnee;
              vm.form.totalCours =
                data.data.user.leader[0].totalCours === 0
                  ? undefined
                  : data.data.user.leader[0].totalCours;
              vm.form.totalPrev =
                data.data.user.leader[0].totalPrev === 0
                  ? undefined
                  : data.data.user.leader[0].totalPrev;
            }

            if (data.data.user.address.length > 0) {
              vm.form.address = data.data.user.address[0].address;
              vm.form.postalCode = data.data.user.address[0].postalCode;
              vm.form.city = data.data.user.address[0].city;
              vm.showCityInput = true;
            }

            vm.form.civility = data.data.user.title === '' ? 'monsieur' : data.data.user.title;
            vm.form.firstName = data.data.user.name;
            vm.form.lastName = data.data.user.lastname;
            vm.form.phone = data.data.user.phone === '' ? undefined : data.data.user.phone;
            vm.form.mobile = data.data.user.mobile;
            vm.form.email = data.data.user.email;

            vm.chosenMenu = parseInt(localStorage.getItem('chosenMenu'));

            //paymentMethod form
            vm.paymentDay = 10;
            vm.paymentMethod = 'sepa';

            if (data.data.user.payment.length > 0) {
              vm.paymentDay = data.data.user.payment[0].pickingDate;
              vm.bic = data.data.user.payment[0].BIC;
              vm.iban = data.data.user.payment[0].IBAN;
              vm.holder = data.data.user.payment[0].holder;
              vm.exactTitle = data.data.user.payment[0].exactTitle;
              vm.ribImg = data.data.user.payment[0].ribImg;
              vm.isPayed = true;
            }

            if (data.data.user.firstPaymentCb.length > 0) {
              // vm.bic = data.data.user.firstPaymentCb[0].BIC;
              // vm.iban = data.data.user.firstPaymentCb[0].IBAN;
              // vm.holder = data.data.user.firstPaymentCb[0].holder;
              // vm.exactTitle = data.data.user.firstPaymentCb[0].exactTitle;
              vm.firstPaymentResponseCode = data.data.user.firstPaymentCb[0].responseCode;
              vm.firstPaymentOfferType = data.data.user.firstPaymentCb[0].offerType;
              if (vm.firstPaymentResponseCode === '00' && vm.firstPaymentOfferType === 'MD') {
                vm.paymentMethod = 'cb';
                vm.isPayed = true;
              }
            }

            vm.contracts = [];

            //transaction success
            if (data.data.user.contract.length > 0) {
              vm.contractRef =
                data.data.user.contract[data.data.user.contract.length - 1].reference;
              vm.leaderRef = data.data.user.leader[0].reference;
              vm.mandatRef = data.data.user.contract[data.data.user.contract.length - 1].sepaMndRef;
            }

            //information success page
            vm.mySpaceUpdatedTarget = localStorage.getItem('mySpaceUpdatedTarget');

            //help form
            if (data.data.user.help.length > 0) {
              vm.phone = data.data.user.help[0].phone;
              vm.timing = data.data.user.help[0].choice;
              vm.title = data.data.user.help[0].title;
              vm.name = data.data.user.help[0].name;
              vm.lastname = data.data.user.help[0].lastname;
              vm.helpEmail = data.data.user.help[0].email;
              vm.requestDateDay =
                moment(new Date(data.data.user.help[0].date)).date() >= 0 &&
                moment(new Date(data.data.user.help[0].date)).date() <= 9
                  ? '0' + moment(new Date(data.data.user.help[0].date)).date()
                  : moment(new Date(data.data.user.help[0].date))
                      .date()
                      .toString();
              vm.requestDateMonth =
                moment(new Date(data.data.user.help[0].date))
                  .add(1, 'months')
                  .month() >= 0 &&
                moment(new Date(data.data.user.help[0].date))
                  .add(1, 'months')
                  .month() <= 9
                  ? '0' +
                    moment(new Date(data.data.user.help[0].date))
                      .add(1, 'months')
                      .month()
                  : moment(new Date(data.data.user.help[0].date))
                      .add(1, 'months')
                      .month()
                      .toString();
              vm.requestDateYear = moment(new Date(data.data.user.help[0].date)).year();
              vm.requestDateHour = data.data.user.help[0].hour.split(':')[0];
              vm.requestDateMinutes = data.data.user.help[0].hour.split(':')[1];
            }

            //Contact form
            if (data.data.user.contact.length > 0) {
              vm.customer = data.data.user.contact[0].customer;
              vm.message = data.data.user.contact[0].message;
              vm.subject = data.data.user.contact[0].subject;
              vm.mobile = data.data.user.contact[0].mobile;
              vm.firstname = data.data.user.contact[0].firstname;
              vm.lastname = data.data.user.contact[0].lastname;
              vm.contactEmail = data.data.user.contact[0].email;
            }
            if (
              $window.location.pathname === '/menu' &&
              $window.location.search.split('?')[1].split('=')[1] === 'RCMS' &&
              vm.turnover === undefined
            ) {
              $window.location.href = 'simulation?offerType=RCMS';
            }
          } else {
            vm.turnover =
              localStorage.getItem('turnoverTmp') === null
                ? undefined
                : parseInt(localStorage.getItem('turnoverTmp'));
            if (
              $window.location.pathname === '/menu' &&
              $window.location.search.split('?')[1].split('=')[1] === 'RCMS' &&
              vm.turnover === undefined
            ) {
              $window.location.href = 'simulation?offerType=RCMS';
            }
            vm.chosenMenu = parseInt(localStorage.getItem('chosenMenu'));
            vm.email = localStorage.getItem('email');
            vm.registrationEmail = localStorage.getItem('registrationEmail');
          }

          $scope.$applyAsync();
        })
        .then(null, error => {
          console.log(error);
          vm.error = error.data.error;
          $scope.$applyAsync();
        });
    };

    vm.authenticated();

    // subscription path //
    // save rcms //
    vm.store = function() {
      if (vm.user.user) {
        if (vm.turnover && parseInt(vm.turnover.toString().replace(/\s/g, '')) <= 1500000) {
          WS.post(
            'rcms',
            Object.assign(
              {},
              {
                turnover: parseInt(vm.turnover.toString().replace(/\s/g, '')),
                type: 'RCMS',
              },
            ),
          )
            .then(data => {
              if (data.data.message) {
                $window.location.href = 'menu?offerType=RCMS';
              }
            })
            .then(null, error => {
              vm.error = error.data.error;
              $scope.$applyAsync();
            });
        } else {
          if ($scope.sim1Form.$valid) {
            $window.location.href = 'custom-rate?offerType=RCMS';
          }
        }
      } else {
        if (vm.turnover && parseInt(vm.turnover.toString().replace(/\s/g, '')) <= 1500000) {
          localStorage.setItem('turnoverTmp', parseInt(vm.turnover.toString().replace(/\s/g, '')));
          $window.location.href = 'menu?offerType=RCMS';
        } else {
          if ($scope.sim1Form.$valid) {
            localStorage.setItem('turnoverTmp', vm.turnover.toString().replace(/\s/g, ''));
            $window.location.href = 'custom-rate?offerType=RCMS';
          }
        }
      }
    };

    // Promo MD Quotation (menu)
    vm.rcmsSimQuotation = function() {
      WS.post('rcmsSimQuotation', Object.assign({}, {}))
        .then(data => {
          vm.rcmsQuotationAnnual = parseFloat(data.data.rcmsQuotation).toFixed(2);
          vm.rcmsQuotationMonthly = parseFloat(vm.rcmsQuotationAnnual / 12).toFixed(2);
          vm.authenticated();
          $scope.$applyAsync();
        })
        .then(null, error => {
          vm.error = error.data.error;
          $scope.$applyAsync();
        });
    };

    vm.fractRCMSQuotation = function() {
      $scope.$watch('vm.turnover', (newVal, oldVal) => {
        if (newVal !== oldVal) {
          (async () => {
            try {
              const taxValues = await calculateRcms(WS, vm.fractQuotation, 12, vm.effectDate);
              vm.fractQuotationMonthly = parseFloat(taxValues.fractQuotation);
              vm.fractQuotation = vm.fractQuotationMonthly;
              vm.cashQuotation = vm.cashQuotation
                ? vm.cashQuotation
                : correctMonthlyCashQuotation(
                    vm.deadlineDate,
                    vm.effectDate,
                    vm.fraction,
                    vm.fractQuotation,
                  ).cashQuotation;
              vm.fractQuotationAnnual = parseFloat(taxValues.fractQuotation * 12);
              vm.totTaxesFrais = parseFloat(taxValues.totTaxesFrais);
              vm.turnover = localStorage.getItem('turnoverTmp')
                ? localStorage.getItem('turnoverTmp')
                : localStorage.getItem('turnover');
              if (vm.fraction === 1) {
                vm.fractQuotation = parseFloat(vm.fractQuotationMonthly * 12);
              } else if (vm.fraction === 2) {
                vm.fractQuotation = parseFloat(vm.fractQuotationMonthly * 6);
              } else if (vm.fraction === 4) {
                vm.fractQuotation = parseFloat(vm.fractQuotationMonthly * 3);
              } else {
                vm.fractQuotation = vm.fractQuotationMonthly;
              }

              $scope.$applyAsync();
            } catch (error) {
              console.log(error);
            }
          })();
        }
      });
    };

    // update adhesion summary RCMS quotation
    vm.chosenFraction = function(fraction, quotation, effectDate) {
      (async () => {
        try {
          const taxValues = await calculateRCMS(WS, fraction, quotation, effectDate);
          vm.fractQuotation = parseFloat(quotation);
          vm.deadlineDate = calculateDeadline(fraction, effectDate).deadlineDate;
          vm.afterDeadlineDate = calculateDeadline(fraction, effectDate).afterDeadlineDate;
          vm.fraction = parseInt(fraction);
          vm.cashQuotation = correctMonthlyCashQuotation(
            vm.deadlineDate,
            effectDate,
            vm.fraction,
            vm.fractQuotation,
          ).cashQuotation;
          vm.totTaxesFrais = parseFloat(taxValues.totTaxesFrais);
          $scope.$applyAsync();
        } catch (error) {
          console.log(error);
        }
      })();
    };

    // update chosenMenu value
    vm.chooseMenu = function(value) {
      localStorage.setItem('chosenMenu', value);
      localStorage.setItem('type', 'RCMS');
      if (localStorage.getItem('chosenMenu') === '4') {
        $window.location.href = 'rappeler';
      } else {
        $window.location.href = 'info?offerType=RCMS';
      }
    };

    vm.getCityName = function() {
      vm.postalCodeError = undefined;
      if (vm.form.postalCode && vm.form.postalCode.length === 5) {
        WS.post('getCityName', JSON.stringify({ postalCode: vm.form.postalCode }))
          .then(data => {
            if (data.data.message) {
              vm.form.cities = data.data.message;
              vm.postalCodeError = undefined;
              vm.cityError = undefined;
              vm.showCityInput = false;
            }
            $scope.$applyAsync();
          })
          .then(null, error => {
            vm.postalCodeError = error.data.error;
            vm.form.city = undefined;
            vm.cityError = 'Veuillez saisir votre ville';
            $scope.$applyAsync();
          });
      }
    };

    vm.getSirenCoordinates = function() {
      if (vm.form.siren && vm.form.siren.length === 9) {
        WS.post(
          'getSirenCoordinates',
          JSON.stringify({
            siren:
              vm.form.siren[0] +
              vm.form.siren[1] +
              vm.form.siren[2] +
              ' ' +
              vm.form.siren[3] +
              vm.form.siren[4] +
              vm.form.siren[5] +
              ' ' +
              vm.form.siren[6] +
              vm.form.siren[7] +
              vm.form.siren[8],
          }),
        )
          .then(data => {
            if (data.data.message) {
              vm.form.siren = data.data.message.siren.replace(/ /g, '');
              vm.form.companyName = data.data.message.legalEntity;
              vm.form.address = data.data.message.address;
              vm.form.postalCode = data.data.message.postalCode;
              vm.showCityInput = true;
              vm.form.city = data.data.message.city;
              vm.form.phone = data.data.message.telephone;
              vm.form.email = data.data.message.email;
            }
            $scope.$applyAsync();
          })
          .then(null, error => {});
      }
    };

    // save form infos
    vm.saveInfos = function() {
      if (vm.form.infoTerm1 && vm.form.infoTerm2) {
        vm.error = undefined;
        if (document.getElementById('saveInfos').checkValidity()) {
          WS.post('subscriptionInfos', JSON.stringify(vm.form))
            .then(data => {
              if (data.data.redirect) {
                localStorage.setItem('email', vm.form.email);
                $window.location.href = data.data.redirect;
              }
            })
            .then(null, error => {
              vm.error = error.data.error;
              $scope.$applyAsync();
            });
        }
      } else {
        vm.error = 'Veuillez Accepter les termes';
      }
    };

    // get continue later email
    vm.getContinueLaterEmail = function() {
      WS.get('continueEmail', `offerType=${encodeURIComponent('RCMS')}`)
        .then(() => {})
        .then(null, error => {
          vm.error = error.data.error;
          $scope.$applyAsync();
        });
    };

    // continue subscription
    vm.continueSubscription = function(value) {
      if (value) {
        postToggleContinueLater(
          WS,
          false,
          $window,
          `menu?offerType=${localStorage.getItem('type')}`,
          getOfferTypeFr(localStorage.getItem('type')),
        );
        postToggleContinueLaterPopup(WS, false, getOfferTypeFr(localStorage.getItem('type')));
      } else {
        postToggleContinueLater(WS, false, $window, getOfferTypeFr(localStorage.getItem('type')));
        postToggleContinueLaterPopup(WS, false, getOfferTypeFr(localStorage.getItem('type')));
      }
    };

    // validate effectDate in adhesion page
    vm.validateEffectDate = function() {
      if (vm.effectDateDay && vm.effectDateMonth && vm.effectDateYear) {
        const isAfterEffectDate = moment(
          new Date(`${vm.effectDateYear}-${vm.effectDateMonth}-${vm.effectDateDay}`),
        ).isAfter(moment());
        vm.effectDate = moment()
          .add(1, 'days')
          .format('DD-MM-YYYY');
        const isValidEffectDate = moment(
          `${vm.effectDateYear}-${vm.effectDateMonth}-${vm.effectDateDay}`,
        ).isValid();
        if (!isAfterEffectDate) {
          document
            .getElementById('effectDateDay')
            .setCustomValidity(`La date d'effet doit être supérieur ou égale à ${vm.effectDate}`);
          document
            .getElementById('effectDateMonth')
            .setCustomValidity(`La date d'effet doit être supérieur ou égale à ${vm.effectDate}`);
          document
            .getElementById('effectDateYear')
            .setCustomValidity(`La date d'effet doit être supérieur ou égale à ${vm.effectDate}`);
          vm.validateEffectDateRes = false;
          return `La date d'effet doit être supérieur ou égale à ${vm.effectDate}`;
        } else if (!isValidEffectDate) {
          document
            .getElementById('effectDateDay')
            .setCustomValidity(`La date d'effet n'est pas valide`);
          document
            .getElementById('effectDateMonth')
            .setCustomValidity(`La date d'effet n'est pas valide`);
          document
            .getElementById('effectDateYear')
            .setCustomValidity(`La date d'effet n'est pas valide`);
          vm.validateEffectDateRes = false;
          return `La date d'effet n'est pas valide`;
        } else {
          document.getElementById('effectDateDay').setCustomValidity('');
          document.getElementById('effectDateMonth').setCustomValidity('');
          document.getElementById('effectDateYear').setCustomValidity('');
        }
        vm.effectDate = moment(
          new Date(`${vm.effectDateYear}-${vm.effectDateMonth}-${vm.effectDateDay}`),
        ).format('DD-MM-YYYY');
        vm.deadlineDate = calculateDeadline(vm.fraction, vm.effectDate).deadlineDate;
        vm.afterDeadlineDate = calculateDeadline(vm.fraction, vm.effectDate).afterDeadlineDate;
        vm.validateEffectDateRes = true;
        $scope.$applyAsync();
      }
    };

    // get adhesion summary
    vm.submitAdhesion = function() {
      if (vm.fraction) {
        $('#errorPeriod').remove();
        if (vm.chosenMenu === 1) {
          if (vm.form.infoTerm1 && vm.form.infoTerm2) {
            if (document.forms['adhesionForm'].checkValidity()) {
              postUpdateAdhesion(
                WS,
                'RCMS',
                vm.fraction,
                vm.effectDate,
                vm.fractQuotation,
                vm.deadlineDate,
                vm.afterDeadlineDate,
                vm.cashQuotation,
                vm.franchise,
                vm.turnover,
                vm.error,
                $scope,
              );
              if (vm.chosenMenu === 1) {
                $window.location.href = 'terms?offerType=RCMS';
              } else if (vm.chosenMenu === 2) {
                $window.location.href = 'quote?offerType=RCMS';
              } else if (vm.chosenMenu === 3) {
                $window.location.href = 'save?offerType=RCMS';
              } else {
                $window.location.href = 'rappeler';
              }
              localStorage.setItem('currentUrlPath', $window.location.pathname);
            }
          } else {
            vm.error = 'Veuillez Accepter les termes';
          }
        } else {
          if (document.forms['adhesionForm'].checkValidity()) {
            postUpdateAdhesion(
              WS,
              'RCMS',
              vm.fraction,
              vm.effectDate,
              vm.fractQuotation,
              vm.deadlineDate,
              vm.afterDeadlineDate,
              vm.cashQuotation,
              vm.franchise,
              vm.turnover,
              vm.error,
              $scope,
            );
            if (vm.chosenMenu === 1) {
              $window.location.href = 'terms?offerType=RCMS';
            } else if (vm.chosenMenu === 2) {
              $window.location.href = 'quote?offerType=RCMS';
            } else if (vm.chosenMenu === 3) {
              $window.location.href = 'save?offerType=RCMS';
            } else {
              $window.location.href = 'rappeler';
            }
            localStorage.setItem('currentUrlPath', $window.location.pathname);
          }
        }
      } else {
        $('html, body').animate(
          {
            scrollTop: $('div.periodicite').offset().top - 100,
          },
          500,
        );
        vm.errorPeriod = 'Veuillez choisir la périodicité';
      }
    };

    //save paymentMethod
    vm.savePaymentMethod = function() {
      const formdata = new FormData(document.getElementById('paymentForm'));
      if (
        document.getElementById('file').files.length > 0 &&
        document.getElementById('file').files[0].name &&
        vm.ribImg
      ) {
        formdata.append('updateRibFile', true);
      }
      (async () => {
        try {
          const { data } = await $http.post('/paymentInfos', formdata, {
            headers: { 'Content-Type': undefined },
            transformResponse: angular.identity,
          });
          if (JSON.parse(data).redirect) {
            $window.location.href = JSON.parse(data).redirect;
          }
        } catch (error) {
          vm.error = JSON.parse(error.data).error;
          $scope.$applyAsync();
        }
      })();
    };

    //download ribFile
    vm.downloadRibFile = function(ribFileName) {
      $window.open(`/downloadRib?filename=${ribFileName}`);
    };

    // go to transaction success page
    vm.goToTXSuccessPage = function() {
      if (vm.form.infoTerm1 && vm.form.infoTerm2) {
        $window.location.href = 'payment?offerType=RCMS';
      } else {
        vm.error = 'Veuillez Accepter les termes';
      }
    };

    // go to universign
    vm.signContract = function() {
      WS.get('getUniversignTransaction', 'type=RCMS')
        .then(data => {
          if (data.data.url) {
            $window.location.href = data.data.url;
          }
        })
        .then(null, error => {
          vm.error = error.data.error;
          $scope.$applyAsync();
        });
    };

    // --------------------------------------------------------------

    // rate path
    // get rate email
    vm.getRateEmail = function() {
      if (
        localStorage.getItem('currentUrlPath') &&
        localStorage.getItem('currentUrlPath') !== $window.location.pathname
      ) {
        WS.get('rateEmail', 'type=RCMS')
          .then(data => {
            if (data.data.rateFileName) {
              vm.rateFileNameGenerated = data.data.rateFileName;
              localStorage.setItem('rateFileNameGenerated', vm.rateFileNameGenerated);
              $scope.$applyAsync();
            }
          })
          .then(null, error => {
            vm.error = error.data.error;
            $scope.$applyAsync();
          });
      } else {
        vm.rateFileNameGenerated = localStorage.getItem('rateFileNameGenerated');
      }
      localStorage.setItem('currentUrlPath', $window.location.pathname);
    };

    // downloadRate
    vm.downloadRate = function(filename) {
      $window.open(`/downloadRate?filename=${filename}`);
    };

    vm.continueSubscriptionFromRate = function() {
      localStorage.setItem('chosenMenu', 1);
      $window.location.href = 'adhesion-summary?offerType=RCMS';
    };

    //saveCustomRate request
    vm.saveCustomRate = function() {
      if ($scope.customRateForm.$valid) {
        WS.post(
          'saveCustomRate',
          JSON.stringify({
            // turnover: parseInt(vm.chiffreAffaires.toString().replace(/\s/g, '')),
            firstname: vm.firstname,
            lastname: vm.lastname,
            phone: vm.phone,
            email: vm.requestEmail,
          }),
        ).then(data => {
          if (data.data.message) {
            $window.location.href = 'custom-rate-check?offerType=RCMS';
          }
        });
      }
    };
  },
]);
