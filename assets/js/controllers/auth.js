import { app } from '../app';
import { checkRedirectOnConnect } from '../utils';
import $ from 'jquery';

// Authentication Controller
app.controller('AuthController', [
  '$scope',
  'WS',
  '$window',
  '$http',
  function($scope, WS, $window, $http) {
    const vm = this;
    vm.user = {};

    if (localStorage.getItem('loginError')) {
      vm.loginError = localStorage.getItem('loginError');
      localStorage.removeItem('loginError');
    }

    vm.authenticated = function() {
      WS.get('csrftoken').then(data => {
        localStorage.setItem('vetoptim-csrf-token', data.data._csrf);
      });

      WS.get('authenticated')
        .then(data => {
          vm.user = data.data;
          if (data.data.user) {
            if (
              data.data.user.name &&
              data.data.user.lastname &&
              data.data.user.name.length + data.data.user.lastname.length >= 15
            ) {
              vm.user.user.name = vm.user.user.name[0] + '.';
            }
            if (data.data.user.adhesion.length > 0) {
              if (
                data.data.user.adhesion[0].continueLater &&
                data.data.user.adhesion[0].continueLaterShowPopup
              ) {
                setTimeout(() => {
                  $('#demande-sauvegarder').modal();
                }, 3000);
              }
            }
          }
          $scope.$applyAsync();
        })
        .then(null, error => {
          vm.error = error.data.error;
          $scope.$applyAsync();
        });
    };

    vm.authenticated();

    vm.login = function() {
      WS.post('login', vm.user)
        .then(async res => {
          if (res.data.success) {
            await checkRedirectOnConnect($window, $http);
          }
        })
        .then(null, error => {
          vm.loginError = error.data.message;
          $scope.$applyAsync();
        });
    };

    vm.register = function() {
      WS.post('register', {
        login: vm.user.emailr,
        password: vm.user.passwordr,
        email: vm.user.emailr,
        mobile: vm.user.mobiler,
      })
        .then(res => {
          if (res.data.message) {
            localStorage.setItem('registrationEmail', vm.user.emailr);
            $window.location.href = `mail-check${
              localStorage.getItem('vetoptim-offer-type')
                ? '?OfferType=' + localStorage.getItem('vetoptim-offer-type')
                : ''
            }`;
          }
        })
        .then(null, error => {
          vm.registerError = error.data.error;
          $scope.$applyAsync();
        });
    };

    vm.registerPopup = function() {
      if (vm.user.infoTerm1) {
        vm.error = undefined;
        if (document.getElementById('registerInfos').checkValidity()) {
          WS.post('register', {
            login: vm.user.emailr,
            password: vm.user.passwordr,
            email: vm.user.emailr,
            mobile: vm.user.mobiler,
          })
            .then(res => {
              if (res.data.message) {
                localStorage.setItem('registrationEmail', vm.user.emailr);
                $window.location.href = `mail-check${
                  localStorage.getItem('vetoptim-offer-type')
                    ? '?OfferType=' + localStorage.getItem('vetoptim-offer-type')
                    : ''
                }`;
              }
            })
            .then(null, error => {
              vm.registerError = error.data.error;
              $scope.$applyAsync();
            });
        }
      } else {
        vm.error = 'Veuillez saisir les champs et accepter les termes';
      }
    };

    vm.getResetPassword = function() {
      WS.get('resetPassword', 'email=' + vm.user.resetemail)
        .then(data => {
          if (data.data.message) {
            vm.resetSuccess = data.data.message;
            $scope.$apply();
          }
        })
        .then(null, error => {
          vm.resetError = error.data.error;
          $scope.$applyAsync();
        });
    };
  },
]);
