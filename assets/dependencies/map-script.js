/* --------------------------------------------
Google Map
-------------------------------------------- */

window.onload = MapLoadScript;
function GmapInit() {
  Gmap = $('.map-canvas');
  Gmap.each(function() {
    var $this = $(this),
      lat = '',
      lng = '',
      zoom = 12,
      scrollwheel = false,
      zoomcontrol = true,
      draggable = true,
      mapType = google.maps.MapTypeId.ROADMAP,
      title = '',
      contentString = '',
      theme_icon_path = $this.data('icon-path'),
      dataLat = $this.data('lat'),
      dataLng = $this.data('lng'),
      dataZoom = $this.data('zoom'),
      dataType = $this.data('type'),
      dataScrollwheel = $this.data('scrollwheel'),
      dataZoomcontrol = $this.data('zoomcontrol'),
      dataHue = $this.data('hue'),
      dataTitle = $this.data('title'),
      dataContent = $this.data('content');

    if (dataZoom !== undefined && dataZoom !== false) {
      zoom = parseFloat(dataZoom);
    }
    if (dataLat !== undefined && dataLat !== false) {
      lat = parseFloat(dataLat);
    }
    if (dataLng !== undefined && dataLng !== false) {
      lng = parseFloat(dataLng);
    }
    if (dataScrollwheel !== undefined && dataScrollwheel !== null) {
      scrollwheel = dataScrollwheel;
    }
    if (dataZoomcontrol !== undefined && dataZoomcontrol !== null) {
      zoomcontrol = dataZoomcontrol;
    }
    if (dataType !== undefined && dataType !== false) {
      if (dataType == 'satellite') {
        mapType = google.maps.MapTypeId.SATELLITE;
      } else if (dataType == 'hybrid') {
        mapType = google.maps.MapTypeId.HYBRID;
      } else if (dataType == 'terrain') {
        mapType = google.maps.MapTypeId.TERRAIN;
      }
    }
    if (dataTitle !== undefined && dataTitle !== false) {
      title = dataTitle;
    }
    if (navigator.userAgent.match(/iPad|iPhone|Android/i)) {
      draggable = false;
    }

    var mapOptions = {
      zoom: zoom,
      scrollwheel: scrollwheel,
      zoomControl: zoomcontrol,
      draggable: draggable,
      center: new google.maps.LatLng(lat, lng),
      mapTypeId: mapType,
    };
    var map = new google.maps.Map($this[0], mapOptions);

    //var image = 'images/icons/map-marker.png';
    var image = theme_icon_path;

    if (dataContent !== undefined && dataContent !== false) {
      contentString =
        '<div class="map-data">' +
        '<h6 class="color-blue">' +
        title +
        '</h6>' +
        '<div class="map-content">' +
        dataContent +
        '</div>' +
        '</div>';
    }
    var infowindow = new google.maps.InfoWindow({
      content: contentString,
    });

    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(lat, lng),
      map: map,
      icon: image,
      title: title,
    });
    if (dataContent !== undefined && dataContent !== false) {
      google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map, marker);
      });
    }

    if (dataHue !== undefined && dataHue !== false) {
      var styles = [
        {
          featureType: 'all',
          elementType: 'geometry.fill',
          stylers: [
            {
              weight: '2.00',
            },
          ],
        },
        {
          featureType: 'all',
          elementType: 'geometry.stroke',
          stylers: [
            {
              color: '#9c9c9c',
            },
          ],
        },
        {
          featureType: 'all',
          elementType: 'labels.text',
          stylers: [
            {
              visibility: 'on',
            },
          ],
        },
        {
          featureType: 'landscape',
          elementType: 'all',
          stylers: [
            {
              color: '#f2f2f2',
            },
          ],
        },
        {
          featureType: 'landscape',
          elementType: 'geometry.fill',
          stylers: [
            {
              color: '#ffffff',
            },
          ],
        },
        {
          featureType: 'landscape.man_made',
          elementType: 'geometry.fill',
          stylers: [
            {
              color: '#ffffff',
            },
          ],
        },
        {
          featureType: 'poi',
          elementType: 'all',
          stylers: [
            {
              visibility: 'off',
            },
          ],
        },
        {
          featureType: 'road',
          elementType: 'all',
          stylers: [
            {
              saturation: -100,
            },
            {
              lightness: 45,
            },
          ],
        },
        {
          featureType: 'road',
          elementType: 'geometry.fill',
          stylers: [
            {
              color: '#eeeeee',
            },
          ],
        },
        {
          featureType: 'road',
          elementType: 'labels.text.fill',
          stylers: [
            {
              color: '#7b7b7b',
            },
          ],
        },
        {
          featureType: 'road',
          elementType: 'labels.text.stroke',
          stylers: [
            {
              color: '#ffffff',
            },
          ],
        },
        {
          featureType: 'road.highway',
          elementType: 'all',
          stylers: [
            {
              visibility: 'simplified',
            },
          ],
        },
        {
          featureType: 'road.arterial',
          elementType: 'labels.icon',
          stylers: [
            {
              visibility: 'off',
            },
          ],
        },
        {
          featureType: 'transit',
          elementType: 'all',
          stylers: [
            {
              visibility: 'off',
            },
          ],
        },
        {
          featureType: 'water',
          elementType: 'all',
          stylers: [
            {
              color: '#46bcec',
            },
            {
              visibility: 'on',
            },
          ],
        },
        {
          featureType: 'water',
          elementType: 'geometry.fill',
          stylers: [
            {
              color: '#c8d7d4',
            },
          ],
        },
        {
          featureType: 'water',
          elementType: 'labels.text.fill',
          stylers: [
            {
              color: '#070707',
            },
          ],
        },
        {
          featureType: 'water',
          elementType: 'labels.text.stroke',
          stylers: [
            {
              color: '#ffffff',
            },
          ],
        },
      ];
      map.setOptions({ styles: styles });
    }
  });
}

function MapLoadScript() {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  GmapInit();
  document.body.appendChild(script);
}
