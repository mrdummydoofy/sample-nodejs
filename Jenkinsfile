pipeline {
  options {
    buildDiscarder logRotator(numToKeepStr: '5')
    disableConcurrentBuilds()
  }

  agent {
    kubernetes {
      cloud 'kubernetes'
      label "vetoptim"
      serviceAccount "build"
      yamlFile "k8s/vetoptim.yaml"
    }      
  }

  environment {
    image = "docker-registry.spicy.digital/vetoptim"
    dockerRegistry = "docker-registry.spicy.digital"

    slackChannel = "#builds"
    slackTeamDomain = "spicy-agency.slack.com"
    slackToken = credentials("slackToken")

    projectName = "vetoptim"
  }

  stages {

    stage("deploy to staging") {
      when {
        branch "devel"
      }
      steps {
        script {
          env.projectType = 'staging'
          env.deployUrl = "http://192.168.100.201:1001/"
        }
        container("docker") {
          sh 'docker rmi -f $(docker images -q -f dangling=true) | true'
          sh "docker image build . -t $image:staging -f ./Dockerfile.stg"
          withCredentials([usernamePassword(
              credentialsId: "docker-registry",
              usernameVariable: "USER",
              passwordVariable: "PASS"
          )]) {
              sh "docker login -u $USER -p $PASS $dockerRegistry"
          }
          sh "docker push $image:staging"
        }
        container("helm") {
          sh "helm del --purge vetoptim-staging | true"
          sh """helm upgrade -i \
            vetoptim-staging \
            helm/vetoptim \
            --namespace staging \
            --set image.tag=staging \
            --reuse-values"""
        }
        container("kubectl") {
          sh """kubectl -n staging \
            rollout status deployment \
            vetoptim-staging"""
        }
      }
    }

    stage("deploy to feature") {
      when {
          branch "feature/*"
      }
      steps {
        script {
          env.projectType = 'feature'
          env.deployUrl = "http://192.168.100.201:1002/"
        }
        container("docker") {
          sh 'docker rmi -f $(docker images -q -f dangling=true)  | true'
          sh "docker image build . -t $image:feature -f ./Dockerfile.ft"
          withCredentials([usernamePassword(
              credentialsId: "docker-registry",
              usernameVariable: "USER",
              passwordVariable: "PASS"
          )]) {
              sh "docker login -u $USER -p $PASS $dockerRegistry"
          }
          sh "docker push $image:feature"
        }
        container("helm") {
          sh "helm del --purge vetoptim-feature | true"
          sh """helm upgrade -i \
            vetoptim-feature \
            helm/vetoptim \
            --namespace feature \
            --set image.tag=feature \
            --reuse-values"""
        }
        container("kubectl") {
          sh """kubectl -n feature \
            rollout status deployment \
            vetoptim-feature"""
        }
      }
    }

    stage("deploy to prod") {
      when {
          branch "master"
      }
      steps {
        script {
          env.projectType = 'production'
          env.deployUrl = "http://192.168.100.201:1003/"
        }
        container("docker") {
          sh 'docker rmi -f $(docker images -q -f dangling=true) | true'
          sh "docker image build . -t $image:prod -f ./Dockerfile.prod"
          withCredentials([usernamePassword(
              credentialsId: "docker-registry",
              usernameVariable: "USER",
              passwordVariable: "PASS"
          )]) {
              sh "docker login -u $USER -p $PASS $dockerRegistry"
          }
          sh "docker push $image:prod"
        }
        container("helm") {
          sh "helm del --purge vetoptim-prod | true"
          sh """helm upgrade -i \
            vetoptim-prod \
            helm/vetoptim \
            --namespace prod \
            --set image.tag=prod \
            --reuse-values"""
        }
        container("kubectl") {
          sh """kubectl -n prod \
            rollout status deployment \
            vetoptim-prod"""
        }
      }
    }
    
  }

  post {
    success {
      slackSend(
        teamDomain: "${env.slackTeamDomain}",
        token: "${env.slackToken}",
        channel: "${env.slackChannel}",
        color: "good",
        message: "${env.projectName} ${env.projectType} deploy success: <${env.deployUrl}|Access service> - <${env.BUILD_URL}|Check build>"
      )
    }

    failure {
      slackSend(
        teamDomain: "${env.slackTeamDomain}",
        token: "${env.slackToken}",
        channel: "${env.slackChannel}",
        color: "danger",
        message: "${env.projectName} ${env.projectType} deploy failed: <${env.BUILD_URL}|Check build>"
      )
    }
  }
}