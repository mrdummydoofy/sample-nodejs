const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');

passport.serializeUser((user, cb) => {
  cb(null, user.id);
});

passport.deserializeUser((id, cb) => {
  User.findOne({ id })
    .populate('customer')
    .populate('address')
    .populate('adhesion')
    .populate('payment')
    .populate('contract')
    .populate('firstPaymentCb')
    .populate('contact')
    .populate('help')
    .populate('customRate')
    .populate('sinister')
    .populate('commercialContracts')
    .populate('leader')
    .exec(async (err, user) => {
      if (user) {
        delete user.password;
        delete user.isRegistred;
        delete user.isProspect;
        delete user.registrationToken;
        delete user.resetPasswordToken;
        if (user.firstPaymentCb.length > 0) {
          delete user.firstPaymentCb[0].createdAt;
          delete user.firstPaymentCb[0].updatedAt;
          delete user.firstPaymentCb[0].id;
          delete user.firstPaymentCb[0].transactionId;
          delete user.firstPaymentCb[0].transmissionDate;
          delete user.firstPaymentCb[0].paymentDate;
          delete user.firstPaymentCb[0].amount;
          delete user.firstPaymentCb[0].paymentCertificate;
          delete user.firstPaymentCb[0].authorisationId;
          delete user.firstPaymentCb[0].currencyCode;
          delete user.firstPaymentCb[0].cardNumber;
          delete user.firstPaymentCb[0].cvvFlag;
          delete user.firstPaymentCb[0].cvvResponseCode;
          delete user.firstPaymentCb[0].bankResponseCode;
          delete user.firstPaymentCb[0].cardValidity;
          delete user.firstPaymentCb[0].ipAddress;
          delete user.firstPaymentCb[0].cvvResponseCode;
        }
        user.contract = await Contract.find({
          user: user.id,
          signatureStatus: 'completed',
          contractStatus: 'active',
          isContract: true,
          isSigned: true,
        }).populate('quotation');
        user.leader = await Leader.find({ user: user.id }).populate('children');
        user.adhesion = await AdhesionTmp.find({ user: user.id })
          .populate('CRSinisterStatement')
          .populate('PMRSinisterStatement');
        user.commercialContracts = await Contract.find({
          commercial: user.id,
          signatureStatus: 'completed',
          contractStatus: 'active',
          isContract: true,
          isSigned: true,
        })
          .populate('user')
          .populate('quotation');
        for (let contract of user.commercialContracts) {
          if (contract.user) {
            contract.user = await User.findOne({ id: contract.user.id })
              .populate('customer')
              .populate('leader');
          }
        }
        user.sinister = await Sinister.find({ user: user.id }).populate('contract');
        for (let i = 0; i < user.sinister.length; i++) {
          if (user.sinister[i].contract) {
            user.sinister[i].contract = await Contract.findOne({
              id: user.sinister[i].contract.id,
            }).populate('quotation');
          }
        }
        if (user.isAdmin) {
          user.adminContracts = await Contract.find({
            signatureStatus: 'completed',
            contractStatus: 'active',
            isContract: true,
            isSigned: true,
          })
            .populate('user')
            .populate('quotation');
          for (let contract of user.adminContracts) {
            if (contract.user) {
              contract.user = await User.findOne({ id: contract.user.id })
                .populate('customer')
                .populate('leader');
            }
          }
          user.adminRates = await AdhesionTmp.find({
            rateFileName: { '!=': '' },
          });
        }
      }
      cb(err, user);
    });
});

passport.use(
  new LocalStrategy(
    {
      usernameField: 'login',
      passportField: 'password',
    },
    (login, password, cb) => {
      User.findOne({ login }, (err, user) => {
        if (err) {
          return cb(err);
        }
        if (!user) {
          return cb(null, false, { message: 'Login ou mot de passe invalide' });
        }

        bcrypt.compare(password, user.password, (err, res) => {
          if (err) {
            return cb(null, false, { message: err });
          }
          if (!res) {
            return cb(null, false, { message: 'Login ou mot de passe invalide' });
          }

          const userDetails = {
            email: user.email,
            login: user.login,
            isAdmin: user.isAdmin,
            id: user.id,
          };

          return cb(null, userDetails, { message: 'connexion réussite' });
        });
      });
    },
  ),
);
