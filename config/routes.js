/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {
  //  ╦ ╦╔═╗╔╗ ╔═╗╔═╗╔═╗╔═╗╔═╗
  //  ║║║║╣ ╠╩╗╠═╝╠═╣║ ╦║╣ ╚═╗
  //  ╚╩╝╚═╝╚═╝╩  ╩ ╩╚═╝╚═╝╚═╝

  /***************************************************************************
   *                                                                          *
   * Make the view located at `views/homepage.ejs` your home page.            *
   *                                                                          *
   * (Alternatively, remove this and add an `index.html` file in your         *
   * `assets` directory)                                                      *
   *                                                                          *
   ***************************************************************************/

  // Views
  '/': 'ViewController.index', // 'pages/index'
  '/page-introuvable': 'ViewController.pageIntrouvable', // 'pages/erreur-page'
  '/erreur-utilisateur': 'ViewController.userError', // 'pages/erreur-utilisateur'
  '/erreur-commercial': 'ViewController.commercialError', // 'pages/erreur-commercial'
  '/erreur-simulation': 'ViewController.simulationError', // 'pages/erreur-simulation'
  '/protection-activite': 'ViewController.protectionActivite', // 'pages/zoom/protection-activite'
  '/responsabilite-civile': 'ViewController.responsabiliteCivile', // 'pages/zoom/responsabilite-civile'
  '/multirisque-professionelles': 'ViewController.multirisqueProfessionelles', // 'pages/zoom/multirisque-professionelles'
  '/mrp-plus-rc': 'ViewController.MRPRC', // 'pages/zoom/mrp-plus-rc'
  '/mutuelle-entreprise-sante': 'ViewController.mutuelleEntrepriseSante', // 'pages/zoom/mutuelle-entreprise-sante'
  '/responsabilite-civile-dirigeants': 'ViewController.responsabiliteCivileDirigeants', // 'pages/zoom/responsabilite-civile-dirigeants'

  '/a-propos': 'ViewController.aPropos', // 'pages/informations/a-propos'
  '/contact': 'ViewController.contact', // 'pages/informations/contact'
  '/ogv': 'ViewController.ogv', // 'pages/informations/ogv'
  '/recrutement': 'ViewController.recrutement', // 'pages/informations/recrutement'
  '/responsabilite-societale': 'ViewController.responsabiliteSocietale', // 'pages/informations/responsabilite-societale'
  '/nos-engagements': 'ViewController.engagements', // 'pages/informations/nos-engagements'
  '/partenariat-bourgelat': 'ViewController.partenariatBourgelat', // 'pages/informations/partenariat-bourgelat'
  '/partenariat-bourgelat-send': 'ViewController.partenariatBourgelatSend', // 'pages/informations/partenariat-bourgelat-send'

  '/welcome': 'ViewController.welcome', // 'pages/welcome'
  '/register': 'ViewController.register', // 'pages/register'
  '/mail-check': 'ViewController.mailCheck', // 'pages/mail-check'
  '/process-mail-check': 'ViewController.processMailCheck', // 'pages/process-mail-check'
  '/login-check': 'ViewController.loginCheck', // 'pages/login-check'
  '/subscription-cancel': 'ViewController.subscriptionCancel', // 'pages/simulation/subscription-cancel'
  '/rappeler': 'ViewController.helpA', // 'pages/simulation/sim6a-help'
  '/succes-rappeler': 'ViewController.helpB', // 'pages/simulation/sim6b-help'
  '/custom-quote': 'ViewController.customQuote', // 'pages/simulation/custom-quote'
  '/custom-quote-check': 'ViewController.customQuoteCheck', // 'pages/simulation/custom-quote-check'

  // Espace Assuré
  '/journal': 'ViewController.profileJournal', // 'pages/profile/journal'
  '/informations': 'ViewController.profileInformations', // 'pages/profile/informations'
  '/informations-courtier': 'ViewController.profileBrokerInformations', // 'pages/profile/informations-courtier'
  '/informations-success': 'ViewController.profileInformationsSuccess', // 'pages/profile/success-update'
  '/modifer-password': 'ViewController.profilePasswordUpdate', // 'pages/profile/modifer-password'
  '/devis': 'ViewController.profileDevis', // 'pages/profile/devis'
  '/sinistres': 'ViewController.profileSinistres', // 'pages/profile/sinistres'
  '/declare-sinistre': 'ViewController.profileDeclareSinistre', // 'pages/profile/declare-sinistre'
  '/success-sinistre': 'ViewController.profileSinistreSuccess', // 'pages/profile/success-sinistre'
  '/contrats': 'ViewController.profileContrats', // 'pages/profile/contrats'
  '/supprimer-compte': 'ViewController.deleteAccountView', // 'pages/profile/supprimer-compte'
  '/supprimer-check': 'ViewController.deleteAccountCheckView', // 'pages/profile/supprimer-check'
  '/supprimer-succes': 'ViewController.deleteAccountSucceed', // 'pages/profile/supprimer-check'
  '/ajouter-contrat': 'ViewController.addContract', // 'pages/profile/supprimer-check'
  '/contrats-crees': 'ViewController.contractCreated', // 'pages/profile/supprimer-check'
  '/contrats-courtier': 'ViewController.commercialContractCreatedAdmin',
  '/commercial-subscription-success': 'ViewController.commercialSubscriptionSuccess', // 'pages/profile/commercial-subscription-success'

  '/offres': 'ViewController.simulation', // 'pages/simulation/simulation'

  // Simulation
  '/simulation': 'ViewController.sim1', // 'pages/simulation/sim1'
  '/custom-rate': 'ViewController.customRate', // 'pages/custom-rate'
  '/custom-rate-check': 'ViewController.customRateCheck', // 'pages/custom-rate'
  '/menu': 'ViewController.menu', // 'pages/simulation/sim2-menu'
  '/info': 'ViewController.info', // 'pages/simulation/sim3-info'
  '/adhesion-summary': 'ViewController.adhesionSummary', // 'pages/simulation/sim4-adhesion'
  '/payment': 'ViewController.payment', // 'pages/simulation/sim3b-subscribe'
  '/terms': 'ViewController.terms', // 'pages/simulation/sim3c-subscribe'
  '/sign-sepa': 'ViewController.signSepa', // 'pages/simulation/sim3d-subscribe-sepa'
  '/sign-bc': 'ViewController.signBc', // 'pages/simulation/sim3d-subscribe-bc'
  '/sign-summary': 'ViewController.signSummary', // 'pages/simulation/sim3e-subscribe'
  '/sign-cancel': 'ViewController.signCancel', // 'pages/simulation/sim3f-subscribe'
  '/quote': 'ViewController.quote', //'pages/simulation/sim4a-quote'
  '/save': 'ViewController.save', // 'pages/simulation/sim5a-save'
  '/payment-bc': 'ViewController.paymentBc', //'pages/simulation/sim-bc-payment'
  '/payment-bc-cancel': 'ViewController.paymentBcCancel', //'pages/simulation/sim-bc-payment-cancel'

  //BackOffice
  '/admin/index': 'ViewController.indexAdmin',
  '/admin/login': 'ViewController.loginAdmin',
  '/admin/users/index': 'ViewController.indexUsersAdmin',
  '/admin/users/create': 'ViewController.createUserAdmin',
  '/admin/users/update': 'ViewController.updateUserAdmin',

  /***************************************************************************
   *                                                                          *
   * More custom routes here...                                               *
   * (See https://sailsjs.com/config/routes for examples.)                    *
   *                                                                          *
   * If a request to a URL doesn't match any of the routes in this file, it   *
   * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
   * not match any of those, it is matched against static assets.             *
   *                                                                          *
   ***************************************************************************/

  //  ╔═╗╔═╗╦  ╔═╗╔╗╔╔╦╗╔═╗╔═╗╦╔╗╔╔╦╗╔═╗
  //  ╠═╣╠═╝║  ║╣ ║║║ ║║╠═╝║ ║║║║║ ║ ╚═╗
  //  ╩ ╩╩  ╩  ╚═╝╝╚╝═╩╝╩  ╚═╝╩╝╚╝ ╩ ╚═╝

  //  ╦ ╦╔═╗╔╗ ╦ ╦╔═╗╔═╗╦╔═╔═╗
  //  ║║║║╣ ╠╩╗╠═╣║ ║║ ║╠╩╗╚═╗
  //  ╚╩╝╚═╝╚═╝╩ ╩╚═╝╚═╝╩ ╩╚═╝

  //  ╔╦╗╦╔═╗╔═╗
  //  ║║║║╚═╗║
  //  ╩ ╩╩╚═╝╚═╝

  // Auth
  'POST /login': 'AuthController.login',
  '/logout': 'AuthController.logout',
  '/authenticated': 'AuthController.authenticated',
  '/csrfToken': { action: 'security/grant-csrf-token' },

  // Universign service
  '/getUniversignTransaction': 'UniversignController.getUniversignTransaction',
  '/transactionSuccess': 'UniversignController.transactionSuccess',
  '/commercialTransactionSuccess': 'UniversignController.commercialTransactionSuccess',
  '/transactionFailure': 'UniversignController.transactionFailure',
  '/commercialTransactionFailure': 'UniversignController.commercialTransactionFailure',
  '/transactionCancel': 'UniversignController.transactionCancel',
  '/commercialTransactionCancel': 'UniversignController.commercialTransactionCancel',

  // User
  'POST /register': 'UserController.postRegister',
  'POST /activateAccount': 'UserController.activateAccount',
  '/resetPassword': 'UserController.getResetPassword',
  'POST /resetPassword': 'UserController.postResetPassword',
  '/resendActivationMail': 'UserController.resendActivationEmail',
  '/resendProcessActivationMail': 'UserController.resendProcessActivationEmail',
  'POST /changePassword': 'UserController.changePassword',
  'POST /deleteAccount': 'UserController.deleteAccount',
  '/checkSignedContract': 'UserController.checkSignedContract',
  'POST /subscribeToNewsletter': 'UserController.subscribeToNewsletter',
  'POST /unsubscribeFromNewsletter': 'UserController.unsubscribeFromNewsletter',

  // Simulation CR
  'POST /cr': 'SimulationCRController.postCr',
  'POST /crQuotation': 'SimulationCRController.crQuotation',
  'POST /crUrbanTax': 'SimulationCRController.crUrbanTax',
  'POST /crRuralTax': 'SimulationCRController.crRuralTax',

  // Simulation PMR
  'POST /pmr': 'SimulationPMRController.postPmr',
  'POST /pmrQuotation': 'SimulationPMRController.pmrQuotation',
  'POST /pmrQuotationPromo': 'SimulationPMRController.pmrQuotationPromo',
  'POST /pmrWarranty': 'SimulationPMRController.pmrWarranty',
  'POST /pmrUrbanTax': 'SimulationPMRController.pmrUrbanTax',
  'POST /pmrRuralMixedTax': 'SimulationPMRController.pmrRuralMixedTax',

  // Simulation  PMRCR
  'POST /pmrcr': 'SimulationPMRCRController.postpmrCr',
  'POST /pmrCrQuotation': 'SimulationPMRCRController.pmrCrQuotation',
  'POST /pmrCrQuotationPromo': 'SimulationPMRCRController.pmrCrQuotationPromo',
  'POST /pmrCrWarranty': 'SimulationPMRCRController.pmrCrWarranty',
  'POST /pmrCrUrbanTax': 'SimulationPMRCRController.pmrCrUrbanTax',
  'POST /pmrCrRuralMixedTax': 'SimulationPMRCRController.pmrCrRuralMixedTax',

  // Simulation  MD
  'POST /md': 'SimulationMDController.postMd',
  'POST /mdQuotationPromo': 'SimulationMDController.mdQuotationPromo',
  'POST /mdQuotation': 'SimulationMDController.mdQuotation',

  // Simulation  RCMS
  'POST /rcms': 'SimulationRCMSController.postRcms',
  'POST /rcmsSimQuotation': 'SimulationRCMSController.rcmsSimQuotation',
  'POST /rcmsQuotation': 'SimulationRCMSController.rcmsQuotation',

  // Common Simulation
  'POST /subscriptionInfos': 'SimulationCommonController.subscriptionInfos',
  'POST /updateAdhesion': 'SimulationCommonController.updateAdhesion',
  'POST /paymentInfos': 'SimulationCommonController.paymentInfos',
  '/rateEmail': 'SimulationCommonController.rateEmail',
  '/continueEmail': 'SimulationCommonController.continueEmail',
  'POST /toggleContinueLater': 'SimulationCommonController.toggleContinueLaterOnce',
  'POST /toggleContinueLaterPopup': 'SimulationCommonController.toggleContinueLaterOncePopup',
  'POST /saveHelp': 'SimulationCommonController.saveHelp',
  'POST /saveCustomRate': 'SimulationCommonController.saveCustomRate',
  'POST /getCityName': 'SimulationCommonController.getCityName',
  'POST /getSirenCoordinates': 'SimulationCommonController.getSirenCoordinates',
  '/downloadRate': 'SimulationCommonController.downloadRate',
  '/downloadContract': 'SimulationCommonController.downloadContract',
  '/downloadRib': 'SimulationCommonController.downloadRibImg',
  'POST /saveContact': 'SimulationCommonController.saveContact',
  'POST /saveSinister': 'SimulationCommonController.saveSinister',
  'POST /savePaymentMethod': 'SimulationCommonController.savePaymentMethod',
  'POST /saveCustomQuote': 'SimulationCommonController.saveCustomQuote',
  'POST /savePartnership': 'SimulationCommonController.savePartnership',

  // Commercial Controller
  '/quotation': 'CommercialController.quotation',
  'POST /commercialCreateAdhesion': 'CommercialController.commercialCreateAdhesion',
  'POST /commercialSaveInfos': 'CommercialController.commercialSaveInfos',

  // Backoffice
  'POST /loginAdmin': 'AdminController.loginAdmin',
  '/logoutAdmin': 'AdminController.logoutAdmin',
  'POST /saveUser': 'AdminController.saveUser',
  'POST /updateUser': 'AdminController.updateUser',
  '/listUsers': 'AdminController.listUsers',

  // SEPA
  '/admin/dailyexport': 'SepaController.dailyExport',

  // Test
  '/contract-test': 'SimulationCommonController.testContractPdf',
};
