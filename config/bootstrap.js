/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs just before your Sails app gets lifted.
 * > Need more flexibility?  You can also do this by creating a hook.
 *
 * For more information on bootstrapping your app, check out:
 * https://sailsjs.com/config/bootstrap
 */

const path = require('path');
const fs = require('fs');
const crypto = require('crypto');
const bcrypt = require('bcrypt');

module.exports.bootstrap = async function(done) {
  // By convention, this is a good place to set up fake data during development.
  //
  // For example:
  // ```
  // // Set up fake development data (or if we already have some, avast)
  // if (await User.count() > 0) {
  //   return done();
  // }
  //
  // await User.createEach([
  //   { emailAddress: 'ry@example.com', fullName: 'Ryan Dahl', },
  //   { emailAddress: 'rachael@example.com', fullName: 'Rachael Shaw', },
  //   // etc.
  // ]);
  // ```

  // Don't forget to trigger `done()` when this bootstrap function's logic is finished.
  // (otherwise your server will never lift, since it's waiting on the bootstrap)

  // .permanent directory creation
  try {
    const childDirectories = ['ribFiles', 'contracts', 'rates'];
    !fs.existsSync(path.join(__dirname, '..', '.permanent')) &&
      fs.mkdirSync(path.join(__dirname, '..', '.permanent'));
    for (let i of childDirectories) {
      !fs.existsSync(path.join(__dirname, '..', '.permanent', i)) &&
        fs.mkdirSync(path.join(__dirname, '..', '.permanent', i));
    }
  } catch (error) {
    console.log(error);
  }

  // generates subscriptions for test only
  if (
    process.env.BASE_URL === 'https://recette.vetoptim.com/' ||
    process.env.NODE_ENV === undefined
  ) {
    let usersBatch = [];
    let addressesBatch = [];
    let paymentMethodBatch = [];
    let contractsBatch = [];
    let quotationsBatch = [];

    if ((await User.count()) === 0) {
      for (let i = 0; i < 24; i++) {
        let fraction = [1, 2, 4, 12][Math.floor(Math.random() * 4)];
        let effectDate = [
          '2018-01-01',
          '2018-02-01',
          '2018-03-01',
          '2018-04-01',
          '2018-05-01',
          '2018-06-01',
          '2018-07-01',
          '2018-08-01',
          '2018-09-01',
          '2018-10-01',
          '2018-11-01',
          '2018-12-01',
          '2019-01-01',
          '2019-02-01',
          '2019-03-01',
          '2019-04-01',
          '2019-05-01',
          '2019-06-01',
          '2019-07-01',
          '2019-08-01',
          '2017-09-01',
          '2018-10-01',
          '2018-11-01',
          '2018-12-01',
        ][i];

        usersBatch.push({
          title: ['monsieur', 'madame'][Math.floor(Math.random() * 2)],
          name: ['alain', 'isabelle', 'bruno', 'estelle', 'jean', 'philippe', 'jaques', 'pascal'][
            Math.floor(Math.random() * 8)
          ],
          lastname: [
            'giroud',
            'dupont',
            'chrisphoer',
            'delon',
            'adjani',
            'lelouche',
            'parmentier',
            'dupuis',
          ][Math.floor(Math.random() * 8)],
          email: `${crypto.randomBytes(3).toString('hex')}_vetoptim@mail.com`,
          mobile: crypto.randomBytes(5).readUInt32BE(0, true),
          phone: crypto.randomBytes(5).readUInt32BE(0, true),
          login: crypto.randomBytes(5).toString('hex'),
          password: bcrypt.hashSync('test', 10),
          isAdmin: false,
          isRegistred: true,
        });

        addressesBatch.push({
          address: crypto.randomBytes(5).toString('hex'),
          postalCode: ['75000', '13001', '69001', '38000'][Math.floor(Math.random() * 4)],
          city: ['paris', 'marseille', 'lyon', 'bordeaux'][Math.floor(Math.random() * 4)],
          user: i + 1,
        });

        paymentMethodBatch.push({
          pickingDate: 10,
          IBAN: [
            'FR7610096180880001234567844',
            'FR1420041010050500013M02606',
            'FR7502471467312461522158655',
            'FR5044120792630353840704984',
            'FR3255623646737995470926879',
            'FR7029019778568964647795762',
            'FR5424314634983385229273525',
            'FR0574737438362801733471174',
            'FR4361381140109241943866829',
            'FR3445273644928230779918501',
          ][Math.floor(Math.random() * 10)],
          holder: crypto.randomBytes(5).toString('hex'),
          exactTitle: crypto.randomBytes(5).toString('hex'),
          BIC: [
            'FR761009',
            'FR1420041010',
            'FR750247146731',
            'FR50441207926',
            'FR325562364673',
            'FR7029019778',
            'FR5424314634',
            'FR0574737438',
            'FR43613811401',
            'FR34452736449',
          ][Math.floor(Math.random() * 10)],
          user: i + 1,
        });

        contractsBatch.push({
          contractPlace: 'adistance',
          signatureDate: [
            '2018-01-16',
            '2018-02-14',
            '2018-03-11',
            '2018-04-16',
            '2018-05-21',
            '2018-06-17',
            '2018-07-06',
            '2018-08-23',
            '2018-09-18',
            '2018-10-09',
            '2018-11-14',
            '2018-12-31',
            '2019-01-14',
            '2019-02-28',
            '2019-03-31',
            '2019-04-16',
            '2019-05-21',
            '2019-06-17',
            '2019-07-06',
            '2019-08-23',
            '2019-09-18',
            '2019-10-09',
            '2019-11-14',
            '2019-12-14',
          ][i],
          effectDate,
          contractStatus: 'active',
          isContract: true,
          isSigned: true,
          signatureStatus: 'completed',
          reference: (16082809 + i) * 1000 + i,
          sepaMndRef: `VETOSEPA${(16082809 + i) * 1000 + 1}M01`,
          user: i + 1,
        });

        quotationsBatch.push({
          fraction,
          ttc: Math.floor(Math.random() * 2000),
          cashQuotation: Math.floor(Math.random() * 1500),
          offerType: ['MRP + RC', 'RC', 'MRP', 'RCMS', 'MD'][Math.floor(Math.random() * 5)],
          contract: i + 1,
        });
      }

      if ((await User.count()) === 0) {
        await User.createEach(usersBatch);
      }
      if ((await Address.count()) === 0) {
        await Address.createEach(addressesBatch);
      }
      if ((await PaymentMethod.count()) === 0) {
        await PaymentMethod.createEach(paymentMethodBatch);
      }
      if ((await Contract.count()) === 0) {
        await Contract.createEach(contractsBatch);
      }
      if ((await Quotation.count()) === 0) {
        await Quotation.createEach(quotationsBatch);
      }

      delete usersBatch;
      delete addressesBatch;
      delete paymentMethodBatch;
      delete contractsBatch;
      delete quotationsBatch;

      await User.create({
        login: 'user',
        password: 'user',
        email: 'test@test.tn',
        isRegistred: true,
      });
      await User.create({
        login: 'admin',
        password: 'admin',
        email: 'admin@test.tn',
        isRegistred: true,
        isAdmin: true,
      });
      await User.create({
        login: 'commercial',
        password: 'commercial',
        email: 'commercial@test.tn',
        isRegistred: true,
        isCommercial: true,
      });
    }
  }

  if (process.env.NODE_ENV === 'production') {
    return done();
  }

  if (!(await User.findOne({ email: 'contact@vetoptim.com' }))) {
    await User.create({
      login: 'RIGAUDC',
      password: 'Bmslan',
      email: 'contact@vetoptim.com',
      isRegistred: true,
      isCommercial: true,
    });
  }

  if ((await Cities.count()) === 0) {
    await Cities.createEach(
      JSON.parse(
        await require('util').promisify(fs.readFile)(
          path.join(__dirname, '..', 'json', 'cities.json'),
          'utf-8',
        ),
      ),
    );
  }

  if ((await Siren.count()) === 0) {
    await Siren.createEach(
      JSON.parse(
        await require('util').promisify(fs.readFile)(
          path.join(__dirname, '..', 'json', 'siren.json'),
          'utf-8',
        ),
      ).data,
    );
  }

  return done();
};
