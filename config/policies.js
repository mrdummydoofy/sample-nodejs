/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {
  /***************************************************************************
   *                                                                          *
   * Default policy for all controllers and actions, unless overridden.       *
   * (`true` allows public access)                                            *
   *                                                                          *
   ***************************************************************************/

  // '*': true,

  ViewController: {
    '*': ['isLoggedIn', 'isRegistred', 'isNotCommercial'],
    index: true,
    pageIntrouvable: true,
    protectionActivite: true,
    responsabiliteCivile: true,
    engagements: true,
    multirisqueProfessionelles: true,
    MRPRC: true,
    mutuelleEntrepriseSante: true,
    responsabiliteCivileDirigeants: true,
    register: true,
    mailCheck: true,
    processMailCheck: true,
    loginCheck: true,
    sim1: ['isNotCommercial'],
    customRate: ['isNotCommercial'],
    customRateCheck: ['isNotCommercial'],
    menu: ['isNotCommercial'],
    info: ['isNotCommercial'],
    helpA: ['isNotCommercial'],
    helpB: ['isNotCommercial'],
    aPropos: true,
    contact: true,
    ogv: true,
    partenariatBourgelat: true,
    partenariatBourgelatSend: true,
    recrutement: true,
    responsabiliteSocietale: true,
    deleteAccountSucceed: true,
    loginAdmin: true,
    subscriptionCancel: true,
    customQuote: true,
    customQuoteCheck: true,
    commercialError: true,
    userError: true,
    contractCreated: ['isLoggedIn', 'isRegistred', 'isNotUser'],
    addContract: ['isLoggedIn', 'isRegistred', 'isCommercial'],
    profileJournal: ['isLoggedIn', 'isRegistred'],
    commercialSubscriptionSuccess: ['isLoggedIn', 'isRegistred', 'isCommercial'],
    signCancel: ['isLoggedIn', 'isRegistred'],
    simulation: true,
  },

  UserController: {
    '*': ['isLoggedIn', 'isRegistred', 'isNotCommercial'],
    postRegister: true,
    activateAccount: true,
    getResetPassword: true,
    postResetPassword: true,
    resendProcessActivationEmail: true,
    resendActivationEmail: true,
    subscribeToNewsletter: true,
    unsubscribeFromNewsletter: true,
  },

  SimulationCRController: {
    '*': ['isLoggedIn', 'isRegistred', 'isNotCommercial'],
    crQuotation: true,
    crUrbanTax: true,
    crRuralTax: true,
  },

  SimulationPMRController: {
    '*': ['isLoggedIn', 'isRegistred', 'isNotCommercial'],
    pmrQuotationPromo: true,
    pmrQuotation: true,
    pmrWarranty: true,
    pmrUrbanTax: true,
    pmrRuralMixedTax: true,
  },

  SimulationPMRCRController: {
    '*': ['isLoggedIn', 'isRegistred', 'isNotCommercial'],
    pmrCrQuotationPromo: true,
    pmrCrQuotation: true,
    pmrCrWarranty: true,
    pmrCrUrbanTax: true,
    pmrCrRuralMixedTax: true,
  },

  SimulationMDController: {
    '*': ['isLoggedIn', 'isRegistred', 'isNotCommercial'],
    mdQuotationPromo: true,
  },

  SimulationRCMSController: {
    '*': ['isLoggedIn', 'isRegistred', 'isNotCommercial'],
    rcmsSimQuotation: true,
  },

  SimulationCommonController: {
    '*': ['isLoggedIn', 'isRegistred', 'isNotCommercial'],
    subscriptionInfos: true,
    saveCustomRate: true,
    getCityName: true,
    getSirenCoordinates: true,
    saveHelp: true,
    testContractPdf: true,
    saveContact: true,
    saveCustomQuote: true,
    savePartnership: true,
    downloadContract: ['isLoggedIn', 'isRegistred'],
  },

  AdminController: {
    '*': ['isLoggedIn', 'isRegistred', 'isAdmin', 'isNotCommercial'],
    loginAdmin: true,
  },

  UniversignController: {
    '*': ['isLoggedIn', 'isRegistred'],
    commercialTransactionSuccess: ['isLoggedIn', 'isRegistred', 'isCommercial'],
    commercialTransactionFailure: ['isLoggedIn', 'isRegistred', 'isCommercial'],
    commercialTransactionCancel: ['isLoggedIn', 'isRegistred', 'isCommercial'],
  },

  CommercialController: {
    '*': ['isLoggedIn', 'isRegistred', 'isCommercial'],
  },

  SepaController: {
    dailyExport: ['isLoggedIn', 'isRegistred', 'isAdmin'],
  },
};
