const PDFMerge = require('pdf-merge');
// const path = require('path');

// module.exports.mergeNotice = async pdfContractPath => {
//   const files = [
//     pdfContractPath,
//     path.join(__dirname, '..', '..', 'assets', 'pdf', 'pdf-test1.pdf'),
//     path.join(__dirname, '..', '..', 'assets', 'pdf', 'pdf-test2.pdf'),
//     path.join(__dirname, '..', '..', 'assets', 'pdf', 'pdf-test3.pdf'),
//     path.join(__dirname, '..', '..', 'assets', 'pdf', 'pdf-test4.pdf'),
//     path.join(__dirname, '..', '..', 'assets', 'pdf', 'pdf-test5.pdf'),
//   ];

//   const buffer = await PDFMerge(files);

//   return Buffer.from(buffer).toString('base64');
// };

module.exports.mergeSEPA = async (contractPath, sepaPath) => {
  const files = [contractPath, sepaPath];

  const buffer = await PDFMerge(files);

  return Buffer.from(buffer).toString('base64');
};
