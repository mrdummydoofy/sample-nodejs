const fs = require('fs');
const path = require('path');
const util = require('util');
const moment = require('moment');
const asyncWriteFile = util.promisify(fs.writeFile);

module.exports.build = async (sepaExportId, day) => {
  // get data from the db
  const sepaExport = await SepaExport.findOne({ id: sepaExportId });
  const sepaTransactions = await SepaTransaction.find({ sepaExport: sepaExportId }).populate(
    'contract',
  );
  const sepaTransactionsFirsts = sepaTransactions.filter(tx => tx.type === 'FRST');
  const sepaTransactionsRecurrents = sepaTransactions.filter(tx => tx.type === 'RCUR');
  let xml = '';
  let xmlResponse = '';

  let xmlHeader = `<?xml version="1.0" encoding="UTF-8"?>
<Document xmlns="urn:iso:std:iso:20022:tech:xsd:pain.008.001.02" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<CstmrDrctDbtInitn><GrpHdr><MsgId>${sepaExport.messageId}</MsgId>
<CreDtTm>${moment().format('YYYY-MM-DDThh:mm:ss')}</CreDtTm>
<NbOfTxs>${sepaExport.nbTransaction}</NbOfTxs>
<CtrlSum>${sepaExport.totalTransaction}</CtrlSum>
<InitgPty>
<Nm>Coverus Assurance</Nm>
<PstlAdr>
<PstCd>75020</PstCd>
<TwnNm>Paris</TwnNm>
<Ctry>FR</Ctry>
<AdrLine>39 Avenue Gambetta</AdrLine>
</PstlAdr>
<CtctDtls>
<Nm>Coverus Assurance</Nm>
<EmailAdr>contact@coverus.fr</EmailAdr>
</CtctDtls>
</InitgPty>
</GrpHdr>`;

  if (sepaTransactionsFirsts.length > 0) {
    xmlResponse = `<PmtInf>
<PmtInfId>${sepaExport.paymentInfoNew}</PmtInfId>
<PmtMtd>DD</PmtMtd>
<CtrlSum>${sepaExport.totalCashQuotation}</CtrlSum>
<PmtTpInf>
<SvcLvl>
<Cd>SEPA</Cd>
</SvcLvl>
<LclInstrm>
<Cd>CORE</Cd>
</LclInstrm>
<SeqTp>FRST</SeqTp>
</PmtTpInf>
<ReqdColltnDt>${moment()
      .add(2, 'days')
      .format('YYYY-MM-DD')}</ReqdColltnDt>
<Cdtr>
<Nm>Coverus Assurance</Nm>
<PstlAdr>
<PstCd>75020</PstCd>
<TwnNm>Paris</TwnNm>
<Ctry>FR</Ctry>
<AdrLine>39 Avenue Gambetta</AdrLine>
</PstlAdr>
<CtctDtls>
<Nm>Coverus Assurance</Nm>
<EmailAdr>contact@coverus.fr</EmailAdr>
</CtctDtls>
</Cdtr>
<CdtrAcct> 
<Id>
<IBAN>FR0330002008120000375336F33</IBAN>
</Id>
</CdtrAcct>
<CdtrAgt>
<FinInstnId>
<BIC>CRLYFRPP</BIC>
</FinInstnId>
</CdtrAgt>
<ChrgBr>SLEV</ChrgBr>
<CdtrSchmeId>
<Id>
<PrvtId>
<Othr>
<Id>FR56ZZZ83826F</Id>
<SchmeNm>
<Prtry>SEPA</Prtry>
</SchmeNm>
</Othr>
</PrvtId>
</Id>
</CdtrSchmeId>`;

    //xml += xmlResponse.replace(/((?!\n))/g, '');
    xml += xmlResponse;

    for (let i = 0; i < sepaTransactionsFirsts.length; i++) {
      xmlResponse = `<DrctDbtTxInf>
<PmtId>
<EndToEndId>${sepaTransactionsFirsts[i].endToEndId}</EndToEndId>
</PmtId>
<PmtTpInf>
</PmtTpInf>
<InstdAmt Ccy="EUR">${sepaTransactionsFirsts[i].amount}</InstdAmt>
<DrctDbtTx>
<MndtRltdInf>
<MndtId>${sepaTransactionsFirsts[i].contract.sepaMndRef}</MndtId>
<DtOfSgntr>${moment(sepaTransactionsFirsts[i].sigDate).format('YYYY-MM-DD')}</DtOfSgntr>
</MndtRltdInf>
</DrctDbtTx>
<DbtrAgt>
<FinInstnId>
<BIC>${sepaTransactionsFirsts[i].BIC}</BIC>
</FinInstnId>
</DbtrAgt>
<Dbtr>
<Nm>${sepaTransactionsFirsts[i].exactTitle}</Nm>
<PstlAdr>
<PstCd>${sepaTransactionsFirsts[i].postalCode}
</PstCd>
<TwnNm>${sepaTransactionsFirsts[i].city}</TwnNm>
<Ctry>FR</Ctry>
<AdrLine>${sepaTransactionsFirsts[i].address}</AdrLine>
</PstlAdr>
</Dbtr>
<DbtrAcct>
<Id>
<IBAN>${sepaTransactionsFirsts[i].IBAN}</IBAN>
</Id>
</DbtrAcct>
</DrctDbtTxInf>`;
      //xml += xmlResponse.replace(/((?!\n))/g, '');
      xml += xmlResponse;
    }
    xml += '</PmtInf>';
  }

  if (sepaTransactionsRecurrents.length > 0) {
    xmlResponse = `<PmtInf>
<PmtInfId>${sepaExport.paymentInfoRecurring}</PmtInfId>
<PmtMtd>DD</PmtMtd>
<CtrlSum>${sepaExport.totalQuotation}</CtrlSum>
<PmtTpInf>
<SvcLvl>
<Cd>SEPA</Cd>
</SvcLvl>
<LclInstrm>
<Cd>CORE</Cd>
</LclInstrm>
<SeqTp>RCUR</SeqTp>
</PmtTpInf>
<ReqdColltnDt>${moment()
      .add(2, 'days')
      .format('YYYY-MM-DD')}</ReqdColltnDt>
<Cdtr>
<Nm>Coverus Assurance</Nm>
<PstlAdr>
<PstCd>75020</PstCd>
<TwnNm>Paris</TwnNm>
<Ctry>FR</Ctry>
<AdrLine>39 Avenue Gambetta</AdrLine>
</PstlAdr>
<CtctDtls>
<Nm>Coverus Assurance</Nm>
<EmailAdr>contact@coverus.fr</EmailAdr>
</CtctDtls>
</Cdtr>
<CdtrAcct> 
<Id>
<IBAN>FR0330002008120000375336F33</IBAN>
</Id>
</CdtrAcct>
<CdtrAgt>
<FinInstnId>
<BIC>CRLYFRPP</BIC>
</FinInstnId>
</CdtrAgt>
<ChrgBr>SLEV</ChrgBr>
<CdtrSchmeId>
<Id>
<PrvtId>
<Othr>
<Id>FR56ZZZ83826F</Id>
<SchmeNm>
<Prtry>SEPA</Prtry>
</SchmeNm>
</Othr>
</PrvtId>
</Id>
</CdtrSchmeId>`;

    //xml += xmlResponse.replace(/((?!\n))/g, '');
    xml += xmlResponse;

    for (let i = 0; i < sepaTransactionsRecurrents.length; i++) {
      xmlResponse = `<DrctDbtTxInf>
<PmtId>
<EndToEndId>${sepaTransactionsRecurrents[i].endToEndId}</EndToEndId>
</PmtId>
<PmtTpInf>
</PmtTpInf>
<InstdAmt Ccy="EUR">${sepaTransactionsRecurrents[i].amount}</InstdAmt>
<DrctDbtTx>
<MndtRltdInf>
<MndtId>${sepaTransactionsRecurrents[i].contract.sepaMndRef}</MndtId>
<DtOfSgntr>${moment(sepaTransactionsRecurrents[i].sigDate).format('YYYY-MM-DD')}</DtOfSgntr>
</MndtRltdInf>
</DrctDbtTx>
<DbtrAgt>
<FinInstnId>
<BIC>${sepaTransactionsRecurrents[i].BIC}</BIC>
</FinInstnId>
</DbtrAgt>
<Dbtr>
<Nm>${sepaTransactionsRecurrents[i].exactTitle}</Nm>
<PstlAdr>
<PstCd>${sepaTransactionsRecurrents[i].postalCode}</PstCd>
<TwnNm>${sepaTransactionsRecurrents[i].city}</TwnNm>
<Ctry>FR</Ctry>
<AdrLine>${sepaTransactionsRecurrents[i].address}</AdrLine>
</PstlAdr>
</Dbtr>
<DbtrAcct>
<Id>
<IBAN>${sepaTransactionsRecurrents[i].IBAN}</IBAN>
</Id>
</DbtrAcct>
</DrctDbtTxInf>`;
      //xml += xmlResponse.replace(/((?!\n))/g, '');
      xml += xmlResponse;
    }
    xml += '</PmtInf>';
  }

  await asyncWriteFile(
    path.join(
      __dirname,
      '..',
      '..',
      '.permanent',
      'xml',
      `prelevements_${moment(day)
        .subtract(2, 'days')
        .format('YYYY-MM-DD')}.xml`,
    ),
    xmlHeader + xml + '\n\t</CstmrDrctDbtInitn>\n</Document>',
  );
};
