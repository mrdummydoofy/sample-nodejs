const path = require('path');
const moment = require('moment');
const fs = require('fs');

module.exports.sendActivationMail = function(obj, password) {
  sails.hooks.email.send(
    'activation',
    {
      login: obj.login,
      password,
      //verifUrl: sails.config.custom.baseUrl + '/activateAccount?token=' + obj.registrationToken,
      verifCode: obj.registrationToken,
    },
    {
      to: obj.email,
      subject: 'Activation Vetoptim',
    },
    err => {
      console.log(err || 'Activation Mail Sent!');
    },
  );
};

module.exports.sendProcessActivationMail = function(obj, password) {
  sails.hooks.email.send(
    'processActivation',
    {
      login: obj.login,
      password,
      //verifUrl: sails.config.custom.baseUrl + '/activateAccount?token=' + obj.registrationToken,
      verifCode: obj.registrationToken,
    },
    {
      to: obj.email,
      subject: 'Activation Vetoptim',
    },
    err => {
      console.log(err || 'Activation Mail Sent!');
    },
  );
};

module.exports.sendCommercialActivationMail = function(obj, password) {
  sails.hooks.email.send(
    'commercialAccountCreation',
    {
      login: obj.login,
      password,
    },
    {
      to: obj.email,
      subject: 'Création de compte Vetoptim',
    },
    err => {
      console.log(err || 'Account Creation Mail Sent!');
    },
  );
};

module.exports.sendResetPasswordMail = function(obj, admin) {
  sails.hooks.email.send(
    'reset',
    {
      name: obj.name,
      lastname: obj.lastname,
      verifUrl: sails.config.custom.baseUrl + '/resetPassword?token=' + obj.resetPasswordToken,
      admin,
    },
    {
      to: obj.email,
      subject: 'Changement Mot de passe Vetoptim',
    },
    err => {
      console.log(err || 'Reset Mail Sent!');
    },
  );
};

module.exports.sendResetSuccessMail = function(obj) {
  sails.hooks.email.send(
    'resetSuccess',
    {
      name: obj.name,
      lastname: obj.lastname,
    },
    {
      to: obj.email,
      subject: 'Mot de passe changé Vetoptim',
    },
    err => {
      console.log(err || 'Reset Password Success Mail Sent!');
    },
  );
};

module.exports.sendSubscriptionSuccessMail = function(
  obj,
  currentCustomerRef,
  contract,
  filesPaths,
  offerType,
  currentLeaderRef,
) {
  const attachments = [
    {
      path: path.join(__dirname, '..', '..', '.permanent', 'contracts', contract.contractFileName),
    },
    {
      path: filesPaths.vetoConsentPath,
    },
  ];

  if (offerType === 'RC') {
    attachments.push({
      path: require('path').join(
        __dirname,
        '..',
        '..',
        'assets',
        'pdf',
        'CG RC Entreprises P 014 BA 118 2018.pdf',
      ),
    });
  } else if (offerType === 'MRP') {
    attachments.push({
      path: require('path').join(__dirname, '..', '..', 'assets', 'pdf', 'CG MRP P 420 BA O18.pdf'),
    });
  } else if (offerType === 'MRP + RC') {
    attachments.push(
      {
        path: require('path').join(
          __dirname,
          '..',
          '..',
          'assets',
          'pdf',
          'CG RC Entreprises P 014 BA 118 2018.pdf',
        ),
      },
      {
        path: require('path').join(
          __dirname,
          '..',
          '..',
          'assets',
          'pdf',
          'CG MRP P 420 BA O18.pdf',
        ),
      },
    );
  } else if (offerType === 'RCMS') {
    attachments.push(
      {
        path: require('path').join(
          __dirname,
          '..',
          '..',
          'assets',
          'pdf',
          'CONTRAT HYALIN RCMS_012019_ASCO.PDF',
        ),
      },
      {
        path: require('path').join(
          __dirname,
          '..',
          '..',
          'assets',
          'pdf',
          'NAVG - IPID - HYALIN PRO RCMS.pdf',
        ),
      },
      {
        path: require('path').join(
          __dirname,
          '..',
          '..',
          'assets',
          'pdf',
          'PROTOCOLE_MODELE TYPE_MALESHERBES_2018.pdf',
        ),
      },
    );
  }
  sails.hooks.email.send(
    'subscriptionSuccess',
    {
      name: obj.name,
      lastname: obj.lastname,
      email: obj.email,
      contractReference: contract.reference,
      customerReference: currentCustomerRef,
      mndReference: contract.sepaMndRef,
      leaderReference: currentLeaderRef,
      offerType,
    },
    {
      to: obj.email,
      subject: 'Votre adhésion sur Vetoptim',
      attachments,
    },
    err => {
      console.log(err || 'Subscription Success Mail Sent!');
    },
  );
};

module.exports.sendCommercialSubscriptionSuccessMail = function(
  obj,
  currentCustomerRef,
  contract,
  req,
  currentLeaderRef,
  offerType,
  filesPaths,
) {
  const attachments = [
    {
      path: path.join(__dirname, '..', '..', '.permanent', 'contracts', contract.contractFileName),
    },
    {
      path: filesPaths.vetoConsentPath,
    },
  ];

  if (offerType === 'RC') {
    attachments.push({
      path: require('path').join(
        __dirname,
        '..',
        '..',
        'assets',
        'pdf',
        'CG RC Entreprises P 014 BA 118 2018.pdf',
      ),
    });
  } else if (offerType === 'MRP') {
    attachments.push({
      path: require('path').join(__dirname, '..', '..', 'assets', 'pdf', 'CG MRP P 420 BA O18.pdf'),
    });
  } else if (offerType === 'MRP + RC') {
    attachments.push(
      {
        path: require('path').join(
          __dirname,
          '..',
          '..',
          'assets',
          'pdf',
          'CG RC Entreprises P 014 BA 118 2018.pdf',
        ),
      },
      {
        path: require('path').join(
          __dirname,
          '..',
          '..',
          'assets',
          'pdf',
          'CG MRP P 420 BA O18.pdf',
        ),
      },
    );
  } else if (offerType === 'RCMS') {
    attachments.push(
      {
        path: require('path').join(
          __dirname,
          '..',
          '..',
          'assets',
          'pdf',
          'CONTRAT HYALIN RCMS_012019_ASCO.PDF',
        ),
      },
      {
        path: require('path').join(
          __dirname,
          '..',
          '..',
          'assets',
          'pdf',
          'NAVG - IPID - HYALIN PRO RCMS.pdf',
        ),
      },
      {
        path: require('path').join(
          __dirname,
          '..',
          '..',
          'assets',
          'pdf',
          'PROTOCOLE_MODELE TYPE_MALESHERBES_2018.pdf',
        ),
      },
    );
  }
  sails.hooks.email.send(
    'createSubscriptionSuccess',
    {
      name: obj.name,
      lastname: obj.lastname,
      email: obj.email,
      contractReference: contract.reference,
      customerReference: currentCustomerRef,
      mndReference: contract.sepaMndRef,
      leaderReference: currentLeaderRef,
      offerType,
    },
    {
      to: obj.email,
      bcc: [req.user.email, 'souscription@vetoptim.com'],
      subject: "Création d'adhésion sur Vetoptim",
      attachments: attachments,
    },
    err => {
      console.log(err || 'Commercial Subscription Success Mail Sent!');
    },
  );
};

module.exports.sendQuotationMail = function(
  obj,
  fileContent,
  fileName,
  adviceFileContent,
  adviceFileName,
  offerType,
) {
  const attachments = [
    {
      filename: fileName,
      content: fileContent,
    },
  ];
  if (offerType !== 'MD' && offerType !== 'RCMS') {
    attachments.push({
      filename: adviceFileName,
      content: adviceFileContent,
    });
  }
  if (offerType === 'RC') {
    attachments.push(
      {
        path: require('path').join(
          __dirname,
          '..',
          '..',
          'assets',
          'pdf',
          'CG RC Entreprises P 014 BA 118 2018.pdf',
        ),
      },
      {
        path: require('path').join(
          __dirname,
          '..',
          '..',
          'assets',
          'pdf',
          'IPID RC ENTREPRISE O18.pdf',
        ),
      },
    );
  } else if (offerType === 'MRP') {
    attachments.push(
      {
        path: require('path').join(
          __dirname,
          '..',
          '..',
          'assets',
          'pdf',
          'CG MRP P 420 BA O18.pdf',
        ),
      },
      {
        path: require('path').join(__dirname, '..', '..', 'assets', 'pdf', 'IPID MRP O18.pdf'),
      },
    );
  } else if (offerType === 'MRP + RC') {
    attachments.push(
      {
        path: require('path').join(
          __dirname,
          '..',
          '..',
          'assets',
          'pdf',
          'CG RC Entreprises P 014 BA 118 2018.pdf',
        ),
      },
      {
        path: require('path').join(
          __dirname,
          '..',
          '..',
          'assets',
          'pdf',
          'IPID RC ENTREPRISE O18.pdf',
        ),
      },
      {
        path: require('path').join(
          __dirname,
          '..',
          '..',
          'assets',
          'pdf',
          'CG MRP P 420 BA O18.pdf',
        ),
      },
      {
        path: require('path').join(__dirname, '..', '..', 'assets', 'pdf', 'IPID MRP O18.pdf'),
      },
    );
  }
  sails.hooks.email.send(
    'rate',
    {
      name: obj.name,
      lastname: obj.lastname,
    },
    {
      to: obj.email,
      subject: 'Votre devis sur Vetoptim',
      attachments,
    },
    err => {
      console.log(err || 'rate Mail Sent!');
    },
  );
};

module.exports.sendContinueMail = function(obj, adhesion) {
  sails.hooks.email.send(
    'continue',
    {
      user: obj,
      adhesion,
      calculatePeriod: Utils.calculatePeriod,
      currencyFormatter: require('currency-formatter'),
    },
    {
      to: obj.email,
      subject: 'Continuer plus tard dans Vetoptim',
    },
    err => {
      console.log(err || 'continue Later Mail Sent!');
    },
  );
};

module.exports.sendHelpMail = function(obj, help) {
  sails.hooks.email.send(
    'help',
    {
      user: obj,
      help,
    },
    {
      to: obj.email,
      subject: "Votre demande d'assistance sur Vetoptim",
    },
    err => {
      console.log(err || 'Help Mail Sent!');
    },
  );
};

module.exports.sendSupportMail = function(obj, moment, req) {
  sails.hooks.email.send(
    'support',
    {
      user: obj,
      moment,
      req,
    },
    {
      to: sails.config.custom.supportEmail,
      subject: 'Support Vetoptim',
    },
    err => {
      console.log(err || 'Support Mail Sent!');
    },
  );
};

module.exports.sendDeletedAccountMail = function(obj) {
  sails.hooks.email.send(
    'deletedAccount',
    {
      user: obj,
    },
    {
      to: obj.email,
      subject: 'Suppression du compte Vetoptim',
    },
    err => {
      console.log(err || 'Deleted Account Mail Sent!');
    },
  );
};

module.exports.sendInsurerMail = function(user, contract) {
  const attachments = [];
  attachments.push(
    {
      path: require('path').join(
        __dirname,
        '..',
        '..',
        '.permanent',
        'ribFiles',
        user.payment[0].ribImg,
      ),
    },
    {
      path: require('path').join(
        __dirname,
        '..',
        '..',
        '.permanent',
        'contracts',
        contract.contractFileName,
      ),
    },
  );
  sails.hooks.email.send(
    'insurer',
    {
      user,
      contract,
      currency: require('currency-formatter'),
    },
    {
      to: sails.config.custom.InsurerEmail,
      subject: 'Assureur Vetoptim',
      attachments,
    },
    err => {
      console.log(err || 'Insurer Mail Sent!');
    },
  );
};

module.exports.sendNewsletterMail = function(obj) {
  sails.hooks.email.send(
    'newsletter',
    {},
    {
      to: obj.email,
      subject: 'Inscription Newsletter Vetoptim',
    },
    err => {
      console.log(err || 'newsletter Mail Sent!');
    },
  );
};

module.exports.sendExcelMail = function(obj) {
  let attachments = [
    {
      path: path.join(
        __dirname,
        '..',
        '..',
        '.permanent',
        'excels',
        `prelevements_${moment(obj)
          .add(1, 'days')
          .format('YYYY-MM-DD')}.xlsx`,
      ),
    },
    {
      path: path.join(
        __dirname,
        '..',
        '..',
        '.permanent',
        'xml',
        `prelevements_${moment(obj)
          .add(1, 'days')
          .format('YYYY-MM-DD')}.xml`,
      ),
    },
  ];

  if (
    fs.existsSync(
      path.join(
        __dirname,
        '..',
        '..',
        '.permanent',
        'csv',
        `Europ_vetoptim_${moment(obj)
          .add(1, 'days')
          .format('YYYY-MM-DD')}.csv`,
      ),
    )
  ) {
    attachments.push({
      path: path.join(
        __dirname,
        '..',
        '..',
        '.permanent',
        'csv',
        `Europ_vetoptim_${moment(obj)
          .add(1, 'days')
          .format('YYYY-MM-DD')}.csv`,
      ),
    });
  }

  sails.hooks.email.send(
    'excelEmail',
    {},
    {
      to: sails.config.custom.exportMailTo,
      //bcc:'infra@spicy.digital',
      subject: `Adhésions du ${obj} et prélèvements récurrents d'aujourd'hui`,
      attachments: attachments,
    },
    err => {
      console.log(err || 'excel and xml sepa Mail Sent!');
    },
  );
};

module.exports.sendExcelEmptyMail = function(obj) {
  sails.hooks.email.send(
    'excelEmptyEmail',
    {},
    {
      to: sails.config.custom.exportMailTo,
      //bcc:'infra@spicy.digital',
      subject: `Adhésions du ${obj}`,
    },
    err => {
      console.log(err || 'empty Mail Sent!');
    },
  );
};

module.exports.sendPartnershipMail = function(data, support) {
  sails.hooks.email.send(
    'partnership',
    {
      data,
      support,
    },
    {
      to: support ? sails.config.custom.contactEmail : data.email,
      subject: 'Partenariat Bourgelat',
    },
    err => {
      console.log(err || 'partnership Mail Sent!');
    },
  );
};
