'use strict';

const jayson = require('jayson/promise');
const moment = require('moment');

const options = {
  host: 'sign.test.cryptolog.com',
  path: '/json/sign/rpc/',
  auth: 'slim.essafi@xeniusconsulting.com:20182018',
};

const _client = jayson.client.https(options);

function handleErrorInSuccessfulResponse(res) {
  if (!('error' in res)) {
    return;
  }
  //throw new Error(`Error received from server: ${res.error.message}`);
  throw new Error('Une erreur est survenue lors de la signature électronique');
}

module.exports.signDocument = async (docName, docBuffer, user, commercial, currentContractId) => {
  let request = {
    profile: 'default',
    successURL: `${sails.config.custom.baseUrl}/transactionSuccess`,
    failURL: `${sails.config.custom.baseUrl}/transactionFailure`,
    cancelURL: `${sails.config.custom.baseUrl}/transactionCancel`,
    documents: [
      {
        name: docName,
        content: `base64,${docBuffer}`,
      },
    ],
    signers: [
      {
        firstname: user.name,
        lastname: user.lastname,
        emailAddress: user.email,
        phoneNum: user.mobile,
      },
    ],
    certificateTypes: ['simple'],
    chainingMode: 'none',
    finalDocSent: true,
    finalDocRequesterSent: true,
    mustContactFirstSigner: true,
    description: 'Service de signature de contrats Vetoptim',

    language: 'fr',
  };

  if (commercial) {
    request.successURL = `${sails.config.custom.baseUrl}/commercialTransactionSuccess`;
    request.failURL = `${sails.config.custom.baseUrl}/commercialTransactionFailure`;
    request.cancelURL = `${sails.config.custom.baseUrl}/commercialTransactionCancel`;
  }

  const res = await _client.request('requester.requestTransaction', [request]);

  try {
    handleErrorInSuccessfulResponse(res);
  } catch (error) {
    console.log(error);
  }
  try {
    await Contract.update({ id: currentContractId }).set({ signatureId: res.result.id });
  } catch (error) {
    console.log(error);
  }

  return res;
};

module.exports.statusDocument = async (user, signatureId, currentContractId) => {
  /*
    update the signature status of a user contract
  */
  const res = await _client.request('requester.getTransactionInfo', [signatureId]);
  handleErrorInSuccessfulResponse(res);
  if (res.result.status === 'completed') {
    await Contract.update({ id: currentContractId }).set({
      isSigned: true,
      isContract: true,
      signatureStatus: 'completed',
      contractStatus: 'active',
      signatureDate: moment().format('YYYY-MM-DD hh:mm:ss'),
    });
  } else if (res.result.status === 'ready') {
    await Contract.update({ id: currentContractId }).set({ signatureStatus: 'ready' });
  } else if (res.result.status === 'canceled') {
    await Contract.update({ id: currentContractId }).set({ signatureStatus: 'canceled' });
  }
  return res.result.creationDate;
};

module.exports.statusDocumentAll = async signatureId => {
  /*
    return all info of a document
  */
  const res = await _client.request('requester.getTransactionInfo', [signatureId]);
  handleErrorInSuccessfulResponse(res);
  return res.result;
};
