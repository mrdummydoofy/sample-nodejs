const fs = require('fs');
const path = require('path');
const util = require('util');
const moment = require('moment');
const asyncWriteFile = util.promisify(fs.writeFile);

module.exports.build = async (contracts, day) => {
  let csv = '';

  for (let i = 0; i < contracts.length; i++) {
    const address = await Address.findOne({ user: contracts[i].user.id });
    let quality = null;

    if (contracts[i].user.title === 'monsieur') {
      quality = 'MR';
    }
    if (contracts[i].user.title === 'madame') {
      quality = 'MME';
    }

    if (i === contracts.length - 1) {
      csv += `RZ5;${contracts[i].id};1;ACT;EN;${moment().format('YYYYMMDD')};${moment(
        contracts[i].effectDate,
      ).format('YYYYMMDD')};${moment()
        .endOf('year')
        .add(1, 'months')
        .date(1)
        .format('YYYYMMDD')};000;1;${quality};${contracts[i].user.name};${
        contracts[i].user.lastname
      };;${address.address};;;${address.postalCode};Coverus Assurance ${address.city};France;${
        contracts[i].user.mobile
      };;;;;;;;;;;;;;${moment(contracts[i].effectDate).format('YYYYMMDD')};;999974;G;;;2;;1;`;
    } else {
      csv += `RZ5;${contracts[i].id};1;ACT;EN;${moment().format('YYYYMMDD')};${moment(
        contracts[i].effectDate,
      ).format('YYYYMMDD')};${moment()
        .endOf('year')
        .add(1, 'months')
        .date(1)
        .format('YYYYMMDD')};000;1;${quality};${contracts[i].user.name};${
        contracts[i].user.lastname
      };;${address.address};;;${address.postalCode};Coverus Assurance ${address.city};France;${
        contracts[i].user.mobile
      };;;;;;;;;;;;;;${moment(contracts[i].effectDate).format('YYYYMMDD')};;999974;G;;;2;;1;\n`;
    }
  }

  await asyncWriteFile(
    path.join(
      __dirname,
      '..',
      '..',
      '.permanent',
      'csv',
      `Europ_vetoptim_${moment(day)
        .subtract(2, 'days')
        .format('YYYY-MM-DD')}.csv`,
    ),
    csv,
  );
};
