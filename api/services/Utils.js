const rp = require('request-promise');
const moment = require('moment');
const path = require('path');
const currency = require('currency-formatter');

module.exports.validateIBAN = async iban => {
  const res = await rp(`https://www.ibanbic.be/IBANBIC.asmx/controleIBAN?iban=${iban}`);
  if (res.match(/true/i) !== null) {
    return true;
  } else {
    return false;
  }
};

module.exports.signDocumentProcess = async (
  contractValues,
  user,
  res,
  commercial,
  req,
  commercialInfos,
) => {
  let template = null;
  const templateSEPA = path.join(__dirname, '..', '..', 'views', 'pdf', 'vetoSEPA.ejs');
  let currentAdhesionByType = null;

  if (commercialInfos && commercialInfos.adhesion.length === 0) {
    currentAdhesionByType = [
      Object.assign({}, contractValues.addedQuotation, contractValues.addedContract),
    ];
  } else {
    currentAdhesionByType = req.user.adhesion.filter(
      adhesion => adhesion.offerType === contractValues.addedQuotation.offerType,
    );
  }

  if (contractValues.addedQuotation.offerType === 'MRP') {
    template = path.join(__dirname, '..', '..', 'views', 'pdf', 'contractMRP.ejs');
  } else if (contractValues.addedQuotation.offerType === 'RC') {
    template = path.join(__dirname, '..', '..', 'views', 'pdf', 'contractRC.ejs');
  } else if (contractValues.addedQuotation.offerType === 'MRP + RC') {
    template = path.join(__dirname, '..', '..', 'views', 'pdf', 'contractMRPRC.ejs');
  }

  const pdfPath = path.join(
    __dirname,
    '..',
    '..',
    '.permanent',
    'contracts',
    `contrat_adhesion_vetoptim_0000${contractValues.addedContract.id}_${user.name}_${
      user.lastname
    }.pdf`,
  );

  const vetoSEPAPath = path.join(
    __dirname,
    '..',
    '..',
    '.permanent',
    'contracts',
    `sepa_vetoptim_0000${contractValues.addedContract.id}_${user.name}_${user.lastname}.pdf`,
  );

  if (
    contractValues.addedQuotation.offerType !== 'MD' &&
    contractValues.addedQuotation.offerType !== 'RCMS'
  ) {
    await PdfExport.generate(
      template,
      {
        adhesion: currentAdhesionByType[0],
        user: req.user,
        moment: moment,
        period: Utils.calculatePeriod,
        currency: require('currency-formatter'),
        Warranty: Warranty,
        Quotation: Quotation,
        indiceFFB: '988,1 €',
        contractRef: 'pas encore disponible',
        customerRef: 'pas encore disponible',
        tauxChiffreAffaires: 0.055,
      },
      pdfPath,
    );
  } else if (contractValues.addedQuotation.offerType === 'MD') {
    await Utils.generateMDContract(
      req,
      currentAdhesionByType,
      `contrat_adhesion_vetoptim_0000${contractValues.addedContract.id}_${user.name}_${
        user.lastname
      }.pdf`,
      'pas encore disponible',
      commercial,
      commercialInfos,
    );
  } else if (contractValues.addedQuotation.offerType === 'RCMS') {
    await Utils.generateRCMSContract(
      req,
      currentAdhesionByType,
      `contrat_adhesion_vetoptim_0000${contractValues.addedContract.id}_${user.name}_${
        user.lastname
      }.pdf`,
      'pas encore disponible',
      commercial,
    );
  }

  await PdfExport.generate(
    templateSEPA,
    {
      contract: contractValues.addedContract,
      user: req.user,
      moment,
      adhesion: currentAdhesionByType[0],
    },
    vetoSEPAPath,
  );

  try {
    let sign = null;
    let pdfBuffer = null;
    if (contractValues.addedQuotation.offerType !== 'MD') {
      pdfBuffer = await PdfMerge.mergeSEPA(pdfPath, vetoSEPAPath);
    } else {
      pdfBuffer = await require('util').promisify(require('fs').readFile)(pdfPath, 'base64');
    }
    await Contract.update(
      { id: contractValues.addedContract.id },
      {
        contractFileName: `contrat_adhesion_vetoptim_0000${contractValues.addedContract.id}_${
          user.name
        }_${user.lastname}.pdf`,
        sepaFileName: `sepa_vetoptim_0000${contractValues.addedContract.id}_${user.name}_${
          user.lastname
        }.pdf`,
      },
    );
    if (commercial) {
      sign = await Universign.signDocument(
        `Contrat ${Utils.getOfferTypeFullNameFR(contractValues.addedQuotation.offerType)}`,
        pdfBuffer,
        user,
        commercial,
        contractValues.addedContract.id,
      );
    } else {
      req.session.currentContractId = contractValues.addedContract.id;
      sign = await Universign.signDocument(
        `Contrat ${Utils.getOfferTypeFullNameFR(contractValues.addedQuotation.offerType)}`,
        pdfBuffer,
        user,
        undefined,
        contractValues.addedContract.id,
      );
    }
    await Universign.statusDocument(user, sign.result.id, contractValues.addedContract.id);
    res.status(200).json({ url: sign.result.url, id: sign.id });
  } catch (error) {
    if (error) {
      console.log(error);
      res.status(400).json({ error });
    }
  }
};

module.exports.createContract = async (user, offerType) => {
  const addedContract = await Contract.create({
    contractPlace: 'adistance',
    creationDate: moment().format('YYYY-MM-DD hh:mm:ss'),
    effectDate: user.adhesion.find(e => e.offerType === offerType).effectDate,
    deadlineDate: user.adhesion.find(e => e.offerType === offerType).deadlineDate,
    afterDeadlineDate: user.adhesion.find(e => e.offerType === offerType).afterDeadlineDate,
    firstPaymentType: user.adhesion.find(e => e.offerType === offerType).firstPaymentType,
    user: user.id,
  }).fetch();
  const addedWarranty = await Warranty.create({
    status: true,
    franchise: user.customer[0].franchise,
    contract: addedContract.id,
  }).fetch();
  const addedQuotation = await Quotation.create({
    fraction: user.adhesion.find(e => e.offerType === offerType).fraction,
    tarifType: user.customer[0].activity,
    period: Utils.calculatePeriod(user.adhesion.find(e => e.offerType === offerType).fraction),
    ttc: user.adhesion.find(e => e.offerType === offerType).chosenQuotation,
    tax: Utils.getContractTaxFromOffer(offerType, user),
    net:
      user.adhesion.find(e => e.offerType === offerType).chosenQuotation -
      Utils.getContractTaxFromOffer(offerType, user),
    cashQuotation: user.adhesion.find(e => e.offerType === offerType).cashQuotation,
    offerType: user.adhesion.find(e => e.offerType === offerType).offerType,
    contract: addedContract.id,
  }).fetch();

  return { addedContract, addedWarranty, addedQuotation };
};

module.exports.updateContract = async (user, offerType) => {
  // update contract with new values
  const addedContract = await Contract.update(
    { id: contract[0].id },
    {
      contractPlace: 'adistance',
      creationDate: moment().format('YYYY-MM-DD hh:mm:ss'),
      effectDate: user.adhesion.find(e => e.offerType === offerType).effectDate,
      deadlineDate: user.adhesion.find(e => e.offerType === offerType).deadlineDate,
      afterDeadlineDate: user.adhesion.find(e => e.offerType === offerType).afterDeadlineDate,
      firstPaymentType: user.adhesion.find(e => e.offerType === offerType).firstPaymentType,
      user: user.id,
    },
  ).fetch();
  const addedWarranty = await Warranty.update(
    { id: contract[0].id },
    {
      status: true,
      franchise: user.customer[0].franchise,
      contract: updatedContract.id,
    },
  ).fetch();
  const addedQuotation = await Quotation.update(
    { id: contract[0].id },
    {
      fraction: user.adhesion.find(e => e.offerType === offerType).fraction,
      tarifType: user.customer[0].activity,
      period: Utils.calculatePeriod(user.adhesion.find(e => e.offerType === offerType).fraction),
      ttc: user.adhesion.find(e => e.offerType === offerType).chosenQuotation,
      tax: Utils.getContractTaxFromOffer(offerType, user),
      net:
        user.adhesion.find(e => e.offerType === offerType).chosenQuotation -
        Utils.getContractTaxFromOffer(offerType, user),
      cashQuotation: user.adhesion.find(e => e.offerType === offerType).cashQuotation,
      offerType: user.adhesion.find(e => e.offerType === offerType).offerType,
      contract: updatedContract.id,
    },
  ).fetch();
  return { addedContract, addedWarranty, addedQuotation };
};

module.exports.calculatePeriod = fraction => {
  if (fraction === 1) {
    return 'annuelle';
  } else if (fraction === 2) {
    return 'semestrielle';
  } else if (fraction === 4) {
    return 'trimestrielle';
  } else {
    return 'mensuelle';
  }
};

module.exports.getOfferTypeFr = offerType => {
  if (offerType === 'CR') {
    return 'RC';
  } else if (offerType === 'PMR') {
    return 'MRP';
  } else if (offerType === 'PMRCR') {
    return 'MRP + RC';
  } else if (offerType === 'MD') {
    return 'MD';
  } else if (offerType === 'RCMS') {
    return 'RCMS';
  }
};

module.exports.getOfferTypeEng = offerType => {
  if (offerType === 'RC') {
    return 'CR';
  } else if (offerType === 'MRP') {
    return 'PMR';
  } else if (offerType === 'MRP + RC') {
    return 'PMRCR';
  } else if (offerType === 'MD') {
    return 'MD';
  } else if (offerType === 'RCMS') {
    return 'RCMS';
  }
};

module.exports.getOfferTypeFullNameFR = offerType => {
  if (offerType === 'RC') {
    return 'RESPONSABILITÉ CIVILE PROFESSIONNELLE';
  } else if (offerType === 'MRP') {
    return 'MULTIRISQUE PROFESSIONNELLE';
  } else if (offerType === 'MRP + RC') {
    return 'MULTIRISQUE + RC PRO';
  } else if (offerType === 'MD') {
    return 'MUTUELLE DES DIRIGEANTS NON SALARIÉS';
  } else if (offerType === 'RCMS') {
    return 'RÉSPONSABILITÉ CIVILE DES DIRIGEANTS';
  }
};

module.exports.formInfosSubmitted = async req => {
  if (req.session.siren) {
    await User.update(
      { id: req.user.id },
      {
        title: req.session.title,
        name: req.session.name,
        lastname: req.session.lastname,
        phone: req.session.phone,
        mobile: req.session.mobile,
      },
    );

    const customer = await Customer.findOne({ user: req.user.id });
    if (customer) {
      await Customer.update(
        { user: req.user.id },
        {
          siren: req.session.siren,
          legalForm: req.session.legalForm,
          startActivityDate: req.session.startActivityDate,
          companyName: req.session.companyName,
          structure: req.session.structure,
          centralRef: req.session.centralRef,
          individualCabinet: req.session.individualCabinet,
          liberalCollaborator: req.session.liberalCollaborator,
          franchise: req.session.franchise,
          centralRefName: req.session.centralRefName,
          endStatement: req.session.endStatement,
          endStatementDate: req.session.endStatementDate,
          currentContract: req.session.currentContract,
          currentContractDate: req.session.currentContractDate,
          currentContractCancel: req.session.currentContractCancel,
          alarm: req.session.alarm,
          tenant: req.session.tenant,
          animalsValue: req.session.animalsValue,
          approvedAlarm: req.session.approvedAlarm,
          insuranceCancel: req.session.insuranceCancel,
          cancelReasonsSinister: req.session.cancelReasonsSinister,
          cancelReasonsPayment: req.session.cancelReasonsPayment,
          quotationsUpdated: req.session.quotationsUpdated,
          sinistreConnaissance: req.session.sinistreConnaissance,
          sinistreConnaissance5Years: req.session.sinistreConnaissance5Years,
          bdmSup: req.session.bdmSup,
          bdm: req.session.bdm,
          localArea: req.session.surfaceLocaux,
          localContent: req.session.contenuLocaux,
        },
      );
    } else {
      await Customer.create({
        siren: req.session.siren,
        legalForm: req.session.legalForm,
        startActivityDate: req.session.startActivityDate,
        companyName: req.session.companyName,
        structure: req.session.structure,
        centralRef: req.session.centralRef,
        individualCabinet: req.session.individualCabinet,
        liberalCollaborator: req.session.liberalCollaborator,
        franchise: req.session.franchise,
        centralRefName: req.session.centralRefName,
        endStatement: req.session.endStatement,
        endStatementDate: req.session.endStatementDate,
        currentContract: req.session.currentContract,
        currentContractDate: req.session.currentContractDate,
        currentContractCancel: req.session.currentContractCancel,
        alarm: req.session.alarm,
        tenant: req.session.tenant,
        approvedAlarm: req.session.approvedAlarm,
        insuranceCancel: req.session.insuranceCancel,
        cancelReasonsSinister: req.session.cancelReasonsSinister,
        cancelReasonsPayment: req.session.cancelReasonsPayment,
        quotationsUpdated: req.session.quotationsUpdated,
        animalsValue: req.session.animalsValue,
        sinistreConnaissance: req.session.sinistreConnaissance,
        sinistreConnaissance5Years: req.session.sinistreConnaissance5Years,
        bdmSup: req.session.bdmSup,
        bdm: req.session.bdm,
        localArea: req.session.surfaceLocaux,
        localContent: req.session.contenuLocaux,
        user: req.user.id,
      });
    }

    const address = await Address.findOne({ user: req.user.id });
    if (address) {
      await Address.update(
        { user: req.user.id },
        {
          address: req.session.address,
          postalCode: req.session.postalCode,
          city: req.session.city,
        },
      );
    } else {
      await Address.create({
        address: req.session.address,
        postalCode: req.session.postalCode,
        city: req.session.city,
        user: req.user.id,
      });
    }

    if (req.session.type === 'MD' || req.session.type === 'RCMS') {
      const leader = await Leader.findOne({ user: req.user.id });
      if (leader) {
        await Leader.update(
          { user: req.user.id },
          {
            siren: req.session.siren,
            companyName: req.session.companyName,
            legalForm: req.session.legalForm,
            startActivityDate: req.session.startActivityDate,
            indivAccident: req.session.indivAccident,
            indivAccidentChoix: req.session.indivAccidentChoix,
            indivEnfants: req.session.indivEnfants,
            rapatriement: req.session.rapatriement,
            rapatriementChoix: req.session.rapatriementChoix,
            turnover: req.session.turnover,
            capitauxPropres: req.session.capitauxPropres,
            resultatNet: req.session.resultatNet,
            formula: req.session.formula,
            chiffreAffaireCours: req.session.chiffreAffaireCours,
            chiffreAffairePrev: req.session.chiffreAffairePrev,
            totalActifAnnee: req.session.totalActifAnnee,
            totalActifCours: req.session.totalActifCours,
            totalActifPrev: req.session.totalActifPrev,
            capitauxAnnee: req.session.capitauxAnnee,
            capitauxCours: req.session.capitauxCours,
            capitauxPrev: req.session.capitauxPrev,
            totalAnnee: req.session.totalAnnee,
            totalCours: req.session.totalCours,
            totalPrev: req.session.totalPrev,
          },
        );
      } else {
        await Leader.create({
          siren: req.session.siren,
          companyName: req.session.companyName,
          legalForm: req.session.legalForm,
          startActivityDate: req.session.startActivityDate,
          indivAccident: req.session.indivAccident,
          indivAccidentChoix: req.session.indivAccidentChoix,
          indivEnfants: req.session.indivEnfants,
          rapatriement: req.session.rapatriement,
          rapatriementChoix: req.session.rapatriementChoix,
          turnover: req.session.turnover,
          capitauxPropres: req.session.capitauxPropres,
          resultatNet: req.session.resultatNet,
          formula: req.session.formula,
          chiffreAffaireCours: req.session.chiffreAffaireCours,
          chiffreAffairePrev: req.session.chiffreAffairePrev,
          totalActifAnnee: req.session.totalActifAnnee,
          totalActifCours: req.session.totalActifCours,
          totalActifPrev: req.session.totalActifPrev,
          capitauxAnnee: req.session.capitauxAnnee,
          capitauxCours: req.session.capitauxCours,
          capitauxPrev: req.session.capitauxPrev,
          totalAnnee: req.session.totalAnnee,
          totalCours: req.session.totalCours,
          totalPrev: req.session.totalPrev,
          user: req.user.id,
        });
      }
    }

    let advisor = false;

    if (
      parseInt(req.session.surfaceLocaux) > 1000 ||
      parseInt(req.session.contenuLocaux) > 400000
    ) {
      advisor = true;
    }

    const currentAdhesion = await AdhesionTmp.findOne({
      user: req.user.id,
      offerType: Utils.getOfferTypeFr(req.session.type),
    });

    if (!currentAdhesion) {
      await AdhesionTmp.create({
        user: req.user.id,
        advisor,
        offerType: Utils.getOfferTypeFr(req.session.type),
      });
    } else {
      await AdhesionTmp.update(
        { user: req.user.id, offerType: Utils.getOfferTypeFr(req.session.type) },
        {
          advisor,
        },
      );
    }

    if (req.session.type === 'CR' && req.session.sinisterAmountCR) {
      if (currentAdhesion) {
        await CRSinisterStatement.destroy({ adhesion: currentAdhesion.id });
      }

      if (req.session.sinistreConnaissance5Years) {
        for (let i = 0; i < Object.keys(req.session.sinisterAmountCR).length; i++) {
          await CRSinisterStatement.create({
            amount: Object.keys(req.session.sinisterAmountCR).map(
              e => req.session.sinisterAmountCR[e],
            )[i],
            date: moment(
              new Date(
                `${
                  Object.keys(req.session.sinisterDateYearCR).map(
                    e => req.session.sinisterDateYearCR[e],
                  )[i]
                }-${
                  Object.keys(req.session.sinisterDateMonthCR).map(
                    e => req.session.sinisterDateMonthCR[e],
                  )[i]
                }-${
                  Object.keys(req.session.sinisterDateDayCR).map(
                    e => req.session.sinisterDateDayCR[e],
                  )[i]
                }`,
              ),
            ).format('DD-MM-YYYY'),
            adhesion: currentAdhesion.id,
          });
        }
      }
    } else if (req.session.type === 'PMR' && req.session.sinisterType) {
      if (currentAdhesion) {
        await PMRSinisterStatement.destroy({ adhesion: currentAdhesion.id });
      }

      if (req.session.sinistreConnaissance) {
        for (let i = 0; i < Object.keys(req.session.sinisterType).length; i++) {
          await PMRSinisterStatement.create({
            date: moment(
              new Date(
                `${
                  Object.keys(req.session.sinisterDateYearPMR).map(
                    e => req.session.sinisterDateYearPMR[e],
                  )[i]
                }-${
                  Object.keys(req.session.sinisterDateMonthPMR).map(
                    e => req.session.sinisterDateMonthPMR[e],
                  )[i]
                }-${
                  Object.keys(req.session.sinisterDateDayPMR).map(
                    e => req.session.sinisterDateDayPMR[e],
                  )[i]
                }`,
              ),
            ).format('DD-MM-YYYY'),
            type: Object.keys(req.session.sinisterType).map(e => req.session.sinisterType[e])[i],
            adhesion: currentAdhesion.id,
          });
        }
      }
    } else if (
      req.session.type === 'PMRCR' &&
      req.session.sinisterAmountCR &&
      req.session.sinisterType
    ) {
      if (currentAdhesion) {
        await CRSinisterStatement.destroy({ adhesion: currentAdhesion.id });
        await PMRSinisterStatement.destroy({ adhesion: currentAdhesion.id });
      }

      if (req.session.sinistreConnaissance5Years) {
        for (let i = 0; i < Object.keys(req.session.sinisterType).length; i++) {
          await CRSinisterStatement.create({
            amount: Object.keys(req.session.sinisterAmountCR).map(
              e => req.session.sinisterAmountCR[e],
            )[i],
            date: moment(
              new Date(
                `${
                  Object.keys(req.session.sinisterDateYearCR).map(
                    e => req.session.sinisterDateYearCR[e],
                  )[i]
                }-${
                  Object.keys(req.session.sinisterDateMonthCR).map(
                    e => req.session.sinisterDateMonthCR[e],
                  )[i]
                }-${
                  Object.keys(req.session.sinisterDateDayCR).map(
                    e => req.session.sinisterDateDayCR[e],
                  )[i]
                }`,
              ),
            ).format('DD-MM-YYYY'),
            adhesion: currentAdhesion.id,
          });
        }
      }

      if (req.session.sinistreConnaissance) {
        for (let i = 0; i < Object.keys(req.session.sinisterType).length; i++) {
          await PMRSinisterStatement.create({
            date: moment(
              new Date(
                `${
                  Object.keys(req.session.sinisterDateYearPMR).map(
                    e => req.session.sinisterDateYearPMR[e],
                  )[i]
                }-${
                  Object.keys(req.session.sinisterDateMonthPMR).map(
                    e => req.session.sinisterDateMonthPMR[e],
                  )[i]
                }-${
                  Object.keys(req.session.sinisterDateDayPMR).map(
                    e => req.session.sinisterDateDayPMR[e],
                  )[i]
                }`,
              ),
            ).format('DD-MM-YYYY'),
            type: Object.keys(req.session.sinisterType).map(e => req.session.sinisterType[e])[i],
            adhesion: currentAdhesion.id,
          });
        }
      }
    }
  }
};

module.exports.generateContractRefs = async (req, commercial) => {
  let updatedContract = null;
  let updatedCustomer = null;
  let updatedLeader = null;
  let currentUserId = null;

  if (commercial) {
    currentUserId = req.session.createdUser.id;
  } else {
    currentUserId = req.user.id;
  }
  // generate subscriber and contract reference
  const lastCustomer = await Customer.find()
    .limit(1)
    .select(['reference'])
    .sort('reference DESC');
  const currentCustomer = await Customer.findOne({ user: currentUserId });
  const lastLeader = await Leader.find()
    .limit(1)
    .select(['reference'])
    .sort('reference DESC');
  const currentLeader = await Leader.findOne({ user: currentUserId });
  const lastContractCurrentUser = await Contract.find({ user: currentUserId })
    .limit(1)
    .select(['reference', 'user', 'id'])
    .sort('id DESC');

  if (
    lastCustomer[0] &&
    lastCustomer[0].reference === 0 &&
    currentCustomer &&
    currentCustomer.reference === 0
  ) {
    updatedCustomer = await Customer.update(
      { user: currentUserId },
      { reference: 16082809 },
    ).fetch();
  } else if (
    lastCustomer[0] &&
    lastCustomer[0].reference !== 0 &&
    currentCustomer &&
    currentCustomer.reference === 0
  ) {
    updatedCustomer = await Customer.update(
      { user: currentUserId },
      { reference: lastCustomer[0].reference + 1 },
    ).fetch();
  } else if (
    lastCustomer[0] &&
    lastCustomer[0].reference !== 0 &&
    currentCustomer &&
    currentCustomer.reference !== 0
  ) {
    updatedCustomer = [currentCustomer];
  }

  if (
    lastLeader[0] &&
    lastLeader[0].reference === 0 &&
    currentLeader &&
    currentLeader.reference === 0
  ) {
    updatedLeader = await Leader.update({ user: currentUserId }, { reference: 16082809 }).fetch();
  } else if (
    lastLeader[0] &&
    lastLeader[0].reference !== 0 &&
    currentLeader &&
    currentLeader.reference === 0
  ) {
    updatedLeader = await Leader.update(
      { user: currentUserId },
      { reference: lastLeader[0].reference + 1 },
    ).fetch();
  } else if (
    lastLeader[0] &&
    lastLeader[0].reference !== 0 &&
    currentLeader &&
    currentLeader.reference !== 0
  ) {
    updatedLeader = [currentLeader];
  }

  if (updatedCustomer !== null || updatedLeader !== null) {
    const lastContract = await Contract.find()
      .limit(2)
      .select(['reference'])
      .sort('id DESC');
    if (lastContract.length > 0 && lastContract[1].reference !== '') {
      let contractRef = null;
      if (
        req.session.offerType === 'RC' ||
        req.session.offerType === 'MRP' ||
        req.session.offerType === 'MRP + RC'
      ) {
        contractRef = `0WE${parseInt(lastContract[1].reference.slice(-6)) + 1}`;
      } else if (req.session.offerType === 'MD') {
        contractRef = `VETO-MD-${parseInt(lastContract[1].reference.slice(-6)) + 1}`;
      } else if (req.session.offerType === 'RCMS') {
        contractRef = `VETO-RCMS-${parseInt(lastContract[1].reference.slice(-6)) + 1}`;
      }
      updatedContract = await Contract.update(
        { id: lastContractCurrentUser[0].id },
        {
          reference: contractRef,
        },
      ).fetch();
    } else {
      let contractRef = null;
      if (
        req.session.offerType === 'RC' ||
        req.session.offerType === 'MRP' ||
        req.session.offerType === 'MRP + RC'
      ) {
        contractRef = '0WE100001';
      } else if (req.session.offerType === 'MD') {
        contractRef = 'VETO-MD-100001';
      } else if (req.session.offerType === 'RCMS') {
        contractRef = 'VETO-RCMS-100001';
      }
      updatedContract = await Contract.update(
        { id: lastContractCurrentUser[0].id },
        {
          reference: contractRef,
        },
      ).fetch();
    }
  }

  return {
    customerRef: updatedCustomer ? updatedCustomer[0].reference : updatedCustomer,
    leaderRef: updatedLeader ? updatedLeader[0].reference : updatedLeader,
    contractRef: updatedContract ? updatedContract[0].reference : updatedContract,
  };
};

module.exports.calculateDeadline = (fraction, effectDate) => {
  if (effectDate === null) {
    effectDate = moment().format('DD-MM-YYYY');
  }
  let deadlineDate = null;
  let afterDeadlineDate = null;
  effectDate = effectDate
    .split('-')
    .reverse()
    .join('-');

  var effectMonth = moment(effectDate).month() + 1;
  if (parseInt(fraction) === 4) {
    if (effectMonth <= 3) {
      deadlineDate = moment()
        .set({ month: 2, date: 31, year: moment(effectDate).year() })
        .format('DD-MM-YYYY');
    }
    if (effectMonth > 3 && effectMonth <= 6) {
      deadlineDate = moment()
        .set({ month: 5, date: 30, year: moment(effectDate).year() })
        .format('DD-MM-YYYY');
    }
    if (effectMonth > 6 && effectMonth <= 9) {
      deadlineDate = moment()
        .set({ month: 8, date: 30, year: moment(effectDate).year() })
        .format('DD-MM-YYYY');
    }
    if (effectMonth > 9 && effectMonth <= 12) {
      deadlineDate = moment()
        .set({ month: 11, date: 31, year: moment(effectDate).year() })
        .format('DD-MM-YYYY');
    }
  }
  if (parseInt(fraction) === 2) {
    if (effectMonth <= 6) {
      deadlineDate = moment()
        .set({ month: 5, date: 30, year: moment(effectDate).year() })
        .format('DD-MM-YYYY');
    }
    if (effectMonth > 6 && effectMonth <= 12) {
      deadlineDate = moment()
        .set({ month: 11, date: 31, year: moment(effectDate).year() })
        .format('DD-MM-YYYY');
    }
  }
  if (parseInt(fraction) === 1) {
    deadlineDate = moment()
      .set({ month: 11, date: 31, year: moment(effectDate).year() })
      .format('DD-MM-YYYY');
  }
  if (parseInt(fraction) === 12) {
    deadlineDate = moment(effectDate)
      .endOf('month')
      .format('DD-MM-YYYY');
  }

  const parsedDeadlineDate = deadlineDate.split('-');
  afterDeadlineDate = moment(
    new Date(`${parsedDeadlineDate[2]}-${parsedDeadlineDate[1]}-${parsedDeadlineDate[0]}`),
  )
    .add(1, 'days')
    .format('DD-MM-YYYY');

  return {
    deadlineDate,
    afterDeadlineDate,
  };
};

module.exports.generateContractAddons = async (
  user,
  contract,
  offerType,
  req,
  allUserData,
  commercial,
) => {
  let pdfBuffer = null;
  let currentAdhesionByType = null;
  let template = null;
  let commercialInfos = null;

  if (req.user.adhesion.length === 0) {
    currentAdhesionByType = [
      Object.assign({}, allUserData.contract[0], allUserData.contract[0].quotation[0]),
    ];
    commercialInfos = req.user;
    req = { user: allUserData };
  } else {
    currentAdhesionByType = req.user.adhesion.filter(adhesion => adhesion.offerType === offerType);
  }

  if (commercial) {
    if (allUserData.contract[0].quotation[0].offerType === 'MRP') {
      template = path.join(__dirname, '..', '..', 'views', 'pdf', 'contractMRP.ejs');
    } else if (allUserData.contract[0].quotation[0].offerType === 'RC') {
      template = path.join(__dirname, '..', '..', 'views', 'pdf', 'contractRC.ejs');
    } else if (allUserData.contract[0].quotation[0].offerType === 'MRP + RC') {
      template = path.join(__dirname, '..', '..', 'views', 'pdf', 'contractMRPRC.ejs');
    }
  } else {
    if (offerType === 'MRP') {
      template = path.join(__dirname, '..', '..', 'views', 'pdf', 'contractMRP.ejs');
    } else if (offerType === 'RC') {
      template = path.join(__dirname, '..', '..', 'views', 'pdf', 'contractRC.ejs');
    } else if (offerType === 'MRP + RC') {
      template = path.join(__dirname, '..', '..', 'views', 'pdf', 'contractMRPRC.ejs');
    }
  }

  const templateSEPA = path.join(__dirname, '..', '..', 'views', 'pdf', 'vetoSEPA.ejs');
  const templateCONSENT = path.join(__dirname, '..', '..', 'views', 'pdf', 'vetoConsent.ejs');

  const pdfPath = path.join(
    __dirname,
    '..',
    '..',
    '.permanent',
    'contracts',
    `contrat_adhesion_vetoptim_0000${contract.id}_${commercial ? allUserData.name : user.name}_${
      commercial ? allUserData.lastname : user.lastname
    }.pdf`,
  );

  const vetoSEPAPath = path.join(
    __dirname,
    '..',
    '..',
    '.permanent',
    'contracts',
    `sepa_vetoptim_0000${contract.id}_${commercial ? allUserData.name : user.name}_${
      commercial ? allUserData.lastname : user.lastname
    }.pdf`,
  );

  const vetoConsentPath = path.join(
    __dirname,
    '..',
    '..',
    '.permanent',
    'contracts',
    `receuil_vetoptim_0000${contract.id}_${commercial ? allUserData.name : user.name}_${
      commercial ? allUserData.lastname : user.lastname
    }.pdf`,
  );

  if (offerType !== 'MD') {
    await PdfExport.generate(
      templateSEPA,
      {
        contract,
        user: commercial ? allUserData : user,
        moment,
        adhesion: currentAdhesionByType[0],
      },
      vetoSEPAPath,
    );
  }

  await PdfExport.generate(
    templateCONSENT,
    {
      contract,
      user: commercial ? allUserData : user,
      moment,
      offer: offerType,
    },
    vetoConsentPath,
  );

  const currentContract = await Contract.update(
    {
      id: contract.id,
    },
    {
      sepaFileName:
        offerType !== 'MD'
          ? `sepa_vetoptim_0000${contract.id}_${user.name}_${user.lastname}.pdf`
          : '',
      consentFileName:
        offerType !== 'MD' && offerType !== 'RCMS'
          ? `receuil_vetoptim_0000${contract.id}_${user.name}_${user.lastname}.pdf`
          : '',
    },
  ).fetch();

  if (offerType !== 'MD' && offerType !== 'RCMS') {
    await PdfExport.generate(
      template,
      {
        adhesion: currentAdhesionByType[0],
        user: req.user,
        moment: moment,
        period: Utils.calculatePeriod,
        currency: require('currency-formatter'),
        Warranty: Warranty,
        Quotation: Quotation,
        indiceFFB: '988,1 €',
        contractRef: commercial ? currentAdhesionByType[0].reference : currentContract[0].reference,
        customerRef: commercial
          ? allUserData.customer[0].reference
          : currentAdhesionByType[0].customerReference,
        tauxChiffreAffaires: 0.055,
      },
      pdfPath,
    );
    pdfBuffer = await PdfMerge.mergeSEPA(
      path.join(
        __dirname,
        '..',
        '..',
        '.permanent',
        'contracts',
        currentContract[0].contractFileName,
      ),
      vetoSEPAPath,
    );

    await require('util').promisify(require('fs').writeFile)(
      path.join(
        __dirname,
        '..',
        '..',
        '.permanent',
        'contracts',
        currentContract[0].contractFileName,
      ),
      pdfBuffer,
      { encoding: 'base64' },
    );
  } else if (offerType === 'MD') {
    await Utils.generateMDContract(
      req,
      currentAdhesionByType,
      currentContract[0].contractFileName,
      currentContract[0].sepaMndRef,
      commercial,
      commercialInfos,
    );
  } else if (offerType === 'RCMS') {
    await Utils.generateRCMSContract(
      req,
      currentAdhesionByType,
      currentContract[0].contractFileName,
      currentContract[0].sepaMndRef,
    );
    pdfBuffer = await PdfMerge.mergeSEPA(
      path.join(
        __dirname,
        '..',
        '..',
        '.permanent',
        'contracts',
        currentContract[0].contractFileName,
      ),
      vetoSEPAPath,
    );

    await require('util').promisify(require('fs').writeFile)(
      path.join(
        __dirname,
        '..',
        '..',
        '.permanent',
        'contracts',
        currentContract[0].contractFileName,
      ),
      pdfBuffer,
      { encoding: 'base64' },
    );
  }

  return {
    vetoSEPAPath,
    vetoConsentPath,
  };
};

module.exports.saveOrUpdatePaymentMethod = async (req, res, ribImg, offerType) => {
  // save or update payment infos
  const paymentMethod = await PaymentMethod.findOne({ user: req.user.id });

  await AdhesionTmp.update(
    { user: req.user.id, offerType: offerType },
    {
      firstPaymentType: req.param('paymentMethod'),
    },
  );

  if (paymentMethod) {
    await PaymentMethod.update(
      { user: req.user.id },
      {
        pickingDate: req.param('paymentDay'),
        IBAN: req.param('iban').toUpperCase(),
        BIC: req.param('bic').toUpperCase(),
        holder: req.param('holder'),
        exactTitle: req.param('exactTitle'),
        ribImg,
        offerType: req.param('from'),
      },
    );
  } else {
    await PaymentMethod.create({
      pickingDate: req.param('paymentDay'),
      IBAN: req.param('iban').toUpperCase(),
      BIC: req.param('bic').toUpperCase(),
      holder: req.param('holder'),
      exactTitle: req.param('exactTitle'),
      ribImg,
      offerType: req.param('from'),
      user: req.user.id,
    });
  }

  if (req.param('paymentMethod') === 'sepa') {
    return res.status(200).json({ redirect: `sign-sepa?offerType=${req.param('from')}` });
  } else {
    return res.status(200).json({ redirect: `sign-bc?offerType=${req.param('from')}` });
  }
};

module.exports.updateContractRefsInPdf = async (contract, req) => {
  let template = null;
  const currentAdhesionByType = req.user.adhesion.filter(
    adhesion => adhesion.offerType === contract.quotation[0].offerType,
  );
  if (contract.quotation[0].offerType === 'MRP') {
    template = path.join(__dirname, '..', '..', 'views', 'pdf', 'contractMRP.ejs');
  } else if (contract.quotation[0].offerType === 'RC') {
    template = path.join(__dirname, '..', '..', 'views', 'pdf', 'contractRC.ejs');
  } else if (contract.quotation[0].offerType === 'MRP + RC') {
    template = path.join(__dirname, '..', '..', 'views', 'pdf', 'contractMRPRC.ejs');
  }

  const pdfPath = path.join(
    __dirname,
    '..',
    '..',
    '.permanent',
    'contracts',
    `contrat_adhesion_vetoptim_0000${contract.id}_${req.user.name}_${req.user.lastname}.pdf`,
  );

  const generatedRefs = await Utils.generateContractRefs(req);

  if (contract.quotation[0].offerType !== 'MD' && contract.quotation[0].offerType !== 'RCMS') {
    await PdfExport.generate(
      template,
      {
        adhesion: currentAdhesionByType[0],
        user: req.user,
        moment: moment,
        period: Utils.calculatePeriod,
        currency: require('currency-formatter'),
        Warranty: Warranty,
        Quotation: Quotation,
        indiceFFB: '988,1 €',
        contractRef: generatedRefs.contractRef,
        customerRef: generatedRefs.customerRef,
        leaderRef: generatedRefs.leaderRef,
        tauxChiffreAffaires: 0.055,
      },
      pdfPath,
    );
  } else if (contract.quotation[0].offerType === 'MD') {
    await Utils.generateMDContract(
      req,
      currentAdhesionByType,
      `contrat_adhesion_vetoptim_0000${contract.id}_${req.user.name}_${req.user.lastname}.pdf`,
      contract.sepaMndRef,
    );
  } else if (contract.quotation[0].offerType === 'RCMS') {
    await Utils.generateRCMSContract(
      req,
      currentAdhesionByType,
      `contrat_adhesion_vetoptim_0000${contract.id}_${req.user.name}_${req.user.lastname}.pdf`,
      contract.sepaMndRef,
    );
  }

  await AdhesionTmp.update(
    { id: currentAdhesionByType[0].id },
    {
      customerReference: generatedRefs.customerRef
        ? generatedRefs.customerRef
        : 'pas encore disponible',
      leaderReference: generatedRefs.leaderRef ? generatedRefs.leaderRef : 'pas encore disponible',
    },
  );
};

module.exports.generateRateReference = async adhesion => {
  let generatedRateRef = null;
  const lastRateRef = await AdhesionTmp.find()
    .sort('rateReference DESC')
    .limit(2);
  if (lastRateRef.length === 2 && lastRateRef[1].rateReference !== 'pas encore disponible') {
    generatedRateRef = `devis-${parseInt(lastRateRef[1].rateReference.replace(/[a-zA-Z-]/g, '')) +
      1}`;
  } else {
    generatedRateRef = 'devis-100001';
  }
  await AdhesionTmp.update(
    {
      id: adhesion.id,
    },
    {
      rateReference: generatedRateRef,
    },
  );
};

module.exports.correctCashTax = (
  effectDate,
  fraction,
  totalTaxFraisContr,
  totalTaxFraisContrSem,
  totalTaxFraisContrTrim,
  totalTaxFraisContrMens,
) => {
  let taxQuotation = null;
  let fractTaxQuotation = null;
  let remainingMonths = 0;

  const effectDateMonth =
    moment(
      new Date(
        `${effectDate.split('-')[2]}-${effectDate.split('-')[1]}-${effectDate.split('-')[0]}`,
      ),
    ).month() + 1;

  if (fraction === 12) {
    taxQuotation = totalTaxFraisContrMens;
    fractTaxQuotation = totalTaxFraisContrMens;
  } else if (fraction === 4) {
    taxQuotation = totalTaxFraisContrTrim / 3;
    fractTaxQuotation = totalTaxFraisContrTrim;
    if (
      effectDateMonth === 1 ||
      effectDateMonth === 4 ||
      effectDateMonth === 7 ||
      effectDateMonth === 10
    ) {
      remainingMonths = 2;
    } else if (
      effectDateMonth === 2 ||
      effectDateMonth === 5 ||
      effectDateMonth === 8 ||
      effectDateMonth === 11
    ) {
      remainingMonths = 1;
    } else {
      remainingMonths = 0;
    }
  } else if (fraction === 2) {
    taxQuotation = totalTaxFraisContrSem / 6;
    fractTaxQuotation = totalTaxFraisContrSem;
    if (effectDateMonth <= 6) {
      remainingMonths = 6 - effectDateMonth;
    } else {
      remainingMonths = 12 - effectDateMonth;
    }
  } else if (fraction === 1) {
    taxQuotation = totalTaxFraisContr / 12;
    fractTaxQuotation = totalTaxFraisContr;
    remainingMonths = 12 - effectDateMonth;
  }

  const daysInMonth = moment(
    new Date(`${effectDate.split('-')[2]}-${effectDate.split('-')[1]}-${effectDate.split('-')[0]}`),
  ).daysInMonth();

  const remainingDays =
    1 +
    daysInMonth -
    moment(
      new Date(
        `${effectDate.split('-')[2]}-${effectDate.split('-')[1]}-${effectDate.split('-')[0]}`,
      ),
    ).date();

  const remaningQuotation = (taxQuotation / daysInMonth) * remainingDays;
  const remainingMonthsQuotation = taxQuotation * remainingMonths;

  const tax = remaningQuotation + remainingMonthsQuotation;

  return {
    tax,
    fractTaxQuotation,
  };
};

module.exports.correctMonthlyCashQuotation = (effectDate, fraction, fractQuotation) => {
  let monthlyQuotation = null;
  let remainingMonths = 0;

  const effectDateMonth =
    moment(
      new Date(
        `${effectDate.split('-')[2]}-${effectDate.split('-')[1]}-${effectDate.split('-')[0]}`,
      ),
    ).month() + 1;

  if (fraction === 12) {
    monthlyQuotation = fractQuotation;
  } else if (fraction === 4) {
    monthlyQuotation = fractQuotation / 3;
    if (
      effectDateMonth === 1 ||
      effectDateMonth === 4 ||
      effectDateMonth === 7 ||
      effectDateMonth === 10
    ) {
      remainingMonths = 2;
    } else if (
      effectDateMonth === 2 ||
      effectDateMonth === 5 ||
      effectDateMonth === 8 ||
      effectDateMonth === 11
    ) {
      remainingMonths = 1;
    } else {
      remainingMonths = 0;
    }
  } else if (fraction === 2) {
    monthlyQuotation = fractQuotation / 6;
    if (effectDateMonth <= 6) {
      remainingMonths = 6 - effectDateMonth;
    } else {
      remainingMonths = 12 - effectDateMonth;
    }
  } else if (fraction === 1) {
    monthlyQuotation = fractQuotation / 12;
    remainingMonths = 12 - effectDateMonth;
  }

  const daysInMonth = moment(
    new Date(`${effectDate.split('-')[2]}-${effectDate.split('-')[1]}-${effectDate.split('-')[0]}`),
  ).daysInMonth();

  const remainingDays =
    1 +
    daysInMonth -
    moment(
      new Date(
        `${effectDate.split('-')[2]}-${effectDate.split('-')[1]}-${effectDate.split('-')[0]}`,
      ),
    ).date();

  const remaningQuotation = (monthlyQuotation / daysInMonth) * remainingDays;
  const remainingMonthsQuotation = monthlyQuotation * remainingMonths;

  const cashQuotation = remaningQuotation + remainingMonthsQuotation;

  return {
    cashQuotation,
  };
};

module.exports.getMonthlyQuotation = (fraction, chosenQuotation) => {
  let monthlyQuotation = null;
  if (fraction === 12) {
    monthlyQuotation = chosenQuotation;
  } else if (fraction === 4) {
    monthlyQuotation = chosenQuotation / 3;
  } else if (fraction === 2) {
    monthlyQuotation = chosenQuotation / 6;
  } else {
    monthlyQuotation = chosenQuotation / 12;
  }
  return { monthlyQuotation };
};

module.exports.getOptionalWarrantiesQuotations = (
  accident,
  accidentChoice,
  child,
  repatriation,
  repatriationChoice,
) => {
  let total = 0;
  let accidentQuotation = null;
  let childQuotation = null;
  let repatriationQuotation = null;

  const quotations = {
    accident: {
      individuel: 0.5,
      famille: 0.75,
    },
    child: {
      value: 1.0,
    },
    repatriation: {
      '1adulte': 6.06,
      '2adultes': 9.08,
      famille: 10.54,
    },
  };

  if (accident) {
    accidentQuotation = quotations['accident'][accidentChoice];
    total += accidentQuotation;
  }

  if (child) {
    childQuotation = quotations['child'].value;
    total += childQuotation;
  }

  if (repatriation) {
    repatriationQuotation = quotations['repatriation'][repatriationChoice];
    total += repatriationQuotation;
  }

  return {
    accidentQuotation,
    childQuotation,
    repatriationQuotation,
    total,
  };
};

module.exports.generateMDContract = async (
  req,
  currentAdhesionByType,
  fileName,
  sepaMndRef,
  commercial,
  commercialInfos,
) => {
  const legalForms = ['travailleur indépendant', 'snc', 'eurl', 'sarl', 'sa', 'sas', 'autre'];

  const sourcePDF = 'assets/pdf/md.pdf';
  const destinationPDF = `.permanent/contracts/${fileName}`;

  const data = {
    effect_date: currentAdhesionByType[0].effectDate,
    last_name: 'RIGAUD',
    first_name: 'Christophe',
    civility: 'Président de VETOPTIM Assurance',
    phone: '01 77 66 40 89',
    email: 'contact@vetoptim.com',
    num_adherent1: '',
    num_adherent: '',
    acs: '',
    acs_date: '',
    vente_type: 'distance',
    new_adhesion: 'on',
    contract_place: '',
    contract_num: '',
    avenant: '',
    raison_sociale: req.user.leader[0].companyName,
    forme_juridiqe:
      req.user.leader[0].legalForm.toLowerCase() !== 'autre' &&
      !legalForms.includes(req.user.leader[0].legalForm.toLowerCase())
        ? 'autre'
        : req.user.leader[0].legalForm.toLowerCase(),
    forme_juridiqe_other:
      req.user.leader[0].legalForm.toLowerCase() !== 'autre'
        ? !legalForms.includes(req.user.leader[0].legalForm.toLowerCase())
          ? req.user.leader[0].legalForm
          : ''
        : 'indéfini',
    address: req.user.address[0].address,
    postal_code: req.user.address[0].postalCode,
    city: req.user.address[0].city,
    siret: req.user.leader[0].siren,
    siret2: '',
    code_naf: '',
    effectif_salarie: '',
    effectif_agirc: '',
    effectif_non_agirc: '',
    activity: '',
    convention: '',
    souscripteur_last_name: req.user.lastname,
    souscripteur_first_name: req.user.name,
    souscripteur_address: '',
    dirigeant: '',
    souscripteur_siret1: '',
    souscripteur_siret2: '',
    assure_last_name: req.user.lastname,
    assure_first_name: req.user.name,
    assure_birthday: req.user.leader[0].leaderBirthdate,
    assure_regime: '',
    assure_organisme: '',
    assure_situation: req.user.leader[0].haveConjoint ? 'marie' : 'celibataire',
    assure_address: req.user.address[0].address.replace(/[a-zA-Z -]*/g, ''),
    assure_voie: req.user.address[0].address.replace(/[0-9]*/g, ''),
    assure_postal_code: req.user.address[0].postalCode,
    assure_city: req.user.address[0].city,
    assure_country: 'France',
    assure_phone: req.user.mobile,
    assure_email: req.user.email,
    assure_profession: '',
    conjoint_last_name: req.user.leader[0].conjointLastname,
    conjoint_first_name: req.user.leader[0].conjointName,
    conjoint_birthday: req.user.leader[0].conjointBirthdate,
    conjoint_organisme: '',
    conjoint_regime_social: '',
    conjoint_organisme_name: '',
    conjoint_date_exercice: '',
    cotisation_mensuelle: currency.format(
      Utils.getMonthlyQuotation(
        currentAdhesionByType[0].fraction,
        currentAdhesionByType[0].chosenQuotation
          ? currentAdhesionByType[0].chosenQuotation
          : currentAdhesionByType[0].ttc,
      ).monthlyQuotation -
        Utils.getOptionalWarrantiesQuotations(
          req.user.leader[0].indivAccident,
          req.user.leader[0].indivAccidentChoix,
          req.user.leader[0].indivEnfants,
          req.user.leader[0].rapatriement,
          req.user.leader[0].rapatriementChoix,
        ).total,
      { symbol: '€', decimal: '.', thousand: ' ', precision: 2, format: '%v' },
    ),
    garantie: req.user.leader[0].formula,
    nbr_adulte: req.user.leader[0].haveConjoint ? 2 : 1,
    nbr_enfant: req.user.leader[0].children
      .map(e =>
        moment(
          new Date(
            `${e.birthdate.split('-')[2]}-${e.birthdate.split('-')[1]}-${
              e.birthdate.split('-')[0]
            }`,
          ),
        ),
      )
      .reduce((acc, current) => (moment().diff(current, 'year') < 26 ? (acc += 1) : acc), 0),
    garantie_accident: req.user.leader[0].indivAccident ? 'on' : '',
    garantie_enfant: req.user.leader[0].indivEnfants ? 'on' : '',
    garantie_rapatriement: req.user.leader[0].rapatriement ? 'on' : '',
    garantie_accident_val: Utils.getOptionalWarrantiesQuotations(
      req.user.leader[0].indivAccident,
      req.user.leader[0].indivAccidentChoix,
      req.user.leader[0].indivEnfants,
      req.user.leader[0].rapatriement,
      req.user.leader[0].rapatriementChoix,
    ).accidentQuotation
      ? currency.format(
          Utils.getOptionalWarrantiesQuotations(
            req.user.leader[0].indivAccident,
            req.user.leader[0].indivAccidentChoix,
            req.user.leader[0].indivEnfants,
            req.user.leader[0].rapatriement,
            req.user.leader[0].rapatriementChoix,
          ).accidentQuotation,
          { symbol: '€', decimal: '.', thousand: ' ', precision: 2, format: '%v' },
        )
      : '',
    garantie_enfant_val: Utils.getOptionalWarrantiesQuotations(
      req.user.leader[0].indivAccident,
      req.user.leader[0].indivAccidentChoix,
      req.user.leader[0].indivEnfants,
      req.user.leader[0].rapatriement,
      req.user.leader[0].rapatriementChoix,
    ).childQuotation
      ? currency.format(
          Utils.getOptionalWarrantiesQuotations(
            req.user.leader[0].indivAccident,
            req.user.leader[0].indivAccidentChoix,
            req.user.leader[0].indivEnfants,
            req.user.leader[0].rapatriement,
            req.user.leader[0].rapatriementChoix,
          ).childQuotation,
          { symbol: '€', decimal: '.', thousand: ' ', precision: 2, format: '%v' },
        )
      : '',
    garantie_rapatriement_val: Utils.getOptionalWarrantiesQuotations(
      req.user.leader[0].indivAccident,
      req.user.leader[0].indivAccidentChoix,
      req.user.leader[0].indivEnfants,
      req.user.leader[0].rapatriement,
      req.user.leader[0].rapatriementChoix,
    ).repatriationQuotation
      ? currency.format(
          Utils.getOptionalWarrantiesQuotations(
            req.user.leader[0].indivAccident,
            req.user.leader[0].indivAccidentChoix,
            req.user.leader[0].indivEnfants,
            req.user.leader[0].rapatriement,
            req.user.leader[0].rapatriementChoix,
          ).repatriationQuotation,
          { symbol: '€', decimal: '.', thousand: ' ', precision: 2, format: '%v' },
        )
      : '',
    cotisation_soussigner: req.user.lastname + ' ' + req.user.name,
    cotisation_date_prelevement: moment(
      new Date(
        `${
          currentAdhesionByType[0].afterDeadlineDate
            ? currentAdhesionByType[0].afterDeadlineDate.split('-')[2]
            : Utils.calculateDeadline(
                currentAdhesionByType[0].fraction,
                currentAdhesionByType[0].effectDate,
              ).afterDeadlineDate.split('-')[2]
        }-${
          currentAdhesionByType[0].afterDeadlineDate
            ? currentAdhesionByType[0].afterDeadlineDate.split('-')[1]
            : Utils.calculateDeadline(
                currentAdhesionByType[0].fraction,
                currentAdhesionByType[0].effectDate,
              ).afterDeadlineDate.split('-')[1]
        }-${
          currentAdhesionByType[0].afterDeadlineDate
            ? currentAdhesionByType[0].afterDeadlineDate.split('-')[0]
            : Utils.calculateDeadline(
                currentAdhesionByType[0].fraction,
                currentAdhesionByType[0].effectDate,
              ).afterDeadlineDate.split('-')[0]
        }`,
      ),
    )
      .date(10)
      .format('DD-MM-YYYY'),
    cotisation_periodicite: Utils.calculatePeriod(currentAdhesionByType[0].fraction),
    soussigne: req.user.lastname + ' ' + req.user.name,
    num_mobile: req.user.mobile,
    pays_origine: 'France',
    exemplaires_date: moment().format('DD/MM/YYYY'),
    exemplaires_a: 'Paris',
    banque_titulaire_compte: req.user.payment[0].exactTitle,
    banque_postal_code: req.user.address[0].postalCode,
    banque_city: req.user.address[0].city,
    banque_country: 'France',
    banque_iban: req.user.payment[0].IBAN,
    banque_bic: req.user.payment[0].BIC,
    sepa_titulaire_compte: req.user.payment[0].exactTitle,
    sepa_address: req.user.address[0].address,
    sepa_cp: req.user.address[0].postalCode,
    sepa_city: req.user.address[0].city,
    sepa_country: 'France',
    sepa_iban: req.user.payment[0].IBAN,
    sepa_bic: req.user.payment[0].BIC,
    sepa_a: 'Paris',
    sepa_date: moment().format('DD/MM/YYYY'),
    sepa_reference: sepaMndRef,
    sepa_paiement: currentAdhesionByType[0].firstPaymentType === 'sepa' ? 'on' : '',
  };

  for (let i = 0; i < req.user.leader[0].children.length; i++) {
    data[`children_${i + 1}_last_name`] = req.user.leader[0].children[i].lastname;
    data[`children_${i + 1}_first_name`] = req.user.leader[0].children[i].name;
    data[`children_${i + 1}_birthday`] = req.user.leader[0].children[i].birthdate;
  }

  if (commercial) {
    data['last_name'] = commercialInfos.lastname;
    data['first_name'] = commercialInfos.name;
    data['civility'] = 'Commercial';
    data['phone'] = commercialInfos.mobile;
    data['email'] = commercialInfos.email;
  } else {
    data['last_name'] = 'RIGAUD';
    data['first_name'] = 'Christophe';
    data['civility'] = 'Président de VETOPTIM Assurance';
    data['phone'] = '01 77 66 40 89';
    data['email'] = 'contact@vetoptim.com';
  }

  const pdfFiller = new Promise((resolve, reject) => {
    require('pdffiller').fillForm(sourcePDF, destinationPDF, data, err => {
      if (err) return reject(err);
      return resolve(true);
    });
  });

  await pdfFiller.catch(e =>
    console.log('une erreur est survenu lors de la génération du contrat MD'),
  );
};

module.exports.generateMDRate = async (req, currentAdhesionByType, fileName) => {
  const legalForms = ['travailleur indépendant', 'snc', 'eurl', 'sarl', 'sa', 'sas', 'autre'];

  const sourcePDF = 'assets/pdf/md-devis.pdf';
  const destinationPDF = `.permanent/rates/${fileName}`;

  const data = {
    effect_date: currentAdhesionByType[0].effectDate,
    last_name: 'RIGAUD',
    first_name: 'Christophe',
    civility: 'Président de VETOPTIM Assurance',
    phone: '01 77 66 40 89',
    email: 'contact@vetoptim.com',
    num_adherent1: '',
    num_adherent: '',
    acs: '',
    acs_date: '',
    vente_type: 'distance',
    new_adhesion: 'on',
    contract_place: '',
    contract_num: '',
    avenant: '',
    raison_sociale: req.user.leader[0].companyName,
    forme_juridiqe:
      req.user.leader[0].legalForm.toLowerCase() !== 'autre' &&
      !legalForms.includes(req.user.leader[0].legalForm.toLowerCase())
        ? 'autre'
        : req.user.leader[0].legalForm.toLowerCase(),
    forme_juridiqe_other:
      req.user.leader[0].legalForm.toLowerCase() !== 'autre'
        ? !legalForms.includes(req.user.leader[0].legalForm.toLowerCase())
          ? req.user.leader[0].legalForm
          : ''
        : 'indéfini',
    address: req.user.address[0].address,
    postal_code: req.user.address[0].postalCode,
    city: req.user.address[0].city,
    siret: req.user.leader[0].siren,
    siret2: '',
    code_naf: '',
    effectif_salarie: '',
    effectif_agirc: '',
    effectif_non_agirc: '',
    activity: '',
    convention: '',
    souscripteur_last_name: req.user.lastname,
    souscripteur_first_name: req.user.name,
    souscripteur_address: '',
    dirigeant: '',
    souscripteur_siret1: '',
    souscripteur_siret2: '',
    assure_last_name: req.user.lastname,
    assure_first_name: req.user.name,
    assure_birthday: req.user.leader[0].leaderBirthdate,
    assure_regime: '',
    assure_organisme: '',
    assure_situation: req.user.leader[0].haveConjoint ? 'marie' : 'celibataire',
    assure_address: req.user.address[0].address.replace(/[a-zA-Z -]*/g, ''),
    assure_voie: req.user.address[0].address.replace(/[0-9]*/g, ''),
    assure_postal_code: req.user.address[0].postalCode,
    assure_city: req.user.address[0].city,
    assure_country: 'France',
    assure_phone: req.user.mobile,
    assure_email: req.user.email,
    assure_profession: '',
    conjoint_last_name: req.user.leader[0].conjointLastname,
    conjoint_first_name: req.user.leader[0].conjointName,
    conjoint_birthday: req.user.leader[0].conjointBirthdate,
    conjoint_organisme: '',
    conjoint_regime_social: '',
    conjoint_organisme_name: '',
    conjoint_date_exercice: '',
    cotisation_mensuelle: currency.format(
      Utils.getMonthlyQuotation(
        currentAdhesionByType[0].fraction,
        currentAdhesionByType[0].chosenQuotation,
      ).monthlyQuotation -
        Utils.getOptionalWarrantiesQuotations(
          req.user.leader[0].indivAccident,
          req.user.leader[0].indivAccidentChoix,
          req.user.leader[0].indivEnfants,
          req.user.leader[0].rapatriement,
          req.user.leader[0].rapatriementChoix,
        ).total,
      { symbol: '€', decimal: '.', thousand: ' ', precision: 2, format: '%v' },
    ),
    garantie: req.user.leader[0].formula,
    nbr_adulte: req.user.leader[0].haveConjoint ? 2 : 1,
    nbr_enfant: req.user.leader[0].children
      .map(e =>
        moment(
          new Date(
            `${e.birthdate.split('-')[2]}-${e.birthdate.split('-')[1]}-${
              e.birthdate.split('-')[0]
            }`,
          ),
        ),
      )
      .reduce((acc, current) => (moment().diff(current, 'year') < 26 ? (acc += 1) : acc), 0),
    garantie_accident: req.user.leader[0].indivAccident ? 'on' : '',
    garantie_enfant: req.user.leader[0].indivEnfants ? 'on' : '',
    garantie_rapatriement: req.user.leader[0].rapatriement ? 'on' : '',
    garantie_accident_val: Utils.getOptionalWarrantiesQuotations(
      req.user.leader[0].indivAccident,
      req.user.leader[0].indivAccidentChoix,
      req.user.leader[0].indivEnfants,
      req.user.leader[0].rapatriement,
      req.user.leader[0].rapatriementChoix,
    ).accidentQuotation
      ? currency.format(
          Utils.getOptionalWarrantiesQuotations(
            req.user.leader[0].indivAccident,
            req.user.leader[0].indivAccidentChoix,
            req.user.leader[0].indivEnfants,
            req.user.leader[0].rapatriement,
            req.user.leader[0].rapatriementChoix,
          ).accidentQuotation,
          { symbol: '€', decimal: '.', thousand: ' ', precision: 2, format: '%v' },
        )
      : '',
    garantie_enfant_val: Utils.getOptionalWarrantiesQuotations(
      req.user.leader[0].indivAccident,
      req.user.leader[0].indivAccidentChoix,
      req.user.leader[0].indivEnfants,
      req.user.leader[0].rapatriement,
      req.user.leader[0].rapatriementChoix,
    ).childQuotation
      ? currency.format(
          Utils.getOptionalWarrantiesQuotations(
            req.user.leader[0].indivAccident,
            req.user.leader[0].indivAccidentChoix,
            req.user.leader[0].indivEnfants,
            req.user.leader[0].rapatriement,
            req.user.leader[0].rapatriementChoix,
          ).childQuotation,
          { symbol: '€', decimal: '.', thousand: ' ', precision: 2, format: '%v' },
        )
      : '',
    garantie_rapatriement_val: Utils.getOptionalWarrantiesQuotations(
      req.user.leader[0].indivAccident,
      req.user.leader[0].indivAccidentChoix,
      req.user.leader[0].indivEnfants,
      req.user.leader[0].rapatriement,
      req.user.leader[0].rapatriementChoix,
    ).repatriationQuotation
      ? currency.format(
          Utils.getOptionalWarrantiesQuotations(
            req.user.leader[0].indivAccident,
            req.user.leader[0].indivAccidentChoix,
            req.user.leader[0].indivEnfants,
            req.user.leader[0].rapatriement,
            req.user.leader[0].rapatriementChoix,
          ).repatriationQuotation,
          { symbol: '€', decimal: '.', thousand: ' ', precision: 2, format: '%v' },
        )
      : '',
    cotisation_soussigner: req.user.lastname + ' ' + req.user.name,
    cotisation_date_prelevement: moment(
      new Date(
        `${currentAdhesionByType[0].afterDeadlineDate.split('-')[2]}-${
          currentAdhesionByType[0].afterDeadlineDate.split('-')[1]
        }-${currentAdhesionByType[0].afterDeadlineDate.split('-')[0]}`,
      ),
    )
      .date(10)
      .format('DD-MM-YYYY'),
    cotisation_periodicite: Utils.calculatePeriod(currentAdhesionByType[0].fraction),
    soussigne: req.user.lastname + ' ' + req.user.name,
    num_mobile: req.user.mobile,
    pays_origine: 'France',
    exemplaires_date: moment().format('DD/MM/YYYY'),
    exemplaires_a: 'Paris',
  };

  for (let i = 0; i < req.user.leader[0].children.length; i++) {
    data[`children_${i + 1}_last_name`] = req.user.leader[0].children[i].lastname;
    data[`children_${i + 1}_first_name`] = req.user.leader[0].children[i].name;
    data[`children_${i + 1}_birthday`] = req.user.leader[0].children[i].birthdate;
  }

  data['last_name'] = 'RIGAUD';
  data['first_name'] = 'Christophe';
  data['civility'] = 'Président de VETOPTIM Assurance';
  data['phone'] = '01 77 66 40 89';
  data['email'] = 'contact@vetoptim.com';

  const pdfFiller = new Promise((resolve, reject) => {
    require('pdffiller').fillForm(sourcePDF, destinationPDF, data, err => {
      if (err) return reject(err);
      return resolve(true);
    });
  });

  await pdfFiller.catch(e =>
    console.log('une erreur est survenu lors de la génération du contrat MD'),
  );
};

module.exports.generateRCMSContract = async (
  req,
  currentAdhesionByType,
  fileName,
  sepaMndRef,
  commercial,
) => {
  const sourcePDF = 'assets/pdf/rcms.pdf';
  const destinationPDF = `.permanent/contracts/${fileName}`;

  const data = {
    satisafaction: 'oui',
    raison_sociale: req.user.leader[0].companyName,
    address_siege_social: req.user.address[0].address,
    postal_code: req.user.address[0].postalCode,
    city: req.user.address[0].city,
    phone: req.user.mobile,
    name_dirigeant: req.user.name + ' ' + req.user.lastname,
    email: req.user.email,
    date_creation: moment().format('DD/MM/YYYY'),
    date_activity: moment(new Date(`${req.user.leader[0].startActivityDate}`)).format('DD/MM/YYYY'),
    code_ape: '',
    siret: req.user.leader[0].siren,
    code_naf: '',
    chiffre_affaire_1: currency.format(req.user.leader[0].turnover, {
      symbol: '€',
      decimal: '.',
      thousand: ' ',
      precision: 2,
      format: '%v %s',
    }),
    chiffre_affaire_2: currency.format(req.user.leader[0].chiffreAffaireCours, {
      symbol: '€',
      decimal: '.',
      thousand: ' ',
      precision: 2,
      format: '%v %s',
    }),
    chiffre_affaire_3: currency.format(req.user.leader[0].chiffreAffairePrev, {
      symbol: '€',
      decimal: '.',
      thousand: ' ',
      precision: 2,
      format: '%v %s',
    }),
    total_actif_1: currency.format(req.user.leader[0].totalActifAnnee, {
      symbol: '€',
      decimal: '.',
      thousand: ' ',
      precision: 2,
      format: '%v %s',
    }),
    total_actif_2: currency.format(req.user.leader[0].totalActifCours, {
      symbol: '€',
      decimal: '.',
      thousand: ' ',
      precision: 2,
      format: '%v %s',
    }),
    total_actif_3: currency.format(req.user.leader[0].totalActifPrev, {
      symbol: '€',
      decimal: '.',
      thousand: ' ',
      precision: 2,
      format: '%v %s',
    }),
    capital_propre_1: currency.format(req.user.leader[0].capitauxAnnee, {
      symbol: '€',
      decimal: '.',
      thousand: ' ',
      precision: 2,
      format: '%v %s',
    }),
    capital_propre_2: currency.format(req.user.leader[0].capitauxCours, {
      symbol: '€',
      decimal: '.',
      thousand: ' ',
      precision: 2,
      format: '%v %s',
    }),
    capital_propre_3: currency.format(req.user.leader[0].capitauxPrev, {
      symbol: '€',
      decimal: '.',
      thousand: ' ',
      precision: 2,
      format: '%v %s',
    }),
    resultat_net_1: currency.format(req.user.leader[0].totalAnnee, {
      symbol: '€',
      decimal: '.',
      thousand: ' ',
      precision: 2,
      format: '%v %s',
    }),
    resultat_net_2: currency.format(req.user.leader[0].totalCours, {
      symbol: '€',
      decimal: '.',
      thousand: ' ',
      precision: 2,
      format: '%v %s',
    }),
    resultat_net_3: currency.format(req.user.leader[0].totalPrev, {
      symbol: '€',
      decimal: '.',
      thousand: ' ',
      precision: 2,
      format: '%v %s',
    }),
    prime_annuelle: '218.93 €',
    effect_date: moment(
      new Date(
        `${currentAdhesionByType[0].effectDate.split('-')[2]}-${
          currentAdhesionByType[0].effectDate.split('-')[1]
        }-${currentAdhesionByType[0].effectDate.split('-')[0]}`,
      ),
    ).format('DD/MM/YYYY'),
    fait_a: 'Paris',
    fait_le: moment().format('DD/MM/YYYY'),
  };

  const pdfFiller = new Promise((resolve, reject) => {
    require('pdffiller').fillForm(sourcePDF, destinationPDF, data, err => {
      if (err) return reject(err);
      return resolve(true);
    });
  });

  await pdfFiller.catch(e =>
    console.log('une erreur est survenu lors de la génération du contrat MD'),
  );
};

module.exports.generateRCMSRate = async (
  req,
  currentAdhesionByType,
  fileName,
  sepaMndRef,
  commercial,
) => {
  const sourcePDF = 'assets/pdf/rcms-devis.pdf';
  const destinationPDF = `.permanent/rates/${fileName}`;

  const data = {
    satisafaction: 'oui',
    raison_sociale: req.user.leader[0].companyName,
    address_siege_social: req.user.address[0].address,
    postal_code: req.user.address[0].postalCode,
    city: req.user.address[0].city,
    phone: req.user.mobile,
    name_dirigeant: req.user.name + ' ' + req.user.lastname,
    email: req.user.email,
    date_creation: moment().format('DD/MM/YYYY'),
    date_activity: moment(new Date(`${req.user.leader[0].startActivityDate}`)).format('DD/MM/YYYY'),
    code_ape: '',
    siret: req.user.leader[0].siren,
    code_naf: '',
    chiffre_affaire_1: currency.format(req.user.leader[0].turnover, {
      symbol: '€',
      decimal: '.',
      thousand: ' ',
      precision: 2,
      format: '%v %s',
    }),
    chiffre_affaire_2: currency.format(req.user.leader[0].chiffreAffaireCours, {
      symbol: '€',
      decimal: '.',
      thousand: ' ',
      precision: 2,
      format: '%v %s',
    }),
    chiffre_affaire_3: currency.format(req.user.leader[0].chiffreAffairePrev, {
      symbol: '€',
      decimal: '.',
      thousand: ' ',
      precision: 2,
      format: '%v %s',
    }),
    total_actif_1: currency.format(req.user.leader[0].totalActifAnnee, {
      symbol: '€',
      decimal: '.',
      thousand: ' ',
      precision: 2,
      format: '%v %s',
    }),
    total_actif_2: currency.format(req.user.leader[0].totalActifCours, {
      symbol: '€',
      decimal: '.',
      thousand: ' ',
      precision: 2,
      format: '%v %s',
    }),
    total_actif_3: currency.format(req.user.leader[0].totalActifPrev, {
      symbol: '€',
      decimal: '.',
      thousand: ' ',
      precision: 2,
      format: '%v %s',
    }),
    capital_propre_1: currency.format(req.user.leader[0].capitauxAnnee, {
      symbol: '€',
      decimal: '.',
      thousand: ' ',
      precision: 2,
      format: '%v %s',
    }),
    capital_propre_2: currency.format(req.user.leader[0].capitauxCours, {
      symbol: '€',
      decimal: '.',
      thousand: ' ',
      precision: 2,
      format: '%v %s',
    }),
    capital_propre_3: currency.format(req.user.leader[0].capitauxPrev, {
      symbol: '€',
      decimal: '.',
      thousand: ' ',
      precision: 2,
      format: '%v %s',
    }),
    resultat_net_1: currency.format(req.user.leader[0].totalAnnee, {
      symbol: '€',
      decimal: '.',
      thousand: ' ',
      precision: 2,
      format: '%v %s',
    }),
    resultat_net_2: currency.format(req.user.leader[0].totalCours, {
      symbol: '€',
      decimal: '.',
      thousand: ' ',
      precision: 2,
      format: '%v %s',
    }),
    resultat_net_3: currency.format(req.user.leader[0].totalPrev, {
      symbol: '€',
      decimal: '.',
      thousand: ' ',
      precision: 2,
      format: '%v %s',
    }),
    prime_annuelle: '218.93 €',
    effect_date: moment(
      new Date(
        `${currentAdhesionByType[0].effectDate.split('-')[2]}-${
          currentAdhesionByType[0].effectDate.split('-')[1]
        }-${currentAdhesionByType[0].effectDate.split('-')[0]}`,
      ),
    ).format('DD/MM/YYYY'),
    fait_a: 'Paris',
    fait_le: moment().format('DD/MM/YYYY'),
  };

  const pdfFiller = new Promise((resolve, reject) => {
    require('pdffiller').fillForm(sourcePDF, destinationPDF, data, err => {
      if (err) return reject(err);
      return resolve(true);
    });
  });

  await pdfFiller.catch(e =>
    console.log('une erreur est survenu lors de la génération du contrat MD'),
  );
};

module.exports.getContractTaxFromOffer = (offerType, user, commercial, fraction, contract) => {
  const allTaxes = {
    RC: {
      '1': {
        urbain: {
          value: Quotation.rcUrbanCalculateTAX(
            parseInt(user.customer[0].turnover),
            parseInt(
              commercial ? fraction : user.adhesion.find(e => e.offerType === offerType).fraction,
            ),
            undefined,
            commercial
              ? contract.effectDate
              : user.adhesion.find(e => e.offerType === offerType).effectDate,
            parseFloat(user.customer[0].brokerFee),
          ).TOTAL_TAX_FRAIS_CONTR,
        },
        rurale: {
          value: Quotation.rcRuralCalculateTAX(
            parseInt(user.customer[0].turnover),
            parseInt(
              commercial ? fraction : user.adhesion.find(e => e.offerType === offerType).fraction,
            ),
            undefined,
            commercial
              ? contract.effectDate
              : user.adhesion.find(e => e.offerType === offerType).effectDate,
            parseFloat(user.customer[0].brokerFee),
          ).TOTAL_TAX_FRAIS_CONTR,
        },
      },
      '2': {
        urbain: {
          value: Quotation.rcUrbanCalculateTAX(
            parseInt(user.customer[0].turnover),
            parseInt(
              commercial ? fraction : user.adhesion.find(e => e.offerType === offerType).fraction,
            ),
            undefined,
            commercial
              ? contract.effectDate
              : user.adhesion.find(e => e.offerType === offerType).effectDate,
            parseFloat(user.customer[0].brokerFee),
          ).TOTAL_TAX_FRAIS_CONTR_SEM,
        },
        rurale: {
          value: Quotation.rcRuralCalculateTAX(
            parseInt(user.customer[0].turnover),
            parseInt(
              commercial ? fraction : user.adhesion.find(e => e.offerType === offerType).fraction,
            ),
            undefined,
            commercial
              ? contract.effectDate
              : user.adhesion.find(e => e.offerType === offerType).effectDate,
            parseFloat(user.customer[0].brokerFee),
          ).TOTAL_TAX_FRAIS_CONTR_SEM,
        },
      },
      '4': {
        urbain: {
          value: Quotation.rcUrbanCalculateTAX(
            parseInt(user.customer[0].turnover),
            parseInt(
              commercial ? fraction : user.adhesion.find(e => e.offerType === offerType).fraction,
            ),
            undefined,
            commercial
              ? contract.effectDate
              : user.adhesion.find(e => e.offerType === offerType).effectDate,
            parseFloat(user.customer[0].brokerFee),
          ).TOTAL_TAX_FRAIS_CONTR_TRIM,
        },
        rurale: {
          value: Quotation.rcRuralCalculateTAX(
            parseInt(user.customer[0].turnover),
            parseInt(
              commercial ? fraction : user.adhesion.find(e => e.offerType === offerType).fraction,
            ),
            undefined,
            commercial
              ? contract.effectDate
              : user.adhesion.find(e => e.offerType === offerType).effectDate,
            parseFloat(user.customer[0].brokerFee),
          ).TOTAL_TAX_FRAIS_CONTR_TRIM,
        },
      },
      '12': {
        urbain: {
          value: Quotation.rcUrbanCalculateTAX(
            parseInt(user.customer[0].turnover),
            parseInt(
              commercial ? fraction : user.adhesion.find(e => e.offerType === offerType).fraction,
            ),
            undefined,
            commercial
              ? contract.effectDate
              : user.adhesion.find(e => e.offerType === offerType).effectDate,
            parseFloat(user.customer[0].brokerFee),
          ).TOTAL_TAX_FRAIS_CONTR_MENS,
        },
        rurale: {
          value: Quotation.rcRuralCalculateTAX(
            parseInt(user.customer[0].turnover),
            parseInt(
              commercial ? fraction : user.adhesion.find(e => e.offerType === offerType).fraction,
            ),
            undefined,
            commercial
              ? contract.effectDate
              : user.adhesion.find(e => e.offerType === offerType).effectDate,
            parseFloat(user.customer[0].brokerFee),
          ).TOTAL_TAX_FRAIS_CONTR_MENS,
        },
      },
    },
    MRP: {
      '1': {
        urbain: {
          value: Quotation.MrpUrbanCalculateTAX(
            parseInt(user.customer[0].turnover),
            parseInt(
              commercial ? fraction : user.adhesion.find(e => e.offerType === offerType).fraction,
            ),
            user.customer[0].franchise,
            undefined,
            commercial
              ? contract.effectDate
              : user.adhesion.find(e => e.offerType === offerType).effectDate,
            user.customer[0].brokerFee,
            user.customer[0].alarm,
            user.customer[0].bdm,
          ).TOTAL_TAX_FRAIS_CONTR,
        },
        rurale: {
          value: Quotation.MrpRuralMixteCalculateTAX(
            parseInt(user.customer[0].turnover),
            parseInt(
              commercial ? fraction : user.adhesion.find(e => e.offerType === offerType).fraction,
            ),
            user.customer[0].franchise,
            undefined,
            commercial
              ? contract.effectDate
              : user.adhesion.find(e => e.offerType === offerType).effectDate,
            user.customer[0].brokerFee,
            user.customer[0].alarm,
            user.customer[0].bdm,
          ).TOTAL_TAX_FRAIS_CONTR,
        },
      },
      '2': {
        urbain: {
          value: Quotation.MrpUrbanCalculateTAX(
            parseInt(user.customer[0].turnover),
            parseInt(
              commercial ? fraction : user.adhesion.find(e => e.offerType === offerType).fraction,
            ),
            user.customer[0].franchise,
            undefined,
            commercial
              ? contract.effectDate
              : user.adhesion.find(e => e.offerType === offerType).effectDate,
            user.customer[0].brokerFee,
            user.customer[0].alarm,
            user.customer[0].bdm,
          ).TOTAL_TAX_FRAIS_CONTR_SEM,
        },
        rurale: {
          value: Quotation.MrpRuralMixteCalculateTAX(
            parseInt(user.customer[0].turnover),
            parseInt(
              commercial ? fraction : user.adhesion.find(e => e.offerType === offerType).fraction,
            ),
            user.customer[0].franchise,
            undefined,
            commercial
              ? contract.effectDate
              : user.adhesion.find(e => e.offerType === offerType).effectDate,
            user.customer[0].brokerFee,
            user.customer[0].alarm,
            user.customer[0].bdm,
          ).TOTAL_TAX_FRAIS_CONTR_SEM,
        },
      },
      '4': {
        urbain: {
          value: Quotation.MrpUrbanCalculateTAX(
            parseInt(user.customer[0].turnover),
            parseInt(
              commercial ? fraction : user.adhesion.find(e => e.offerType === offerType).fraction,
            ),
            user.customer[0].franchise,
            undefined,
            commercial
              ? contract.effectDate
              : user.adhesion.find(e => e.offerType === offerType).effectDate,
            user.customer[0].brokerFee,
            user.customer[0].alarm,
            user.customer[0].bdm,
          ).TOTAL_TAX_FRAIS_CONTR_TRIM,
        },
        rurale: {
          value: Quotation.MrpRuralMixteCalculateTAX(
            parseInt(user.customer[0].turnover),
            parseInt(
              commercial ? fraction : user.adhesion.find(e => e.offerType === offerType).fraction,
            ),
            user.customer[0].franchise,
            undefined,
            commercial
              ? contract.effectDate
              : user.adhesion.find(e => e.offerType === offerType).effectDate,
            user.customer[0].brokerFee,
            user.customer[0].alarm,
            user.customer[0].bdm,
          ).TOTAL_TAX_FRAIS_CONTR_TRIM,
        },
      },
      '12': {
        urbain: {
          value: Quotation.MrpUrbanCalculateTAX(
            parseInt(user.customer[0].turnover),
            parseInt(
              commercial ? fraction : user.adhesion.find(e => e.offerType === offerType).fraction,
            ),
            user.customer[0].franchise,
            undefined,
            commercial
              ? contract.effectDate
              : user.adhesion.find(e => e.offerType === offerType).effectDate,
            user.customer[0].brokerFee,
            user.customer[0].alarm,
            user.customer[0].bdm,
          ).TOTAL_TAX_FRAIS_CONTR_MENS,
        },
        rurale: {
          value: Quotation.MrpRuralMixteCalculateTAX(
            parseInt(user.customer[0].turnover),
            parseInt(
              commercial ? fraction : user.adhesion.find(e => e.offerType === offerType).fraction,
            ),
            user.customer[0].franchise,
            undefined,
            commercial
              ? contract.effectDate
              : user.adhesion.find(e => e.offerType === offerType).effectDate,
            user.customer[0].brokerFee,
            user.customer[0].alarm,
            user.customer[0].bdm,
          ).TOTAL_TAX_FRAIS_CONTR_MENS,
        },
      },
    },
    'MRP + RC': {
      '1': {
        urbain: {
          value: Quotation.MrpCrUrbanCalculateTAX(
            parseInt(user.customer[0].turnover),
            parseInt(
              commercial ? fraction : user.adhesion.find(e => e.offerType === offerType).fraction,
            ),
            user.customer[0].franchise,
            undefined,
            commercial
              ? contract.effectDate
              : user.adhesion.find(e => e.offerType === offerType).effectDate,
            user.customer[0].brokerFee,
            user.customer[0].alarm,
            user.customer[0].bdm,
          ).TOTAL_TAX_FRAIS_CONTR,
        },
        rurale: {
          value: Quotation.MrpCrRuralMixedCalculateTAX(
            parseInt(user.customer[0].turnover),
            parseInt(
              commercial ? fraction : user.adhesion.find(e => e.offerType === offerType).fraction,
            ),
            user.customer[0].franchise,
            undefined,
            commercial
              ? contract.effectDate
              : user.adhesion.find(e => e.offerType === offerType).effectDate,
            user.customer[0].brokerFee,
            user.customer[0].alarm,
            user.customer[0].bdm,
          ).TOTAL_TAX_FRAIS_CONTR,
        },
      },
      '2': {
        urbain: {
          value: Quotation.MrpCrUrbanCalculateTAX(
            parseInt(user.customer[0].turnover),
            parseInt(
              commercial ? fraction : user.adhesion.find(e => e.offerType === offerType).fraction,
            ),
            user.customer[0].franchise,
            undefined,
            commercial
              ? contract.effectDate
              : user.adhesion.find(e => e.offerType === offerType).effectDate,
            user.customer[0].brokerFee,
            user.customer[0].alarm,
            user.customer[0].bdm,
          ).TOTAL_TAX_FRAIS_CONTR_SEM,
        },
        rurale: {
          value: Quotation.MrpCrRuralMixedCalculateTAX(
            parseInt(user.customer[0].turnover),
            parseInt(
              commercial ? fraction : user.adhesion.find(e => e.offerType === offerType).fraction,
            ),
            user.customer[0].franchise,
            undefined,
            commercial
              ? contract.effectDate
              : user.adhesion.find(e => e.offerType === offerType).effectDate,
            user.customer[0].brokerFee,
            user.customer[0].alarm,
            user.customer[0].bdm,
          ).TOTAL_TAX_FRAIS_CONTR_SEM,
        },
      },
      '4': {
        urbain: {
          value: Quotation.MrpCrUrbanCalculateTAX(
            parseInt(user.customer[0].turnover),
            parseInt(
              commercial ? fraction : user.adhesion.find(e => e.offerType === offerType).fraction,
            ),
            user.customer[0].franchise,
            undefined,
            commercial
              ? contract.effectDate
              : user.adhesion.find(e => e.offerType === offerType).effectDate,
            user.customer[0].brokerFee,
            user.customer[0].alarm,
            user.customer[0].bdm,
          ).TOTAL_TAX_FRAIS_CONTR_TRIM,
        },
        rurale: {
          value: Quotation.MrpCrRuralMixedCalculateTAX(
            parseInt(user.customer[0].turnover),
            parseInt(
              commercial ? fraction : user.adhesion.find(e => e.offerType === offerType).fraction,
            ),
            user.customer[0].franchise,
            undefined,
            commercial
              ? contract.effectDate
              : user.adhesion.find(e => e.offerType === offerType).effectDate,
            user.customer[0].brokerFee,
            user.customer[0].alarm,
            user.customer[0].bdm,
          ).TOTAL_TAX_FRAIS_CONTR_TRIM,
        },
      },
      '12': {
        urbain: {
          value: Quotation.MrpCrUrbanCalculateTAX(
            parseInt(user.customer[0].turnover),
            parseInt(
              commercial ? fraction : user.adhesion.find(e => e.offerType === offerType).fraction,
            ),
            user.customer[0].franchise,
            undefined,
            commercial
              ? contract.effectDate
              : user.adhesion.find(e => e.offerType === offerType).effectDate,
            user.customer[0].brokerFee,
            user.customer[0].alarm,
            user.customer[0].bdm,
          ).TOTAL_TAX_FRAIS_CONTR_MENS,
        },
        rurale: {
          value: Quotation.MrpCrRuralMixedCalculateTAX(
            parseInt(user.customer[0].turnover),
            parseInt(
              commercial ? fraction : user.adhesion.find(e => e.offerType === offerType).fraction,
            ),
            user.customer[0].franchise,
            undefined,
            commercial
              ? contract.effectDate
              : user.adhesion.find(e => e.offerType === offerType).effectDate,
            user.customer[0].brokerFee,
            user.customer[0].alarm,
            user.customer[0].bdm,
          ).TOTAL_TAX_FRAIS_CONTR_MENS,
        },
      },
    },
    RCMS: {
      value: Quotation.rcmsCalculateQuotation(
        undefined,
        parseInt(
          commercial ? fraction : user.adhesion.find(e => e.offerType === offerType).fraction,
        ),
        commercial
          ? contract.effectDate
          : user.adhesion.find(e => e.offerType === offerType).effectDate,
        parseFloat(user.customer[0].brokerFee).toFixed(2),
      ).totTaxesFrais,
    },
  };

  if (offerType === 'RCMS') {
    return allTaxes[offerType]['value'];
  } else {
    return allTaxes[offerType][
      commercial ? fraction : user.adhesion.find(e => e.offerType === offerType).fraction
    ][user.customer[0].activity]['value'];
  }
};
