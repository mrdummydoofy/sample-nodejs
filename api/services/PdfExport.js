// generate pdf service

const puppeteer = require('puppeteer');
const ejs = require('ejs');
const util = require('util');
const fs = require('fs');
const readFile = util.promisify(fs.readFile);

module.exports.generate = async (template, data, pdfPath) => {
  try {
    const browser = await puppeteer.launch({ args: ['--no-sandbox'] });
    const page = await browser.newPage();
    const file = await readFile(template, 'utf8');
    const html = ejs.render(file, data);
    await page.setContent(html);
    await page.emulateMedia('screen');
    const pdfFile = await page.pdf({
      path: pdfPath,
      format: 'A4',
      printBackground: true,
    });
    await browser.close();
    return pdfFile;
  } catch (e) {
    console.log('generate pdf error : ', e);
  }
};
