const excel = require('node-excel-export');
const fs = require('fs');
const path = require('path');
const moment = require('moment');

module.exports.buildReport = async (contractsFirstPayments, contractsRecurrings, day) => {
  const styles = {
    headerDark: {
      font: {
        color: {
          rgb: 'FF000000',
        },
        sz: 12,
        bold: true,
      },
    },
  };

  const specification = {
    lastname: {
      displayName: 'Prénom',
      headerStyle: styles.headerDark,
      width: 120,
    },
    exactTitle: {
      displayName: 'Nom du souscripteur',
      headerStyle: styles.headerDark,
      width: 220,
    },
    holder: {
      displayName: 'Titulaire du compte',
      headerStyle: styles.headerDark,
      width: 220,
    },
    IBAN: {
      displayName: 'IBAN',
      headerStyle: styles.headerDark,
      width: 220,
    },
    BIC: {
      displayName: 'BIC',
      headerStyle: styles.headerDark,
      width: 220,
    },
    day: {
      displayName: 'Jour de prélèvement',
      headerStyle: styles.headerDark,
      width: 180,
    },
    effectDate: {
      displayName: "Date d'effet",
      headerStyle: styles.headerDark,
      width: 120,
    },
    periodicity: {
      displayName: 'Périodicité',
      headerStyle: styles.headerDark,
      width: 150,
    },
    monthlyQuotation: {
      displayName: 'Cotisation',
      headerStyle: styles.headerDark,
      width: 200,
    },
    cashQuotation: {
      displayName: 'Souscription',
      headerStyle: styles.headerDark,
      width: 200,
    },
    type: {
      displayName: 'Type',
      headerStyle: styles.headerDark,
      width: 200,
    },
    signatureDate: {
      displayName: 'Date de signature',
      headerStyle: styles.headerDark,
      width: 200,
    },
  };

  let dataset = [];

  for (let i = 0; i < contractsFirstPayments.length; i++) {
    let periodicity = null;
    if (contractsFirstPayments[i].quotation[0].fraction === 1) {
      periodicity = 'annuelle';
    }
    if (contractsFirstPayments[i].quotation[0].fraction === 2) {
      periodicity = 'semestrielle';
    }
    if (contractsFirstPayments[i].quotation[0].fraction === 4) {
      periodicity = 'trimestrielle';
    }
    if (contractsFirstPayments[i].quotation[0].fraction === 12) {
      periodicity = 'mensuelle';
    }

    const payment = await PaymentMethod.find({ user: contractsFirstPayments[i].user.id });
    dataset.push({
      lastname: contractsFirstPayments[i].user.lastname,
      exactTitle: payment[0].exactTitle,
      holder: payment[0].holder,
      IBAN: payment[0].IBAN,
      BIC: payment[0].BIC,
      day: payment[0].pickingDate,
      effectDate: moment(contractsFirstPayments[i].effectDate).format('DD-MM-YYYY'),
      periodicity: periodicity,
      monthlyQuotation: contractsFirstPayments[i].quotation[0].ttc,
      cashQuotation: contractsFirstPayments[i].quotation[0].cashQuotation,
      type: 'nouveau',
      signatureDate: moment(contractsFirstPayments[i].signatureDate).format('DD-MM-YYYY'),
    });
  }

  for (let i = 0; i < contractsRecurrings.length; i++) {
    let periodicity = null;
    if (contractsRecurrings[i].fraction === 1) {
      periodicity = 'annuelle';
    }
    if (contractsRecurrings[i].fraction === 2) {
      periodicity = 'semestrielle';
    }
    if (contractsRecurrings[i].fraction === 4) {
      periodicity = 'trimestrielle';
    }
    if (contractsRecurrings[i].fraction === 12) {
      periodicity = 'mensuelle';
    }

    const payment = await PaymentMethod.find({ user: contractsRecurrings[i].user }).populate(
      'user',
    );
    dataset.push({
      lastname: payment[0].user.lastname,
      exactTitle: payment[0].exactTitle,
      holder: payment[0].holder,
      IBAN: payment[0].IBAN,
      BIC: payment[0].BIC,
      day: payment[0].pickingDate,
      effectDate: moment(contractsRecurrings[i].effectDate).format('DD-MM-YYYY'),
      periodicity: periodicity,
      monthlyQuotation: contractsRecurrings[i].ttc,
      cashQuotation: contractsRecurrings[i].cashQuotation,
      type: 'recurrent',
      signatureDate: moment(contractsRecurrings[i].signatureDate).format('DD-MM-YYYY'),
    });
  }

  const report = excel.buildExport([
    {
      name: 'Report',
      specification: specification,
      data: dataset,
    },
  ]);

  fs.writeFileSync(
    path.join(
      __dirname,
      '..',
      '..',
      '.permanent',
      'excels',
      `prelevements_${moment(day)
        .subtract(2, 'days')
        .format('YYYY-MM-DD')}.xlsx`,
    ),
    report,
  );
};
