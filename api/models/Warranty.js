/**
 * Warranty.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝

    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    status: {
      //statutgarantie
      type: 'boolean',
    },
    franchise: {
      //franchise//
      type: 'number',
    },
    risk: {
      //Risque//
      type: 'string',
    },
    contract: {
      model: 'contract',
      unique: true,
    },
  },

  // Garanties MRP //

  mrpWarranties(turnover, bdm) {
    // Variables
    let indice = 3.75;
    let perteRecettes;
    let perteValeurVenale;
    let incendieReconstitution;
    let tempeteReconstitution;
    let vandalismeReconstitution;
    let brisMachine = 20000;
    let fraisSupplementaire;

    // Logique
    perteRecettes = turnover;
    perteValeurVenale = turnover;
    incendieReconstitution = indice * 1000;
    tempeteReconstitution = indice * 1000;
    vandalismeReconstitution = indice * 1000;
    fraisSupplementaire = turnover * 0.2;

    if (bdm === 'limite20000') {
      brisMachine = 20000;
    } else if (bdm === 'limite40000') {
      brisMachine = 40000;
    } else if (bdm === 'limite60000') {
      brisMachine = 60000;
    } else if (bdm === 'limite80000') {
      brisMachine = 80000;
    } else {
      brisMachine = 100000;
    }

    return {
      perteRecettes,
      perteValeurVenale,
      incendieReconstitution,
      tempeteReconstitution,
      vandalismeReconstitution,
      brisMachine,
      fraisSupplementaire,
    };
  },
};
