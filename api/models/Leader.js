/**
 * Leader.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝

    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    reference: {
      type: 'number',
    },
    function: {
      type: 'string',
      defaultsTo: 'vétérinaire',
    },
    leaderBirthdate: {
      type: 'string',
    },
    haveConjoint: {
      type: 'boolean',
    },
    conjointBirthdate: {
      type: 'string',
    },
    conjointName: {
      type: 'string',
    },
    conjointLastname: {
      type: 'string',
    },
    haveChildren: {
      type: 'boolean',
    },
    childrenNumber: {
      type: 'number',
    },
    siren: {
      type: 'string',
      minLength: 9,
    },
    companyName: {
      type: 'string',
    },
    legalForm: {
      type: 'string',
    },
    startActivityDate: {
      type: 'string',
    },
    indivAccident: {
      type: 'boolean',
    },
    indivAccidentChoix: {
      type: 'string',
    },
    indivEnfants: {
      type: 'boolean',
    },
    rapatriement: {
      type: 'boolean',
    },
    rapatriementChoix: {
      type: 'string',
    },
    turnover: {
      type: 'number',
    },
    capitauxPropres: {
      type: 'boolean',
    },
    resultatNet: {
      type: 'boolean',
    },
    formula: {
      type: 'number',
      defaultsTo: 1,
    },
    turnover: {
      type: 'number',
    },
    chiffreAffaireCours: {
      type: 'number',
    },
    chiffreAffairePrev: {
      type: 'number',
    },
    totalActifAnnee: {
      type: 'number',
    },
    totalActifCours: {
      type: 'number',
    },
    totalActifPrev: {
      type: 'number',
    },
    capitauxAnnee: {
      type: 'number',
    },
    capitauxCours: {
      type: 'number',
    },
    capitauxPrev: {
      type: 'number',
    },
    totalAnnee: {
      type: 'number',
    },
    totalCours: {
      type: 'number',
    },
    totalPrev: {
      type: 'number',
    },
    children: {
      collection: 'children',
      via: 'leader',
    },
    user: {
      model: 'user',
      unique: true,
    },
  },
};
