/**
 * Help.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    phone: { //telephone demande d'aide//
      type: 'string',
    },
    date: { //date demande d'aide//
      type: 'string',
    },
    hour: { // heure demande d'aide}
      type: 'string',
    },
    status: { //statut demande d'aide//
      type: 'string',
      isIn: ['new', 'unreachable', 'closed'],
      defaultsTo: 'new'
    },
    choice: { //choix demande d'aide//
      type: 'string'
    },
    title: { //titre
      type: 'string',
      minLength: 2
    },
    name: { //nom
      type: 'string',
      minLength: 3
    },
    lastname: { //prénom
      type: 'string',
      minLength: 3
    },
    email: { //email//
      type: 'string',
    },
    offerType: { //offerType//
      type: 'string',
      allowNull: true
    },
    user: {
      model: 'user'
    },
  },

};

