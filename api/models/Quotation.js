/**
 * Quotation.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */
const moment = require('moment');

module.exports = {
  attributes: {
    fraction: {
      //Fractionnement
      type: 'number',
    },
    tarifType: {
      //type tarif
      type: 'string',
    },
    period: {
      // période
      type: 'string',
      isIn: ['annuelle', 'semestrielle', 'trimestrielle', 'mensuelle'],
    },
    commission: {
      //commission
      type: 'number',
    },
    ttc: {
      //cotisation récurrente
      type: 'number',
    },
    net: {
      //cotisationNet
      type: 'number',
    },
    tax: {
      //taxes
      type: 'number',
    },
    offerType: {
      //formule de l'offre
      type: 'string',
      isIn: ['standard', 'GIE', 'MRP + RC', 'RC', 'MRP', 'RCMS', 'MD'],
    },
    brokerFee: {
      //frais courtier
      type: 'number',
    },
    cashQuotation: {
      //cotisation comptant
      type: 'number',
    },
    contract: {
      model: 'contract',
      unique: true,
    },
  },

  // RC Quotation for Urban Activity
  crUrbanQuotation(turnover) {
    // var declarations
    let vetoTarifPercent;
    let minProvionQuot;
    let crAnnualQuot;
    let crUrbanQuotation;

    // logic
    if (turnover <= 250000) {
      vetoTarifPercent = 0.0017;
      minProvionQuot = 330;
    } else if (turnover <= 500000 && turnover > 250000) {
      vetoTarifPercent = 0.0016;
      minProvionQuot = 425;
    } else if (turnover <= 750000 && turnover > 500000) {
      vetoTarifPercent = 0.0015;
      minProvionQuot = 800;
    } else if (turnover <= 1000000 && turnover > 750000) {
      vetoTarifPercent = 0.0014;
      minProvionQuot = 1125;
    } else if (turnover <= 1250000 && turnover > 1000000) {
      vetoTarifPercent = 0.0013;
      minProvionQuot = 1400;
    } else {
      vetoTarifPercent = 0.0012;
      minProvionQuot = 1625;
    }

    crAnnualQuot = turnover * vetoTarifPercent;

    if (crAnnualQuot > minProvionQuot) {
      crUrbanQuotation = crAnnualQuot;
    } else {
      crUrbanQuotation = minProvionQuot;
    }

    // result
    return {
      crUrbanQuotation,
      vetoTarifPercent,
    };
  },

  // RC Quotation for Rural or mixed Activity
  crRuralMixteQuotation(turnover) {
    // var declarations
    let vetoTarifPercent;
    let minProvionQuot;
    let crAnnualQuot;
    let crRuralMixteQuotation;

    // logic
    if (turnover <= 250000) {
      vetoTarifPercent = 0.0032;
      minProvionQuot = 650;
    } else if (turnover <= 500000 && turnover > 250000) {
      vetoTarifPercent = 0.0031;
      minProvionQuot = 800;
    } else if (turnover <= 750000 && turnover > 500000) {
      vetoTarifPercent = 0.003;
      minProvionQuot = 1550;
    } else if (turnover <= 1000000 && turnover > 750000) {
      vetoTarifPercent = 0.0029;
      minProvionQuot = 2250;
    } else if (turnover <= 1250000 && turnover > 1000000) {
      vetoTarifPercent = 0.0028;
      minProvionQuot = 2900;
    } else {
      vetoTarifPercent = 0.0027;
      minProvionQuot = 3500;
    }

    crAnnualQuot = turnover * vetoTarifPercent;

    if (crAnnualQuot > minProvionQuot) {
      crRuralMixteQuotation = crAnnualQuot;
    } else {
      crRuralMixteQuotation = minProvionQuot;
    }

    // result
    return {
      crRuralMixteQuotation,
      vetoTarifPercent,
    };
  },

  // MRP Quotation for Urban Activity
  mrpUrbanQuotation(turnover) {
    // var declarations
    let vetoTarifPercent;
    let minProvionQuot;
    let mrpAnnualQuot;
    let mrpUrbanQuotation;
    let mrpUrbanQuotationPromo;

    // logic
    if (turnover <= 250000) {
      vetoTarifPercent = 0.0036;
      minProvionQuot = 675;
    } else if (turnover <= 500000 && turnover > 250000) {
      vetoTarifPercent = 0.0027;
      minProvionQuot = 900;
    } else if (turnover <= 750000 && turnover > 500000) {
      vetoTarifPercent = 0.0022;
      minProvionQuot = 1350;
    } else if (turnover <= 1000000 && turnover > 750000) {
      vetoTarifPercent = 0.002;
      minProvionQuot = 1650;
    } else if (turnover <= 1250000 && turnover > 1000000) {
      vetoTarifPercent = 0.0018;
      minProvionQuot = 2000;
    } else {
      vetoTarifPercent = 0.0016;
      minProvionQuot = 2250;
    }

    mrpAnnualQuot = turnover * vetoTarifPercent;

    if (mrpAnnualQuot > minProvionQuot) {
      mrpUrbanQuotation = mrpAnnualQuot;
    } else {
      mrpUrbanQuotation = minProvionQuot;
    }

    mrpUrbanQuotationPromo = mrpUrbanQuotation - mrpUrbanQuotation * 0.1;

    // result
    return {
      mrpUrbanQuotation,
      mrpUrbanQuotationPromo,
      vetoTarifPercent,
    };
  },

  // MRP Quotation for Rural or mixed Activity
  mrpRuralMixteQuotation(turnover) {
    // var declarations
    let vetoTarifPercent;
    let minProvionQuot;
    let mrpAnnualQuot;
    let mrpRuralMixteQuotation;
    let mrpRuralMixteQuotationPromo;

    // logic
    if (turnover <= 250000) {
      vetoTarifPercent = 0.004472;
      minProvionQuot = 849;
    } else if (turnover <= 500000 && turnover > 250000) {
      vetoTarifPercent = 0.003272;
      minProvionQuot = 1118;
    } else if (turnover <= 750000 && turnover > 500000) {
      vetoTarifPercent = 0.002624;
      minProvionQuot = 1636;
    } else if (turnover <= 1000000 && turnover > 750000) {
      vetoTarifPercent = 0.00236;
      minProvionQuot = 1968;
    } else if (turnover <= 1250000 && turnover > 1000000) {
      vetoTarifPercent = 0.002096;
      minProvionQuot = 2360;
    } else {
      vetoTarifPercent = 0.0018;
      minProvionQuot = 2620;
    }

    mrpAnnualQuot = turnover * vetoTarifPercent;

    if (mrpAnnualQuot > minProvionQuot) {
      mrpRuralMixteQuotation = mrpAnnualQuot;
    } else {
      mrpRuralMixteQuotation = minProvionQuot;
    }

    mrpRuralMixteQuotationPromo = mrpRuralMixteQuotation - mrpRuralMixteQuotation * 0.1;

    // result
    return {
      mrpRuralMixteQuotation,
      mrpRuralMixteQuotationPromo,
      vetoTarifPercent,
    };
  },

  // (MRP+RC) Quotation for Urban Activity
  mrpRcUrbanQuotation(turnover) {
    // var declarations
    let vetoTarifPercent;
    let minProvionQuot;
    let AnnualQuot;
    let mrpRcUrbanQuotation;
    let mrpRcUrbanQuotationPromo;

    // logic
    if (turnover <= 250000) {
      vetoTarifPercent = 0.004505;
      minProvionQuot = 854.25;
    } else if (turnover <= 500000 && turnover >= 250001) {
      vetoTarifPercent = 0.00366;
      minProvionQuot = 1126.25;
    } else if (turnover <= 737109 && turnover >= 500001) {
      vetoTarifPercent = 0.0032;
      minProvionQuot = 1827.5;
    } else if (turnover <= 963333 && turnover >= 737110) {
      vetoTarifPercent = 0.003;
      minProvionQuot = 2358.75;
    } else if (turnover <= 1176339 && turnover >= 963334) {
      vetoTarifPercent = 0.0028;
      minProvionQuot = 2890;
    } else {
      vetoTarifPercent = 0.00238;
      minProvionQuot = 3293.75;
    }

    AnnualQuot = turnover * vetoTarifPercent;

    if (AnnualQuot > minProvionQuot) {
      mrpRcUrbanQuotation = AnnualQuot;
    } else {
      mrpRcUrbanQuotation = minProvionQuot;
    }

    mrpRcUrbanQuotationPromo = mrpRcUrbanQuotation - mrpRcUrbanQuotation * 0.1;

    // result
    return {
      mrpRcUrbanQuotation,
      mrpRcUrbanQuotationPromo,
      vetoTarifPercent,
    };
  },

  // (MRP+RC) Quotation for Rural or mixed Activity
  mrpRcRuralMixteQuotation(turnover) {
    // var declarations
    let vetoTarifPercent;
    let minProvionQuot;
    let AnnualQuot;
    let mrpRcRuralMixteQuotation;
    let mrpRcRuralMixteQuotationPromo;

    // logic
    if (turnover <= 250000) {
      vetoTarifPercent = 0.005754;
      minProvionQuot = 1124.25;
    } else if (turnover <= 500000 && turnover >= 250001) {
      vetoTarifPercent = 0.004779;
      minProvionQuot = 1438.5;
    } else if (turnover <= 566232 && turnover >= 500001) {
      vetoTarifPercent = 0.00422;
      minProvionQuot = 2389.5;
    } else if (turnover <= 750000 && turnover >= 566232) {
      vetoTarifPercent = 0.004218;
      minProvionQuot = 2389.5;
    } else if (turnover <= 996212 && turnover >= 750001) {
      vetoTarifPercent = 0.00396;
      minProvionQuot = 3163.5;
    } else if (turnover <= 1250000 && turnover >= 996213) {
      vetoTarifPercent = 0.003672;
      minProvionQuot = 3945;
    } else {
      vetoTarifPercent = 0.003375;
      minProvionQuot = 4590;
    }

    AnnualQuot = turnover * vetoTarifPercent;

    if (AnnualQuot > minProvionQuot) {
      mrpRcRuralMixteQuotation = AnnualQuot;
    } else {
      mrpRcRuralMixteQuotation = minProvionQuot;
    }

    mrpRcRuralMixteQuotationPromo = mrpRcRuralMixteQuotation - mrpRcRuralMixteQuotation * 0.1;

    // result
    return {
      mrpRcRuralMixteQuotation,
      mrpRcRuralMixteQuotationPromo,
      vetoTarifPercent,
    };
  },

  //METHODE DE CALCUL DES TAXES DE L'OFFRE MRP URBAIN
  MrpUrbanCalculateTAX(
    turnover,
    fraction,
    franchise,
    quotation,
    effectDate,
    brokerFee,
    alarm,
    bdm,
  ) {
    // DECLARATION CONSTANTES
    const COEF_REP_INCENDIE = 0.069104;
    const COEF_REP_TEMPETE = 0.016934;
    const COEF_REP_VANDALISME = 0.01218;
    const COEF_REP_DELECT = 0.010862;
    const COEF_REP_BDM = 0;
    const COEF_REP_MARCH = 0;
    const COEF_REP_DDE = 0.059525;
    const COEF_REP_BDG = 0.135389;
    const COEF_REP_VOL = 0.442258;
    const COEF_REP_PE = 0.076886;
    const COEF_REP_PVV = 0;
    const COEF_REP_MARCH_TRANSP = 0.049272;
    const COEF_ATT = 0.04;
    const COEF_CATNAT = 0.12;
    const COEF_REP_RC = 0;
    const COEF_REP_PJ = 0;
    const COEF_TAX_INCENDIE = 0.07;
    const COEF_TAX_TEMPETE = 0.09;
    const COEF_TAX_VANDALISME = 0.09;
    const COEF_TAX_DELECT = 0.09;
    const COEF_TAX_BDM = 0.09;
    const COEF_TAX_MARCH = 0.09;
    const COEF_TAX_DDE = 0.09;
    const COEF_TAX_BDG = 0.09;
    const COEF_TAX_VOL = 0.09;
    const COEF_TAX_PE = 0.07;
    const COEF_TAX_PVV = 0.07;
    const COEF_TAX_RC = 0.09;
    const COEF_TAX_PJ = 0.134;
    const COEF_TAX_MARCH_TRANSP = 0;
    const COEF_TAX_ATT = 0.09;
    const COEF_TAX_CATNAT = 0;
    const COEF_TAX_ASSISTANCE = 0.0906;
    const COT_ASSISTANCE_HT = 5.3;
    let TAX_COURTIER = null;
    if (brokerFee !== undefined) {
      TAX_COURTIER = brokerFee;
    } else {
      TAX_COURTIER = 0.3;
    }
    // DECLARATION VARIABLES
    let CONTRB_ATT = 5.9;
    let TAX_BDM = 0;
    let TAX_MARCH = 0;
    let TAX_PVV = 0;
    let TAX_RC = 0;
    let TAX_PJ = 0;
    let TAX_MARCH_TRANSP = 0;
    let mrpUrbanQuot = null;
    if (quotation) {
      mrpUrbanQuot = quotation;
    } else {
      mrpUrbanQuot = Quotation.mrpUrbanQuotation(turnover);
    }
    let TOTAL_COT_HT;
    let COT_INCENDIE_HT;
    let COT_TEMPETE_HT;
    let COT_VANDALISME_HT;
    let COT_DELECT_HT;
    let COT_BDM_HT;
    let COT_MARCH_HT;
    let COT_DDE_HT;
    let COT_BDG_HT;
    let COT_VOL_HT;
    let COT_PE_HT;
    let COT_PVV_HT;
    let COT_RC_HT;
    let COT_PJ_HT;
    let COT_MARCH_TRANSP_HT;
    let COT_ATT_HT;
    let COT_CATNAT_HT;
    let TAX_INCENDIE;
    let TAX_TEMPETE;
    let TAX_VANDALISME;
    let TAX_DELECT;
    let TAX_DDE;
    let TAX_BDG;
    let TAX_VOL;
    let TAX_PE;
    let TAX_ATT;
    let TAX_CATNAT;
    let TAX_ASSISTANCE;
    let TOTAL_TAX;
    let TOT_TAXES;
    let TOTAL_TAX_FRAIS_CONTR;
    let COT_ASSISTANCE_TTC;
    let COT_INCENDIE_TTC;
    let COT_TEMPETE_TTC;
    let COT_VANDALISME_TTC;
    let COT_DELECT_TTC;
    let COT_BDM_TTC;
    let COT_MARCH_TTC;
    let COT_DDE_TTC;
    let COT_BDG_TTC;
    let COT_VOL_TTC;
    let COT_PE_TTC;
    let COT_PVV_TTC;
    let COT_RC_TTC;
    let COT_PJ_TTC;
    let COT_MARCH_TRANSP_TTC;
    let ASSIET_ATT;
    let ASSIET_CATNAT;
    let FRAIS_COURTIER;
    let TOT_CONTR_ATT;
    let TOT_CATNAT;
    let mrpUrbanQuotation;
    let cashQuotation;
    let remainingMonths;
    let PRIM_ANN;
    let fractQuotation;
    let TOTAL_TAX_SEM;
    let TOTAL_TAX_TRIM;
    let TOTAL_TAX_MENS;
    let FRAIS_COURTIER_SEM;
    let FRAIS_COURTIER_TRIM;
    let FRAIS_COURTIER_MENS;
    let TOTAL_TAX_FRAIS_CONTR_SEM;
    let TOTAL_TAX_FRAIS_CONTR_TRIM;
    let TOTAL_TAX_FRAIS_CONTR_MENS;
    let CONTRB_ATT_SEM;
    let CONTRB_ATT_TRIM;
    let CONTRB_ATT_MENS;
    let TOT_HT;
    let TOTAL_COT_HT_SEM;
    let TOTAL_COT_HT_TRIM;
    let TOTAL_COT_HT_MENS;
    let TOTAL_COT_TTC;
    let RED_VOL_HT;
    let RED_ATT;
    let RED_CATNAT;
    let RED_CATNAT_TOT;
    let RED_VOL_TTC;
    let RED_TAX;
    let RED_VOL_TTC_SEM;
    let RED_VOL_TTC_TRIM;
    let RED_VOL_TTC_MENS;
    let remainingDays;

    remainingDays =
      1 +
      moment(
        new Date(
          `${Utils.calculateDeadline(fraction, effectDate).deadlineDate.split('-')[2]}-${
            Utils.calculateDeadline(fraction, effectDate).deadlineDate.split('-')[1]
          }-${Utils.calculateDeadline(fraction, effectDate).deadlineDate.split('-')[0]}`,
        ),
      ).diff(
        moment(
          new Date(
            `${effectDate.split('-')[2]}-${effectDate.split('-')[1]}-${effectDate.split('-')[0]}`,
          ),
        ),
        'days',
      );
    // logic
    // FRANCHISE 300
    if (franchise === 300) {
      mrpUrbanQuotation = mrpUrbanQuot.mrpUrbanQuotation;
      // FRANCHISE 500
    } else {
      mrpUrbanQuotation = mrpUrbanQuot.mrpUrbanQuotationPromo;
    }
    //TAXE ASSISTANCE
    TAX_ASSISTANCE = COT_ASSISTANCE_HT * COEF_TAX_ASSISTANCE;
    //COTISATION ASSISTANCE TTC
    COT_ASSISTANCE_TTC = COT_ASSISTANCE_HT + TAX_ASSISTANCE;
    //PRIME ANNUELLE HORS ASSISTANCE ET ATTENTATS
    TOTAL_COT_TTC = mrpUrbanQuotation - (CONTRB_ATT + COT_ASSISTANCE_TTC);
    // COTISATIONS DE REFERENCE TTC
    COT_INCENDIE_TTC = COEF_REP_INCENDIE * TOTAL_COT_TTC;
    COT_TEMPETE_TTC = COEF_REP_TEMPETE * TOTAL_COT_TTC;
    COT_VANDALISME_TTC = COEF_REP_VANDALISME * TOTAL_COT_TTC;
    COT_DELECT_TTC = COEF_REP_DELECT * TOTAL_COT_TTC;
    COT_MARCH_TTC = COEF_REP_MARCH * TOTAL_COT_TTC;
    COT_BDM_TTC = COEF_REP_BDM * TOTAL_COT_TTC;
    COT_DDE_TTC = COEF_REP_DDE * TOTAL_COT_TTC;
    COT_BDG_TTC = COEF_REP_BDG * TOTAL_COT_TTC;
    COT_VOL_TTC = COEF_REP_VOL * TOTAL_COT_TTC;
    COT_PE_TTC = COEF_REP_PE * TOTAL_COT_TTC;
    COT_PVV_TTC = COEF_REP_PVV * TOTAL_COT_TTC;
    COT_RC_TTC = COEF_REP_RC * TOTAL_COT_TTC;
    COT_PJ_TTC = COEF_REP_PJ * TOTAL_COT_TTC;
    COT_MARCH_TRANSP_TTC = COEF_REP_MARCH_TRANSP * TOTAL_COT_TTC;
    // COTISATIONS DE REFERENCE HT
    COT_INCENDIE_HT = COT_INCENDIE_TTC / (1 + COEF_TAX_INCENDIE);
    COT_TEMPETE_HT = COT_TEMPETE_TTC / (1 + COEF_TAX_TEMPETE);
    COT_VANDALISME_HT = COT_VANDALISME_TTC / (1 + COEF_TAX_VANDALISME);
    COT_DELECT_HT = COT_DELECT_TTC / (1 + COEF_TAX_DELECT);
    COT_BDM_HT = COT_BDM_TTC / (1 + COEF_TAX_BDM);
    COT_MARCH_HT = COT_MARCH_TTC / (1 + COEF_TAX_MARCH);
    COT_DDE_HT = COT_DDE_TTC / (1 + COEF_TAX_DDE);
    COT_BDG_HT = COT_BDG_TTC / (1 + COEF_TAX_BDG);
    COT_VOL_HT = COT_VOL_TTC / (1 + COEF_TAX_VOL);
    COT_PE_HT = COT_PE_TTC / (1 + COEF_TAX_PE);
    COT_PVV_HT = COT_PVV_TTC / (1 + COEF_TAX_PVV);
    COT_RC_HT = COT_RC_TTC / (1 + COEF_TAX_RC);
    COT_PJ_HT = COT_PJ_TTC / (1 + COEF_TAX_PJ);
    COT_MARCH_TRANSP_HT = COT_MARCH_TRANSP_TTC / (1 + COEF_TAX_MARCH_TRANSP);
    // TOTAL COTISATIONS DE REFERENCE HT
    TOTAL_COT_HT =
      COT_INCENDIE_HT +
      COT_TEMPETE_HT +
      COT_VANDALISME_HT +
      COT_DELECT_HT +
      COT_BDM_HT +
      COT_MARCH_HT +
      COT_DDE_HT +
      COT_BDG_HT +
      COT_VOL_HT +
      COT_PE_HT +
      COT_PVV_HT +
      COT_RC_HT +
      COT_PJ_HT +
      COT_MARCH_TRANSP_HT;
    // ASSIETTE ATTENTAT
    ASSIET_ATT = TOTAL_COT_HT - COT_MARCH_TRANSP_HT;
    // COTISATION DE REFERENCE ATTENTAT
    COT_ATT_HT = ASSIET_ATT * COEF_ATT;
    //ASSIETTE CAT_NAT
    ASSIET_CATNAT = ASSIET_ATT + COT_ATT_HT;
    //COTISATION DE REFERENCE CAT_NAT
    COT_CATNAT_HT = ASSIET_CATNAT * COEF_CATNAT;
    //TOTAL COTISATION HORS TAXES
    TOT_HT = TOTAL_COT_HT + COT_ASSISTANCE_HT + COT_CATNAT_HT + COT_ATT_HT;
    //FRAIS COURTIER
    FRAIS_COURTIER = (TOTAL_COT_HT + COT_ASSISTANCE_HT) * TAX_COURTIER;

    //TAXES DES GARANTIES
    TAX_INCENDIE = COT_INCENDIE_TTC - COT_INCENDIE_HT;
    TAX_TEMPETE = COT_TEMPETE_TTC - COT_TEMPETE_HT;
    TAX_VANDALISME = COT_VANDALISME_TTC - COT_VANDALISME_HT;
    TAX_DELECT = COT_DELECT_TTC - COT_DELECT_HT;
    TAX_BDM = COT_BDM_TTC - COT_BDM_HT;
    TAX_MARCH = COT_MARCH_TTC - COT_MARCH_HT;
    TAX_DDE = COT_DDE_TTC - COT_DDE_HT;
    TAX_BDG = COT_BDG_TTC - COT_BDG_HT;
    TAX_VOL = COT_VOL_TTC - COT_VOL_HT;
    TAX_PE = COT_PE_TTC - COT_PE_HT;
    TAX_PVV = COT_PVV_TTC - COT_PVV_HT;
    TAX_RC = COT_RC_TTC - COT_RC_HT;
    TAX_PJ = COT_PJ_TTC - COT_PJ_HT;
    TAX_MARCH_TRANSP = COT_MARCH_TRANSP_TTC - COT_MARCH_TRANSP_HT;
    //TOTAL TAXES GARANTIES
    TOTAL_TAX =
      TAX_INCENDIE +
      TAX_TEMPETE +
      TAX_VANDALISME +
      TAX_DELECT +
      TAX_BDM +
      TAX_MARCH +
      TAX_DDE +
      TAX_BDG +
      TAX_VOL +
      TAX_PE +
      TAX_PVV +
      TAX_RC +
      TAX_PJ +
      TAX_MARCH_TRANSP;
    //TAXE ATTENTAT
    TAX_ATT = COT_ATT_HT * COEF_TAX_ATT;
    //TAXE CAT_NAT
    TAX_CATNAT = COT_CATNAT_HT * COEF_TAX_CATNAT;
    //TOTAL CONTRIBUTION ATTENTAT
    TOT_CONTR_ATT = COT_ATT_HT + TAX_ATT;
    //TOTAL CAT_NAT
    TOT_CATNAT = COT_CATNAT_HT + TAX_CATNAT;
    //TOTAL DES TAXES Y COMPRIS TAXE ATTENTAT ET TAXE ASSISTANCE
    TOT_TAXES = TOTAL_TAX + TAX_ATT + TAX_ASSISTANCE;
    //TOTAL DES TAXESY COMPRIS  FRAIS ET CONTRIBUTION
    TOTAL_TAX_FRAIS_CONTR =
      TAX_ASSISTANCE + TOT_CATNAT + TOT_CONTR_ATT + CONTRB_ATT + FRAIS_COURTIER + TOTAL_TAX;

    //PRIMES TTC/TOTAL TAXES/TOTAL TAXES,FRAIS ET CONTRIBUTOIN/FRAIS COURTIER/CONTRIBUTION ATTENTAT/TOTAL COTISATION HT SELON FRACTIONNEMENT
    //CHOIX D'ALARME
    if (alarm === true) {
      RED_VOL_HT = COT_VOL_HT * -0.15;
      RED_ATT = RED_VOL_HT * 0.04;
      RED_CATNAT_TOT = RED_VOL_HT + RED_ATT;
      RED_CATNAT = RED_CATNAT_TOT * 0.12;
      RED_TAX = RED_CATNAT_TOT * 0.09;
      RED_VOL_TTC = RED_VOL_HT + RED_ATT + RED_CATNAT + RED_TAX;
      PRIM_ANN = mrpUrbanQuotation + FRAIS_COURTIER + RED_VOL_TTC;
      TOTAL_TAX_FRAIS_CONTR = TOTAL_TAX_FRAIS_CONTR + RED_ATT + RED_CATNAT + RED_TAX;
    } else {
      PRIM_ANN = mrpUrbanQuotation + FRAIS_COURTIER;
      TOTAL_TAX_FRAIS_CONTR = TOTAL_TAX_FRAIS_CONTR;
    }
    //CHOIX DE BRIS DE MACHINES
    if (bdm === 'limite40000') {
      PRIM_ANN = PRIM_ANN + 231.79;
      TOTAL_TAX_FRAIS_CONTR = TOTAL_TAX_FRAIS_CONTR + 72.86;
    } else if (bdm === 'limite60000') {
      PRIM_ANN = PRIM_ANN + 289.73;
      TOTAL_TAX_FRAIS_CONTR = TOTAL_TAX_FRAIS_CONTR + 91.07;
    } else if (bdm === 'limite80000') {
      PRIM_ANN = PRIM_ANN + 347.68;
      TOTAL_TAX_FRAIS_CONTR = TOTAL_TAX_FRAIS_CONTR + 109.28;
    }

    if (fraction === 1) {
      fractQuotation = PRIM_ANN;
      cashQuotation = (PRIM_ANN / 365) * remainingDays;
    } else if (fraction === 2) {
      fractQuotation = PRIM_ANN / fraction;
      TOTAL_TAX_SEM = TOT_TAXES / fraction;
      FRAIS_COURTIER_SEM = FRAIS_COURTIER / fraction;
      TOTAL_TAX_FRAIS_CONTR_SEM = TOTAL_TAX_FRAIS_CONTR / fraction;
      CONTRB_ATT_SEM = CONTRB_ATT / fraction;
      TOTAL_COT_HT_SEM = TOT_HT / fraction;
      RED_VOL_TTC_SEM = RED_VOL_TTC / fraction;
      cashQuotation = (PRIM_ANN / 365) * remainingDays;
    } else if (fraction === 4) {
      fractQuotation = PRIM_ANN / fraction;
      TOTAL_TAX_TRIM = TOT_TAXES / fraction;
      FRAIS_COURTIER_TRIM = FRAIS_COURTIER / fraction;
      TOTAL_TAX_FRAIS_CONTR_TRIM = TOTAL_TAX_FRAIS_CONTR / fraction;
      CONTRB_ATT_TRIM = CONTRB_ATT / fraction;
      TOTAL_COT_HT_TRIM = TOT_HT / fraction;
      RED_VOL_TTC_TRIM = RED_VOL_TTC / fraction;
      cashQuotation = (PRIM_ANN / 365) * remainingDays;
    } else {
      fractQuotation = PRIM_ANN / fraction;
      cashQuotation = (PRIM_ANN / 365) * remainingDays;
      TOTAL_TAX_MENS = TOT_TAXES / fraction;
      FRAIS_COURTIER_MENS = FRAIS_COURTIER / fraction;
      TOTAL_TAX_FRAIS_CONTR_MENS = TOTAL_TAX_FRAIS_CONTR / fraction;
      CONTRB_ATT_MENS = CONTRB_ATT / fraction;
      TOTAL_COT_HT_MENS = TOT_HT / fraction;
      RED_VOL_TTC_MENS = RED_VOL_TTC / fraction;
    }
    // RESULTATS
    return {
      TOTAL_COT_HT_SEM,
      TOTAL_COT_HT_TRIM,
      TOTAL_COT_HT_MENS,
      TOT_HT,
      PRIM_ANN,
      fractQuotation,
      cashQuotation,
      TOTAL_TAX_FRAIS_CONTR,
      TOT_TAXES,
      TAX_INCENDIE,
      TAX_TEMPETE,
      TAX_VANDALISME,
      TAX_DELECT,
      TAX_DDE,
      TAX_BDG,
      TAX_VOL,
      TAX_PE,
      TAX_ATT,
      COT_ASSISTANCE_TTC,
      CONTRB_ATT,
      FRAIS_COURTIER,
      TOTAL_TAX_SEM,
      FRAIS_COURTIER_SEM,
      TOTAL_TAX_FRAIS_CONTR_SEM,
      CONTRB_ATT_SEM,
      TOTAL_TAX_TRIM,
      FRAIS_COURTIER_TRIM,
      TOTAL_TAX_FRAIS_CONTR_TRIM,
      CONTRB_ATT_TRIM,
      TOTAL_TAX_MENS,
      FRAIS_COURTIER_MENS,
      TOTAL_TAX_FRAIS_CONTR_MENS,
      CONTRB_ATT_MENS,
      remainingMonths,
      RED_VOL_HT,
      RED_ATT,
      RED_CATNAT,
      RED_TAX,
      RED_VOL_TTC,
      RED_VOL_TTC_SEM,
      RED_VOL_TTC_TRIM,
      RED_VOL_TTC_MENS,
      TOT_CATNAT,
    };
  },

  //METHODE DE CALCUL DES TAXES DE L'OFFRE MRP RURAL/MIXTE
  MrpRuralMixteCalculateTAX(
    turnover,
    fraction,
    franchise,
    quotation,
    effectDate,
    brokerFee,
    alarm,
    bdm,
  ) {
    // DECLARATION CONSTANTES
    const COEF_REP_INCENDIE = 0.100764;
    const COEF_REP_TEMPETE = 0.015408;
    const COEF_REP_VANDALISME = 0.011087;
    const COEF_REP_DELECT = 0.022388;
    const COEF_REP_BDM = 0;
    const COEF_REP_MARCH = 0;
    const COEF_REP_DDE = 0.054177;
    const COEF_REP_BDG = 0.123223;
    const COEF_REP_VOL = 0.404953;
    const COEF_REP_PE = 0.098722;
    const COEF_REP_PVV = 0;
    const COEF_REP_MARCH_TRANSP = 0.04036;
    const COEF_ATT = 0.04;
    const COEF_CATNAT = 0.12;
    const COEF_REP_RC = 0;
    const COEF_REP_PJ = 0;
    const COEF_TAX_INCENDIE = 0.07;
    const COEF_TAX_TEMPETE = 0.09;
    const COEF_TAX_VANDALISME = 0.09;
    const COEF_TAX_DELECT = 0.09;
    const COEF_TAX_BDM = 0.09;
    const COEF_TAX_MARCH = 0.09;
    const COEF_TAX_DDE = 0.09;
    const COEF_TAX_BDG = 0.09;
    const COEF_TAX_VOL = 0.09;
    const COEF_TAX_PE = 0.07;
    const COEF_TAX_PVV = 0.07;
    const COEF_TAX_RC = 0.09;
    const COEF_TAX_PJ = 0.134;
    const COEF_TAX_MARCH_TRANSP = 0;
    const COEF_TAX_ATT = 0.09;
    const COEF_TAX_CATNAT = 0;
    const COEF_TAX_ASSISTANCE = 0.0906;
    const COT_ASSISTANCE_HT = 5.3;
    let TAX_COURTIER = null;
    if (brokerFee !== undefined) {
      TAX_COURTIER = brokerFee;
    } else {
      TAX_COURTIER = 0.3;
    }
    // DECLARATION VARIABLES
    let TAX_BDM = 0;
    let TAX_MARCH = 0;
    let TAX_PVV = 0;
    let TAX_RC = 0;
    let TAX_PJ = 0;
    let TAX_MARCH_TRANSP = 0;
    let CONTRB_ATT = 5.9;
    let mrpRuralQuot = null;
    if (quotation) {
      mrpRuralQuot = quotation;
    } else {
      mrpRuralQuot = Quotation.mrpRuralMixteQuotation(turnover);
    }
    let TOTAL_COT_HT;
    let COT_INCENDIE_HT;
    let COT_TEMPETE_HT;
    let COT_VANDALISME_HT;
    let COT_DELECT_HT;
    let COT_BDM_HT;
    let COT_MARCH_HT;
    let COT_DDE_HT;
    let COT_BDG_HT;
    let COT_VOL_HT;
    let COT_PE_HT;
    let COT_PVV_HT;
    let COT_RC_HT;
    let COT_PJ_HT;
    let COT_MARCH_TRANSP_HT;
    let COT_ATT_HT;
    let COT_CATNAT_HT;
    let TAX_INCENDIE;
    let TAX_TEMPETE;
    let TAX_VANDALISME;
    let TAX_DELECT;
    let TAX_DDE;
    let TAX_BDG;
    let TAX_VOL;
    let TAX_PE;
    let TAX_ATT;
    let TAX_CATNAT;
    let TAX_ASSISTANCE;
    let TOTAL_TAX;
    let TOT_TAXES;
    let TOTAL_TAX_FRAIS_CONTR;
    let COT_ASSISTANCE_TTC;
    let COT_INCENDIE_TTC;
    let COT_TEMPETE_TTC;
    let COT_VANDALISME_TTC;
    let COT_DELECT_TTC;
    let COT_BDM_TTC;
    let COT_MARCH_TTC;
    let COT_DDE_TTC;
    let COT_BDG_TTC;
    let COT_VOL_TTC;
    let COT_PE_TTC;
    let COT_PVV_TTC;
    let COT_RC_TTC;
    let COT_PJ_TTC;
    let COT_MARCH_TRANSP_TTC;
    let ASSIET_ATT;
    let ASSIET_CATNAT;
    let FRAIS_COURTIER;
    let TOT_CONTR_ATT;
    let TOT_CATNAT;
    let mrpRuralMixteQuotation;
    let PRIM_ANN;
    let fractQuotation;
    let cashQuotation;
    let remainingMonths;
    let TOTAL_TAX_SEM;
    let TOTAL_TAX_TRIM;
    let TOTAL_TAX_MENS;
    let FRAIS_COURTIER_SEM;
    let FRAIS_COURTIER_TRIM;
    let FRAIS_COURTIER_MENS;
    let TOTAL_TAX_FRAIS_CONTR_SEM;
    let TOTAL_TAX_FRAIS_CONTR_TRIM;
    let TOTAL_TAX_FRAIS_CONTR_MENS;
    let CONTRB_ATT_SEM;
    let CONTRB_ATT_TRIM;
    let CONTRB_ATT_MENS;
    let TOT_HT;
    let TOTAL_COT_HT_SEM;
    let TOTAL_COT_HT_TRIM;
    let TOTAL_COT_HT_MENS;
    let TOTAL_COT_TTC;
    let RED_VOL_HT;
    let RED_ATT;
    let RED_CATNAT;
    let RED_CATNAT_TOT;
    let RED_VOL_TTC;
    let RED_TAX;
    let RED_VOL_TTC_SEM;
    let RED_VOL_TTC_TRIM;
    let RED_VOL_TTC_MENS;
    let remainingDays;

    remainingDays =
      1 +
      moment(
        new Date(
          `${Utils.calculateDeadline(fraction, effectDate).deadlineDate.split('-')[2]}-${
            Utils.calculateDeadline(fraction, effectDate).deadlineDate.split('-')[1]
          }-${Utils.calculateDeadline(fraction, effectDate).deadlineDate.split('-')[0]}`,
        ),
      ).diff(
        moment(
          new Date(
            `${effectDate.split('-')[2]}-${effectDate.split('-')[1]}-${effectDate.split('-')[0]}`,
          ),
        ),
        'days',
      );
    // logic
    // FRANCHISE 300
    if (franchise === 300) {
      mrpRuralMixteQuotation = mrpRuralQuot.mrpRuralMixteQuotation;
      // FRANCHISE 500
    } else {
      mrpRuralMixteQuotation = mrpRuralQuot.mrpRuralMixteQuotationPromo;
    }
    //TAXE ASSISTANCE
    TAX_ASSISTANCE = COT_ASSISTANCE_HT * COEF_TAX_ASSISTANCE;
    //COTISATION ASSISTANCE TTC
    COT_ASSISTANCE_TTC = COT_ASSISTANCE_HT + TAX_ASSISTANCE;
    //COTISATION MRP RURAL(PRIME ANNUELLE HORS FRACTIONNEMENT)
    //PRIME ANNUELLE HORS ASSISTANCE ET ATTENTATS
    TOTAL_COT_TTC = mrpRuralMixteQuotation - (CONTRB_ATT + COT_ASSISTANCE_TTC);
    // COTISATIONS DE REFERENCE TTC
    COT_INCENDIE_TTC = COEF_REP_INCENDIE * TOTAL_COT_TTC;
    COT_TEMPETE_TTC = COEF_REP_TEMPETE * TOTAL_COT_TTC;
    COT_VANDALISME_TTC = COEF_REP_VANDALISME * TOTAL_COT_TTC;
    COT_DELECT_TTC = COEF_REP_DELECT * TOTAL_COT_TTC;
    COT_MARCH_TTC = COEF_REP_MARCH * TOTAL_COT_TTC;
    COT_BDM_TTC = COEF_REP_BDM * TOTAL_COT_TTC;
    COT_DDE_TTC = COEF_REP_DDE * TOTAL_COT_TTC;
    COT_BDG_TTC = COEF_REP_BDG * TOTAL_COT_TTC;
    COT_VOL_TTC = COEF_REP_VOL * TOTAL_COT_TTC;
    COT_PE_TTC = COEF_REP_PE * TOTAL_COT_TTC;
    COT_PVV_TTC = COEF_REP_PVV * TOTAL_COT_TTC;
    COT_RC_TTC = COEF_REP_RC * TOTAL_COT_TTC;
    COT_PJ_TTC = COEF_REP_PJ * TOTAL_COT_TTC;
    COT_MARCH_TRANSP_TTC = COEF_REP_MARCH_TRANSP * TOTAL_COT_TTC;
    // COTISATIONS DE REFERENCE HT
    COT_INCENDIE_HT = COT_INCENDIE_TTC / (1 + COEF_TAX_INCENDIE);
    COT_TEMPETE_HT = COT_TEMPETE_TTC / (1 + COEF_TAX_TEMPETE);
    COT_VANDALISME_HT = COT_VANDALISME_TTC / (1 + COEF_TAX_VANDALISME);
    COT_DELECT_HT = COT_DELECT_TTC / (1 + COEF_TAX_DELECT);
    COT_BDM_HT = COT_BDM_TTC / (1 + COEF_TAX_BDM);
    COT_MARCH_HT = COT_MARCH_TTC / (1 + COEF_TAX_MARCH);
    COT_DDE_HT = COT_DDE_TTC / (1 + COEF_TAX_DDE);
    COT_BDG_HT = COT_BDG_TTC / (1 + COEF_TAX_BDG);
    COT_VOL_HT = COT_VOL_TTC / (1 + COEF_TAX_VOL);
    COT_PE_HT = COT_PE_TTC / (1 + COEF_TAX_PE);
    COT_PVV_HT = COT_PVV_TTC / (1 + COEF_TAX_PVV);
    COT_RC_HT = COT_RC_TTC / (1 + COEF_TAX_RC);
    COT_PJ_HT = COT_PJ_TTC / (1 + COEF_TAX_PJ);
    COT_MARCH_TRANSP_HT = COT_MARCH_TRANSP_TTC / (1 + COEF_TAX_MARCH_TRANSP);
    // TOTAL COTISATIONS DE REFERENCE HT
    TOTAL_COT_HT =
      COT_INCENDIE_HT +
      COT_TEMPETE_HT +
      COT_VANDALISME_HT +
      COT_DELECT_HT +
      COT_BDM_HT +
      COT_MARCH_HT +
      COT_DDE_HT +
      COT_BDG_HT +
      COT_VOL_HT +
      COT_PE_HT +
      COT_PVV_HT +
      COT_RC_HT +
      COT_PJ_HT +
      COT_MARCH_TRANSP_HT;
    // ASSIETTE ATTENTAT
    ASSIET_ATT = TOTAL_COT_HT - COT_MARCH_TRANSP_HT;
    // COTISATION DE REFERENCE ATTENTAT
    COT_ATT_HT = ASSIET_ATT * COEF_ATT;
    //ASSIETTE CATNAT
    ASSIET_CATNAT = ASSIET_ATT + COT_ATT_HT;
    //COTISATION DE REFERENCE CATNAT
    COT_CATNAT_HT = ASSIET_CATNAT * COEF_CATNAT;
    //TOTAL HORS TAXES
    TOT_HT = TOTAL_COT_HT + COT_ASSISTANCE_HT + COT_CATNAT_HT + COT_ATT_HT;
    //FRAIS COURTIER
    FRAIS_COURTIER = (TOTAL_COT_HT + COT_ASSISTANCE_HT) * TAX_COURTIER;
    //TAXES DES GARANTIES
    TAX_INCENDIE = COT_INCENDIE_TTC - COT_INCENDIE_HT;
    TAX_TEMPETE = COT_TEMPETE_TTC - COT_TEMPETE_HT;
    TAX_VANDALISME = COT_VANDALISME_TTC - COT_VANDALISME_HT;
    TAX_DELECT = COT_DELECT_TTC - COT_DELECT_HT;
    TAX_BDM = COT_BDM_TTC - COT_BDM_HT;
    TAX_MARCH = COT_MARCH_TTC - COT_MARCH_HT;
    TAX_DDE = COT_DDE_TTC - COT_DDE_HT;
    TAX_BDG = COT_BDG_TTC - COT_BDG_HT;
    TAX_VOL = COT_VOL_TTC - COT_VOL_HT;
    TAX_PE = COT_PE_TTC - COT_PE_HT;
    TAX_PVV = COT_PVV_TTC - COT_PVV_HT;
    TAX_RC = COT_RC_TTC - COT_RC_HT;
    TAX_PJ = COT_PJ_TTC - COT_PJ_HT;
    TAX_MARCH_TRANSP = COT_MARCH_TRANSP_TTC - COT_MARCH_TRANSP_HT;
    //TOTAL TAXES GARANTIES
    TOTAL_TAX =
      TAX_INCENDIE +
      TAX_TEMPETE +
      TAX_VANDALISME +
      TAX_DELECT +
      TAX_BDM +
      TAX_MARCH +
      TAX_DDE +
      TAX_BDG +
      TAX_VOL +
      TAX_PE +
      TAX_PVV +
      TAX_RC +
      TAX_PJ +
      TAX_MARCH_TRANSP;
    //TAXE ATTENTAT
    TAX_ATT = COT_ATT_HT * COEF_TAX_ATT;
    //TAXE CAT_NAT
    TAX_CATNAT = COT_CATNAT_HT * COEF_TAX_CATNAT;
    //TOTAL CONTRIBUTION ATTENTAT
    TOT_CONTR_ATT = COT_ATT_HT + TAX_ATT;
    //TOTAL CAT_NAT
    TOT_CATNAT = COT_CATNAT_HT + TAX_CATNAT;
    //TOTAL TAXES Y COMPRIS TAXE ATTENTAT ET TAXE ASSISTANCE
    TOT_TAXES = TOTAL_TAX + TAX_ATT + TAX_ASSISTANCE;
    //TOTAL DES TAXES Y COMPRIS FRAIS ET CONTRIBUTION
    TOTAL_TAX_FRAIS_CONTR =
      TAX_ASSISTANCE + TOT_CATNAT + TOT_CONTR_ATT + CONTRB_ATT + FRAIS_COURTIER + TOTAL_TAX;
    //PRIMES TTC/TOTAL TAXES/TOTAL TAXES,FRAIS ET CONTRIBUTION/FRAIS COURTIER/CONTRIBUTION ATTENTAT/TOTAL COTISATION HT SELON LE FRACTIONNEMENT
    //CHOIX D'ALARME
    if (alarm === true) {
      RED_VOL_HT = COT_VOL_HT * -0.15;
      RED_ATT = RED_VOL_HT * 0.04;
      RED_CATNAT_TOT = RED_VOL_HT + RED_ATT;
      RED_CATNAT = RED_CATNAT_TOT * 0.12;
      RED_TAX = RED_CATNAT_TOT * 0.09;
      RED_VOL_TTC = RED_VOL_HT + RED_ATT + RED_CATNAT + RED_TAX;
      PRIM_ANN = mrpRuralMixteQuotation + FRAIS_COURTIER + RED_VOL_TTC;
      TOTAL_TAX_FRAIS_CONTR = TOTAL_TAX_FRAIS_CONTR + RED_ATT + RED_CATNAT + RED_TAX;
    } else {
      PRIM_ANN = mrpRuralMixteQuotation + FRAIS_COURTIER;
      TOTAL_TAX_FRAIS_CONTR = TOTAL_TAX_FRAIS_CONTR;
    }
    //CHOIX DE BRIS DE MACHINE
    if (bdm === 'limite40000') {
      PRIM_ANN = PRIM_ANN + 231.79;
      TOTAL_TAX_FRAIS_CONTR = TOTAL_TAX_FRAIS_CONTR + 72.86;
    } else if (bdm === 'limite60000') {
      PRIM_ANN = PRIM_ANN + 289.73;
      TOTAL_TAX_FRAIS_CONTR = TOTAL_TAX_FRAIS_CONTR + 91.07;
    } else if (bdm === 'limite80000') {
      PRIM_ANN = PRIM_ANN + 347.68;
      TOTAL_TAX_FRAIS_CONTR = TOTAL_TAX_FRAIS_CONTR + 109.28;
    }
    if (fraction === 1) {
      fractQuotation = PRIM_ANN;
      cashQuotation = (PRIM_ANN / 365) * remainingDays;
    } else if (fraction === 2) {
      fractQuotation = PRIM_ANN / fraction;
      TOTAL_TAX_SEM = TOT_TAXES / fraction;
      FRAIS_COURTIER_SEM = FRAIS_COURTIER / fraction;
      TOTAL_TAX_FRAIS_CONTR_SEM = TOTAL_TAX_FRAIS_CONTR / fraction;
      CONTRB_ATT_SEM = CONTRB_ATT / fraction;
      TOTAL_COT_HT_SEM = TOT_HT / fraction;
      RED_VOL_TTC_SEM = RED_VOL_TTC / fraction;
      cashQuotation = (PRIM_ANN / 365) * remainingDays;
    } else if (fraction === 4) {
      fractQuotation = PRIM_ANN / fraction;
      TOTAL_TAX_TRIM = TOT_TAXES / fraction;
      FRAIS_COURTIER_TRIM = FRAIS_COURTIER / fraction;
      TOTAL_TAX_FRAIS_CONTR_TRIM = TOTAL_TAX_FRAIS_CONTR / fraction;
      CONTRB_ATT_TRIM = CONTRB_ATT / fraction;
      TOTAL_COT_HT_TRIM = TOT_HT / fraction;
      RED_VOL_TTC_TRIM = RED_VOL_TTC / fraction;
      cashQuotation = (PRIM_ANN / 365) * remainingDays;
    } else {
      fractQuotation = PRIM_ANN / fraction;
      cashQuotation = (PRIM_ANN / 365) * remainingDays;
      TOTAL_TAX_MENS = TOT_TAXES / fraction;
      FRAIS_COURTIER_MENS = FRAIS_COURTIER / fraction;
      TOTAL_TAX_FRAIS_CONTR_MENS = TOTAL_TAX_FRAIS_CONTR / fraction;
      CONTRB_ATT_MENS = CONTRB_ATT / fraction;
      TOTAL_COT_HT_MENS = TOT_HT / fraction;
      RED_VOL_TTC_MENS = RED_VOL_TTC / fraction;
    }
    // RESULTATS
    return {
      TOTAL_COT_HT_SEM,
      TOTAL_COT_HT_TRIM,
      TOTAL_COT_HT_MENS,
      TOT_HT,
      PRIM_ANN,
      fractQuotation,
      cashQuotation,
      TOTAL_TAX_FRAIS_CONTR,
      TOT_TAXES,
      TAX_INCENDIE,
      TAX_TEMPETE,
      TAX_VANDALISME,
      TAX_DELECT,
      TAX_DDE,
      TAX_BDG,
      TAX_VOL,
      TAX_PE,
      TAX_ATT,
      COT_ASSISTANCE_TTC,
      CONTRB_ATT,
      FRAIS_COURTIER,
      TOTAL_TAX_SEM,
      FRAIS_COURTIER_SEM,
      TOTAL_TAX_FRAIS_CONTR_SEM,
      CONTRB_ATT_SEM,
      TOTAL_TAX_TRIM,
      FRAIS_COURTIER_TRIM,
      TOTAL_TAX_FRAIS_CONTR_TRIM,
      CONTRB_ATT_TRIM,
      TOTAL_TAX_MENS,
      FRAIS_COURTIER_MENS,
      TOTAL_TAX_FRAIS_CONTR_MENS,
      CONTRB_ATT_MENS,
      remainingMonths,
      RED_VOL_HT,
      RED_ATT,
      RED_CATNAT,
      RED_TAX,
      RED_VOL_TTC,
      RED_VOL_TTC_SEM,
      RED_VOL_TTC_TRIM,
      RED_VOL_TTC_MENS,
      TOT_CATNAT,
    };
  },

  //METHODE DE CALCUL DES TAXES DE L'OFFRE (MRP+RC) URBAIN
  MrpCrUrbanCalculateTAX(
    turnover,
    fraction,
    franchise,
    quotation,
    effectDate,
    brokerFee,
    alarm,
    bdm,
  ) {
    // DECLARATION CONSTANTES
    const COEF_REP_INCENDIE = 0.062908;
    const COEF_REP_TEMPETE = 0.015414;
    const COEF_REP_VANDALISME = 0.011088;
    const COEF_REP_DELECT = 0.027392;
    const COEF_REP_BDM = 0;
    const COEF_REP_MARCH = 0;
    const COEF_REP_DDE = 0.054187;
    const COEF_REP_BDG = 0.123248;
    const COEF_REP_VOL = 0.405839;
    const COEF_REP_PE = 0.069992;
    const COEF_REP_PVV = 0;
    const COEF_REP_MARCH_TRANSP = 0.039066;
    const COEF_ATT = 0.04;
    const COEF_CATNAT = 0.12;
    const COEF_REP_RC = 0.046437;
    const COEF_REP_PJ = 0.025071;
    const COEF_TAX_INCENDIE = 0.07;
    const COEF_TAX_TEMPETE = 0.09;
    const COEF_TAX_VANDALISME = 0.09;
    const COEF_TAX_DELECT = 0.09;
    const COEF_TAX_BDM = 0.09;
    const COEF_TAX_MARCH = 0.09;
    const COEF_TAX_DDE = 0.09;
    const COEF_TAX_BDG = 0.09;
    const COEF_TAX_VOL = 0.09;
    const COEF_TAX_PE = 0.07;
    const COEF_TAX_PVV = 0.07;
    const COEF_TAX_RC = 0.09;
    const COEF_TAX_PJ = 0.134;
    const COEF_TAX_MARCH_TRANSP = 0;
    const COEF_TAX_ATT = 0.09;
    const COEF_TAX_CATNAT = 0;
    const COEF_TAX_ASSISTANCE = 0.0906;
    const COT_ASSISTANCE_HT = 5.3;
    let TAX_COURTIER = null;
    if (brokerFee !== undefined) {
      TAX_COURTIER = brokerFee;
    } else {
      TAX_COURTIER = 0.3;
    }
    // DECLARATION VARIABLES
    let CONTRB_ATT = 5.9;
    let TAX_BDM = 0;
    let TAX_MARCH = 0;
    let TAX_PVV = 0;
    let TAX_RC = 0;
    let TAX_PJ = 0;
    let TAX_MARCH_TRANSP = 0;
    let mrpRcUrbanQuot = null;
    if (quotation) {
      mrpRcUrbanQuot = quotation;
    } else {
      mrpRcUrbanQuot = Quotation.mrpRcUrbanQuotation(turnover);
    }
    let TOTAL_COT_HT;
    let COT_INCENDIE_HT;
    let COT_TEMPETE_HT;
    let COT_VANDALISME_HT;
    let COT_DELECT_HT;
    let COT_BDM_HT;
    let COT_MARCH_HT;
    let COT_DDE_HT;
    let COT_BDG_HT;
    let COT_VOL_HT;
    let COT_PE_HT;
    let COT_PVV_HT;
    let COT_RC_HT;
    let COT_PJ_HT;
    let COT_MARCH_TRANSP_HT;
    let COT_ATT_HT;
    let COT_CATNAT_HT;
    let TAX_INCENDIE;
    let TAX_TEMPETE;
    let TAX_VANDALISME;
    let TAX_DELECT;
    let TAX_DDE;
    let TAX_BDG;
    let TAX_VOL;
    let TAX_PE;
    let TAX_ATT;
    let TAX_CATNAT;
    let TAX_ASSISTANCE;
    let TOTAL_TAX;
    let TOT_TAXES;
    let TOTAL_TAX_FRAIS_CONTR;
    let COT_ASSISTANCE_TTC;
    let COT_INCENDIE_TTC;
    let COT_TEMPETE_TTC;
    let COT_VANDALISME_TTC;
    let COT_DELECT_TTC;
    let COT_BDM_TTC;
    let COT_MARCH_TTC;
    let COT_DDE_TTC;
    let COT_BDG_TTC;
    let COT_VOL_TTC;
    let COT_PE_TTC;
    let COT_PVV_TTC;
    let COT_RC_TTC;
    let COT_PJ_TTC;
    let COT_MARCH_TRANSP_TTC;
    let ASSIET_ATT;
    let ASSIET_CATNAT;
    let FRAIS_COURTIER;
    let TOT_CONTR_ATT;
    let TOT_CATNAT;
    let mrpRcUrbanQuotation;
    let PRIM_ANN;
    let fractQuotation;
    let cashQuotation;
    let prorata;
    let remainingMonths;
    let TOT_HT;
    let TOTAL_COT_HT_SEM;
    let TOTAL_COT_HT_TRIM;
    let TOTAL_COT_HT_MENS;
    let FRAIS_COURTIER_SEM;
    let FRAIS_COURTIER_TRIM;
    let FRAIS_COURTIER_MENS;
    let TOTAL_TAX_SEM;
    let TOTAL_TAX_TRIM;
    let TOTAL_TAX_MENS;
    let TOTAL_TAX_FRAIS_CONTR_SEM;
    let TOTAL_TAX_FRAIS_CONTR_TRIM;
    let TOTAL_TAX_FRAIS_CONTR_MENS;
    let CONTRB_ATT_SEM;
    let CONTRB_ATT_TRIM;
    let CONTRB_ATT_MENS;
    let TOTAL_COT_TTC;
    let RED_VOL_HT;
    let RED_ATT;
    let RED_CATNAT;
    let RED_CATNAT_TOT;
    let RED_VOL_TTC;
    let RED_TAX;
    let RED_VOL_TTC_SEM;
    let RED_VOL_TTC_TRIM;
    let RED_VOL_TTC_MENS;
    let remainingDays;

    remainingDays =
      1 +
      moment(
        new Date(
          `${Utils.calculateDeadline(fraction, effectDate).deadlineDate.split('-')[2]}-${
            Utils.calculateDeadline(fraction, effectDate).deadlineDate.split('-')[1]
          }-${Utils.calculateDeadline(fraction, effectDate).deadlineDate.split('-')[0]}`,
        ),
      ).diff(
        moment(
          new Date(
            `${effectDate.split('-')[2]}-${effectDate.split('-')[1]}-${effectDate.split('-')[0]}`,
          ),
        ),
        'days',
      );

    // logic
    // FRANCHISE 300
    if (franchise === 300) {
      mrpRcUrbanQuotation = mrpRcUrbanQuot.mrpRcUrbanQuotation;
      // FRANCHISE 500
    } else {
      mrpRcUrbanQuotation = mrpRcUrbanQuot.mrpRcUrbanQuotationPromo;
    }
    //TAXE ASSISTANCE
    TAX_ASSISTANCE = COT_ASSISTANCE_HT * COEF_TAX_ASSISTANCE;
    //COTISATION ASSISTANCE TTC
    COT_ASSISTANCE_TTC = COT_ASSISTANCE_HT + TAX_ASSISTANCE;
    //PRIME ANNUELLE HORS ASSISTANCE ET ATTENTATS
    TOTAL_COT_TTC = mrpRcUrbanQuotation - (CONTRB_ATT + COT_ASSISTANCE_TTC);
    // COTISATIONS DE REFERENCE TTC
    COT_INCENDIE_TTC = COEF_REP_INCENDIE * TOTAL_COT_TTC;
    COT_TEMPETE_TTC = COEF_REP_TEMPETE * TOTAL_COT_TTC;
    COT_VANDALISME_TTC = COEF_REP_VANDALISME * TOTAL_COT_TTC;
    COT_DELECT_TTC = COEF_REP_DELECT * TOTAL_COT_TTC;
    COT_MARCH_TTC = COEF_REP_MARCH * TOTAL_COT_TTC;
    COT_BDM_TTC = COEF_REP_BDM * TOTAL_COT_TTC;
    COT_DDE_TTC = COEF_REP_DDE * TOTAL_COT_TTC;
    COT_BDG_TTC = COEF_REP_BDG * TOTAL_COT_TTC;
    COT_VOL_TTC = COEF_REP_VOL * TOTAL_COT_TTC;
    COT_PE_TTC = COEF_REP_PE * TOTAL_COT_TTC;
    COT_PVV_TTC = COEF_REP_PVV * TOTAL_COT_TTC;
    COT_RC_TTC = COEF_REP_RC * TOTAL_COT_TTC;
    COT_PJ_TTC = COEF_REP_PJ * TOTAL_COT_TTC;
    COT_MARCH_TRANSP_TTC = COEF_REP_MARCH_TRANSP * TOTAL_COT_TTC;
    // COTISATIONS DE REFERENCE HT
    COT_INCENDIE_HT = COT_INCENDIE_TTC / (1 + COEF_TAX_INCENDIE);
    COT_TEMPETE_HT = COT_TEMPETE_TTC / (1 + COEF_TAX_TEMPETE);
    COT_VANDALISME_HT = COT_VANDALISME_TTC / (1 + COEF_TAX_VANDALISME);
    COT_DELECT_HT = COT_DELECT_TTC / (1 + COEF_TAX_DELECT);
    COT_BDM_HT = COT_BDM_TTC / (1 + COEF_TAX_BDM);
    COT_MARCH_HT = COT_MARCH_TTC / (1 + COEF_TAX_MARCH);
    COT_DDE_HT = COT_DDE_TTC / (1 + COEF_TAX_DDE);
    COT_BDG_HT = COT_BDG_TTC / (1 + COEF_TAX_BDG);
    COT_VOL_HT = COT_VOL_TTC / (1 + COEF_TAX_VOL);
    COT_PE_HT = COT_PE_TTC / (1 + COEF_TAX_PE);
    COT_PVV_HT = COT_PVV_TTC / (1 + COEF_TAX_PVV);
    COT_RC_HT = COT_RC_TTC / (1 + COEF_TAX_RC);
    COT_PJ_HT = COT_PJ_TTC / (1 + COEF_TAX_PJ);
    COT_MARCH_TRANSP_HT = COT_MARCH_TRANSP_TTC / (1 + COEF_TAX_MARCH_TRANSP);
    // TOTAL COTISATIONS DE REFERENCE HT
    TOTAL_COT_HT =
      COT_INCENDIE_HT +
      COT_TEMPETE_HT +
      COT_VANDALISME_HT +
      COT_DELECT_HT +
      COT_BDM_HT +
      COT_MARCH_HT +
      COT_DDE_HT +
      COT_BDG_HT +
      COT_VOL_HT +
      COT_PE_HT +
      COT_PVV_HT +
      COT_RC_HT +
      COT_PJ_HT +
      COT_MARCH_TRANSP_HT;
    // ASSIETTE ATTENTAT
    ASSIET_ATT = TOTAL_COT_HT - (COT_MARCH_TRANSP_HT + COT_RC_HT + COT_PJ_HT);
    // COTISATION DE REFERENCE ATTENTAT
    COT_ATT_HT = ASSIET_ATT * COEF_ATT;
    //ASSIETTE CATNAT
    ASSIET_CATNAT = ASSIET_ATT + COT_ATT_HT;
    //COTISATION DE REFERENCE CATNAT
    COT_CATNAT_HT = ASSIET_CATNAT * COEF_CATNAT;
    //TOTAL HORS TAXES
    TOT_HT = TOTAL_COT_HT + COT_ASSISTANCE_HT + COT_CATNAT_HT + COT_ATT_HT;
    //FRAIS COURTIER
    FRAIS_COURTIER = (TOTAL_COT_HT + COT_ASSISTANCE_HT) * TAX_COURTIER;
    //TAXES DES GARANTIES
    TAX_INCENDIE = COT_INCENDIE_TTC - COT_INCENDIE_HT;
    TAX_TEMPETE = COT_TEMPETE_TTC - COT_TEMPETE_HT;
    TAX_VANDALISME = COT_VANDALISME_TTC - COT_VANDALISME_HT;
    TAX_DELECT = COT_DELECT_TTC - COT_DELECT_HT;
    TAX_BDM = COT_BDM_TTC - COT_BDM_HT;
    TAX_MARCH = COT_MARCH_TTC - COT_MARCH_HT;
    TAX_DDE = COT_DDE_TTC - COT_DDE_HT;
    TAX_BDG = COT_BDG_TTC - COT_BDG_HT;
    TAX_VOL = COT_VOL_TTC - COT_VOL_HT;
    TAX_PE = COT_PE_TTC - COT_PE_HT;
    TAX_PVV = COT_PVV_TTC - COT_PVV_HT;
    TAX_RC = COT_RC_TTC - COT_RC_HT;
    TAX_PJ = COT_PJ_TTC - COT_PJ_HT;
    TAX_MARCH_TRANSP = COT_MARCH_TRANSP_TTC - COT_MARCH_TRANSP_HT;
    //TOTAL TAXES GARANTIES
    TOTAL_TAX =
      TAX_INCENDIE +
      TAX_TEMPETE +
      TAX_VANDALISME +
      TAX_DELECT +
      TAX_BDM +
      TAX_MARCH +
      TAX_DDE +
      TAX_BDG +
      TAX_VOL +
      TAX_PE +
      TAX_PVV +
      TAX_RC +
      TAX_PJ +
      TAX_MARCH_TRANSP;
    //TAXE ATTENTAT
    TAX_ATT = COT_ATT_HT * COEF_TAX_ATT;
    //TAXE CAT_NAT
    TAX_CATNAT = COT_CATNAT_HT * COEF_TAX_CATNAT;
    //TOTAL CONTRIBUTION ATTENTAT
    TOT_CONTR_ATT = COT_ATT_HT + TAX_ATT;
    //TOTAL CAT_NAT
    TOT_CATNAT = COT_CATNAT_HT + TAX_CATNAT;
    //TAXES Y COMPRIS TAXE ATTENTAT ET TAXE ASSISTANCE
    TOT_TAXES = TOTAL_TAX + TAX_ATT + TAX_ASSISTANCE;
    //TOTAL DES TAXES, FRAIS ET CONTRIBUTION
    TOTAL_TAX_FRAIS_CONTR =
      TAX_ASSISTANCE + TOT_CATNAT + TOT_CONTR_ATT + CONTRB_ATT + FRAIS_COURTIER + TOTAL_TAX;
    //PRIMES TTC/TOTAL TAXES/TOTAL TAXES, FRAIS ET CONTRIBUTION/FRAIS COURTIER/CONTRIBUTION ATTENTAT/TOTAL COTISATION HT SELON LE FRACTIONNEMENT
    //CHOIX D'ALARME
    if (alarm === true) {
      RED_VOL_HT = COT_VOL_HT * -0.15;
      RED_ATT = RED_VOL_HT * 0.04;
      RED_CATNAT_TOT = RED_VOL_HT + RED_ATT;
      RED_CATNAT = RED_CATNAT_TOT * 0.12;
      RED_TAX = RED_CATNAT_TOT * 0.09;
      RED_VOL_TTC = RED_VOL_HT + RED_ATT + RED_CATNAT + RED_TAX;
      PRIM_ANN = mrpRcUrbanQuotation + FRAIS_COURTIER + RED_VOL_TTC;
      TOTAL_TAX_FRAIS_CONTR = TOTAL_TAX_FRAIS_CONTR + RED_ATT + RED_CATNAT + RED_TAX;
    } else {
      PRIM_ANN = mrpRcUrbanQuotation + FRAIS_COURTIER;
      TOTAL_TAX_FRAIS_CONTR = TOTAL_TAX_FRAIS_CONTR;
    }
    //CHOIX DE BRIS DE MACHINE
    if (bdm === 'limite40000') {
      PRIM_ANN = PRIM_ANN + 231.79;
      TOTAL_TAX_FRAIS_CONTR = TOTAL_TAX_FRAIS_CONTR + 72.86;
    } else if (bdm === 'limite60000') {
      PRIM_ANN = PRIM_ANN + 289.73;
      TOTAL_TAX_FRAIS_CONTR = TOTAL_TAX_FRAIS_CONTR + 91.07;
    } else if (bdm === 'limite80000') {
      PRIM_ANN = PRIM_ANN + 347.68;
      TOTAL_TAX_FRAIS_CONTR = TOTAL_TAX_FRAIS_CONTR + 109.28;
    }
    if (fraction === 1) {
      fractQuotation = PRIM_ANN;
      cashQuotation = (PRIM_ANN / 365) * remainingDays;
    } else if (fraction === 2) {
      fractQuotation = PRIM_ANN / fraction;
      TOTAL_TAX_SEM = TOT_TAXES / fraction;
      FRAIS_COURTIER_SEM = FRAIS_COURTIER / fraction;
      TOTAL_TAX_FRAIS_CONTR_SEM = TOTAL_TAX_FRAIS_CONTR / fraction;
      CONTRB_ATT_SEM = CONTRB_ATT / fraction;
      TOTAL_COT_HT_SEM = TOT_HT / fraction;
      RED_VOL_TTC_SEM = RED_VOL_TTC / fraction;
      cashQuotation = (PRIM_ANN / 365) * remainingDays;
    } else if (fraction === 4) {
      fractQuotation = PRIM_ANN / fraction;
      TOTAL_TAX_TRIM = TOT_TAXES / fraction;
      FRAIS_COURTIER_TRIM = FRAIS_COURTIER / fraction;
      TOTAL_TAX_FRAIS_CONTR_TRIM = TOTAL_TAX_FRAIS_CONTR / fraction;
      CONTRB_ATT_TRIM = CONTRB_ATT / fraction;
      TOTAL_COT_HT_TRIM = TOT_HT / fraction;
      RED_VOL_TTC_TRIM = RED_VOL_TTC / fraction;
      cashQuotation = (PRIM_ANN / 365) * remainingDays;
    } else {
      fractQuotation = PRIM_ANN / fraction;
      cashQuotation = (PRIM_ANN / 365) * remainingDays;
      TOTAL_TAX_MENS = TOT_TAXES / fraction;
      FRAIS_COURTIER_MENS = FRAIS_COURTIER / fraction;
      TOTAL_TAX_FRAIS_CONTR_MENS = TOTAL_TAX_FRAIS_CONTR / fraction;
      CONTRB_ATT_MENS = CONTRB_ATT / fraction;
      TOTAL_COT_HT_MENS = TOT_HT / fraction;
      RED_VOL_TTC_MENS = RED_VOL_TTC / fraction;
    }
    // RESULTATS
    return {
      mrpRcUrbanQuotation,
      TOTAL_COT_HT_SEM,
      TOTAL_COT_HT_TRIM,
      TOTAL_COT_HT_MENS,
      TOT_HT,
      PRIM_ANN,
      fractQuotation,
      cashQuotation,
      TOTAL_TAX_FRAIS_CONTR,
      TOT_TAXES,
      TAX_INCENDIE,
      TAX_TEMPETE,
      TAX_VANDALISME,
      TAX_DELECT,
      TAX_DDE,
      TAX_BDG,
      TAX_VOL,
      TAX_PE,
      TAX_ATT,
      COT_ASSISTANCE_TTC,
      CONTRB_ATT,
      FRAIS_COURTIER,
      FRAIS_COURTIER_SEM,
      FRAIS_COURTIER_TRIM,
      FRAIS_COURTIER_MENS,
      TOTAL_TAX_SEM,
      TOTAL_TAX_TRIM,
      TOTAL_TAX_MENS,
      TOTAL_TAX_FRAIS_CONTR_SEM,
      TOTAL_TAX_FRAIS_CONTR_TRIM,
      TOTAL_TAX_FRAIS_CONTR_MENS,
      CONTRB_ATT_SEM,
      CONTRB_ATT_TRIM,
      CONTRB_ATT_MENS,
      remainingMonths,
      RED_VOL_HT,
      RED_ATT,
      RED_CATNAT,
      RED_TAX,
      RED_VOL_TTC,
      RED_VOL_TTC_SEM,
      RED_VOL_TTC_TRIM,
      RED_VOL_TTC_MENS,
      TOT_CATNAT,
    };
  },
  //METHODE DE CALCUL DES TAXES DE L'OFFRE (MRP+RC) RURAL/MIXTE
  MrpCrRuralMixedCalculateTAX(
    turnover,
    fraction,
    franchise,
    quotation,
    effectDate,
    brokerFee,
    alarm,
    bdm,
  ) {
    // DECLARATION CONSTANTES
    const COEF_REP_INCENDIE = 0.090649;
    const COEF_REP_TEMPETE = 0.013862;
    const COEF_REP_VANDALISME = 0.009975;
    const COEF_REP_DELECT = 0.023858;
    const COEF_REP_BDM = 0;
    const COEF_REP_MARCH = 0;
    const COEF_REP_DDE = 0.048738;
    const COEF_REP_BDG = 0.110849;
    const COEF_REP_VOL = 0.364355;
    const COEF_REP_PE = 0.088809;
    const COEF_REP_PVV = 0;
    const COEF_REP_MARCH_TRANSP = 0.029439;
    const COEF_ATT = 0.04;
    const COEF_CATNAT = 0.12;
    const COEF_REP_RC = 0.084013;
    const COEF_REP_PJ = 0.018893;
    const COEF_TAX_INCENDIE = 0.07;
    const COEF_TAX_TEMPETE = 0.09;
    const COEF_TAX_VANDALISME = 0.09;
    const COEF_TAX_DELECT = 0.09;
    const COEF_TAX_BDM = 0.09;
    const COEF_TAX_MARCH = 0.09;
    const COEF_TAX_DDE = 0.09;
    const COEF_TAX_BDG = 0.09;
    const COEF_TAX_VOL = 0.09;
    const COEF_TAX_PE = 0.07;
    const COEF_TAX_PVV = 0.07;
    const COEF_TAX_RC = 0.09;
    const COEF_TAX_PJ = 0.134;
    const COEF_TAX_MARCH_TRANSP = 0;
    const COEF_TAX_ATT = 0.09;
    const COEF_TAX_CATNAT = 0;
    const COEF_TAX_ASSISTANCE = 0.0906;
    const COT_ASSISTANCE_HT = 5.3;
    let TAX_COURTIER = null;
    if (brokerFee !== undefined) {
      TAX_COURTIER = brokerFee;
    } else {
      TAX_COURTIER = 0.3;
    }
    // DECLARATION VARIABLES
    let CONTRB_ATT = 5.9;
    let TAX_BDM = 0;
    let TAX_MARCH = 0;
    let TAX_PVV = 0;
    let TAX_RC = 0;
    let TAX_PJ = 0;
    let TAX_MARCH_TRANSP = 0;
    let mrpRcRuralQuot = null;
    if (quotation) {
      mrpRcRuralQuot = quotation;
    } else {
      mrpRcRuralQuot = Quotation.mrpRcRuralMixteQuotation(turnover);
    }
    let TOTAL_COT_HT;
    let COT_INCENDIE_HT;
    let COT_TEMPETE_HT;
    let COT_VANDALISME_HT;
    let COT_DELECT_HT;
    let COT_BDM_HT;
    let COT_MARCH_HT;
    let COT_DDE_HT;
    let COT_BDG_HT;
    let COT_VOL_HT;
    let COT_PE_HT;
    let COT_PVV_HT;
    let COT_RC_HT;
    let COT_PJ_HT;
    let COT_MARCH_TRANSP_HT;
    let COT_ATT_HT;
    let COT_CATNAT_HT;
    let TAX_INCENDIE;
    let TAX_TEMPETE;
    let TAX_VANDALISME;
    let TAX_DELECT;
    let TAX_DDE;
    let TAX_BDG;
    let TAX_VOL;
    let TAX_PE;
    let TAX_ATT;
    let TAX_CATNAT;
    let TAX_ASSISTANCE;
    let TOTAL_TAX;
    let TOT_TAXES;
    let TOTAL_TAX_FRAIS_CONTR;
    let COT_ASSISTANCE_TTC;
    let COT_INCENDIE_TTC;
    let COT_TEMPETE_TTC;
    let COT_VANDALISME_TTC;
    let COT_DELECT_TTC;
    let COT_BDM_TTC;
    let COT_MARCH_TTC;
    let COT_DDE_TTC;
    let COT_BDG_TTC;
    let COT_VOL_TTC;
    let COT_PE_TTC;
    let COT_PVV_TTC;
    let COT_RC_TTC;
    let COT_PJ_TTC;
    let COT_MARCH_TRANSP_TTC;
    let ASSIET_ATT;
    let ASSIET_CATNAT;
    let FRAIS_COURTIER;
    let TOT_CONTR_ATT;
    let TOT_CATNAT;
    let mrpRcRuralMixteQuotation;
    let PRIM_ANN;
    let fractQuotation;
    let cashQuotation;
    let remainingMonths;
    let TOT_HT;
    let TOTAL_COT_HT_SEM;
    let TOTAL_COT_HT_TRIM;
    let TOTAL_COT_HT_MENS;
    let FRAIS_COURTIER_SEM;
    let FRAIS_COURTIER_TRIM;
    let FRAIS_COURTIER_MENS;
    let TOTAL_TAX_SEM;
    let TOTAL_TAX_TRIM;
    let TOTAL_TAX_MENS;
    let TOTAL_TAX_FRAIS_CONTR_SEM;
    let TOTAL_TAX_FRAIS_CONTR_TRIM;
    let TOTAL_TAX_FRAIS_CONTR_MENS;
    let CONTRB_ATT_SEM;
    let CONTRB_ATT_TRIM;
    let CONTRB_ATT_MENS;
    let TOTAL_COT_TTC;
    let RED_VOL_HT;
    let RED_ATT;
    let RED_CATNAT;
    let RED_CATNAT_TOT;
    let RED_VOL_TTC;
    let RED_TAX;
    let RED_VOL_TTC_SEM;
    let RED_VOL_TTC_TRIM;
    let RED_VOL_TTC_MENS;
    let remainingDays;

    remainingDays =
      1 +
      moment(
        new Date(
          `${Utils.calculateDeadline(fraction, effectDate).deadlineDate.split('-')[2]}-${
            Utils.calculateDeadline(fraction, effectDate).deadlineDate.split('-')[1]
          }-${Utils.calculateDeadline(fraction, effectDate).deadlineDate.split('-')[0]}`,
        ),
      ).diff(
        moment(
          new Date(
            `${effectDate.split('-')[2]}-${effectDate.split('-')[1]}-${effectDate.split('-')[0]}`,
          ),
        ),
        'days',
      );

    // LOGIQUE //
    // FRANCHISE 300
    if (franchise === 300) {
      mrpRcRuralMixteQuotation = mrpRcRuralQuot.mrpRcRuralMixteQuotation;
    } else {
      mrpRcRuralMixteQuotation = mrpRcRuralQuot.mrpRcRuralMixteQuotationPromo;
    }
    //TAXE ASSISTANCE
    TAX_ASSISTANCE = COT_ASSISTANCE_HT * COEF_TAX_ASSISTANCE;
    //ASSISTANCE TTC
    COT_ASSISTANCE_TTC = COT_ASSISTANCE_HT + TAX_ASSISTANCE;
    //PRIME ANNUELLE HORS ASSISTANCE ET ATTENTATS
    TOTAL_COT_TTC = mrpRcRuralMixteQuotation - (CONTRB_ATT + COT_ASSISTANCE_TTC);
    // COTISATIONS DE REFERENCE TTC
    COT_INCENDIE_TTC = COEF_REP_INCENDIE * TOTAL_COT_TTC;
    COT_TEMPETE_TTC = COEF_REP_TEMPETE * TOTAL_COT_TTC;
    COT_VANDALISME_TTC = COEF_REP_VANDALISME * TOTAL_COT_TTC;
    COT_DELECT_TTC = COEF_REP_DELECT * TOTAL_COT_TTC;
    COT_MARCH_TTC = COEF_REP_MARCH * TOTAL_COT_TTC;
    COT_BDM_TTC = COEF_REP_BDM * TOTAL_COT_TTC;
    COT_DDE_TTC = COEF_REP_DDE * TOTAL_COT_TTC;
    COT_BDG_TTC = COEF_REP_BDG * TOTAL_COT_TTC;
    COT_VOL_TTC = COEF_REP_VOL * TOTAL_COT_TTC;
    COT_PE_TTC = COEF_REP_PE * TOTAL_COT_TTC;
    COT_PVV_TTC = COEF_REP_PVV * TOTAL_COT_TTC;
    COT_RC_TTC = COEF_REP_RC * TOTAL_COT_TTC;
    COT_PJ_TTC = COEF_REP_PJ * TOTAL_COT_TTC;
    COT_MARCH_TRANSP_TTC = COEF_REP_MARCH_TRANSP * TOTAL_COT_TTC;
    // COTISATIONS DE REFERENCE HT
    COT_INCENDIE_HT = COT_INCENDIE_TTC / (1 + COEF_TAX_INCENDIE);
    COT_TEMPETE_HT = COT_TEMPETE_TTC / (1 + COEF_TAX_TEMPETE);
    COT_VANDALISME_HT = COT_VANDALISME_TTC / (1 + COEF_TAX_VANDALISME);
    COT_DELECT_HT = COT_DELECT_TTC / (1 + COEF_TAX_DELECT);
    COT_BDM_HT = COT_BDM_TTC / (1 + COEF_TAX_BDM);
    COT_MARCH_HT = COT_MARCH_TTC / (1 + COEF_TAX_MARCH);
    COT_DDE_HT = COT_DDE_TTC / (1 + COEF_TAX_DDE);
    COT_BDG_HT = COT_BDG_TTC / (1 + COEF_TAX_BDG);
    COT_VOL_HT = COT_VOL_TTC / (1 + COEF_TAX_VOL);
    COT_PE_HT = COT_PE_TTC / (1 + COEF_TAX_PE);
    COT_PVV_HT = COT_PVV_TTC / (1 + COEF_TAX_PVV);
    COT_RC_HT = COT_RC_TTC / (1 + COEF_TAX_RC);
    COT_PJ_HT = COT_PJ_TTC / (1 + COEF_TAX_PJ);
    COT_MARCH_TRANSP_HT = COT_MARCH_TRANSP_TTC / (1 + COEF_TAX_MARCH_TRANSP);
    // TOTAL COTISATIONS DE REFERENCE HT
    TOTAL_COT_HT =
      COT_INCENDIE_HT +
      COT_TEMPETE_HT +
      COT_VANDALISME_HT +
      COT_DELECT_HT +
      COT_BDM_HT +
      COT_MARCH_HT +
      COT_DDE_HT +
      COT_BDG_HT +
      COT_VOL_HT +
      COT_PE_HT +
      COT_PVV_HT +
      COT_RC_HT +
      COT_PJ_HT +
      COT_MARCH_TRANSP_HT;
    // ASSIETTE ATTENTAT
    ASSIET_ATT = TOTAL_COT_HT - (COT_MARCH_TRANSP_HT + COT_RC_HT + COT_PJ_HT);
    // COTISATION DE REFERENCE ATTENTAT
    COT_ATT_HT = ASSIET_ATT * COEF_ATT;
    //ASSIETTE CATNAT
    ASSIET_CATNAT = ASSIET_ATT + COT_ATT_HT;
    //COTISATION DE REFERENCE CATNAT
    COT_CATNAT_HT = ASSIET_CATNAT * COEF_CATNAT;
    //TOTAL HORS TAXES
    TOT_HT = TOTAL_COT_HT + COT_ASSISTANCE_HT + COT_CATNAT_HT + COT_ATT_HT;
    //FRAIS COURTIER
    FRAIS_COURTIER = (TOTAL_COT_HT + COT_ASSISTANCE_HT) * TAX_COURTIER;
    //TAXES DES GARANTIES
    TAX_INCENDIE = COT_INCENDIE_TTC - COT_INCENDIE_HT;
    TAX_TEMPETE = COT_TEMPETE_TTC - COT_TEMPETE_HT;
    TAX_VANDALISME = COT_VANDALISME_TTC - COT_VANDALISME_HT;
    TAX_DELECT = COT_DELECT_TTC - COT_DELECT_HT;
    TAX_BDM = COT_BDM_TTC - COT_BDM_HT;
    TAX_MARCH = COT_MARCH_TTC - COT_MARCH_HT;
    TAX_DDE = COT_DDE_TTC - COT_DDE_HT;
    TAX_BDG = COT_BDG_TTC - COT_BDG_HT;
    TAX_VOL = COT_VOL_TTC - COT_VOL_HT;
    TAX_PE = COT_PE_TTC - COT_PE_HT;
    TAX_PVV = COT_PVV_TTC - COT_PVV_HT;
    TAX_RC = COT_RC_TTC - COT_RC_HT;
    TAX_PJ = COT_PJ_TTC - COT_PJ_HT;
    TAX_MARCH_TRANSP = COT_MARCH_TRANSP_TTC - COT_MARCH_TRANSP_HT;
    //TOTAL TAXES GARANTIES
    TOTAL_TAX =
      TAX_INCENDIE +
      TAX_TEMPETE +
      TAX_VANDALISME +
      TAX_DELECT +
      TAX_BDM +
      TAX_MARCH +
      TAX_DDE +
      TAX_BDG +
      TAX_VOL +
      TAX_PE +
      TAX_PVV +
      TAX_RC +
      TAX_PJ +
      TAX_MARCH_TRANSP;
    //TAXE ATTENTAT
    TAX_ATT = COT_ATT_HT * COEF_TAX_ATT;
    //TAXE CAT_NAT
    TAX_CATNAT = COT_CATNAT_HT * COEF_TAX_CATNAT;
    //TOTAL CONTRIBUTION ATTENTAT
    TOT_CONTR_ATT = COT_ATT_HT + TAX_ATT;
    //TOTAL CAT_NAT
    TOT_CATNAT = COT_CATNAT_HT + TAX_CATNAT;
    //TAXES Y COMPRIS TAXE ATTENTAT ET TAXE ASSISTANCE
    TOT_TAXES = TOTAL_TAX + TAX_ATT + TAX_ASSISTANCE;
    //TOTAL DES TAXES, FRAIS ET CONTRIBUTION
    TOTAL_TAX_FRAIS_CONTR =
      TAX_ASSISTANCE + TOT_CATNAT + TOT_CONTR_ATT + CONTRB_ATT + FRAIS_COURTIER + TOTAL_TAX;
    //PRIMES TTC/TOTAL TAXES/TOTAL TAXES,FRAIS ET CONTRIBUTION/FRAIS COURTIER/CONTRIBUTION ATTENTAT/TOTAL COTISATION HT SELON LE FRACTIONNEMENT
    //CHOIX D'ALARME
    if (alarm === true) {
      RED_VOL_HT = COT_VOL_HT * -0.15;
      RED_ATT = RED_VOL_HT * 0.04;
      RED_CATNAT_TOT = RED_VOL_HT + RED_ATT;
      RED_CATNAT = RED_CATNAT_TOT * 0.12;
      RED_TAX = RED_CATNAT_TOT * 0.09;
      RED_VOL_TTC = RED_VOL_HT + RED_ATT + RED_CATNAT + RED_TAX;
      PRIM_ANN = mrpRcRuralMixteQuotation + FRAIS_COURTIER + RED_VOL_TTC;
      TOTAL_TAX_FRAIS_CONTR = TOTAL_TAX_FRAIS_CONTR + RED_ATT + RED_CATNAT + RED_TAX;
    } else {
      PRIM_ANN = mrpRcRuralMixteQuotation + FRAIS_COURTIER;
      TOTAL_TAX_FRAIS_CONTR = TOTAL_TAX_FRAIS_CONTR;
    }
    //CHOIX DE BRIS DE MACHINES
    if (bdm === 'limite40000') {
      PRIM_ANN = PRIM_ANN + 231.79;
      TOTAL_TAX_FRAIS_CONTR = TOTAL_TAX_FRAIS_CONTR + 72.86;
    } else if (bdm === 'limite60000') {
      PRIM_ANN = PRIM_ANN + 289.73;
      TOTAL_TAX_FRAIS_CONTR = TOTAL_TAX_FRAIS_CONTR + 91.07;
    } else if (bdm === 'limite80000') {
      PRIM_ANN = PRIM_ANN + 347.68;
      TOTAL_TAX_FRAIS_CONTR = TOTAL_TAX_FRAIS_CONTR + 109.28;
    }
    if (fraction === 1) {
      fractQuotation = PRIM_ANN;
      cashQuotation = (PRIM_ANN / 365) * remainingDays;
    } else if (fraction === 2) {
      fractQuotation = PRIM_ANN / fraction;
      TOTAL_TAX_SEM = TOT_TAXES / fraction;
      FRAIS_COURTIER_SEM = FRAIS_COURTIER / fraction;
      TOTAL_TAX_FRAIS_CONTR_SEM = TOTAL_TAX_FRAIS_CONTR / fraction;
      CONTRB_ATT_SEM = CONTRB_ATT / fraction;
      TOTAL_COT_HT_SEM = TOT_HT / fraction;
      RED_VOL_TTC_SEM = RED_VOL_TTC / fraction;
      cashQuotation = (PRIM_ANN / 365) * remainingDays;
    } else if (fraction === 4) {
      fractQuotation = PRIM_ANN / fraction;
      TOTAL_TAX_TRIM = TOT_TAXES / fraction;
      FRAIS_COURTIER_TRIM = FRAIS_COURTIER / fraction;
      TOTAL_TAX_FRAIS_CONTR_TRIM = TOTAL_TAX_FRAIS_CONTR / fraction;
      CONTRB_ATT_TRIM = CONTRB_ATT / fraction;
      TOTAL_COT_HT_TRIM = TOT_HT / fraction;
      RED_VOL_TTC_TRIM = RED_VOL_TTC / fraction;
      cashQuotation = (PRIM_ANN / 365) * remainingDays;
    } else {
      fractQuotation = PRIM_ANN / fraction;
      cashQuotation = (PRIM_ANN / 365) * remainingDays;
      TOTAL_TAX_MENS = TOT_TAXES / fraction;
      FRAIS_COURTIER_MENS = FRAIS_COURTIER / fraction;
      TOTAL_TAX_FRAIS_CONTR_MENS = TOTAL_TAX_FRAIS_CONTR / fraction;
      CONTRB_ATT_MENS = CONTRB_ATT / fraction;
      TOTAL_COT_HT_MENS = TOT_HT / fraction;
      RED_VOL_TTC_MENS = RED_VOL_TTC / fraction;
    }
    // RESULTATS
    return {
      TOTAL_COT_HT_SEM,
      TOTAL_COT_HT_TRIM,
      TOTAL_COT_HT_MENS,
      TOT_HT,
      PRIM_ANN,
      fractQuotation,
      cashQuotation,
      TOTAL_TAX_FRAIS_CONTR,
      TOT_TAXES,
      TAX_INCENDIE,
      TAX_TEMPETE,
      TAX_VANDALISME,
      TAX_DELECT,
      TAX_DDE,
      TAX_BDG,
      TAX_VOL,
      TAX_PE,
      TAX_ATT,
      COT_ASSISTANCE_TTC,
      CONTRB_ATT,
      FRAIS_COURTIER,
      FRAIS_COURTIER_SEM,
      FRAIS_COURTIER_TRIM,
      FRAIS_COURTIER_MENS,
      TOTAL_TAX_SEM,
      TOTAL_TAX_TRIM,
      TOTAL_TAX_MENS,
      TOTAL_TAX_FRAIS_CONTR_SEM,
      TOTAL_TAX_FRAIS_CONTR_TRIM,
      TOTAL_TAX_FRAIS_CONTR_MENS,
      CONTRB_ATT_SEM,
      CONTRB_ATT_TRIM,
      CONTRB_ATT_MENS,
      remainingMonths,
      RED_VOL_HT,
      RED_ATT,
      RED_CATNAT,
      RED_TAX,
      RED_VOL_TTC,
      RED_VOL_TTC_SEM,
      RED_VOL_TTC_TRIM,
      RED_VOL_TTC_MENS,
      TOT_CATNAT,
    };
  },

  //METHODE DE CALCUL DES TAXES DE L'OFFRE RC URBAIN
  rcUrbanCalculateTAX(turnover, fraction, quotation, effectDate, brokerFee) {
    // DECLARATION CONSTANTES
    const COEF_TAX_RC = 0.09;
    let TAX_COURTIER = null;
    if (brokerFee !== undefined) {
      TAX_COURTIER = brokerFee;
    } else {
      TAX_COURTIER = 0.3;
    }
    // DECLARATION VARIABLES
    let rcUrbanQuot = null;
    if (quotation) {
      rcUrbanQuot = quotation;
    } else {
      rcUrbanQuot = Quotation.crUrbanQuotation(turnover);
    }
    let TOTAL_COT_TTC;
    let TOTAL_COT_HT;
    let TOTAL_TAX;
    let TOTAL_TAX_SEM;
    let TOTAL_TAX_TRIM;
    let TOTAL_TAX_MENS;
    let FRAIS_COURTIER;
    let FRAIS_COURTIER_SEM;
    let FRAIS_COURTIER_TRIM;
    let FRAIS_COURTIER_MENS;
    let TOTAL_TAX_FRAIS_CONTR;
    let TOTAL_TAX_FRAIS_CONTR_SEM;
    let TOTAL_TAX_FRAIS_CONTR_TRIM;
    let TOTAL_TAX_FRAIS_CONTR_MENS;
    let rcUrbanQuotation;
    let PRIM_AN;
    let fractQuotation;
    let cashQuotation;
    let TOTAL_COT_HT_SEM;
    let TOTAL_COT_HT_TRIM;
    let TOTAL_COT_HT_MENS;
    let remainingDays;

    remainingDays =
      1 +
      moment(
        new Date(
          `${Utils.calculateDeadline(fraction, effectDate).deadlineDate.split('-')[2]}-${
            Utils.calculateDeadline(fraction, effectDate).deadlineDate.split('-')[1]
          }-${Utils.calculateDeadline(fraction, effectDate).deadlineDate.split('-')[0]}`,
        ),
      ).diff(
        moment(
          new Date(
            `${effectDate.split('-')[2]}-${effectDate.split('-')[1]}-${effectDate.split('-')[0]}`,
          ),
        ),
        'days',
      );

    //COTISATION RC URBAIN
    rcUrbanQuotation = rcUrbanQuot.crUrbanQuotation;
    TOTAL_COT_TTC = rcUrbanQuotation;
    TOTAL_COT_HT = TOTAL_COT_TTC / (1 + COEF_TAX_RC);
    TOTAL_TAX = TOTAL_COT_TTC - TOTAL_COT_HT;
    FRAIS_COURTIER = TOTAL_COT_HT * TAX_COURTIER;
    TOTAL_TAX_FRAIS_CONTR = TOTAL_TAX + FRAIS_COURTIER;
    //PRIMES TTC/TOTAL TAXES/TOTAL TAXES,FRAIS ET CONTRIBUTION/FRAIS COURTIER/CONTRIBUTION ATTENTAT/TOTAL COTISATION HT SELON LE FRACTIONNEMENT
    PRIM_AN = rcUrbanQuotation + FRAIS_COURTIER;
    if (fraction === 1) {
      fractQuotation = PRIM_AN;
      cashQuotation = (PRIM_AN / 365) * remainingDays;
    } else if (fraction === 2) {
      fractQuotation = PRIM_AN / fraction;
      TOTAL_TAX_SEM = TOTAL_TAX / fraction;
      FRAIS_COURTIER_SEM = FRAIS_COURTIER / fraction;
      TOTAL_TAX_FRAIS_CONTR_SEM = TOTAL_TAX_FRAIS_CONTR / fraction;
      TOTAL_COT_HT_SEM = TOTAL_COT_HT / fraction;
      cashQuotation = (PRIM_AN / 365) * remainingDays;
    } else if (fraction === 4) {
      fractQuotation = PRIM_AN / fraction;
      TOTAL_TAX_TRIM = TOTAL_TAX / fraction;
      FRAIS_COURTIER_TRIM = FRAIS_COURTIER / fraction;
      TOTAL_TAX_FRAIS_CONTR_TRIM = TOTAL_TAX_FRAIS_CONTR / fraction;
      TOTAL_COT_HT_TRIM = TOTAL_COT_HT / fraction;
      cashQuotation = (PRIM_AN / 365) * remainingDays;
    } else {
      fractQuotation = PRIM_AN / fraction;
      cashQuotation = (PRIM_AN / 365) * remainingDays;
      TOTAL_TAX_MENS = TOTAL_TAX / fraction;
      FRAIS_COURTIER_MENS = FRAIS_COURTIER / fraction;
      TOTAL_TAX_FRAIS_CONTR_MENS = TOTAL_TAX_FRAIS_CONTR / fraction;
      TOTAL_COT_HT_MENS = TOTAL_COT_HT / fraction;
    }
    // RESULTATS
    return {
      TOTAL_COT_HT_SEM,
      TOTAL_COT_HT_MENS,
      TOTAL_COT_HT_TRIM,
      TOTAL_COT_HT,
      PRIM_AN,
      fractQuotation,
      cashQuotation,
      TOTAL_TAX_FRAIS_CONTR,
      TOTAL_TAX_FRAIS_CONTR_SEM,
      TOTAL_TAX_FRAIS_CONTR_TRIM,
      TOTAL_TAX_FRAIS_CONTR_MENS,
      FRAIS_COURTIER,
      FRAIS_COURTIER_SEM,
      FRAIS_COURTIER_TRIM,
      FRAIS_COURTIER_MENS,
      TOTAL_TAX,
      TOTAL_TAX_SEM,
      TOTAL_TAX_TRIM,
      TOTAL_TAX_MENS,
    };
  },

  //METHODE DE CALCUL DES TAXES DE L'OFFRE RC RURAL/MIXTE
  rcRuralCalculateTAX(turnover, fraction, quotation, effectDate, brokerFee) {
    // DECLARATION CONSTANTES
    const COEF_TAX_RC = 0.09;
    let TAX_COURTIER = null;
    if (brokerFee !== undefined) {
      TAX_COURTIER = brokerFee;
    } else {
      TAX_COURTIER = 0.3;
    }
    // DECLARATION VARIABLES
    let rcRuralQuot = null;
    if (quotation) {
      rcRuralQuot = quotation;
    } else {
      rcRuralQuot = Quotation.crRuralMixteQuotation(turnover);
    }
    let TOTAL_COT_TTC;
    let TOTAL_COT_HT;
    let TOTAL_TAX;
    let FRAIS_COURTIER;
    let TOTAL_TAX_FRAIS_CONTR;
    let crRuralMixteQuotation;
    let PRIM_AN;
    let fractQuotation;
    let cashQuotation;
    let TOTAL_TAX_SEM;
    let TOTAL_TAX_TRIM;
    let TOTAL_TAX_MENS;
    let FRAIS_COURTIER_SEM;
    let FRAIS_COURTIER_TRIM;
    let FRAIS_COURTIER_MENS;
    let TOTAL_TAX_FRAIS_CONTR_SEM;
    let TOTAL_TAX_FRAIS_CONTR_TRIM;
    let TOTAL_TAX_FRAIS_CONTR_MENS;
    let TOTAL_COT_HT_SEM;
    let TOTAL_COT_HT_TRIM;
    let TOTAL_COT_HT_MENS;
    let remainingDays;

    remainingDays =
      1 +
      moment(
        new Date(
          `${Utils.calculateDeadline(fraction, effectDate).deadlineDate.split('-')[2]}-${
            Utils.calculateDeadline(fraction, effectDate).deadlineDate.split('-')[1]
          }-${Utils.calculateDeadline(fraction, effectDate).deadlineDate.split('-')[0]}`,
        ),
      ).diff(
        moment(
          new Date(
            `${effectDate.split('-')[2]}-${effectDate.split('-')[1]}-${effectDate.split('-')[0]}`,
          ),
        ),
        'days',
      );
    //COTISATION RC RURAL/MIXTE
    crRuralMixteQuotation = rcRuralQuot.crRuralMixteQuotation;
    TOTAL_COT_TTC = crRuralMixteQuotation;
    TOTAL_COT_HT = TOTAL_COT_TTC / (1 + COEF_TAX_RC);
    TOTAL_TAX = TOTAL_COT_TTC - TOTAL_COT_HT;
    FRAIS_COURTIER = TOTAL_COT_HT * TAX_COURTIER;
    TOTAL_TAX_FRAIS_CONTR = TOTAL_TAX + FRAIS_COURTIER;
    //PRIMES TTC/TOTAL TAXES/FRAIS COURTIER/CONTRIBUTION ATTENTAT/TOTAL COTISATION HT SELON LE FRACTIONNEMENT
    PRIM_AN = crRuralMixteQuotation + FRAIS_COURTIER;
    if (fraction === 1) {
      fractQuotation = PRIM_AN;
      cashQuotation = (PRIM_AN / 365) * remainingDays;
    } else if (fraction === 2) {
      fractQuotation = PRIM_AN / fraction;
      TOTAL_TAX_SEM = TOTAL_TAX / fraction;
      FRAIS_COURTIER_SEM = FRAIS_COURTIER / fraction;
      TOTAL_TAX_FRAIS_CONTR_SEM = TOTAL_TAX_FRAIS_CONTR / fraction;
      TOTAL_COT_HT_SEM = TOTAL_COT_HT / fraction;
      cashQuotation = (PRIM_AN / 365) * remainingDays;
    } else if (fraction === 4) {
      fractQuotation = PRIM_AN / fraction;
      TOTAL_TAX_TRIM = TOTAL_TAX / fraction;
      FRAIS_COURTIER_TRIM = FRAIS_COURTIER / fraction;
      TOTAL_TAX_FRAIS_CONTR_TRIM = TOTAL_TAX_FRAIS_CONTR / fraction;
      TOTAL_COT_HT_TRIM = TOTAL_COT_HT / fraction;
      cashQuotation = (PRIM_AN / 365) * remainingDays;
    } else {
      fractQuotation = PRIM_AN / fraction;
      cashQuotation = (PRIM_AN / 365) * remainingDays;
      TOTAL_TAX_MENS = TOTAL_TAX / fraction;
      FRAIS_COURTIER_MENS = FRAIS_COURTIER / fraction;
      TOTAL_TAX_FRAIS_CONTR_MENS = TOTAL_TAX_FRAIS_CONTR / fraction;
      TOTAL_COT_HT_MENS = TOTAL_COT_HT / fraction;
    }
    // RESULTATS
    return {
      TOTAL_COT_HT_SEM,
      TOTAL_COT_HT_MENS,
      TOTAL_COT_HT_TRIM,
      TOTAL_COT_HT,
      PRIM_AN,
      fractQuotation,
      cashQuotation,
      TOTAL_TAX_FRAIS_CONTR,
      TOTAL_TAX_FRAIS_CONTR_SEM,
      TOTAL_TAX_FRAIS_CONTR_TRIM,
      TOTAL_TAX_FRAIS_CONTR_MENS,
      FRAIS_COURTIER,
      FRAIS_COURTIER_SEM,
      FRAIS_COURTIER_TRIM,
      FRAIS_COURTIER_MENS,
      TOTAL_TAX,
      TOTAL_TAX_SEM,
      TOTAL_TAX_TRIM,
      TOTAL_TAX_MENS,
    };
  },

  // calcul MD Quotation
  mdQuotation(
    leaderBirthdate,
    conjointBirthdate,
    kidBirthdate,
    formula,
    kidsNumber,
    haveConjoint,
    haveKids,
    indivAccident,
    indivEnfant,
    rapatriement,
  ) {
    // DECLARATION VARIABLES
    let leader_age;
    let conjoint_age;
    const kid_age = [];
    let TOTAL_COT_PROMO;
    let TOTAL_COT;
    let COT_LEADER_PROMO;
    let COT_LEADER;
    let COT_CONJOINT_PROMO = 0;
    let COT_CONJOINT = 0;
    let COT_KIDS_PROMO = 0;
    let COT_KIDS = 0;
    let COT_INDIV_ACC = 0;
    let COT_INDIV_ENF = 0;
    let COT_RAPATR = 0;

    if (leaderBirthdate) {
      leader_age = moment().diff(
        moment(
          new Date(
            `${leaderBirthdate.split('-')[2]}-${leaderBirthdate.split('-')[1]}-${
              leaderBirthdate.split('-')[0]
            }`,
          ),
        ),
        'year',
      );
    }
    if (conjointBirthdate) {
      conjoint_age = moment().diff(
        moment(
          new Date(
            `${conjointBirthdate.split('-')[2]}-${conjointBirthdate.split('-')[1]}-${
              conjointBirthdate.split('-')[0]
            }`,
          ),
        ),
        'year',
      );
    }
    if (kidsNumber > 0) {
      for (let i = 0; i < kidsNumber; i++) {
        kid_age[i] = moment().diff(
          moment(
            new Date(
              `${kidBirthdate[i].split('-')[2]}-${kidBirthdate[i].split('-')[1]}-${
                kidBirthdate[i].split('-')[0]
              }`,
            ),
          ),
          'year',
        );
      }
    }
    if (leader_age <= 65) {
      if (formula === 1) {
        COT_LEADER = 31.31;
        COT_LEADER_PROMO = COT_LEADER;
      } else if (formula === 2) {
        COT_LEADER = 40.9;
      } else if (formula === 3) {
        COT_LEADER = 47.23;
      } else if (formula === 4) {
        COT_LEADER = 55.39;
      } else if (formula === 5) {
        COT_LEADER = 61.67;
      } else {
        COT_LEADER = 73.88;
      }
    } else if (leader_age === 66) {
      if (formula === 1) {
        COT_LEADER = 32.23;
        COT_LEADER_PROMO = COT_LEADER;
      } else if (formula === 2) {
        COT_LEADER = 42.17;
      } else if (formula === 3) {
        COT_LEADER = 48.71;
      } else if (formula === 4) {
        COT_LEADER = 57.15;
      } else if (formula === 5) {
        COT_LEADER = 63.65;
      } else {
        COT_LEADER = 76.29;
      }
    } else if (leader_age === 67) {
      if (formula === 1) {
        COT_LEADER = 33.19;
        COT_LEADER_PROMO = COT_LEADER;
      } else if (formula === 2) {
        COT_LEADER = 43.47;
      } else if (formula === 3) {
        COT_LEADER = 50.24;
      } else if (formula === 4) {
        COT_LEADER = 58.98;
      } else if (formula === 5) {
        COT_LEADER = 65.7;
      } else {
        COT_LEADER = 78.7;
      }
    } else if (leader_age === 68) {
      if (formula === 1) {
        COT_LEADER = 34.17;
        COT_LEADER_PROMO = COT_LEADER;
      } else if (formula === 2) {
        COT_LEADER = 44.81;
      } else if (formula === 3) {
        COT_LEADER = 51.81;
      } else if (formula === 4) {
        COT_LEADER = 60.86;
      } else if (formula === 5) {
        COT_LEADER = 67.83;
      } else {
        COT_LEADER = 81.37;
      }
    } else if (leader_age === 69) {
      if (formula === 1) {
        COT_LEADER = 35.2;
        COT_LEADER_PROMO = COT_LEADER;
      } else if (formula === 2) {
        COT_LEADER = 46.22;
      } else if (formula === 3) {
        COT_LEADER = 53.46;
      } else if (formula === 4) {
        COT_LEADER = 62.82;
      } else if (formula === 5) {
        COT_LEADER = 70.03;
      } else {
        COT_LEADER = 84.04;
      }
    } else if (leader_age === 70) {
      if (formula === 1) {
        COT_LEADER = 36.26;
        COT_LEADER_PROMO = COT_LEADER;
      } else if (formula === 2) {
        COT_LEADER = 47.66;
      } else if (formula === 3) {
        COT_LEADER = 55.16;
      } else if (formula === 4) {
        COT_LEADER = 64.84;
      } else if (formula === 5) {
        COT_LEADER = 72.31;
      } else {
        (COT_LEADER = 86), 81;
      }
    } else if (leader_age === 71) {
      if (formula === 1) {
        COT_LEADER = 37.35;
        COT_LEADER_PROMO = COT_LEADER;
      } else if (formula === 2) {
        COT_LEADER = 49.16;
      } else if (formula === 3) {
        COT_LEADER = 56.9;
      } else if (formula === 4) {
        COT_LEADER = 66.93;
      } else if (formula === 5) {
        COT_LEADER = 74.67;
      } else {
        (COT_LEADER = 89), 68;
      }
    } else if (leader_age === 72) {
      if (formula === 1) {
        COT_LEADER = 38.49;
        COT_LEADER_PROMO = COT_LEADER;
      } else if (formula === 2) {
        COT_LEADER = 50.71;
      } else if (formula === 3) {
        COT_LEADER = 58.72;
      } else if (formula === 4) {
        COT_LEADER = 69.09;
      } else if (formula === 5) {
        COT_LEADER = 77.11;
      } else {
        COT_LEADER = 92.65;
      }
    } else if (leader_age === 73) {
      if (formula === 1) {
        COT_LEADER = 39.67;
        COT_LEADER_PROMO = COT_LEADER;
      } else if (formula === 2) {
        COT_LEADER = 52.31;
      } else if (formula === 3) {
        COT_LEADER = 60.6;
      } else if (formula === 4) {
        COT_LEADER = 71.35;
      } else if (formula === 5) {
        COT_LEADER = 79.62;
      } else {
        COT_LEADER = 95.71;
      }
    } else if (leader_age === 74) {
      if (formula === 1) {
        COT_LEADER = 40.89;
        COT_LEADER_PROMO = COT_LEADER;
      } else if (formula === 2) {
        COT_LEADER = 53.97;
      } else if (formula === 3) {
        COT_LEADER = 62.55;
      } else if (formula === 4) {
        COT_LEADER = 76.07;
      } else if (formula === 5) {
        COT_LEADER = 82.24;
      } else {
        COT_LEADER = 98.9;
      }
    } else if (leader_age === 75) {
      if (formula === 1) {
        COT_LEADER = 42.15;
        COT_LEADER_PROMO = COT_LEADER;
      } else if (formula === 2) {
        COT_LEADER = 55.68;
      } else if (formula === 3) {
        COT_LEADER = 64.56;
      } else if (formula === 4) {
        COT_LEADER = 76.07;
      } else if (formula === 5) {
        COT_LEADER = 84.94;
      } else {
        COT_LEADER = 102.18;
      }
    } else if (leader_age === 76) {
      if (formula === 1) {
        COT_LEADER = 43.45;
        COT_LEADER_PROMO = COT_LEADER;
      } else if (formula === 2) {
        COT_LEADER = 57.46;
      } else if (formula === 3) {
        COT_LEADER = 66.65;
      } else if (formula === 4) {
        COT_LEADER = 78.56;
      } else if (formula === 5) {
        COT_LEADER = 87.75;
      } else {
        COT_LEADER = 105.58;
      }
    } else if (leader_age === 77) {
      if (formula === 1) {
        COT_LEADER = 44.8;
        COT_LEADER_PROMO = COT_LEADER;
      } else if (formula === 2) {
        COT_LEADER = 59.3;
      } else if (formula === 3) {
        COT_LEADER = 68.8;
      } else if (formula === 4) {
        COT_LEADER = 81.14;
      } else if (formula === 5) {
        COT_LEADER = 90.64;
      } else {
        COT_LEADER = 109.1;
      }
    } else if (leader_age === 78) {
      if (formula === 1) {
        COT_LEADER = 46.19;
        COT_LEADER_PROMO = COT_LEADER;
      } else if (formula === 2) {
        COT_LEADER = 61.2;
      } else if (formula === 3) {
        COT_LEADER = 71.03;
      } else if (formula === 4) {
        COT_LEADER = 83.8;
      } else if (formula === 5) {
        COT_LEADER = 93.65;
      } else {
        COT_LEADER = 112.75;
      }
    } else if (leader_age === 79) {
      if (formula === 1) {
        COT_LEADER = 47.64;
        COT_LEADER_PROMO = COT_LEADER;
      } else if (formula === 2) {
        COT_LEADER = 63.16;
      } else if (formula === 3) {
        COT_LEADER = 73.35;
      } else if (formula === 4) {
        COT_LEADER = 86.56;
      } else if (formula === 5) {
        COT_LEADER = 96.76;
      } else {
        COT_LEADER = 116.51;
      }
    } else if (leader_age === 80) {
      if (formula === 1) {
        COT_LEADER = 49.13;
        COT_LEADER_PROMO = COT_LEADER;
      } else if (formula === 2) {
        COT_LEADER = 65.21;
      } else if (formula === 3) {
        COT_LEADER = 75.74;
      } else if (formula === 4) {
        COT_LEADER = 89.42;
      } else if (formula === 5) {
        COT_LEADER = 99.97;
      } else {
        COT_LEADER = 120.41;
      }
    } else if (leader_age === 81) {
      if (formula === 1) {
        COT_LEADER = 50.67;
        COT_LEADER_PROMO = COT_LEADER;
      } else if (formula === 2) {
        COT_LEADER = 67.33;
      } else if (formula === 3) {
        COT_LEADER = 78.21;
      } else if (formula === 4) {
        COT_LEADER = 92.37;
      } else if (formula === 5) {
        COT_LEADER = 103.3;
      } else {
        COT_LEADER = 124.45;
      }
    } else if (leader_age === 82) {
      if (formula === 1) {
        COT_LEADER = 52.28;
        COT_LEADER_PROMO = COT_LEADER;
      } else if (formula === 2) {
        COT_LEADER = 69.51;
      } else if (formula === 3) {
        COT_LEADER = 80.78;
      } else if (formula === 4) {
        COT_LEADER = 95.43;
      } else if (formula === 5) {
        COT_LEADER = 106.74;
      } else {
        COT_LEADER = 128.64;
      }
    } else if (leader_age === 83) {
      if (formula === 1) {
        COT_LEADER = 53.93;
        COT_LEADER_PROMO = COT_LEADER;
      } else if (formula === 2) {
        COT_LEADER = 71.77;
      } else if (formula === 3) {
        COT_LEADER = 83.44;
      } else if (formula === 4) {
        COT_LEADER = 98.6;
      } else if (formula === 5) {
        COT_LEADER = 110.29;
      } else {
        COT_LEADER = 132.97;
      }
    } else if (leader_age === 84) {
      if (formula === 1) {
        COT_LEADER = 55.65;
        COT_LEADER_PROMO = COT_LEADER;
      } else if (formula === 2) {
        COT_LEADER = 74.1;
      } else if (formula === 3) {
        COT_LEADER = 86.18;
      } else if (formula === 4) {
        COT_LEADER = 101.88;
      } else if (formula === 5) {
        COT_LEADER = 113.98;
      } else {
        COT_LEADER = 137.45;
      }
    } else if (leader_age === 85) {
      if (formula === 1) {
        COT_LEADER = 57.43;
        COT_LEADER_PROMO = COT_LEADER;
      } else if (formula === 2) {
        COT_LEADER = 76.53;
      } else if (formula === 3) {
        COT_LEADER = 89.02;
      } else if (formula === 4) {
        COT_LEADER = 105.26;
      } else if (formula === 5) {
        COT_LEADER = 117.8;
      } else {
        COT_LEADER = 142.09;
      }
    } else if (leader_age === 86) {
      if (formula === 1) {
        COT_LEADER = 59.27;
        COT_LEADER_PROMO = COT_LEADER;
      } else if (formula === 2) {
        COT_LEADER = 79.03;
      } else if (formula === 3) {
        COT_LEADER = 91.97;
      } else if (formula === 4) {
        COT_LEADER = 108.78;
      } else if (formula === 5) {
        COT_LEADER = 121.75;
      } else {
        COT_LEADER = 146.88;
      }
    } else if (leader_age === 87) {
      if (formula === 1) {
        COT_LEADER = 61.17;
        COT_LEADER_PROMO = COT_LEADER;
      } else if (formula === 2) {
        COT_LEADER = 81.63;
      } else if (formula === 3) {
        COT_LEADER = 95.02;
      } else if (formula === 4) {
        COT_LEADER = 112.41;
      } else if (formula === 5) {
        COT_LEADER = 125.84;
      } else {
        COT_LEADER = 151.85;
      }
    } else if (leader_age === 88) {
      if (formula === 1) {
        COT_LEADER = 63.14;
        COT_LEADER_PROMO = COT_LEADER;
      } else if (formula === 2) {
        COT_LEADER = 84.31;
      } else if (formula === 3) {
        COT_LEADER = 98.17;
      } else if (formula === 4) {
        COT_LEADER = 116.18;
      } else if (formula === 5) {
        COT_LEADER = 130.07;
      } else {
        COT_LEADER = 157;
      }
    } else if (leader_age === 89) {
      if (formula === 1) {
        COT_LEADER = 65.18;
        COT_LEADER_PROMO = COT_LEADER;
      } else if (formula === 2) {
        COT_LEADER = 87.09;
      } else if (formula === 3) {
        COT_LEADER = 101.43;
      } else if (formula === 4) {
        COT_LEADER = 120.07;
      } else if (formula === 5) {
        COT_LEADER = 134.44;
      } else {
        COT_LEADER = 162.33;
      }
    } else {
      if (formula === 1) {
        COT_LEADER = 67.3;
        COT_LEADER_PROMO = COT_LEADER;
      } else if (formula === 2) {
        COT_LEADER = 89.97;
      } else if (formula === 3) {
        COT_LEADER = 104.81;
      } else if (formula === 4) {
        COT_LEADER = 124.1;
      } else if (formula === 5) {
        COT_LEADER = 138.97;
      } else {
        COT_LEADER = 167.83;
      }
    }
    if (haveKids) {
      if (kidsNumber === 1) {
        for (let i = 0; i < kidsNumber; i++) {
          if (kid_age[i] <= 26) {
            if (formula === 1) {
              COT_KIDS = 18.8;
              COT_KIDS_PROMO = COT_KIDS;
            } else if (formula === 2) {
              COT_KIDS = 26.3;
            } else if (formula === 3) {
              COT_KIDS = 31.63;
            } else if (formula === 4) {
              COT_KIDS = 38.68;
            } else if (formula === 5) {
              COT_KIDS = 43.47;
            } else {
              COT_KIDS = 53.83;
            }
          } else {
            COT_KIDS_PROMO = 0;
            COT_KIDS = 0;
          }
        }
      } else {
        for (let i = 0; i < kidsNumber; i++) {
          if (kid_age[i] <= 26) {
            if (formula === 1) {
              COT_KIDS = 2 * 18.8;
              COT_KIDS_PROMO = COT_KIDS;
            } else if (formula === 2) {
              COT_KIDS = 2 * 26.3;
            } else if (formula === 3) {
              COT_KIDS = 2 * 31.63;
            } else if (formula === 4) {
              COT_KIDS = 2 * 38.68;
            } else if (formula === 5) {
              COT_KIDS = 2 * 43.47;
            } else {
              COT_KIDS = 2 * 53.83;
            }
          }
        }
      }
    }
    if (haveConjoint) {
      if (conjoint_age <= 20) {
        if (formula === 1) {
          COT_CONJOINT = 21.5;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 31.61;
        } else if (formula === 3) {
          COT_CONJOINT = 38.37;
        } else if (formula === 4) {
          COT_CONJOINT = 42.97;
        } else if (formula === 5) {
          COT_CONJOINT = 48.61;
        } else {
          COT_CONJOINT = 59.87;
        }
      } else if (conjoint_age === 21) {
        if (formula === 1) {
          COT_CONJOINT = 21.75;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 32.0;
        } else if (formula === 3) {
          COT_CONJOINT = 38.83;
        } else if (formula === 4) {
          COT_CONJOINT = 43.62;
        } else if (formula === 5) {
          COT_CONJOINT = 49.34;
        } else {
          COT_CONJOINT = 60.77;
        }
      } else if (conjoint_age === 22) {
        if (formula === 1) {
          COT_CONJOINT = 22.02;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 32.37;
        } else if (formula === 3) {
          COT_CONJOINT = 39.29;
        } else if (formula === 4) {
          COT_CONJOINT = 44.27;
        } else if (formula === 5) {
          COT_CONJOINT = 50.09;
        } else {
          COT_CONJOINT = 61.67;
        }
      } else if (conjoint_age === 23) {
        if (formula === 1) {
          (COT_CONJOINT = 22), 28;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          (COT_CONJOINT = 32), 76;
        } else if (formula === 3) {
          (COT_CONJOINT = 39), 76;
        } else if (formula === 4) {
          (COT_CONJOINT = 44), 93;
        } else if (formula === 5) {
          (COT_CONJOINT = 50), 84;
        } else {
          (COT_CONJOINT = 62), 61;
        }
      } else if (conjoint_age === 24) {
        if (formula === 1) {
          COT_CONJOINT = 22.55;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 33.16;
        } else if (formula === 3) {
          COT_CONJOINT = 40.24;
        } else if (formula === 4) {
          COT_CONJOINT = 45.6;
        } else if (formula === 5) {
          COT_CONJOINT = 51.6;
        } else {
          COT_CONJOINT = 63.54;
        }
      } else if (conjoint_age === 25) {
        if (formula === 1) {
          COT_CONJOINT = 22.82;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 33.55;
        } else if (formula === 3) {
          COT_CONJOINT = 40.71;
        } else if (formula === 4) {
          COT_CONJOINT = 46.29;
        } else if (formula === 5) {
          COT_CONJOINT = 52.38;
        } else {
          COT_CONJOINT = 64.5;
        }
      } else if (conjoint_age === 26) {
        if (formula === 1) {
          COT_CONJOINT = 23.09;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 33.96;
        } else if (formula === 3) {
          COT_CONJOINT = 41.21;
        } else if (formula === 4) {
          COT_CONJOINT = 46.99;
        } else if (formula === 5) {
          COT_CONJOINT = 53.16;
        } else {
          COT_CONJOINT = 65.47;
        }
      } else if (conjoint_age === 27) {
        if (formula === 1) {
          COT_CONJOINT = 23.37;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 34.37;
        } else if (formula === 3) {
          COT_CONJOINT = 41.7;
        } else if (formula === 4) {
          COT_CONJOINT = 47.7;
        } else if (formula === 5) {
          COT_CONJOINT = 53.95;
        } else {
          COT_CONJOINT = 66.46;
        }
      } else if (conjoint_age === 28) {
        if (formula === 1) {
          COT_CONJOINT = 23.65;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 34.77;
        } else if (formula === 3) {
          COT_CONJOINT = 42.21;
        } else if (formula === 4) {
          COT_CONJOINT = 48.41;
        } else if (formula === 5) {
          COT_CONJOINT = 54.77;
        } else {
          COT_CONJOINT = 67.44;
        }
      } else if (conjoint_age === 29) {
        if (formula === 1) {
          COT_CONJOINT = 23.94;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 35.2;
        } else if (formula === 3) {
          COT_CONJOINT = 42.71;
        } else if (formula === 4) {
          COT_CONJOINT = 49.13;
        } else if (formula === 5) {
          COT_CONJOINT = 55.58;
        } else {
          COT_CONJOINT = 68.46;
        }
      } else if (conjoint_age === 30) {
        if (formula === 1) {
          COT_CONJOINT = 24.22;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 35.62;
        } else if (formula === 3) {
          COT_CONJOINT = 43.22;
        } else if (formula === 4) {
          COT_CONJOINT = 49.87;
        } else if (formula === 5) {
          COT_CONJOINT = 56.41;
        } else {
          COT_CONJOINT = 69.48;
        }
      } else if (conjoint_age === 31) {
        if (formula === 1) {
          COT_CONJOINT = 24.52;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 36.05;
        } else if (formula === 3) {
          COT_CONJOINT = 43.74;
        } else if (formula === 4) {
          COT_CONJOINT = 50.61;
        } else if (formula === 5) {
          COT_CONJOINT = 57.27;
        } else {
          COT_CONJOINT = 70.53;
        }
      } else if (conjoint_age === 32) {
        if (formula === 1) {
          COT_CONJOINT = 24.81;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 36.48;
        } else if (formula === 3) {
          COT_CONJOINT = 44.27;
        } else if (formula === 4) {
          COT_CONJOINT = 51.37;
        } else if (formula === 5) {
          COT_CONJOINT = 58.12;
        } else {
          COT_CONJOINT = 71.58;
        }
      } else if (conjoint_age === 33) {
        if (formula === 1) {
          COT_CONJOINT = 25.11;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 36.93;
        } else if (formula === 3) {
          COT_CONJOINT = 44.8;
        } else if (formula === 4) {
          COT_CONJOINT = 52.13;
        } else if (formula === 5) {
          COT_CONJOINT = 58.99;
        } else {
          COT_CONJOINT = 72.66;
        }
      } else if (conjoint_age === 34) {
        if (formula === 1) {
          COT_CONJOINT = 25.41;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 37.36;
        } else if (formula === 3) {
          COT_CONJOINT = 45.34;
        } else if (formula === 4) {
          COT_CONJOINT = 52.92;
        } else if (formula === 5) {
          COT_CONJOINT = 59.88;
        } else {
          COT_CONJOINT = 73.76;
        }
      } else if (conjoint_age === 35) {
        if (formula === 1) {
          COT_CONJOINT = 25.71;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 37.81;
        } else if (formula === 3) {
          COT_CONJOINT = 45.88;
        } else if (formula === 4) {
          COT_CONJOINT = 53.71;
        } else if (formula === 5) {
          COT_CONJOINT = 60.78;
        } else {
          COT_CONJOINT = 74.86;
        }
      } else if (conjoint_age === 36) {
        if (formula === 1) {
          COT_CONJOINT = 26.02;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 38.27;
        } else if (formula === 3) {
          COT_CONJOINT = 46.43;
        } else if (formula === 4) {
          COT_CONJOINT = 54.52;
        } else if (formula === 5) {
          COT_CONJOINT = 61.69;
        } else {
          COT_CONJOINT = 75.98;
        }
      } else if (conjoint_age === 37) {
        if (formula === 1) {
          COT_CONJOINT = 26.33;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 38.72;
        } else if (formula === 3) {
          COT_CONJOINT = 46.99;
        } else if (formula === 4) {
          COT_CONJOINT = 55.34;
        } else if (formula === 5) {
          COT_CONJOINT = 62.61;
        } else {
          COT_CONJOINT = 77.13;
        }
      } else if (conjoint_age === 38) {
        if (formula === 1) {
          COT_CONJOINT = 26.65;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 39.19;
        } else if (formula === 3) {
          COT_CONJOINT = 47.55;
        } else if (formula === 4) {
          COT_CONJOINT = 56.17;
        } else if (formula === 5) {
          COT_CONJOINT = 63.55;
        } else {
          COT_CONJOINT = 78.28;
        }
      } else if (conjoint_age === 39) {
        if (formula === 1) {
          COT_CONJOINT = 26.96;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 39.66;
        } else if (formula === 3) {
          COT_CONJOINT = 48.13;
        } else if (formula === 4) {
          COT_CONJOINT = 57.01;
        } else if (formula === 5) {
          COT_CONJOINT = 64.51;
        } else {
          COT_CONJOINT = 79.45;
        }
      } else if (conjoint_age === 40) {
        if (formula === 1) {
          COT_CONJOINT = 27.29;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 40.14;
        } else if (formula === 3) {
          COT_CONJOINT = 48.7;
        } else if (formula === 4) {
          COT_CONJOINT = 57.86;
        } else if (formula === 5) {
          COT_CONJOINT = 65.47;
        } else {
          COT_CONJOINT = 80.64;
        }
      } else if (conjoint_age === 41) {
        if (formula === 1) {
          COT_CONJOINT = 27.61;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 40.61;
        } else if (formula === 3) {
          COT_CONJOINT = 49.29;
        } else if (formula === 4) {
          COT_CONJOINT = 58.74;
        } else if (formula === 5) {
          COT_CONJOINT = 66.46;
        } else {
          COT_CONJOINT = 81.85;
        }
      } else if (conjoint_age === 42) {
        if (formula === 1) {
          COT_CONJOINT = 27.95;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 41.1;
        } else if (formula === 3) {
          COT_CONJOINT = 49.88;
        } else if (formula === 4) {
          COT_CONJOINT = 59.62;
        } else if (formula === 5) {
          COT_CONJOINT = 67.44;
        } else {
          COT_CONJOINT = 83.08;
        }
      } else if (conjoint_age === 43) {
        if (formula === 1) {
          COT_CONJOINT = 28.29;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 41.59;
        } else if (formula === 3) {
          COT_CONJOINT = 50.48;
        } else if (formula === 4) {
          COT_CONJOINT = 60.51;
        } else if (formula === 5) {
          COT_CONJOINT = 68.46;
        } else {
          COT_CONJOINT = 84.32;
        }
      } else if (conjoint_age === 44) {
        if (formula === 1) {
          COT_CONJOINT = 28.62;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 42.1;
        } else if (formula === 3) {
          COT_CONJOINT = 51.09;
        } else if (formula === 4) {
          COT_CONJOINT = 61.42;
        } else if (formula === 5) {
          COT_CONJOINT = 69.48;
        } else {
          COT_CONJOINT = 85.59;
        }
      } else if (conjoint_age === 45) {
        if (formula === 1) {
          COT_CONJOINT = 28.97;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 42.59;
        } else if (formula === 3) {
          COT_CONJOINT = 51.7;
        } else if (formula === 4) {
          COT_CONJOINT = 62.34;
        } else if (formula === 5) {
          COT_CONJOINT = 70.53;
        } else {
          COT_CONJOINT = 86.87;
        }
      } else if (conjoint_age === 46) {
        if (formula === 1) {
          COT_CONJOINT = 29.31;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 43.11;
        } else if (formula === 3) {
          COT_CONJOINT = 52.32;
        } else if (formula === 4) {
          COT_CONJOINT = 63.28;
        } else if (formula === 5) {
          COT_CONJOINT = 71.59;
        } else {
          COT_CONJOINT = 88.17;
        }
      } else if (conjoint_age === 47) {
        if (formula === 1) {
          COT_CONJOINT = 29.67;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 43.62;
        } else if (formula === 3) {
          COT_CONJOINT = 52.94;
        } else if (formula === 4) {
          COT_CONJOINT = 64.23;
        } else if (formula === 5) {
          COT_CONJOINT = 72.67;
        } else {
          COT_CONJOINT = 89.49;
        }
      } else if (conjoint_age === 48) {
        if (formula === 1) {
          COT_CONJOINT = 30.21;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 44.32;
        } else if (formula === 3) {
          COT_CONJOINT = 53.79;
        } else if (formula === 4) {
          COT_CONJOINT = 65.26;
        } else if (formula === 5) {
          COT_CONJOINT = 73.83;
        } else {
          COT_CONJOINT = 90.93;
        }
      } else if (conjoint_age === 49) {
        if (formula === 1) {
          COT_CONJOINT = 30.75;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 45.03;
        } else if (formula === 3) {
          COT_CONJOINT = 54.65;
        } else if (formula === 4) {
          COT_CONJOINT = 66.3;
        } else if (formula === 5) {
          COT_CONJOINT = 75.01;
        } else {
          COT_CONJOINT = 92.38;
        }
      } else if (conjoint_age === 50) {
        if (formula === 1) {
          COT_CONJOINT = 31.3;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 45.74;
        } else if (formula === 3) {
          COT_CONJOINT = 55.53;
        } else if (formula === 4) {
          COT_CONJOINT = 67.36;
        } else if (formula === 5) {
          COT_CONJOINT = 76.22;
        } else {
          COT_CONJOINT = 93.86;
        }
      } else if (conjoint_age === 51) {
        if (formula === 1) {
          COT_CONJOINT = 31.86;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 46.47;
        } else if (formula === 3) {
          COT_CONJOINT = 56.42;
        } else if (formula === 4) {
          COT_CONJOINT = 68.44;
        } else if (formula === 5) {
          COT_CONJOINT = 77.44;
        } else {
          COT_CONJOINT = 95.36;
        }
      } else if (conjoint_age === 52) {
        if (formula === 1) {
          COT_CONJOINT = 32.43;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 47.22;
        } else if (formula === 3) {
          COT_CONJOINT = 57.32;
        } else if (formula === 4) {
          COT_CONJOINT = 69.53;
        } else if (formula === 5) {
          COT_CONJOINT = 78.67;
        } else {
          COT_CONJOINT = 96.89;
        }
      } else if (conjoint_age === 53) {
        if (formula === 1) {
          COT_CONJOINT = 33.01;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 47.98;
        } else if (formula === 3) {
          COT_CONJOINT = 58.24;
        } else if (formula === 4) {
          COT_CONJOINT = 70.65;
        } else if (formula === 5) {
          COT_CONJOINT = 79.93;
        } else {
          COT_CONJOINT = 98.44;
        }
      } else if (conjoint_age === 54) {
        if (formula === 1) {
          COT_CONJOINT = 33.6;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 48.74;
        } else if (formula === 3) {
          COT_CONJOINT = 59.17;
        } else if (formula === 4) {
          COT_CONJOINT = 71.78;
        } else if (formula === 5) {
          COT_CONJOINT = 81.21;
        } else {
          COT_CONJOINT = 100.01;
        }
      } else if (conjoint_age === 55) {
        if (formula === 1) {
          COT_CONJOINT = 34.21;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 49.52;
        } else if (formula === 3) {
          COT_CONJOINT = 60.12;
        } else if (formula === 4) {
          COT_CONJOINT = 72.93;
        } else if (formula === 5) {
          COT_CONJOINT = 82.5;
        } else {
          COT_CONJOINT = 101.61;
        }
      } else if (conjoint_age === 56) {
        if (formula === 1) {
          COT_CONJOINT = 34.83;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 50.31;
        } else if (formula === 3) {
          COT_CONJOINT = 61.07;
        } else if (formula === 4) {
          COT_CONJOINT = 74.09;
        } else if (formula === 5) {
          COT_CONJOINT = 83.82;
        } else {
          COT_CONJOINT = 103.23;
        }
      } else if (conjoint_age === 57) {
        if (formula === 1) {
          COT_CONJOINT = 35.46;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 51.12;
        } else if (formula === 3) {
          COT_CONJOINT = 62.05;
        } else if (formula === 4) {
          COT_CONJOINT = 75.28;
        } else if (formula === 5) {
          COT_CONJOINT = 85.16;
        } else {
          COT_CONJOINT = 104.88;
        }
      } else if (conjoint_age === 58) {
        if (formula === 1) {
          COT_CONJOINT = 36.1;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 51.93;
        } else if (formula === 3) {
          COT_CONJOINT = 63.04;
        } else if (formula === 4) {
          COT_CONJOINT = 76.48;
        } else if (formula === 5) {
          COT_CONJOINT = 86.52;
        } else {
          COT_CONJOINT = 106.56;
        }
      } else if (conjoint_age === 59) {
        if (formula === 1) {
          COT_CONJOINT = 36.75;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 52.76;
        } else if (formula === 3) {
          COT_CONJOINT = 64.05;
        } else if (formula === 4) {
          COT_CONJOINT = 77.7;
        } else if (formula === 5) {
          COT_CONJOINT = 87.9;
        } else {
          COT_CONJOINT = 108.26;
        }
      } else if (conjoint_age === 60) {
        if (formula === 1) {
          COT_CONJOINT = 37.41;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 53.61;
        } else if (formula === 3) {
          COT_CONJOINT = 65.07;
        } else if (formula === 4) {
          COT_CONJOINT = 78.95;
        } else if (formula === 5) {
          COT_CONJOINT = 89.31;
        } else {
          COT_CONJOINT = 110.0;
        }
      } else if (conjoint_age === 61) {
        if (formula === 1) {
          COT_CONJOINT = 38.08;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 54.47;
        } else if (formula === 3) {
          COT_CONJOINT = 66.11;
        } else if (formula === 4) {
          COT_CONJOINT = 80.21;
        } else if (formula === 5) {
          COT_CONJOINT = 90.74;
        } else {
          COT_CONJOINT = 111.76;
        }
      } else if (conjoint_age === 62) {
        if (formula === 1) {
          COT_CONJOINT = 38.76;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 55.34;
        } else if (formula === 3) {
          COT_CONJOINT = 67.18;
        } else if (formula === 4) {
          COT_CONJOINT = 81.5;
        } else if (formula === 5) {
          COT_CONJOINT = 92.2;
        } else {
          COT_CONJOINT = 113.55;
        }
      } else if (conjoint_age === 63) {
        if (formula === 1) {
          COT_CONJOINT = 39.46;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 56.23;
        } else if (formula === 3) {
          COT_CONJOINT = 68.25;
        } else if (formula === 4) {
          COT_CONJOINT = 82.8;
        } else if (formula === 5) {
          COT_CONJOINT = 93.67;
        } else {
          COT_CONJOINT = 115.37;
        }
      } else if (conjoint_age === 64) {
        if (formula === 1) {
          (COT_CONJOINT = 40), 17;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          (COT_CONJOINT = 57), 13;
        } else if (formula === 3) {
          (COT_CONJOINT = 69), 34;
        } else if (formula === 4) {
          (COT_CONJOINT = 84), 13;
        } else if (formula === 5) {
          (COT_CONJOINT = 95), 17;
        } else {
          (COT_CONJOINT = 117), 21;
        }
      } else if (conjoint_age === 65) {
        if (formula === 1) {
          COT_CONJOINT = 40.89;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 58.03;
        } else if (formula === 3) {
          COT_CONJOINT = 70.45;
        } else if (formula === 4) {
          COT_CONJOINT = 85.48;
        } else if (formula === 5) {
          COT_CONJOINT = 96.7;
        } else {
          COT_CONJOINT = 119.08;
        }
      } else if (conjoint_age === 66) {
        if (formula === 1) {
          COT_CONJOINT = 41.63;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 58.96;
        } else if (formula === 3) {
          COT_CONJOINT = 71.58;
        } else if (formula === 4) {
          COT_CONJOINT = 86.84;
        } else if (formula === 5) {
          COT_CONJOINT = 98.24;
        } else {
          COT_CONJOINT = 120.98;
        }
      } else if (conjoint_age === 67) {
        if (formula === 1) {
          COT_CONJOINT = 42.38;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 59.9;
        } else if (formula === 3) {
          COT_CONJOINT = 72.72;
        } else if (formula === 4) {
          COT_CONJOINT = 88.23;
        } else if (formula === 5) {
          COT_CONJOINT = 99.82;
        } else {
          COT_CONJOINT = 122.92;
        }
      } else if (conjoint_age === 68) {
        if (formula === 1) {
          COT_CONJOINT = 43.14;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 60.86;
        } else if (formula === 3) {
          (COT_CONJOINT = 73), 88;
        } else if (formula === 4) {
          COT_CONJOINT = 89.64;
        } else if (formula === 5) {
          COT_CONJOINT = 101.41;
        } else {
          COT_CONJOINT = 124.88;
        }
      } else if (conjoint_age === 69) {
        if (formula === 1) {
          COT_CONJOINT = 43.91;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 61.83;
        } else if (formula === 3) {
          COT_CONJOINT = 75.06;
        } else if (formula === 4) {
          COT_CONJOINT = 91.08;
        } else if (formula === 5) {
          COT_CONJOINT = 103.04;
        } else {
          COT_CONJOINT = 126.88;
        }
      } else if (conjoint_age === 70) {
        if (formula === 1) {
          COT_CONJOINT = 44.7;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 62.82;
        } else if (formula === 3) {
          COT_CONJOINT = 76.27;
        } else if (formula === 4) {
          COT_CONJOINT = 92.53;
        } else if (formula === 5) {
          COT_CONJOINT = 104.69;
        } else {
          COT_CONJOINT = 128.91;
        }
      } else if (conjoint_age === 71) {
        if (formula === 1) {
          COT_CONJOINT = 45.51;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 63.83;
        } else if (formula === 3) {
          COT_CONJOINT = 77.49;
        } else if (formula === 4) {
          COT_CONJOINT = 94.01;
        } else if (formula === 5) {
          COT_CONJOINT = 106.37;
        } else {
          COT_CONJOINT = 130.97;
        }
      } else if (conjoint_age === 72) {
        if (formula === 1) {
          COT_CONJOINT = 46.33;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 64.85;
        } else if (formula === 3) {
          COT_CONJOINT = 78.73;
        } else if (formula === 4) {
          COT_CONJOINT = 95.51;
        } else if (formula === 5) {
          COT_CONJOINT = 108.07;
        } else {
          COT_CONJOINT = 133.06;
        }
      } else if (conjoint_age === 73) {
        if (formula === 1) {
          COT_CONJOINT = 47.16;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 65.89;
        } else if (formula === 3) {
          COT_CONJOINT = 79.99;
        } else if (formula === 4) {
          COT_CONJOINT = 97.04;
        } else if (formula === 5) {
          COT_CONJOINT = 109.8;
        } else {
          COT_CONJOINT = 135.19;
        }
      } else if (conjoint_age === 74) {
        if (formula === 1) {
          COT_CONJOINT = 48.02;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 66.94;
        } else if (formula === 3) {
          COT_CONJOINT = 81.27;
        } else if (formula === 4) {
          COT_CONJOINT = 98.59;
        } else if (formula === 5) {
          COT_CONJOINT = 111.56;
        } else {
          COT_CONJOINT = 137.35;
        }
      } else if (conjoint_age === 75) {
        if (formula === 1) {
          COT_CONJOINT = 48.88;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 68.01;
        } else if (formula === 3) {
          COT_CONJOINT = 82.58;
        } else if (formula === 4) {
          COT_CONJOINT = 100.17;
        } else if (formula === 5) {
          COT_CONJOINT = 113.34;
        } else {
          COT_CONJOINT = 139.55;
        }
      } else if (conjoint_age === 76) {
        if (formula === 1) {
          COT_CONJOINT = 49.76;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 69.1;
        } else if (formula === 3) {
          COT_CONJOINT = 83.9;
        } else if (formula === 4) {
          COT_CONJOINT = 101.78;
        } else if (formula === 5) {
          COT_CONJOINT = 115.15;
        } else {
          COT_CONJOINT = 141.78;
        }
      } else if (conjoint_age === 77) {
        if (formula === 1) {
          COT_CONJOINT = 50.66;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 70.21;
        } else if (formula === 3) {
          COT_CONJOINT = 85.24;
        } else if (formula === 4) {
          COT_CONJOINT = 103.41;
        } else if (formula === 5) {
          COT_CONJOINT = 117.0;
        } else {
          COT_CONJOINT = 144.05;
        }
      } else if (conjoint_age === 78) {
        if (formula === 1) {
          COT_CONJOINT = 51.57;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 71.33;
        } else if (formula === 3) {
          COT_CONJOINT = 86.61;
        } else if (formula === 4) {
          COT_CONJOINT = 105.06;
        } else if (formula === 5) {
          COT_CONJOINT = 118.87;
        } else {
          COT_CONJOINT = 146.36;
        }
      } else if (conjoint_age === 79) {
        if (formula === 1) {
          COT_CONJOINT = 52.5;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 72.47;
        } else if (formula === 3) {
          COT_CONJOINT = 87.99;
        } else if (formula === 4) {
          COT_CONJOINT = 106.74;
        } else if (formula === 5) {
          COT_CONJOINT = 120.78;
        } else {
          COT_CONJOINT = 148.7;
        }
      } else if (conjoint_age === 80) {
        if (formula === 1) {
          COT_CONJOINT = 53.44;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 73.63;
        } else if (formula === 3) {
          COT_CONJOINT = 89.4;
        } else if (formula === 4) {
          COT_CONJOINT = 108.45;
        } else if (formula === 5) {
          COT_CONJOINT = 122.71;
        } else {
          COT_CONJOINT = 151.08;
        }
      } else if (conjoint_age === 81) {
        if (formula === 1) {
          COT_CONJOINT = 54.41;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 74.81;
        } else if (formula === 3) {
          COT_CONJOINT = 90.83;
        } else if (formula === 4) {
          COT_CONJOINT = 110.18;
        } else if (formula === 5) {
          COT_CONJOINT = 124.68;
        } else {
          COT_CONJOINT = 153.49;
        }
      } else if (conjoint_age === 82) {
        if (formula === 1) {
          COT_CONJOINT = 55.38;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 76.01;
        } else if (formula === 3) {
          COT_CONJOINT = 92.28;
        } else if (formula === 4) {
          COT_CONJOINT = 111.95;
        } else if (formula === 5) {
          COT_CONJOINT = 126.68;
        } else {
          COT_CONJOINT = 155.95;
        }
      } else if (conjoint_age === 83) {
        if (formula === 1) {
          COT_CONJOINT = 56.38;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 77.22;
        } else if (formula === 3) {
          COT_CONJOINT = 93.76;
        } else if (formula === 4) {
          COT_CONJOINT = 113.74;
        } else if (formula === 5) {
          COT_CONJOINT = 128.71;
        } else {
          COT_CONJOINT = 158.44;
        }
      } else if (conjoint_age === 84) {
        if (formula === 1) {
          COT_CONJOINT = 57.39;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 78.46;
        } else if (formula === 3) {
          COT_CONJOINT = 95.26;
        } else if (formula === 4) {
          COT_CONJOINT = 115.56;
        } else if (formula === 5) {
          COT_CONJOINT = 130.77;
        } else {
          COT_CONJOINT = 160.98;
        }
      } else if (conjoint_age === 85) {
        if (formula === 1) {
          COT_CONJOINT = 58.43;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 79.71;
        } else if (formula === 3) {
          COT_CONJOINT = 96.79;
        } else if (formula === 4) {
          COT_CONJOINT = 117.4;
        } else if (formula === 5) {
          COT_CONJOINT = 132.86;
        } else {
          COT_CONJOINT = 163.56;
        }
      } else if (conjoint_age === 86) {
        if (formula === 1) {
          COT_CONJOINT = 59.49;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 80.98;
        } else if (formula === 3) {
          COT_CONJOINT = 98.33;
        } else if (formula === 4) {
          COT_CONJOINT = 119.28;
        } else if (formula === 5) {
          COT_CONJOINT = 134.98;
        } else {
          COT_CONJOINT = 166.18;
        }
      } else if (conjoint_age === 87) {
        if (formula === 1) {
          COT_CONJOINT = 60.55;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 82.27;
        } else if (formula === 3) {
          COT_CONJOINT = 99.91;
        } else if (formula === 4) {
          COT_CONJOINT = 121.19;
        } else if (formula === 5) {
          COT_CONJOINT = 137.14;
        } else {
          COT_CONJOINT = 168.84;
        }
      } else if (conjoint_age === 88) {
        if (formula === 1) {
          COT_CONJOINT = 61.64;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 83.59;
        } else if (formula === 3) {
          COT_CONJOINT = 101.51;
        } else if (formula === 4) {
          COT_CONJOINT = 123.12;
        } else if (formula === 5) {
          COT_CONJOINT = 139.34;
        } else {
          COT_CONJOINT = 171.55;
        }
      } else if (conjoint_age === 89) {
        if (formula === 1) {
          COT_CONJOINT = 62.75;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 84.92;
        } else if (formula === 3) {
          COT_CONJOINT = 103.13;
        } else if (formula === 4) {
          COT_CONJOINT = 125.09;
        } else if (formula === 5) {
          COT_CONJOINT = 141.56;
        } else {
          COT_CONJOINT = 174.29;
        }
      } else {
        if (formula === 1) {
          COT_CONJOINT = 63.88;
          COT_CONJOINT_PROMO = COT_CONJOINT;
        } else if (formula === 2) {
          COT_CONJOINT = 86.28;
        } else if (formula === 3) {
          COT_CONJOINT = 104.78;
        } else if (formula === 4) {
          COT_CONJOINT = 127.09;
        } else if (formula === 5) {
          COT_CONJOINT = 143.83;
        } else {
          COT_CONJOINT = 177.08;
        }
      }
    }
    TOTAL_COT_PROMO = COT_LEADER_PROMO + COT_CONJOINT_PROMO + COT_KIDS_PROMO;

    if (indivAccident === 'individuel') {
      COT_INDIV_ACC = 0.5;
    } else if (indivAccident === 'famille') {
      COT_INDIV_ACC = 0.75;
    }
    if (indivEnfant === true) {
      COT_INDIV_ENF = 1;
    }
    if (rapatriement === '1adulte') {
      COT_RAPATR = 6.06;
    } else if (rapatriement === '2adultes') {
      COT_RAPATR = 9.08;
    } else if (rapatriement === 'famille') {
      COT_RAPATR = 10.54;
    }
    TOTAL_COT = COT_LEADER + COT_CONJOINT + COT_KIDS + COT_INDIV_ACC + COT_INDIV_ENF + COT_RAPATR;
    // RESULTATS
    return {
      TOTAL_COT_PROMO,
      TOTAL_COT,
      COT_LEADER_PROMO,
      COT_LEADER,
      COT_CONJOINT_PROMO,
      COT_CONJOINT,
      COT_KIDS_PROMO,
      COT_KIDS,
      leader_age,
      conjoint_age,
      kid_age,
    };
  },

  // Calcul des cotisations offre MD selon le fractionnement
  MdCalculateQuotation(
    leaderBirthdate,
    conjointBirthdate,
    kidBirthdate,
    formula,
    kidsNumber,
    haveConjoint,
    haveKids,
    indivAccident,
    indivEnfant,
    rapatriement,
    quotation,
    fraction,
    effectDate,
  ) {
    // DECLARATION VARIABLES
    let mdQuot = null;
    if (quotation) {
      mdQuot = quotation;
    } else {
      mdQuot = Quotation.mdQuotation(
        leaderBirthdate,
        conjointBirthdate,
        kidBirthdate,
        formula,
        kidsNumber,
        haveConjoint,
        haveKids,
        indivAccident,
        indivEnfant,
        rapatriement,
      );
    }
    let mdQuotation;
    let remainingDays;
    let fractQuotation;
    let cashQuotation;

    remainingDays =
      1 +
      moment(
        new Date(
          `${Utils.calculateDeadline(fraction, effectDate).deadlineDate.split('-')[2]}-${
            Utils.calculateDeadline(fraction, effectDate).deadlineDate.split('-')[1]
          }-${Utils.calculateDeadline(fraction, effectDate).deadlineDate.split('-')[0]}`,
        ),
      ).diff(
        moment(
          new Date(
            `${effectDate.split('-')[2]}-${effectDate.split('-')[1]}-${effectDate.split('-')[0]}`,
          ),
        ),
        'days',
      );

    mdQuotation = mdQuot.TOTAL_COT;

    if (fraction === 1) {
      fractQuotation = mdQuotation * 12;
      cashQuotation = ((mdQuotation * 12) / 365) * remainingDays;
    } else if (fraction === 2) {
      fractQuotation = mdQuotation * 6;
      cashQuotation = ((mdQuotation * 12) / 365) * remainingDays;
    } else if (fraction === 4) {
      fractQuotation = mdQuotation * 4;
      cashQuotation = ((mdQuotation * 12) / 365) * remainingDays;
    } else {
      fractQuotation = mdQuotation;
      cashQuotation = ((mdQuotation * 12) / 365) * remainingDays;
    }
    // resultat
    return {
      fractQuotation,
      cashQuotation,
    };
  },

  // calcul RCMS Quotation
  rcmsQuotation(brokerFee) {
    // declaration des constantes
    const COT_REF_TTC = 171.68;
    const TAUX_TAXES = 0.09;
    const TAUX_COMMISSION = 0.15;
    const CONTR_ATT = 0;

    // declaration des variables
    let COT_REF_HT;
    let FRAIS_COURTIER;
    let TOTAL_TAXES;
    let TOTAL_TAXES_FRAIS_CONTR;
    let COMMISSION_ANN;
    let COTISATION_ANN;
    let TAUX_FRAIS_COURTIER;

    if (brokerFee) {
      TAUX_FRAIS_COURTIER = brokerFee;
    } else {
      TAUX_FRAIS_COURTIER = 0.3;
    }
    // logique
    COT_REF_HT = COT_REF_TTC / (1 + TAUX_TAXES);
    FRAIS_COURTIER = COT_REF_HT * TAUX_FRAIS_COURTIER;
    TOTAL_TAXES = COT_REF_TTC - COT_REF_HT;
    TOTAL_TAXES_FRAIS_CONTR = TOTAL_TAXES + FRAIS_COURTIER + CONTR_ATT;
    COMMISSION_ANN = COT_REF_HT * TAUX_COMMISSION + FRAIS_COURTIER;
    COTISATION_ANN = COT_REF_TTC + FRAIS_COURTIER;
    // resultat
    return {
      COTISATION_ANN,
      FRAIS_COURTIER,
      TOTAL_TAXES_FRAIS_CONTR,
      COMMISSION_ANN,
      TOTAL_TAXES,
    };
  },

  // Calcul des cotisations offre RCMS selon le fractionnement
  rcmsCalculateQuotation(quotation, fraction, effectDate, brokerFee) {
    // DECLARATION VARIABLES
    let rcmsQuot = null;
    if (quotation) {
      rcmsQuot = quotation;
    } else {
      rcmsQuot = Quotation.rcmsQuotation(brokerFee);
    }
    let totTaxesFrais;
    let rcmsQuotation;
    let remainingDays;
    let fractQuotation;
    let cashQuotation;

    remainingDays =
      1 +
      moment(
        new Date(
          `${Utils.calculateDeadline(fraction, effectDate).deadlineDate.split('-')[2]}-${
            Utils.calculateDeadline(fraction, effectDate).deadlineDate.split('-')[1]
          }-${Utils.calculateDeadline(fraction, effectDate).deadlineDate.split('-')[0]}`,
        ),
      ).diff(
        moment(
          new Date(
            `${effectDate.split('-')[2]}-${effectDate.split('-')[1]}-${effectDate.split('-')[0]}`,
          ),
        ),
        'days',
      );

    rcmsQuotation = rcmsQuot.COTISATION_ANN;
    totTaxesFrais = Quotation.rcmsQuotation().TOTAL_TAXES_FRAIS_CONTR;

    if (fraction === 1) {
      fractQuotation = rcmsQuotation;
      totTaxesFrais = totTaxesFrais;
    } else if (fraction === 2) {
      fractQuotation = rcmsQuotation / 2;
      totTaxesFrais = totTaxesFrais / 2;
    } else if (fraction === 4) {
      fractQuotation = rcmsQuotation / 4;
      totTaxesFrais = totTaxesFrais / 4;
    } else {
      fractQuotation = rcmsQuotation / 12;
      totTaxesFrais = totTaxesFrais / 12;
    }

    cashQuotation = (rcmsQuotation / 365) * remainingDays;
    // resultat
    return {
      fractQuotation,
      cashQuotation,
      totTaxesFrais,
    };
  },
};
