/**
 * Sinister.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    reportingDate: { //dateDéclaration//
      type: 'ref',
      columnType: 'datetime'
    },
    provisionAmount: { //montantProvision//
      type: 'number',
    },
    regulationAmount: { //montantRéglement//
      type: 'number',
    },
    status: { //statut Sinistre//
      type: 'string',
      isIn: ['canceled', 'active','in treatment', 'closed'],
      defaultsTo: 'pending'
    },
    franchise: { //franchise//
      type: 'number',
    },
    statementScan: { // scanDeclaration//
      type: 'string'
    },
    challengeScan: { // scanContestation//
      type: 'string'
    },
    user: {
      model: 'user',
    },
    contract: {
      model: 'contract',
    },
  },

};

