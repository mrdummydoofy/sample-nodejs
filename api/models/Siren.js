/**
 * Sirn.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝

    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

    veterinary: {
      type: 'string',
      allowNull: true,
    },
    legalEntity: {
      type: 'string',
      allowNull: true,
    },
    siren: {
      type: 'string',
      allowNull: true,
    },
    siret: {
      type: 'string',
      allowNull: true,
    },
    address: {
      type: 'string',
      allowNull: true,
    },
    postalCode: {
      type: 'string',
      allowNull: true,
    },
    city: {
      type: 'string',
      allowNull: true,
    },
    companyName: {
      type: 'string',
      allowNull: true,
    },
    telephone: {
      type: 'string',
      allowNull: true,
    },
    email: {
      type: 'string',
      allowNull: true,
    },
    GIE: {
      type: 'string',
      allowNull: true,
    },
    department: {
      type: 'string',
      allowNull: true,
    },
    christopheCategory: {
      type: 'string',
      allowNull: true,
    },
    inseeCategory: {
      type: 'string',
      allowNull: true,
    },
  },
};
