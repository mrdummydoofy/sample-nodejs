/**
 * Contact.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝

    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

    customer: {
      //ClientVetoptim
      type: 'boolean',
    },
    firstname: {
      //nom
      type: 'string',
      minLength: 3,
    },
    lastname: {
      //prénom
      type: 'string',
      minLength: 3,
    },
    email: {
      //email//
      type: 'string',
    },
    mobile: {
      //telephone mobile//
      type: 'string',
    },
    subject: {
      //sujet
      type: 'string',
    },
    message: {
      //message
      type: 'string',
    },
    objet: {
      //objet
      type: 'string',
    },
    user: {
      model: 'user',
    },
  },
};
