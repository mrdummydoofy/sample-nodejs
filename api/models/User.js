/**
 * User.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

const bcrypt = require('bcrypt');
const crypto = require('crypto');

module.exports = {
  attributes: {
    title: {
      //titre
      type: 'string',
      minLength: 2,
    },
    name: {
      //nom
      type: 'string',
      minLength: 3,
    },
    lastname: {
      //prénom
      type: 'string',
      minLength: 3,
    },
    birthname: {
      //nom de naissance//
      type: 'string',
      minLength: 3,
    },
    login: {
      //login//
      type: 'string',
    },
    password: {
      //mot de passe//
      type: 'string',
    },
    email: {
      //email//
      type: 'string',
      unique: true,
      isEmail: true,
    },
    isAdmin: {
      //utilisateur administrateur ou non//
      type: 'boolean',
      defaultsTo: false,
    },
    isRegistred: {
      //utilisateur enregistré ou non enregistré//
      type: 'boolean',
      defaultsTo: false,
    },
    isCommercial: {
      //utilisateur commercial ou non //
      type: 'boolean',
      defaultsTo: false,
    },
    lastLogin: {
      type: 'ref',
      columnType: 'datetime',
    },
    phone: {
      //telephone fixe//
      type: 'string',
    },
    mobile: {
      //telephone mobile//
      type: 'string',
    },
    birthdate: {
      //date de naissance//
      type: 'string',
    },
    maritalStatus: {
      //statutContrat//
      type: 'string',
      isIn: ['married', 'single', 'divorced', 'widower', 'pacs'],
    },
    childNumber: {
      //nombre d'enfants//
      type: 'number',
    },
    registrationToken: {
      //token d'enregistrement//
      type: 'string',
      allowNull: true,
    },
    resetPasswordToken: {
      //token mise à jour MDP//
      type: 'string',
      allowNull: true,
    },
    customer: {
      collection: 'customer',
      via: 'user',
    },
    address: {
      collection: 'address',
      via: 'user',
    },
    adhesion: {
      collection: 'adhesiontmp',
      via: 'user',
    },
    payment: {
      collection: 'paymentmethod',
      via: 'user',
    },
    contract: {
      collection: 'contract',
      via: 'user',
    },
    firstPaymentCb: {
      collection: 'firstpaymentcb',
      via: 'user',
    },
    customRate: {
      collection: 'customrate',
      via: 'user',
    },
    contact: {
      collection: 'contact',
      via: 'user',
    },
    help: {
      collection: 'help',
      via: 'user',
    },
    sinister: {
      collection: 'sinister',
      via: 'user',
    },
    commercialContracts: {
      collection: 'contract',
      via: 'commercial',
    },
    leader: {
      collection: 'leader',
      via: 'user',
    },
  },

  beforeCreate(values, cb) {
    bcrypt.hash(values.password, 10, (err, hash) => {
      if (err) {
        return cb(err);
      }
      values.password = hash;
      return cb();
    });
    values.registrationToken = String(parseInt(crypto.randomBytes(3).toString('hex'), 16)).slice(
      -6,
    );
  },

  comparePassword(password, user) {
    return new Promise((resolve, reject) => {
      bcrypt.compare(password, user.password, (err, match) => {
        if (err) {
          reject(err);
        }
        if (match) {
          resolve(true);
        } else {
          reject(false);
        }
      });
    });
  },

  customToJSON: function() {
    return _.omit(this, ['password']);
  },
};
