/**
 * AdhesionTmp.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝

    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

    offerType: {
      type: 'string',
      isIn: ['standard', 'GIE', 'MRP + RC', 'RC', 'MRP', 'RCMS', 'MD'],
    },
    fraction: {
      //Fractionnement//
      type: 'number',
    },
    effectDate: {
      //DateEffet//
      type: 'string',
    },
    deadlineDate: {
      //date d'échéance selon fractionnement//
      type: 'string',
    },
    afterDeadlineDate: {
      //après date d'échéance//
      type: 'string',
    },
    chosenQuotation: {
      // cotisation choisie//
      type: 'number',
    },
    cashQuotation: {
      // cotisation Comptant//
      type: 'number',
    },
    continueLater: {
      type: 'boolean',
    },
    continueLaterShowPopup: {
      type: 'boolean',
    },
    firstPaymentType: {
      type: 'string',
      isIn: ['cb', 'sepa'],
      defaultsTo: 'sepa',
    },
    lastVisitedPage: {
      type: 'string',
    },
    rateFileName: {
      type: 'string',
    },
    adviceFileName: {
      type: 'string',
    },
    advisor: {
      type: 'boolean',
      defaultsTo: false,
    },
    rateReference: {
      type: 'string',
      defaultsTo: 'pas encore disponible',
    },
    customerReference: {
      type: 'string',
      defaultsTo: 'pas encore disponible',
    },
    leaderReference: {
      type: 'string',
      defaultsTo: 'pas encore disponible',
    },
    turnover: {
      type: 'number',
    },
    user: {
      model: 'user',
      required: true,
    },
    CRSinisterStatement: {
      collection: 'crsinisterstatement',
      via: 'adhesion',
    },
    PMRSinisterStatement: {
      collection: 'pmrsinisterstatement',
      via: 'adhesion',
    },
  },
};
