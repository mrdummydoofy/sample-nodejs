/**
 * SepaTransaction.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝

    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    type: {
      type: 'string',
      isIn: ['FRST', 'RCUR', 'FNAL'],
    },
    endToEndId: {
      type: 'string',
    },
    amount: {
      type: 'number',
    },
    sigDate: {
      type: 'string',
    },
    IBAN: {
      type: 'string',
    },
    BIC: {
      type: 'string',
    },
    exactTitle: {
      type: 'string',
    },
    postalCode: {
      type: 'number',
    },
    city: {
      type: 'string',
    },
    address: {
      type: 'string',
    },
    sepaExport: {
      model: 'sepaExport',
    },
    contract: {
      model: 'contract',
    },
    user: {
      model: 'user',
    },
  },
};
