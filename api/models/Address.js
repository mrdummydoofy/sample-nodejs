/**
 * Address.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

    number: {  //numéro//
      type: 'number',
      //required: true
    },
    streetType: { //typeVoie//
      type: 'string',
      //required: true,
      //minLength: 8
    },
    streetName: { //nomVoie//
      type: 'string',
      //required: true,
      //minLength: 8
    },
    complement: { //complément//
      type: 'string',
    },
    SiegeAddress: {
      type: 'string'
    },
    ActivityAddress: {
      type: 'string'
    },
    address: {
      type: 'string'
    },
    postalCode: { //codePostale//
      type: 'string',
      required: true,
      regex: /^[0-9]{5}|[0-9]{4}$/
    },
    city: { //Ville//
      type: 'string',
      required: true,
      minLength: 3
    },
    user: {
      model: 'user',
      unique: true
    },
  },

};

