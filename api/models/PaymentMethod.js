/**
 * PaymentMethod.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    pickingDate: { //date de prélévement//
      type: 'number',
      required: true,
      custom: function(value) {
        return _.isNumber(value) && value >= 1 && value <= 31;
      }
    },
    IBAN: { //IBAN//
      type: 'string',
      required: true
    },
    holder: { //titulaire//
      type: 'string',
      //required: true
    },
    exactTitle: { //intitulé exact//
      type: 'string',
      required: true
    },
    ribImg: {
      type: 'string'
    },
    BIC: { //BIC//
      type: 'string',
      required: true
    },
    offerType: {
      type: 'string'
    },
    user: {
      model: 'user',
      unique: true
    }
  },

};

