/**
 * Prospect.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    isAssociation: { //association ou non//
      type: 'boolean',
      defaultsTo: false
    },
    isBroker: { //courtier ou non//
      type: 'boolean',
      defaultsTo: false
    },
    isCompany: { //compagnie ou non//
      type: 'boolean',
      defaultsTo: false
    },
    isRecipient: { //prestataire ou non//
      type: 'boolean',
      defaultsTo: false
    },
    siret: { //siret //
      type: 'string',
      minLength: 2
    },
    orias: { //orias //
      type: 'string',
      minLength: 2
    },
    service: { //service //
      type: 'string',
      minLength: 2
    },
    siegeMobile: { //telSiège //
      type: 'string',
    },
    serviceMobile: { //telService //
      type: 'string',
    },
    siegeEmail: { //emailSiège //
      type: 'string',
      isEmail: true
    },
    serviceEmail: { //emailService //
      type: 'string',
      isEmail: true
    },


  },

};

