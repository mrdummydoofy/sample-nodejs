/**
 * Customer.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝

    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    reference: {
      //refClient//
      type: 'number',
    },
    type: {
      // type d'offre
      type: 'string',
    },
    siret: {
      //siret
      type: 'string',
      minLength: 2,
    },
    siren: {
      //siren
      type: 'string',
      minLength: 9,
    },
    orderNumber: {
      //N°Ordre
      type: 'number',
    },
    turnover: {
      //chiffre d'affaires CA
      type: 'number',
    },
    Status: {
      //statutClient//
      type: 'string',
      isIn: ['société', 'prof lib'],
      defaultsTo: 'société',
    },
    // title: { //titreClient
    //   type: 'string',
    //   isIn: ['dénomination', 'représentant'],
    //   defaultsTo: 'dénomination'
    // },
    localArea: {
      //surface des locaux
      type: 'number',
    },
    localContent: {
      //contenu des locaux
      type: 'number',
    },
    activity: {
      //activité
      type: 'string',
    },
    creationYears: {
      // age de l'entreprise
      type: 'number',
    },
    companyName: {
      type: 'string',
    },
    structure: {
      type: 'boolean',
    },
    endStatement: {
      type: 'boolean',
    },
    endStatementDate: {
      type: 'string',
    },
    centralRef: {
      type: 'boolean',
    },
    centralRefName: {
      type: 'string',
      allowNull: true,
    },
    individualCabinet: {
      type: 'boolean',
    },
    liberalCollaborator: {
      type: 'boolean',
    },
    legalForm: {
      type: 'string',
    },
    currentContract: {
      type: 'boolean',
    },
    currentContractDate: {
      type: 'string',
      allowNull: true,
    },
    currentContractCancel: {
      type: 'boolean',
    },
    startActivityDate: {
      type: 'string',
    },
    franchise: {
      type: 'number',
      defaultsTo: 500,
    },
    alarm: {
      // alarme
      type: 'boolean',
    },
    approvedAlarm: {
      // alarme agrée
      type: 'boolean',
    },
    insuranceCancel: {
      // Avez vous au cours des 36 derniers mois été résilié par une compagnie d'assurance?
      type: 'boolean',
    },
    cancelReasonsSinister: {
      // Motifs
      type: 'string',
      allowNull: true,
    },
    cancelReasonsPayment: {
      // Motifs
      type: 'string',
      allowNull: true,
    },
    quotationsUpdated: {
      // Etes vous à jour de vos cotisation?
      type: 'boolean',
    },
    animalsValue: {
      // Prenez vous en charge des animaux dont la valeur unitaire est supérieure à 30 000 euros?
      type: 'boolean',
    },
    sinistreConnaissance: {
      //Avez vous connaissance de sinistres pouvant engager votre responsabilité civile sur les 36 dernières années sur degats des eaux , incendies, vol.
      type: 'boolean',
    },
    sinistreConnaissance5Years: {
      //Avez vous connaissance de sinistres pouvant engager votre responsabilité civile sur les 5 dernières années.
      type: 'boolean',
    },
    bdmSup: {
      // Avez vous des machines dont la valeur unitaire est supérieure a 60 000 euros?
      type: 'boolean',
    },
    bdm: {
      // liste de bris de machine
      type: 'string',
      defaultsTo: 'limite20000',
    },
    tenant: {
      type: 'boolean',
      defaultsTo: false,
    },
    brokerFee: {
      type: 'number',
      defaultsTo: 0.2,
    },
    user: {
      model: 'user',
      unique: true,
    },
  },
};
