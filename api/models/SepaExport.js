/**
 * SepaExport.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝

    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    day: {
      type: 'string',
    },
    nbCashQuotation: {
      type: 'number',
    },
    nbQuotation: {
      type: 'number',
    },
    nbTransaction: {
      type: 'number',
    },
    totalCashQuotation: {
      type: 'number',
    },
    totalQuotation: {
      type: 'number',
    },
    totalTransaction: {
      type: 'number',
    },
    messageId: {
      type: 'string',
    },
    paymentInfoNew: {
      type: 'string',
      allowNull: true,
    },
    paymentInfoRecurring: {
      type: 'string',
      allowNull: true,
    },
    filename: {
      type: 'string',
    },
    sepaTransactions: {
      collection: 'sepaTransaction',
      via: 'sepaExport',
    },
  },
};
