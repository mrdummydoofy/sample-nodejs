/**
 * FirstPaymentCb.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

    amount : {
      type: 'number'
    },
    transactionId : {
      type: 'number'
    },
    transmissionDate: {
      type: 'string'
    },
    paymentDate : {
      type: 'string'
    },
    responseCode : {
      type: 'string'
    },
    paymentCertificate : {
      type: 'number'
    },
    authorisationId : {
      type: 'number'
    },
    currencyCode : {
      type: 'string'
    },
    cardNumber : {
      type: 'number'
    },
    cvvFlag: {
      type: 'number'
    },
    cvvResponseCode: {
      type: 'string'
    },
    bankResponseCode: {
      type: 'string'
    },
    cardValidity: {
      type: 'string'
    },
    ipAddress: {
      type: 'string'
    },
    offerType: {
      type: 'string'
    },
    user: {
      model: 'user'
    },
    contract: {
      model: 'contract',
      unique: true
    }
  },

};

