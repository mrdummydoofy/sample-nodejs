/**
 * Contract.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝

    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    contractPlace: {
      //lieuAdhésion//
      type: 'string',
      isIn: ['domicile', 'adistance'],
    },
    contractStatus: {
      //statutContrat//
      type: 'string',
      isIn: ['canceled', 'active', 'pending', 'suspended', 'ineffective'],
      defaultsTo: 'pending',
    },
    creationDate: {
      //Date création du contrat//
      type: 'ref',
      columnType: 'datetime',
    },
    effectDate: {
      //dateEffet//
      type: 'string',
    },
    deadlineDate: {
      //date d'échéance selon fractionnement//
      type: 'string',
    },
    subscriptionDeadlineDate: {
      //date d'échéance totale//
      type: 'string',
    },
    afterDeadlineDate: {
      //après date d'échéance//
      type: 'string',
    },
    endDate: {
      //dateFin//
      type: 'ref',
      columnType: 'datetime',
    },
    reference: {
      //refContrat//
      type: 'string',
    },
    sepaMndRef: {
      //refMandatSepa
      type: 'string',
      allowNull: true,
    },
    cancellationPeriod: {
      //délaiRétractation//
      type: 'number',
    },
    riskStatement: {
      //déclarationRisque//
      type: 'string',
    },
    isContract: {
      //devis ou contrat//
      type: 'boolean',
      defaultsTo: false,
    },
    isSigned: {
      //contrat signé ou pas signé//
      type: 'boolean',
      defaultsTo: false,
    },
    signatureId: {
      //id de signature electronique//
      type: 'string',
      allowNull: true,
    },
    signatureDate: {
      type: 'ref',
      columnType: 'datetime',
    },
    signatureStatus: {
      //Statut signature electronique//
      type: 'string',
      isIn: ['ready', 'completed', 'canceled'],
    },
    firstPaymentType: {
      type: 'string',
      isIn: ['cb', 'sepa'],
    },
    contractFileName: {
      type: 'string',
    },
    sepaFileName: {
      type: 'string',
    },
    consentFileName: {
      type: 'string',
    },
    user: {
      model: 'user',
    },
    warranty: {
      collection: 'warranty',
      via: 'contract',
    },
    quotation: {
      collection: 'quotation',
      via: 'contract',
    },
    sinister: {
      collection: 'sinister',
      via: 'contract',
    },
    firstPaymentCb: {
      collection: 'firstpaymentcb',
      via: 'contract',
    },
    commercial: {
      model: 'user',
    },
  },
};
