// policies/needAdvisor.js
module.exports = async function(req, res, proceed) {
  // excluded urls
  let excludedUrls = [
    '/journal',
    '/informations',
    '/informations-success',
    '/modifer-password',
    '/devis',
    '/sinistres',
    '/declare-sinistre',
    '/success-sinistre',
    '/contrats',
    '/supprimer-compte',
    '/supprimer-check',
    '/supprimer-succes',
    '/ajouter-contrat',
    '/contrats-crees',
    '/commercial-subscription-success',
  ];
  let foundUser = null;
  if (req.isAuthenticated()) {
    foundUser = await User.find({ id: req.user.id }).populate('adhesion');
    if (
      (foundUser[0].adhesion.find(e => e.offerType === Utils.getOfferTypeFr(req.session.type)) &&
        !foundUser[0].adhesion.find(e => e.offerType === Utils.getOfferTypeFr(req.session.type))
          .advisor) ||
      req.session.type === 'CR' ||
      excludedUrls.includes(req.originalUrl)
    ) {
      return proceed();
    } else {
      return res.redirect('subscription-cancel');
    }
  }
  return res.forbidden();
};
