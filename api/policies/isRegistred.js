// policies/isRegistred.js
module.exports = async function (req, res, proceed) {

  let foundUser = null;
  if (req.isAuthenticated()) {
    foundUser = await User.find({id: req.user.id});
    if (foundUser[0].isRegistred) {
      return proceed();
    } else {
      return res.redirect('mail-check');
    }
  }
  return res.forbidden();

};
