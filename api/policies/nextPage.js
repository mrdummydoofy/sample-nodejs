const restrictAccess = (adhesion, req, res, next) => {
  // Ordered Paths
  const crSubscribePath = ['cr', 'cr2-menu', 'cr3-infos', 'cr4-adhesion', 'cr3b-subscribe', 'cr3c-subscribe', 'cr3d-subscribe', 'cr3e-subscribe'];
  const crQuotePath = ['cr', 'cr2-menu', 'cr3-infos', 'cr4-adhesion', 'cr4a-quote'];
  const crSavePath = ['cr', 'cr2-menu', 'cr3-infos', 'cr5a-save'];

  const pmrSubscribePath = ['pmr', 'pmr2-menu', 'pmr3-infos', 'pmr4-adhesion', 'pmr3b-subscribe', 'pmr3c-subscribe', 'pmr3d-subscribe', 'pmr3e-subscribe'];
  const pmrQuotePath = ['pmr', 'pmr2-menu', 'pmr3-infos', 'pmr4-adhesion', 'pmr4a-quote'];
  const pmrSavePath = ['pmr', 'pmr2-menu', 'pmr3-infos', 'pmr5a-save'];

  let currentPath = null;

  if(adhesion.chosenMenu === 1) {
    if((req.url.split('/')[1]).toString().match(/cr/i) !== null) {
      currentPath = crSubscribePath;
    } else {
      currentPath = pmrSubscribePath;
    }
  } else if (adhesion.chosenMenu === 2) {
    if((req.url.split('/')[1]).toString().match(/cr/i) !== null) {
      currentPath = crQuotePath;
    } else {
      currentPath = pmrQuotePath;
    }
  } else if (adhesion.chosenMenu === 3) {
    if((req.url.split('/')[1]).toString().match(/cr/i) !== null) {
      currentPath = crSavePath;
    } else {
      currentPath = pmrSavePath;
    }
  } else {
    if((req.url.split('/')[1]).toString().match(/cr/i) !== null) {
      currentPath = crHelpPath;
    } else {
      currentPath = pmrHelpPath;
    }
  }

  const currentIndex = _.indexOf(currentPath, adhesion.lastVisitedPage);
  const prevIndex = _.indexOf(currentPath, adhesion.lastVisitedPage)-1;
  const nextIndex = _.indexOf(currentPath, adhesion.lastVisitedPage)+1;

  const prevPage = prevIndex >= 0 ? currentPath[prevIndex] : '';
  const nextPage = nextIndex > 0 && nextIndex <= currentPath.length-1 ? currentPath[nextIndex] : '';

  if (prevPage === '' && nextPage === '') {
    return next();
  } else if (prevPage === '') {
    if( req.url.split('/')[1] !== adhesion.lastVisitedPage && _.indexOf(currentPath, req.url.split('/')[1]) >= 0 && _.indexOf(currentPath, req.url.split('/')[1]) !== currentIndex+1 ) {
      return res.serverError('Vous ne pouvez pas sauter les étapes...');
    }
  } else if (nextPage === '') {
    if( req.url.split('/')[1] !== adhesion.lastVisitedPage && _.indexOf(currentPath, req.url.split('/')[1]) >= 0 && _.indexOf(currentPath, req.url.split('/')[1]) !== currentIndex-1 ) {
      return res.serverError('Vous ne pouvez pas sauter les étapes...');
    }
  } else {
    if( req.url.split('/')[1] !== adhesion.lastVisitedPage && _.indexOf(currentPath, req.url.split('/')[1]) >= 0 && _.indexOf(currentPath, req.url.split('/')[1]) !== currentIndex-1 && _.indexOf(currentPath, req.url.split('/')[1]) !== currentIndex+1 ) {
      return next();
    }
  }

  return next();
};


module.exports = async (req, res, next) => {

  if(req.isAuthenticated()) {
    const adhesion = await AdhesionTmp.findOne({user: req.user.id});
    if(adhesion) {
      restrictAccess(adhesion, req, res, next);
    } else {
      return next();
    }
  } else {
    return next();
  }

};
