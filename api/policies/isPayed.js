module.exports = async function (req, res, proceed) {

  if (req.isAuthenticated()) {
    if(req.user.payment.length > 0 || ( req.user.firstPaymentCb.length > 0 && req.user.firstPaymentCb[0].responseCode === '00' ) ) {
      return proceed();
    }
    return res.serverError('Vous devez payer votre première cotisation pour accèder à cette étape');
  }

  return proceed();

};
