// policies/isCommercial.js
module.exports = async function(req, res, proceed) {
  let foundUser = null;
  if (req.isAuthenticated()) {
    foundUser = await User.find({ id: req.user.id });
    if (foundUser[0].isCommercial) {
      return proceed();
    } else {
      return res.redirect('/erreur-utilisateur');
    }
  }
  return res.forbidden();
};
