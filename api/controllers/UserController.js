/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const passport = require('passport');
const moment = require('moment');

moment.suppressDeprecationWarnings = true;

module.exports = {
  // register action
  async postRegister(req, res) {
    if (
      !_.isUndefined(req.param('password')) ||
      !_.isUndefined(req.param('email')) ||
      !_.isUndefined(req.param('login')) ||
      !_.isUndefined(req.param('mobile'))
    ) {
      const user = await User.find({
        or: [{ login: req.param('login') }, { email: req.param('email') }],
      });
      if (user.length > 0) {
        return res.status(400).json({ error: 'Utilisateur possède déjà un compte' });
      } else {
        req.session.email = req.param('login');
        req.session.password = req.param('password');
        const createdUser = await User.create({
          login: req.param('login'),
          password: req.param('password'),
          email: req.param('email'),
          mobile: req.param('mobile'),
          title: req.session.title ? req.session.title : undefined,
          name: req.session.name ? req.session.name : undefined,
          lastname: req.session.lastname ? req.session.lastname : undefined,
          phone: req.session.phone ? req.session.phone : undefined,
        }).fetch();

        Mailer.sendActivationMail(createdUser, req.param('password'));
        return res.status(200).json({ message: 'Compte créé avec succès' });
      }
    } else {
      return res.status(400).json({ error: 'Email, mot de passe et téléphone requis' });
    }
  },

  // account activation
  async activateAccount(req, res) {
    const user = await User.update({ registrationToken: req.param('token') })
      .set({
        registrationToken: null,
        isRegistred: true,
      })
      .fetch();
    if (user.length > 0) {
      req.body.login = user[0].login;
      req.body.password = req.session.password;

      passport.authenticate('local', (err, user, info) => {
        if (err || !user) {
          return res.status(400).json({
            message: info.message,
            success: false,
            user,
          });
        }
        req.logIn(user, async err => {
          if (err) {
            res.send(err);
          }
          await Utils.formInfosSubmitted(req);
          if (req.session.type) {
            if (
              parseInt(req.session.chosenMenu) === 1 ||
              parseInt(req.session.chosenMenu) === 2 ||
              parseInt(req.session.chosenMenu) === 3
            ) {
              return res
                .status(200)
                .json({ redirect: `adhesion-summary?offerType=${req.session.type}` });
            }
            return res.status(200).json({ redirect: 'journal' });
          } else {
            return res.status(200).json({ redirect: '/' });
          }
        });
      })(req, res);
    } else {
      return res.status(400).json({ error: 'Code Incorrect' });
    }
  },

  // reset password request
  async getResetPassword(req, res) {
    let currentUser = null;
    let resetPasswordToken = null;
    if (req.param('email') && req.param('email') !== '') {
      resetPasswordToken = crypto.randomBytes(20).toString('hex');
      currentUser = await User.update({ email: req.param('email') })
        .set({ resetPasswordToken })
        .fetch();
      if (currentUser.length > 0) {
        Mailer.sendResetPasswordMail(currentUser[0], req.param('admin'));
        return res.status(200).json({ message: 'Un email de vérification est envoyé' });
      } else {
        return res.status(400).json({ error: 'Veuillez saisir une adresse e-mail valide' });
      }
    } else if (req.param('token') && req.param('token') !== '') {
      return res.view('pages/reset-password', {
        token: req.param('token'),
        admin: req.param('admin'),
        error: undefined,
      });
    } else {
      return res.status(400).json({ error: 'Veuillez entrer un email' });
    }
  },

  // reset password logic
  async postResetPassword(req, res) {
    let currentUser = null;
    let passwordHash = null;

    if (req.param('password') && req.param('password') !== '' && req.param('token')) {
      passwordHash = bcrypt.hashSync(req.param('password'), 10);
      currentUser = await User.update({ resetPasswordToken: req.param('token').split('?')[0] })
        .set({ password: passwordHash, resetPasswordToken: null })
        .fetch();
      if (currentUser.length > 0) {
        Mailer.sendResetSuccessMail(currentUser[0]);
        if (req.param('token').split('?')[1]) {
          return res.view('admin/login', {
            layout: 'layouts/admin',
            locals: {
              success: 'Un email de vérification est envoyé. Veuillez vous connecter.',
              error: undefined,
            },
          });
        } else {
          return res.view('pages/register', {
            success: 'Un email de vérification est envoyé. Veuillez vous connecter.',
            error: undefined,
          });
        }
      } else {
        return res.view('pages/register', {
          success: undefined,
          error: 'impossible de réinitialiser le mot de passe',
        });
      }
    } else {
      return res.view('pages/register', {
        success: undefined,
        error: 'impossible de réinitialiser le mot de passe',
      });
    }
  },

  // resend Activation email
  async resendActivationEmail(req, res) {
    const currentUser = await User.findOne({ email: req.session.email });
    const generatedActivationCode = String(
      parseInt(crypto.randomBytes(3).toString('hex'), 16),
    ).slice(-6);
    await User.update({ id: currentUser.id }, { registrationToken: generatedActivationCode });
    if (currentUser) {
      Mailer.sendActivationMail(
        {
          login: req.session.email,
          email: req.session.email,
          registrationToken: generatedActivationCode,
        },
        req.session.password,
      );
      return res.status(200).json({ message: 'Un email de vérification est envoyé' });
    }
  },

  // resend Process Activation email
  async resendProcessActivationEmail(req, res) {
    const currentUser = await User.findOne({ email: req.session.email });
    const generatedActivationCode = String(
      parseInt(crypto.randomBytes(3).toString('hex'), 16),
    ).slice(-6);
    await User.update({ id: currentUser.id }, { registrationToken: generatedActivationCode });
    if (currentUser) {
      Mailer.sendProcessActivationMail(
        {
          login: req.session.email,
          email: req.session.email,
          registrationToken: generatedActivationCode,
        },
        req.session.password,
      );
      return res.status(200).json({ message: 'Un email de vérification est envoyé' });
    }
  },

  // renew password logic
  async changePassword(req, res) {
    const currentUser = await User.findOne({ id: req.user.id });
    if (_.isUndefined(req.param('password')) || _.isUndefined(req.param('newPassword'))) {
      return res.status(400).json({ error: 'Veuillez compléter les informations' });
    }
    if (!bcrypt.compareSync(req.param('password'), currentUser.password)) {
      return res.status(400).json({ error: 'Ancien Mot de passe invalide' });
    }
    if (req.param('password') === req.param('newPassword')) {
      return res
        .status(400)
        .json({ error: "L'ancien et le nouveau mot de passe ne peuvent pas être identique" });
    }

    await User.update(
      { id: req.user.id },
      {
        password: bcrypt.hashSync(req.param('newPassword'), 10),
      },
    );

    return res.status(200).json({ message: 'Mot de passe changé avec succès' });
  },

  // check Signed Contract (delete account)
  async checkSignedContract(req, res) {
    const currentUser = await User.findOne({ id: req.user.id }).populate('contract');
    //contract criteria for delete
    if (currentUser.contract.length > 0 && currentUser.contract[0].isSigned === true) {
      res.status(400).json({
        error: 'Vous ne pouvez pas supprimer votre compte car vous avez un contrat signé',
      });
    } else {
      res.status(200).json({ message: 'Vous pouvez supprimer votre compte' });
    }
  },

  // delete Account
  async deleteAccount(req, res) {
    await User.findOne({ id: req.user.id })
      .populate('contract')
      .populate('customer')
      .populate('payment');
    await PaymentMethod.update({ user: req.user.id }).set({
      IBAN: 'deleted',
      BIC: 'deleted',
      holder: 'deleted',
      exactTitle: 'deleted',
    });
    await Customer.update({ user: req.user.id }).set({
      turnover: 0,
      type: 'deleted',
      activity: 'deleted',
      siret: 'deleted',
      legalForm: 'deleted',
      startActivityDate: 'deleted',
      companyName: 'deleted',
      // orderNumber: 0,
      localArea: 0,
      localContent: 0,
      structure: 0,
      centralRef: 0,
      individualCabinet: 0,
      liberalCollaborator: 0,
      franchise: 0,
      centralRefName: 'deleted',
      endStatement: 0,
      endStatementDate: 'deleted',
      currentContract: 0,
      currentContractDate: 'deleted',
      currentContractCancel: 0,
    });
    await User.update({ id: req.user.id }).set({
      title: 'deleted',
      name: 'deleted',
      lastname: 'deleted',
      mobile: 'deleted',
      phone: 'deleted',
      login: 'deleted',
      email: 'deleted@vetoptim.org',
      password: 'deleted',
      isRegistred: false,
      isAdmin: false,
      registrationToken: null,
      resetPasswordToken: null,
    });

    Mailer.sendDeletedAccountMail(req.user);
    res.status(200).json({ message: 'Compte supprimé avec succès' });
    req.logout();
  },

  async subscribeToNewsletter(req, res) {
    if (_.isUndefined(req.param('email'))) {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
    const newsletter = await Newsletter.find({ email: req.param('email') }).limit(1);
    if (newsletter[0] && newsletter[0].status) {
      return res.status(200).json({ message: 'Vous êtes déjà inscrit à newsletter' });
    } else if (newsletter[0] && !newsletter[0].status) {
      await Newsletter.update(
        { email: req.param('email') },
        {
          status: true,
          date: moment().format('YYYY-MM-DD hh:mm:ss'),
        },
      );
      Mailer.sendNewsletterMail({ email: req.param('email') });
      return res.status(200).json({ message: 'Mise à jour des informations avec succès' });
    } else {
      await Newsletter.create({
        email: req.param('email'),
        status: true,
        date: moment().format('YYYY-MM-DD hh:mm:ss'),
      });
      Mailer.sendNewsletterMail({ email: req.param('email') });
      return res.status(200).json({ message: 'Mise à jour des informations avec succès' });
    }
  },

  async unsubscribeFromNewsletter(req, res) {
    if (_.isUndefined(req.param('email'))) {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
    await Newsletter.update(
      { email: req.param('email') },
      {
        status: false,
        date: moment().format('YYYY-MM-DD hh:mm:ss'),
      },
    );
    return res.status(200).json({ message: 'Désincription effectué avec succès' });
  },
};
