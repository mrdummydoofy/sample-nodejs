/**
 * AdminController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const passport = require('passport');

module.exports = {
  //Admin login
  loginAdmin(req, res) {
    passport.authenticate('local', (err, user, info) => {
      if (_.isUndefined(req.param('password')) || _.isUndefined(req.param('login'))) {
        info.message = 'Email ou mot de passe manquant';
      }

      if (!user.isAdmin) {
        info.message = "Vous n'êtes pas un administrateur";
      }

      if (err || !user || !user.isAdmin) {
        return res.status(400).json({
          message: info.message,
          success: false,
          user,
        });
      }
      req.logIn(user, err => {
        if (err) {
          res.send(err);
        }
        return res.status(200).json({
          message: info.message,
          success: true,
          user,
        });
      });
    })(req, res);
  },

  //logout action
  logoutAdmin(req, res) {
    req.logout();
    return res.redirect('/admin/login');
  },

  //Add User
  async saveUser(req, res) {
    if (
      !_.isUndefined(req.param('title')) ||
      !_.isUndefined(req.param('name')) ||
      !_.isUndefined(req.param('lastname')) ||
      !_.isUndefined(req.param('mobile')) ||
      !_.isUndefined(req.param('password')) ||
      !_.isUndefined(req.param('email'))
    ) {
      // user creation
      // const createdUser = await User.create({
      await User.create({
        login: req.param('email'),
        password: req.param('password'),
        email: req.param('email'),
        mobile: req.param('mobile'),
        title: req.param('title'),
        name: req.param('name'),
        lastname: req.param('lastname'),
      }).fetch();
      // Mailer.sendProcessActivationMail(createdUser);
      return res.status(200).json({ message: 'utilisateur créé avec succès' });
    } else {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
  },

  // update User
  async updateUser(req, res) {
    if (
      !_.isUndefined(req.param('user')) ||
      !_.isUndefined(req.param('mobile')) ||
      !_.isUndefined(req.param('title')) ||
      !_.isUndefined(req.param('name')) ||
      !_.isUndefined(req.param('lastname'))
    ) {
      await User.update({ id: req.param('user') })
        .set({
          login: req.param('email'),
          mobile: req.param('mobile'),
          title: req.param('title'),
          name: req.param('name'),
          lastname: req.param('lastname'),
        })
        .fetch();

      return res.status(200).json({ message: 'Mise à jour des informations avec succès' });
    } else {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
  },

  // delete User
  async deleteUser(req, res) {
    const user = await User.findOne({ id: req.user.id });
    if (user) {
      await User.destroy(user);
    }
    return res.status(200).json({ message: 'Utilisateur supprimé avec succès' });
  },

  // list Users
  async listUsers(req, res) {
    const users = await User.find();
    if (users.length > 0) {
      return res.status(200).json({ users });
    } else {
      return res.status(400).json({ error: 'aucun utilisateur trouvé' });
    }
  },
};
