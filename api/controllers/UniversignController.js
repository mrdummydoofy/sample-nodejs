/**
 * UniversignController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  // request universign service
  async getUniversignTransaction(req, res) {
    if (_.isUndefined(req.param('type'))) {
      return res.status(400).json({ error: "Veuillez indiquer le type d'offre" });
    }

    req.session.offerType = req.param('type');

    let user = await User.findOne({ id: req.user.id })
      .populate('contract')
      .populate('customer')
      .populate('adhesion')
      .populate('payment')
      .populate('firstPaymentCb');

    if (user && user.contract.length > 0) {
      user.contract = await Contract.find({ user: req.user.id })
        .populate('warranty')
        .populate('quotation');
      user.contract = user.contract.filter(
        contract =>
          contract.quotation.length > 0 && contract.quotation[0].offerType === req.param('type'),
      );
    }

    // if contract exist
    if (user.contract.length > 0) {
      // if contract status is pending
      if (user.contract[0].contractStatus === 'pending') {
        const contract = await Contract.destroy({ id: user.contract[0].id }).fetch();
        await Warranty.destroy({ contract: contract.id });
        await Quotation.destroy({ contract: contract.id });
        const contractValues = await Utils.createContract(user, req.param('type'));
        await Utils.signDocumentProcess(contractValues, user, res, undefined, req);
      } else {
        // if contract status is not pending and signatureId is empty
        if (user.contract[0].signatureId === null) {
          const contract = await Contract.destroy({ id: user.contract[0].id }).fetch();
          await Warranty.destroy({ contract: contract.id });
          await Quotation.destroy({ contract: contract.id });
          const contractValues = await Utils.createContract(user, req.param('type'));
          await Utils.signDocumentProcess(contractValues, user, res, undefined, req);
        } else {
          // if signatureId is defined and signatureStatus is completed
          if (user.contract[0].signatureStatus === 'completed') {
            res.status(400).json({ error: "l'utilisateur a déjà un contrat" });
          }
          if (user.contract[0].signatureStatus === 'canceled') {
            const contract = await Contract.destroy({ id: user.contract[0].id }).fetch();
            await Warranty.destroy({ contract: contract.id });
            await Quotation.destroy({ contract: contract.id });
            const contractValues = await Utils.createContract(user, req.param('type'));
            await Utils.signDocumentProcess(contractValues, user, res, undefined, req);
          }
          if (user.contract[0].signatureStatus === 'ready') {
            const contract = await Contract.destroy({ id: user.contract[0].id }).fetch();
            await Warranty.destroy({ contract: contract.id });
            await Quotation.destroy({ contract: contract.id });
            const contractValues = await Utils.createContract(user, req.param('type'));
            await Utils.signDocumentProcess(contractValues, user, res, undefined, req);
          }
        }
      }
    } else {
      // if contract is not existant
      const contractValues = await Utils.createContract(user, req.param('type'));
      await Utils.signDocumentProcess(contractValues, user, res, undefined, req);
    }
  },

  // when transaction succeed
  async transactionSuccess(req, res) {
    if (req.isAuthenticated()) {
      // fetch contract by offer type
      let user = await User.findOne({ id: req.user.id })
        .populate('contract')
        .populate('customer')
        .populate('adhesion')
        .populate('payment')
        .populate('firstPaymentCb');
      let currentCustomer = null;
      let currentCustomerRef = null;
      let currentLeader = null;
      let currentLeaderRef = null;
      let generatedFilesPaths = null;

      if (user && user.contract.length > 0) {
        user.contract = await Contract.find({ user: req.user.id })
          .populate('warranty')
          .populate('quotation');
        user.contract = user.contract.filter(
          contract =>
            contract.quotation.length > 0 &&
            contract.quotation[0].offerType === req.session.offerType,
        );
      }

      if (user.isRegistred === true) {
        if (user.contract[0].signatureStatus === 'ready') {
          await Universign.statusDocument(
            user,
            user.contract[0].signatureId,
            req.session.currentContractId,
          );
          // generate subscriber and contract reference

          let mndId = null;
          const lastContract = await Contract.find()
            .limit(1)
            .select(['sepaMndRef', 'reference'])
            .sort('sepaMndRef DESC');
          const lastContractCurrentUser = await Contract.find({ user: req.user.id })
            .limit(2)
            .select(['sepaMndRef', 'reference', 'user', 'id'])
            .sort('id DESC');

          currentCustomer = await Customer.findOne({ user: req.user.id });
          if (currentCustomer) {
            currentCustomerRef = currentCustomer.reference;
          }
          currentLeader = await Leader.findOne({ user: req.user.id });
          if (currentLeader) {
            currentLeaderRef = currentLeader.reference;
          }

          if (
            lastContractCurrentUser.length > 1 &&
            lastContractCurrentUser[1].sepaMndRef !== null
          ) {
            // generate mndId reference id
            if (
              (parseInt(lastContractCurrentUser[1].sepaMndRef.split('M')[1]) + 1).toString()
                .length > 2
            ) {
              mndId = (
                parseInt(lastContractCurrentUser[1].sepaMndRef.split('M')[1]) + 1
              ).toString();
            } else if (
              (parseInt(lastContractCurrentUser[1].sepaMndRef.split('M')[1]) + 1).toString()
                .length === 2
            ) {
              mndId =
                '0' +
                (parseInt(lastContractCurrentUser[1].sepaMndRef.split('M')[1]) + 1).toString();
            } else {
              mndId =
                '00' +
                (parseInt(lastContractCurrentUser[1].sepaMndRef.split('M')[1]) + 1).toString();
            }
          }

          // generate sepa and consent pdfs

          await Utils.updateContractRefsInPdf(user.contract[0], req);

          currentCustomer = await Customer.findOne({ user: req.user.id });
          if (currentCustomer) {
            currentCustomerRef = currentCustomer.reference;
          }
          currentLeader = await Leader.findOne({ user: req.user.id });
          if (currentLeader) {
            currentLeaderRef = currentLeader.reference;
          }

          if (
            lastContract.length > 0 &&
            lastContract[0].sepaMndRef !== null &&
            lastContractCurrentUser[1] &&
            lastContractCurrentUser[1].sepaMndRef !== null
          ) {
            updatedContract = await Contract.update(
              { id: lastContractCurrentUser[0].id },
              {
                sepaMndRef: `VETOPSEPA${
                  req.session.offerType === 'MD' || req.session.offerType === 'RCMS'
                    ? currentLeaderRef + mndId
                    : currentCustomerRef + mndId
                }M${mndId}`,
              },
            ).fetch();
          } else {
            updatedContract = await Contract.update(
              { id: lastContractCurrentUser[0].id },
              {
                sepaMndRef: `VETOPSEPA${
                  req.session.offerType === 'MD' || req.session.offerType === 'RCMS'
                    ? currentLeaderRef * 1000 + 1
                    : currentCustomerRef * 1000 + 1
                }M001`,
              },
            ).fetch();
          }

          req.user.adhesion = await AdhesionTmp.find({ user: req.user.id });
          req.user.customer = await Customer.find({ user: req.user.id });

          generatedFilesPaths = await Utils.generateContractAddons(
            req.user,
            updatedContract[0],
            req.session.offerType,
            req,
          );

          updatedContract = await Contract.find({ id: lastContractCurrentUser[0].id });

          Mailer.sendSubscriptionSuccessMail(
            req.user,
            currentCustomerRef,
            updatedContract[0],
            generatedFilesPaths,
            req.session.offerType,
            currentLeaderRef,
          );

          Mailer.sendInsurerMail(
            req.user,
            await Contract.findOne({ id: updatedContract[0].id })
              .populate('quotation')
              .populate('warranty'),
          );

          res.redirect(`/sign-summary?offerType=${Utils.getOfferTypeEng(req.session.offerType)}`);
        } else {
          res.status(400).json({ error: "Cet utilisateur n'a aucune signature en attente" });
        }
      } else {
        res.status(400).json({ error: 'Utilisateur inactif' });
      }
    } else {
      res.status(400).json({ error: 'Utilisateur non connecté' });
    }
  },

  // when transaction succeed
  async commercialTransactionSuccess(req, res) {
    if (req.isAuthenticated()) {
      // fetch contract by offer type
      let user = await User.findOne({ id: req.session.createdUser.id })
        .populate('contract')
        .populate('customer')
        .populate('adhesion')
        .populate('payment')
        .populate('firstPaymentCb')
        .populate('leader')
        .populate('address');
      let currentCustomer = null;
      let currentCustomerRef = null;
      let currentLeader = null;
      let currentLeaderRef = null;

      if (user && user.contract.length > 0) {
        user.contract = await Contract.find({ user: user.id })
          .populate('warranty')
          .populate('quotation');
        user.contract = user.contract.filter(
          contract =>
            contract.quotation.length > 0 &&
            contract.quotation[0].offerType === req.session.offerType,
        );
      }

      if (user && user.isRegistred === true) {
        if (user.contract[0].signatureStatus === 'ready') {
          await Universign.statusDocument(
            user,
            user.contract[0].signatureId,
            req.session.currentContractId,
          );

          let mndId = null;
          const lastContract = await Contract.find()
            .limit(1)
            .select(['sepaMndRef', 'reference'])
            .sort('sepaMndRef DESC');
          const lastContractCurrentUser = await Contract.find({ user: user.id })
            .limit(2)
            .select(['sepaMndRef', 'reference', 'user', 'id'])
            .sort('id DESC');

          currentCustomer = await Customer.findOne({ user: user.id });
          if (currentCustomer) {
            currentCustomerRef = currentCustomer.reference;
          }
          currentLeader = await Leader.findOne({ user: user.id });
          if (currentLeader) {
            currentLeaderRef = currentLeader.reference;
          }

          if (
            lastContractCurrentUser.length > 1 &&
            lastContractCurrentUser[1].sepaMndRef !== null
          ) {
            // generate mndId reference id
            if (
              (parseInt(lastContractCurrentUser[1].sepaMndRef.split('M')[1]) + 1).toString()
                .length > 2
            ) {
              mndId = (
                parseInt(lastContractCurrentUser[1].sepaMndRef.split('M')[1]) + 1
              ).toString();
            } else if (
              (parseInt(lastContractCurrentUser[1].sepaMndRef.split('M')[1]) + 1).toString()
                .length === 2
            ) {
              mndId =
                '0' +
                (parseInt(lastContractCurrentUser[1].sepaMndRef.split('M')[1]) + 1).toString();
            } else {
              mndId =
                '00' +
                (parseInt(lastContractCurrentUser[1].sepaMndRef.split('M')[1]) + 1).toString();
            }
          }

          await Utils.generateContractRefs(req, true);

          currentCustomer = await Customer.findOne({ user: user.id });
          if (currentCustomer) {
            currentCustomerRef = currentCustomer.reference;
          }
          currentLeader = await Leader.findOne({ user: user.id });
          if (currentLeader) {
            currentLeaderRef = currentLeader.reference;
          }

          if (
            lastContract.length > 0 &&
            lastContract[0].sepaMndRef !== null &&
            lastContractCurrentUser[1] &&
            lastContractCurrentUser[1].sepaMndRef !== null
          ) {
            updatedContract = await Contract.update(
              { id: lastContractCurrentUser[0].id },
              {
                sepaMndRef: `VETOPSEPA${
                  req.session.offerType === 'MD' || req.session.offerType === 'RCMS'
                    ? currentLeaderRef + mndId
                    : currentCustomerRef + mndId
                }M${mndId}`,
              },
            ).fetch();
          } else {
            updatedContract = await Contract.update(
              { id: lastContractCurrentUser[0].id },
              {
                sepaMndRef: `VETOPSEPA${
                  req.session.offerType === 'MD' || req.session.offerType === 'RCMS'
                    ? currentLeaderRef * 1000 + 1
                    : currentCustomerRef * 1000 + 1
                }M001`,
              },
            ).fetch();
          }

          updatedContract = await Contract.find({ id: lastContractCurrentUser[0].id });

          user = await User.findOne({ id: req.session.createdUser.id })
            .populate('contract')
            .populate('customer')
            .populate('adhesion')
            .populate('payment')
            .populate('firstPaymentCb')
            .populate('leader')
            .populate('address');

          user.leader = await Leader.find({ user: user.id }).populate('children');

          if (user && user.contract.length > 0) {
            user.contract = await Contract.find({ user: user.id })
              .populate('warranty')
              .populate('quotation');
            user.contract = user.contract.filter(
              contract =>
                contract.quotation.length > 0 &&
                contract.quotation[0].offerType === req.session.offerType,
            );
          }

          const generatedFilesPaths = await Utils.generateContractAddons(
            req.user,
            updatedContract[0],
            req.session.offerType,
            req,
            user,
            true,
          );

          Mailer.sendCommercialSubscriptionSuccessMail(
            user,
            currentCustomerRef,
            updatedContract[0],
            req,
            currentLeaderRef,
            req.session.offerType,
            generatedFilesPaths,
          );
          req.session.contractReference = updatedContract[0].reference;
          req.session.currentCustomerRef = currentCustomerRef;
          req.session.currentLeaderRef = currentLeaderRef;
          req.session.contractSepaMndRef = updatedContract[0].sepaMndRef;

          res.redirect('/commercial-subscription-success');
        } else {
          res.status(400).json({ error: "Cet utilisateur n'a aucune signature en attente" });
        }
      } else {
        res.status(400).json({ error: 'Utilisateur inactif' });
      }
    } else {
      res.status(400).json({ error: 'Utilisateur non connecté' });
    }
  },

  // when transaction fails
  async transactionFailure(req, res) {
    const contract = await Contract.destroy({ id: req.session.currentContractId }).fetch();
    await Warranty.destroy({ contract: contract.id }).fetch();
    await Quotation.destroy({ contract: contract.id });
    res.redirect(`/sign-cancel?offerType=${Utils.getOfferTypeEng(req.session.offerType)}`);
  },

  // when commercial transaction fails
  async commercialTransactionFailure(req, res) {
    const contract = await Contract.destroy({ id: req.session.currentContractId }).fetch();
    await Warranty.destroy({ contract: contract.id }).fetch();
    await Quotation.destroy({ contract: contract.id });
    req.session.createdUser = undefined;
    res.redirect('/sign-cancel');
  },

  // when transaction canceled
  async transactionCancel(req, res) {
    await Contract.update(
      { id: req.session.currentContractId },
      {
        signatureStatus: 'canceled',
        signatureId: null,
        contractStatus: 'canceled',
      },
    );

    res.redirect(`/sign-cancel?offerType=${Utils.getOfferTypeEng(req.session.offerType)}`);
  },

  // when commercial transaction canceled
  async commercialTransactionCancel(req, res) {
    await Contract.update(
      { id: req.session.currentContractId },
      {
        signatureStatus: 'canceled',
        signatureId: null,
        contractStatus: 'canceled',
      },
    );
    req.session.createdUser = undefined;
    res.redirect('/sign-cancel');
  },
};
