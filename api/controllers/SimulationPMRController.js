/**
 * SimulationPMRController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  // Professional Multi Risk first form post
  async postPmr(req, res) {
    // test params validity
    if (
      !_.isUndefined(req.param('chiffreAffaires')) ||
      !_.isUndefined(req.param('activite')) ||
      !_.isUndefined(req.param('type'))
    ) {
      // do not permit turnover > 1500000 euros
      if (parseInt(req.param('chiffreAffaires')) <= 1500000) {
        // test existance of customer
        Customer.findOrCreate(
          { user: req.user.id },
          {
            turnover: parseInt(req.param('chiffreAffaires')),
            activity: req.param('activite'),
            type: req.param('type'),
            user: req.user.id,
          },
        ).exec(async (err, customer, wasCreated) => {
          if (err) {
            return res.serverError(err);
          }
          if (wasCreated) {
            return res.status(200).json({ message: 'Informations crées avec succès' });
          } else {
            await Customer.update(
              { user: req.user.id },
              {
                turnover: parseInt(req.param('chiffreAffaires')),
                activity: req.param('activite'),
                type: req.param('type'),
              },
            );
            return res.status(200).json({ message: 'Mise à jour des informations avec succès' });
          }
        });
      } else {
        return res.status(400).json({
          error:
            'Informations ne correspondent pas aux critères minimales pour faire une simualation',
        });
      }
    } else {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
  },
  // Quotation of Professional Multi Risk(PMR) in the simulation
  pmrQuotationPromo(req, res) {
    // test params validity
    if (!_.isUndefined(req.param('chiffreAffaires')) || !_.isUndefined(req.param('activite'))) {
      // if the activity is urban
      if (req.param('activite') === 'urbain') {
        res.status(200).json({
          mrpUrbanQuotation: Quotation.mrpUrbanQuotation(parseFloat(req.param('chiffreAffaires')))
            .mrpUrbanQuotationPromo,
        });
      } // if the activity is rural or mixed
      else {
        res.status(200).json({
          mrpRuralMixteQuotation: Quotation.mrpRuralMixteQuotation(
            parseFloat(req.param('chiffreAffaires')),
          ).mrpRuralMixteQuotationPromo,
        });
      }
      // params are invalid
    } else {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
  },

  // Quotation of Professional Multi Risk(PMR)
  async pmrQuotation(req, res) {
    // test params validity
    if (!_.isUndefined(req.param('chiffreAffaires')) || !_.isUndefined(req.param('activite'))) {
      // with franchise 300
      if (req.param('activite') === 'urbain') {
        res.status(200).json({
          mrpUrbanQuotation: Quotation.mrpUrbanQuotation(parseFloat(req.param('chiffreAffaires')))
            .mrpUrbanQuotation,
        });
      } else {
        res.status(200).json({
          mrpRuralMixteQuotation: Quotation.mrpRuralMixteQuotation(
            parseFloat(req.param('chiffreAffaires')),
          ).mrpRuralMixteQuotation,
        });
      }
      // params are invalid
    } else {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
  },

  // Warraties of Professional Multi Risk(PMR)
  pmrWarranty(req, res) {
    // test params validity
    if (!_.isUndefined(req.param('chiffreAffaires')) || !_.isUndefined(req.param('bdm'))) {
      res.status(200).json({
        perteRecettes: Warranty.mrpWarranties(
          parseInt(req.param('chiffreAffaires')),
          req.param('bdm'),
        ).perteRecettes,
        perteValeurVenale: Warranty.mrpWarranties(
          parseInt(req.param('chiffreAffaires')),
          req.param('bdm'),
        ).perteValeurVenale,
        incendieReconstitution: Warranty.mrpWarranties(
          parseInt(req.param('chiffreAffaires')),
          req.param('bdm'),
        ).incendieReconstitution,
        tempeteReconstitution: Warranty.mrpWarranties(
          parseInt(req.param('chiffreAffaires')),
          req.param('bdm'),
        ).tempeteReconstitution,
        vandalismeReconstitution: Warranty.mrpWarranties(
          parseInt(req.param('chiffreAffaires')),
          req.param('bdm'),
        ).vandalismeReconstitution,
        brisMachine: Warranty.mrpWarranties(
          parseInt(req.param('chiffreAffaires')),
          req.param('bdm'),
        ).brisMachine,
        fraisSupplementaire: Warranty.mrpWarranties(
          parseInt(req.param('chiffreAffaires')),
          req.param('bdm'),
        ).fraisSupplementaire,
      });
      // params are invalid
    } else {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
  },

  // Tax of urban Professional Multi Risk(PMR)
  pmrUrbanTax(req, res) {
    // test params validity
    if (
      !_.isUndefined(req.param('chiffreAffaires')) ||
      !_.isUndefined(req.param('fraction')) ||
      !_.isUndefined(req.param('franchise')) ||
      !_.isUndefined(req.param('effectDate')) ||
      !_.isUndefined(req.param('alarm')) ||
      !_.isUndefined(req.param('bdm'))
    ) {
      res.status(200).json({
        fractQuotation: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).fractQuotation,
        cashQuotation: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).cashQuotation,
        contrAttentat: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).CONTRB_ATT,
        contrAttentatSem: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).CONTRB_ATT_SEM,
        contrAttentatTrim: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).CONTRB_ATT_TRIM,
        contrAttentatMens: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).CONTRB_ATT_MENS,
        totalTaxFraisContr: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_TAX_FRAIS_CONTR,
        totalTaxFraisContrSem: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_TAX_FRAIS_CONTR_SEM,
        totalTaxFraisContrTrim: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_TAX_FRAIS_CONTR_TRIM,
        totalTaxFraisContrMens: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_TAX_FRAIS_CONTR_MENS,
        totalTax: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOT_TAXES,
        totalTaxSem: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_TAX_SEM,
        totalTaxTrim: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_TAX_TRIM,
        totalTaxMens: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_TAX_MENS,
        fraisCourtier: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).FRAIS_COURTIER,
        fraisCourtierSem: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).FRAIS_COURTIER_SEM,
        fraisCourtierTrim: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).FRAIS_COURTIER_TRIM,
        fraisCourtierMens: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).FRAIS_COURTIER_MENS,
        fraisAssistance: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).COT_ASSISTANCE_TTC,
        TaxINCENDIE: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_INCENDIE,
        TaxTEMPETE: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_TEMPETE,
        TaxVANDALISME: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_VANDALISME,
        TaxDELECT: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_DELECT,
        TaxDDE: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_DDE,
        TaxBDG: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_BDG,
        TaxVOL: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_VOL,
        TaxPE: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_PE,
        TotalHT: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
        ).TOT_HT,
        TotalHTMens: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_COT_HT_MENS,
        TotalHTSem: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_COT_HT_SEM,
        TotalHTTrim: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_COT_HT_TRIM,
        TaxATT: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_ATT,
        RedVolHT: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_VOL_HT,
        RedATT: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_ATT,
        RedCATNAT: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_CATNAT,
        RedTAX: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_TAX,
        RedVOLTTC: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_VOL_TTC,
        RedVOLTTCSEM: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_VOL_TTC_SEM,
        RedVOLTTCTRIM: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_VOL_TTC_TRIM,
        RedVOLTTCMENS: Quotation.MrpUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_VOL_TTC_MENS,
      });
      // params are invalid
    } else {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
  },

  // Tax of rural or mixed Professional Multi Risk(PMR)
  pmrRuralMixedTax(req, res) {
    // test params validity
    if (
      !_.isUndefined(req.param('chiffreAffaires')) ||
      !_.isUndefined(req.param('fraction')) ||
      !_.isUndefined(req.param('franchise')) ||
      !_.isUndefined(req.param('effectDate')) ||
      !_.isUndefined(req.param('alarm')) ||
      !_.isUndefined(req.param('bdm'))
    ) {
      res.status(200).json({
        fractQuotation: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).fractQuotation,
        cashQuotation: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).cashQuotation,
        contrAttentat: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).CONTRB_ATT,
        contrAttentatSem: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).CONTRB_ATT_SEM,
        contrAttentatTrim: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).CONTRB_ATT_TRIM,
        contrAttentatMens: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).CONTRB_ATT_MENS,
        totalTaxFraisContr: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_TAX_FRAIS_CONTR,
        totalTaxFraisContrSem: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_TAX_FRAIS_CONTR_SEM,
        totalTaxFraisContrTrim: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_TAX_FRAIS_CONTR_TRIM,
        totalTaxFraisContrMens: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_TAX_FRAIS_CONTR_MENS,
        totalTax: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOT_TAXES,
        totalTaxSem: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_TAX_SEM,
        totalTaxTrim: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_TAX_TRIM,
        totalTaxMens: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_TAX_MENS,
        fraisCourtier: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).FRAIS_COURTIER,
        fraisAssistance: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).COT_ASSISTANCE_TTC,
        fraisCourtierSem: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).FRAIS_COURTIER_SEM,
        fraisCourtierTrim: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).FRAIS_COURTIER_TRIM,
        fraisCourtierMens: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).FRAIS_COURTIER_MENS,
        TaxINCENDIE: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_INCENDIE,
        TaxTEMPETE: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_TEMPETE,
        TaxVANDALISME: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_VANDALISME,
        TaxDELECT: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
        ).TAX_DELECT,
        TaxDDE: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_DDE,
        TaxBDG: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_BDG,
        TaxVOL: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_VOL,
        TaxPE: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_PE,
        TotalHT: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOT_HT,
        TotalHTMens: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_COT_HT_MENS,
        TotalHTSem: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_COT_HT_SEM,
        TotalHTTrim: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_COT_HT_TRIM,
        TaxATT: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_ATT,
        RedVolHT: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_VOL_HT,
        RedATT: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_ATT,
        RedCATNAT: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_CATNAT,
        RedTAX: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_TAX,
        RedVOLTTC: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_VOL_TTC,

        RedVolHT: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_VOL_HT,
        RedATT: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_ATT,
        RedCATNAT: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_CATNAT,
        RedTAX: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_TAX,
        RedVOLTTC: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_VOL_TTC,
        RedVOLTTCSEM: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_VOL_TTC_SEM,
        RedVOLTTCTRIM: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_VOL_TTC_TRIM,
        RedVOLTTCMENS: Quotation.MrpRuralMixteCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_VOL_TTC_MENS,
      });
      // params are invalid
    } else {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
  },
};
