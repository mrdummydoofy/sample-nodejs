/**
 * SimulationMDController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const moment = require('moment');
moment.suppressDeprecationWarnings = true;

module.exports = {
  async postMd(req, res) {
    // test params validity
    if (
      !_.isUndefined(req.param('leaderBirthdate')) ||
      !_.isUndefined(req.param('haveConjoint')) ||
      !_.isUndefined(req.param('haveChildren'))
    ) {
      // test existance of leader
      Leader.findOrCreate(
        { user: req.user.id },
        {
          leaderBirthdate: req.param('leaderBirthdate'),
          haveConjoint: req.param('haveConjoint'),
          conjointBirthdate: req.param('haveConjoint') ? req.param('conjointBirthdate') : '',
          conjointLastname: req.param('haveConjoint') ? req.param('conjointLastname') : '',
          conjointName: req.param('haveConjoint') ? req.param('conjointName') : '',
          haveChildren: req.param('haveChildren'),
          childrenNumber: req.param('haveChildren') ? req.param('childrenNumber') : 0,
          user: req.user.id,
        },
      ).exec(async (err, leader, wasCreated) => {
        if (err) {
          return res.serverError(err);
        }
        if (wasCreated) {
          if (
            req.param('haveChildren') &&
            Object.keys(JSON.parse(`${req.param('ChildrenBirthdateDay')}`)).length > 0
          ) {
            for (
              let i = 0;
              i < Object.keys(JSON.parse(`${req.param('ChildrenBirthdateDay')}`)).length;
              i++
            ) {
              await Children.create({
                birthdate: moment(
                  new Date(
                    `${
                      Object.keys(JSON.parse(`${req.param('ChildrenBirthdateYear')}`)).map(
                        e => JSON.parse(`${req.param('ChildrenBirthdateYear')}`)[e],
                      )[i]
                    }-${
                      Object.keys(JSON.parse(`${req.param('ChildrenBirthdateMonth')}`)).map(
                        e => JSON.parse(`${req.param('ChildrenBirthdateMonth')}`)[e],
                      )[i]
                    }-${
                      Object.keys(JSON.parse(`${req.param('ChildrenBirthdateDay')}`)).map(
                        e => JSON.parse(`${req.param('ChildrenBirthdateDay')}`)[e],
                      )[i]
                    }`,
                  ),
                ).format('DD-MM-YYYY'),
                name: JSON.parse(req.param('ChildrenName'))[i],
                lastname: JSON.parse(req.param('ChildrenLastname'))[i],
                leader: leader.id,
              });
            }
          } else if (!req.param('haveChildren')) {
            await Children.destroy({ leader: leader.id });
          }
          return res.status(200).json({ message: 'Informations crées avec succès' });
        } else {
          await Leader.update(
            { user: req.user.id },
            {
              leaderBirthdate: req.param('leaderBirthdate'),
              haveConjoint: req.param('haveConjoint'),
              conjointBirthdate: req.param('haveConjoint') ? req.param('conjointBirthdate') : '',
              conjointLastname: req.param('haveConjoint') ? req.param('conjointLastname') : '',
              conjointName: req.param('haveConjoint') ? req.param('conjointName') : '',
              haveChildren: req.param('haveChildren'),
              childrenNumber: req.param('haveChildren') ? req.param('childrenNumber') : 0,
            },
          );
          if (
            req.param('haveChildren') &&
            Object.keys(JSON.parse(`${req.param('ChildrenBirthdateDay')}`)).length > 0
          ) {
            await Children.destroy({ leader: leader.id });
            for (
              let i = 0;
              i < Object.keys(JSON.parse(`${req.param('ChildrenBirthdateDay')}`)).length;
              i++
            ) {
              await Children.create({
                birthdate: moment(
                  new Date(
                    `${
                      Object.keys(JSON.parse(`${req.param('ChildrenBirthdateYear')}`)).map(
                        e => JSON.parse(`${req.param('ChildrenBirthdateYear')}`)[e],
                      )[i]
                    }-${
                      Object.keys(JSON.parse(`${req.param('ChildrenBirthdateMonth')}`)).map(
                        e => JSON.parse(`${req.param('ChildrenBirthdateMonth')}`)[e],
                      )[i]
                    }-${
                      Object.keys(JSON.parse(`${req.param('ChildrenBirthdateDay')}`)).map(
                        e => JSON.parse(`${req.param('ChildrenBirthdateDay')}`)[e],
                      )[i]
                    }`,
                  ),
                ).format('DD-MM-YYYY'),
                name: JSON.parse(`${req.param('ChildrenName')}`)[i],
                lastname: JSON.parse(`${req.param('ChildrenLastname')}`)[i],
                leader: leader.id,
              });
            }
          } else if (!req.param('haveChildren')) {
            await Children.destroy({ leader: leader.id });
          }

          return res.status(200).json({ message: 'Mise à jour des informations avec succès' });
        }
      });
    } else {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
  },

  async mdQuotationPromo(req, res) {
    // test params validity
    if (
      !_.isUndefined(req.param('leaderBirthdate')) ||
      !_.isUndefined(req.param('haveConjoint')) ||
      !_.isUndefined(req.param('haveChildren'))
    ) {
      res.status(200).json({
        mdPromoQuotation: Quotation.mdQuotation(
          req.param('leaderBirthdate'),
          req.param('conjointBirthdate'),
          req.param('ChildrenBirthdates'),
          1,
          parseInt(req.param('childrenNumbers')),
          req.param('haveConjoint') === 'oui' ? true : false,
          req.param('haveChildren') === 'oui' ? true : false,
        ).TOTAL_COT_PROMO,
        mdLeaderPromoQuotation: Quotation.mdQuotation(
          req.param('leaderBirthdate'),
          req.param('conjointBirthdate'),
          req.param('ChildrenBirthdates'),
          1,
          parseInt(req.param('childrenNumbers')),
          req.param('haveConjoint') === 'oui' ? true : false,
          req.param('haveChildren') === 'oui' ? true : false,
        ).COT_LEADER_PROMO,
        mdConjointPromoQuotation: Quotation.mdQuotation(
          req.param('leaderBirthdate'),
          req.param('conjointBirthdate'),
          req.param('ChildrenBirthdates'),
          1,
          parseInt(req.param('childrenNumbers')),
          req.param('haveConjoint') === 'oui' ? true : false,
          req.param('haveChildren') === 'oui' ? true : false,
        ).COT_CONJOINT_PROMO,
        mdKidsPromoQuotation: Quotation.mdQuotation(
          req.param('leaderBirthdate'),
          req.param('conjointBirthdate'),
          req.param('ChildrenBirthdates'),
          1,
          parseInt(req.param('childrenNumbers')),
          req.param('haveConjoint') === 'oui' ? true : false,
          req.param('haveChildren') === 'oui' ? true : false,
        ).COT_KIDS_PROMO,
        kid_age: Quotation.mdQuotation(
          req.param('leaderBirthdate'),
          req.param('conjointBirthdate'),
          req.param('ChildrenBirthdates'),
          1,
          parseInt(req.param('childrenNumbers')),
          req.param('haveConjoint') === 'oui' ? true : false,
          req.param('haveChildren') === 'oui' ? true : false,
        ).kid_age,
        conjoint_age: Quotation.mdQuotation(
          req.param('leaderBirthdate'),
          req.param('conjointBirthdate'),
          req.param('ChildrenBirthdates'),
          1,
          parseInt(req.param('childrenNumbers')),
          req.param('haveConjoint') === 'oui' ? true : false,
          req.param('haveChildren') === 'oui' ? true : false,
        ).conjoint_age,
        leader_age: Quotation.mdQuotation(
          req.param('leaderBirthdate'),
          req.param('conjointBirthdate'),
          req.param('ChildrenBirthdates'),
          1,
          parseInt(req.param('childrenNumbers')),
          req.param('haveConjoint') === 'oui' ? true : false,
          req.param('haveChildren') === 'oui' ? true : false,
        ).leader_age,
      });
      // params are invalid
    } else {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
  },

  // Md Quotation
  mdQuotation(req, res) {
    // test params validity
    if (
      !_.isUndefined(req.param('leaderBirthdate')) ||
      !_.isUndefined(req.param('haveConjoint')) ||
      !_.isUndefined(req.param('haveChildren')) ||
      !_.isUndefined(req.param('fraction')) ||
      !_.isUndefined(req.param('effectDate'))
    ) {
      res.status(200).json({
        fractQuotation: Quotation.MdCalculateQuotation(
          req.param('leaderBirthdate'),
          req.param('conjointBirthdate'),
          req.param('ChildrenBirthdates'),
          parseInt(req.param('formula')),
          parseInt(req.param('childrenNumbers')),
          req.param('haveConjoint') === 'oui' ? true : false,
          req.param('haveChildren') === 'oui' ? true : false,
          req.param('indivAccidentChoix'),
          req.param('indivEnfants') === 'oui' ? true : false,
          req.param('rapatriementChoix'),
          undefined,
          parseInt(req.param('fraction')),
          req.param('effectDate'),
        ).fractQuotation,
        cashQuotation: Quotation.MdCalculateQuotation(
          req.param('leaderBirthdate'),
          req.param('conjointBirthdate'),
          req.param('ChildrenBirthdates'),
          parseInt(req.param('formula')),
          parseInt(req.param('childrenNumbers')),
          req.param('haveConjoint') === 'oui' ? true : false,
          req.param('haveChildren') === 'oui' ? true : false,
          req.param('indivAccidentChoix'),
          req.param('indivEnfants') === 'oui' ? true : false,
          req.param('rapatriementChoix'),
          undefined,
          parseInt(req.param('fraction')),
          req.param('effectDate'),
        ).cashQuotation,
      });
      // params are invalid
    } else {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
  },
};
