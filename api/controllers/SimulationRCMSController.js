/**
 * SimulationRCMSController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  // RCMS first form post
  async postRcms(req, res) {
    // test params validity
    if (!_.isUndefined(req.param('turnover'))) {
      // do not permit turnover > 1500000 euros
      if (parseInt(req.param('turnover')) <= 1500000) {
        // test existance of customer
        Leader.findOrCreate(
          { user: req.user.id },
          {
            turnover: parseInt(req.param('turnover')),
            type: req.param('type'),
            user: req.user.id,
          },
        ).exec(async (err, leader, wasCreated) => {
          if (err) {
            return res.serverError(err);
          }
          if (wasCreated) {
            return res.status(200).json({ message: 'Informations crées avec succès' });
          } else {
            await Leader.update(
              { user: req.user.id },
              {
                turnover: parseInt(req.param('turnover')),
                type: req.param('type'),
              },
            );
            return res.status(200).json({ message: 'Mise à jour des informations avec succès' });
          }
        });
      } else {
        return res
          .status(400)
          .json({ error: "Chiffre d'affaires HT ne doit pas dépasser 1500000 euros" });
      }
    } else {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
  },

  async rcmsSimQuotation(req, res) {
    // test params validity

    res.status(200).json({
      rcmsQuotation: Quotation.rcmsQuotation().COTISATION_ANN,
    });
  },

  // RCMS Quotation
  rcmsQuotation(req, res) {
    // test params validity
    if (!_.isUndefined(req.param('fraction')) || !_.isUndefined(req.param('effectDate'))) {
      res.status(200).json({
        fractQuotation: Quotation.rcmsCalculateQuotation(
          undefined,
          parseInt(req.param('fraction')),
          req.param('effectDate'),
        ).fractQuotation,
        cashQuotation: Quotation.rcmsCalculateQuotation(
          undefined,
          parseInt(req.param('fraction')),
          req.param('effectDate'),
        ).cashQuotation,
        totTaxesFrais: Quotation.rcmsCalculateQuotation(
          undefined,
          parseInt(req.param('fraction')),
          req.param('effectDate'),
        ).totTaxesFrais,
      });
      // params are invalid
    } else {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
  },
};
