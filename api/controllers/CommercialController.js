/**
 * CommercialController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const crypto = require('crypto');
const moment = require('moment');
const currency = require('currency-formatter');

moment.suppressDeprecationWarnings = true;

module.exports = {
  //get quotation infos
  quotation(req, res) {
    if (
      _.isUndefined(req.param('offer')) ||
      _.isUndefined(req.param('turnover')) ||
      _.isUndefined(req.param('activity')) ||
      _.isUndefined(req.param('franchise')) ||
      _.isUndefined(req.param('brokerFee')) ||
      _.isUndefined(req.param('effectDate'))
    ) {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
    let selectedQuotation = null;
    const allQuotations = {
      RC: {
        '1': {
          urbain: {
            fractQuotation: Quotation.rcUrbanCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
            ).fractQuotation,
            cashQuotation: Quotation.rcUrbanCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
            ).cashQuotation,
          },
          rurale: {
            fractQuotation: Quotation.rcRuralCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
            ).fractQuotation,
            cashQuotation: Quotation.rcRuralCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
            ).cashQuotation,
          },
        },
        '2': {
          urbain: {
            fractQuotation: Quotation.rcUrbanCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
            ).fractQuotation,
            cashQuotation: Quotation.rcUrbanCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
            ).cashQuotation,
          },
          rurale: {
            fractQuotation: Quotation.rcRuralCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
            ).fractQuotation,
            cashQuotation: Quotation.rcRuralCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
            ).cashQuotation,
          },
        },
        '4': {
          urbain: {
            fractQuotation: Quotation.rcUrbanCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
            ).fractQuotation,
            cashQuotation: Quotation.rcUrbanCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
            ).cashQuotation,
          },
          rurale: {
            fractQuotation: Quotation.rcRuralCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
            ).fractQuotation,
            cashQuotation: Quotation.rcRuralCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
            ).cashQuotation,
          },
        },
        '12': {
          urbain: {
            fractQuotation: Quotation.rcUrbanCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
            ).fractQuotation,
            cashQuotation: Quotation.rcUrbanCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
            ).cashQuotation,
          },
          rurale: {
            fractQuotation: Quotation.rcRuralCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
            ).fractQuotation,
            cashQuotation: Quotation.rcRuralCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
            ).cashQuotation,
          },
        },
      },
      MRP: {
        '1': {
          urbain: {
            fractQuotation: Quotation.MrpUrbanCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              parseInt(req.param('franchise')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
              req.param('alarm') === 'oui' ? true : false,
              req.param('bdm'),
            ).fractQuotation,
            cashQuotation: Quotation.MrpUrbanCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              parseInt(req.param('franchise')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
              req.param('alarm') === 'oui' ? true : false,
              req.param('bdm'),
            ).cashQuotation,
          },
          rurale: {
            fractQuotation: Quotation.MrpRuralMixteCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              parseInt(req.param('franchise')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
              req.param('alarm') === 'oui' ? true : false,
              req.param('bdm'),
            ).fractQuotation,
            cashQuotation: Quotation.MrpRuralMixteCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              parseInt(req.param('franchise')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
              req.param('alarm') === 'oui' ? true : false,
              req.param('bdm'),
            ).cashQuotation,
          },
        },
        '2': {
          urbain: {
            fractQuotation: Quotation.MrpUrbanCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              parseInt(req.param('franchise')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
              req.param('alarm') === 'oui' ? true : false,
              req.param('bdm'),
            ).fractQuotation,
            cashQuotation: Quotation.MrpUrbanCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              parseInt(req.param('franchise')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
              req.param('alarm') === 'oui' ? true : false,
              req.param('bdm'),
            ).cashQuotation,
          },
          rurale: {
            fractQuotation: Quotation.MrpRuralMixteCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              parseInt(req.param('franchise')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
              req.param('alarm') === 'oui' ? true : false,
              req.param('bdm'),
            ).fractQuotation,
            cashQuotation: Quotation.MrpRuralMixteCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              parseInt(req.param('franchise')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
              req.param('alarm') === 'oui' ? true : false,
              req.param('bdm'),
            ).cashQuotation,
          },
        },
        '4': {
          urbain: {
            fractQuotation: Quotation.MrpUrbanCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              parseInt(req.param('franchise')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
              req.param('alarm') === 'oui' ? true : false,
              req.param('bdm'),
            ).fractQuotation,
            cashQuotation: Quotation.MrpUrbanCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              parseInt(req.param('franchise')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
              req.param('alarm') === 'oui' ? true : false,
              req.param('bdm'),
            ).cashQuotation,
          },
          rurale: {
            fractQuotation: Quotation.MrpRuralMixteCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              parseInt(req.param('franchise')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
              req.param('alarm') === 'oui' ? true : false,
              req.param('bdm'),
            ).fractQuotation,
            cashQuotation: Quotation.MrpRuralMixteCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              parseInt(req.param('franchise')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
              req.param('alarm') === 'oui' ? true : false,
              req.param('bdm'),
            ).cashQuotation,
          },
        },
        '12': {
          urbain: {
            fractQuotation: Quotation.MrpUrbanCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              parseInt(req.param('franchise')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
              req.param('alarm') === 'oui' ? true : false,
              req.param('bdm'),
            ).fractQuotation,
            cashQuotation: Quotation.MrpUrbanCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              parseInt(req.param('franchise')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
              req.param('alarm') === 'oui' ? true : false,
              req.param('bdm'),
            ).cashQuotation,
          },
          rurale: {
            fractQuotation: Quotation.MrpRuralMixteCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              parseInt(req.param('franchise')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
              req.param('alarm') === 'oui' ? true : false,
              req.param('bdm'),
            ).fractQuotation,
            cashQuotation: Quotation.MrpRuralMixteCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              parseInt(req.param('franchise')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
              req.param('alarm') === 'oui' ? true : false,
              req.param('bdm'),
            ).cashQuotation,
          },
        },
      },
      'MRP + RC': {
        '1': {
          urbain: {
            fractQuotation: Quotation.MrpCrUrbanCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              parseInt(req.param('franchise')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
              req.param('alarm') === 'oui' ? true : false,
              req.param('bdm'),
            ).fractQuotation,
            cashQuotation: Quotation.MrpCrUrbanCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              parseInt(req.param('franchise')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
              req.param('alarm') === 'oui' ? true : false,
              req.param('bdm'),
            ).cashQuotation,
          },
          rurale: {
            fractQuotation: Quotation.MrpCrRuralMixedCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              parseInt(req.param('franchise')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
              req.param('alarm') === 'oui' ? true : false,
              req.param('bdm'),
            ).fractQuotation,
            cashQuotation: Quotation.MrpCrRuralMixedCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              parseInt(req.param('franchise')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
              req.param('alarm') === 'oui' ? true : false,
              req.param('bdm'),
            ).cashQuotation,
          },
        },
        '2': {
          urbain: {
            fractQuotation: Quotation.MrpCrUrbanCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              parseInt(req.param('franchise')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
              req.param('alarm') === 'oui' ? true : false,
              req.param('bdm'),
            ).fractQuotation,
            cashQuotation: Quotation.MrpCrUrbanCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              parseInt(req.param('franchise')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
              req.param('alarm') === 'oui' ? true : false,
              req.param('bdm'),
            ).cashQuotation,
          },
          rurale: {
            fractQuotation: Quotation.MrpCrRuralMixedCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              parseInt(req.param('franchise')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
              req.param('alarm') === 'oui' ? true : false,
              req.param('bdm'),
            ).fractQuotation,
            cashQuotation: Quotation.MrpCrRuralMixedCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              parseInt(req.param('franchise')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
              req.param('alarm') === 'oui' ? true : false,
              req.param('bdm'),
            ).cashQuotation,
          },
        },
        '4': {
          urbain: {
            fractQuotation: Quotation.MrpCrUrbanCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              parseInt(req.param('franchise')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
              req.param('alarm') === 'oui' ? true : false,
              req.param('bdm'),
            ).fractQuotation,
            cashQuotation: Quotation.MrpCrUrbanCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              parseInt(req.param('franchise')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
              req.param('alarm') === 'oui' ? true : false,
              req.param('bdm'),
            ).cashQuotation,
          },
          rurale: {
            fractQuotation: Quotation.MrpCrRuralMixedCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              parseInt(req.param('franchise')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
              req.param('alarm') === 'oui' ? true : false,
              req.param('bdm'),
            ).fractQuotation,
            cashQuotation: Quotation.MrpCrRuralMixedCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              parseInt(req.param('franchise')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
              req.param('alarm') === 'oui' ? true : false,
              req.param('bdm'),
            ).cashQuotation,
          },
        },
        '12': {
          urbain: {
            fractQuotation: Quotation.MrpCrUrbanCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              parseInt(req.param('franchise')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
              req.param('alarm') === 'oui' ? true : false,
              req.param('bdm'),
            ).fractQuotation,
            cashQuotation: Quotation.MrpCrUrbanCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              parseInt(req.param('franchise')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
              req.param('alarm') === 'oui' ? true : false,
              req.param('bdm'),
            ).cashQuotation,
          },
          rurale: {
            fractQuotation: Quotation.MrpCrRuralMixedCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              parseInt(req.param('franchise')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
              req.param('alarm') === 'oui' ? true : false,
              req.param('bdm'),
            ).fractQuotation,
            cashQuotation: Quotation.MrpCrRuralMixedCalculateTAX(
              parseInt(req.param('turnover')),
              parseInt(req.param('fraction')),
              parseInt(req.param('franchise')),
              undefined,
              req.param('effectDate'),
              parseFloat(req.param('brokerFee')),
              req.param('alarm') === 'oui' ? true : false,
              req.param('bdm'),
            ).cashQuotation,
          },
        },
      },
      MD: {
        value: {
          fractQuotation: Quotation.MdCalculateQuotation(
            req.param('leaderBirthdate') === 'undefined' ? undefined : req.param('leaderBirthdate'),
            req.param('conjointBirthdate') === 'undefined'
              ? undefined
              : req.param('conjointBirthdate'),
            req.param('kidBirthdate') === 'undefined' ? undefined : req.param('kidBirthdate'),
            req.param('formula') === 'undefined' ? undefined : parseInt(req.param('formula')),
            req.param('kidsNumber') === 'undefined' ? undefined : req.param('kidsNumber'),
            req.param('haveConjoint') === 'undefined'
              ? undefined
              : req.param('haveConjoint') === 'true'
                ? true
                : false,
            req.param('haveKids') === 'undefined'
              ? undefined
              : req.param('haveKids') === 'true'
                ? true
                : false,
            req.param('indivAccidentChoix') === 'undefined'
              ? undefined
              : req.param('indivAccidentChoix'),
            req.param('indivEnfants') === 'true' ? true : false,
            req.param('rapatriementChoix') === 'undefined'
              ? undefined
              : req.param('rapatriementChoix'),
            undefined,
            parseInt(req.param('fraction')) === 'undefined'
              ? undefined
              : parseInt(req.param('fraction')),
            req.param('effectDate') === 'undefined' ? undefined : req.param('effectDate'),
          ).fractQuotation,
          cashQuotation: Quotation.MdCalculateQuotation(
            req.param('leaderBirthdate') === 'undefined' ? undefined : req.param('leaderBirthdate'),
            req.param('conjointBirthdate') === 'undefined'
              ? undefined
              : req.param('conjointBirthdate'),
            req.param('kidBirthdate') === 'undefined' ? undefined : req.param('kidBirthdate'),
            req.param('formula') === 'undefined' ? undefined : parseInt(req.param('formula')),
            req.param('kidsNumber') === 'undefined' ? undefined : req.param('kidsNumber'),
            req.param('haveConjoint') === 'undefined'
              ? undefined
              : req.param('haveConjoint') === 'true'
                ? true
                : false,
            req.param('haveKids') === 'undefined'
              ? undefined
              : req.param('haveKids') === 'true'
                ? true
                : false,
            req.param('indivAccidentChoix') === 'undefined'
              ? undefined
              : req.param('indivAccidentChoix'),
            req.param('indivEnfants') === 'undefined'
              ? undefined
              : req.param('indivEnfants') === 'true'
                ? true
                : false,
            req.param('rapatriementChoix') === 'undefined'
              ? undefined
              : req.param('rapatriementChoix'),
            undefined,
            parseInt(req.param('fraction')) === 'undefined'
              ? undefined
              : parseInt(req.param('fraction')),
            req.param('effectDate') === 'undefined' ? undefined : req.param('effectDate'),
          ).cashQuotation,
        },
      },
      RCMS: {
        value: {
          fractQuotation: Quotation.rcmsCalculateQuotation(
            undefined,
            parseInt(req.param('fraction')),
            req.param('effectDate'),
            parseFloat(req.param('brokerFee')).toFixed(2),
          ).fractQuotation,
          cashQuotation: Quotation.rcmsCalculateQuotation(
            undefined,
            parseInt(req.param('fraction')),
            req.param('effectDate'),
            parseFloat(req.param('brokerFee')).toFixed(2),
          ).cashQuotation,
        },
      },
    };

    if (req.param('offer') === 'MD' || req.param('offer') === 'RCMS') {
      selectedQuotation = allQuotations[req.param('offer')]['value'];
    } else {
      selectedQuotation =
        allQuotations[req.param('offer')][req.param('fraction')][req.param('activity')];
    }

    return res.status(200).json({
      quotation: selectedQuotation,
    });
  },

  // Commercial add adhesion
  async commercialCreateAdhesion(req, res) {
    //validate params
    if (
      _.isUndefined(req.param('siren')) ||
      _.isUndefined(req.param('companyName')) ||
      _.isUndefined(req.param('legalForm')) ||
      _.isUndefined(req.param('startActivityDate')) ||
      _.isUndefined(req.param('phone')) ||
      _.isUndefined(req.param('address')) ||
      _.isUndefined(req.param('postalCode')) ||
      _.isUndefined(req.param('city')) ||
      _.isUndefined(req.param('civility')) ||
      _.isUndefined(req.param('firstName')) ||
      _.isUndefined(req.param('lastName')) ||
      _.isUndefined(req.param('mobile')) ||
      _.isUndefined(req.param('email')) ||
      _.isUndefined(req.param('offer')) ||
      _.isUndefined(req.param('fractQuotation')) ||
      _.isUndefined(req.param('cashQuotation')) ||
      _.isUndefined(req.param('fraction')) ||
      _.isUndefined(req.param('effectDate')) ||
      _.isUndefined(req.param('bic')) ||
      _.isUndefined(req.param('iban')) ||
      _.isUndefined(req.param('holder')) ||
      _.isUndefined(req.param('exactTitle'))
    ) {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }

    if (!(await Utils.validateIBAN(req.param('iban')))) {
      return res.status(400).json({ error: 'IBAN invalide' });
    }

    let userFound = await User.findOne({ email: req.param('email') })
      .populate('contract')
      .populate('leader')
      .populate('customer');
    let contractsQuotations = [];
    let unsignedContracts = [];

    const password = crypto.randomBytes(5).toString('hex');

    if (!userFound) {
      userFound = await User.create({
        email: req.param('email'),
        login: req.param('email'),
        title: req.param('civility'),
        name: req.param('firstName'),
        lastname: req.param('lastName'),
        mobile: req.param('mobile'),
        phone: req.param('phone'),
        password,
        isRegistred: true,
        registrationToken: null,
      }).fetch();
      userFound.contract = [];
      Mailer.sendCommercialActivationMail(userFound, password);
    } else {
      for (let i = 0; i < userFound.contract.length; i++) {
        const signedContract = await Contract.findOne({
          id: userFound.contract[i].id,
          signatureStatus: 'completed',
        }).populate('quotation');
        if (signedContract) {
          contractsQuotations.push(signedContract);
        } else {
          unsignedContracts.push(
            await Contract.findOne({
              id: userFound.contract[i].id,
            }).populate('quotation'),
          );
        }
      }
      userFound.contract = contractsQuotations;
      userFound.unsignedContract = unsignedContracts;
      userFound.contract = userFound.contract.filter(
        e => e.quotation.length > 0 && e.quotation[0].offerType === req.param('offer'),
      );
    }

    if (userFound && userFound.isCommercial) {
      return res
        .status(400)
        .json({ error: 'Vous ne pouvez pas créer une adhésion car vous êtes un commercial' });
    }

    if (userFound && userFound.contract.length > 0) {
      return res.status(400).json({ error: 'Le client a déjà un compte et un contrat' });
    }

    let customerFound = null;
    let leaderFound = null;
    if (req.param('offer') !== 'MD' && req.param('offer') !== 'RCMS') {
      customerFound = await Customer.findOne({ user: userFound.id });
      if (!customerFound) {
        customerFound = await Customer.create({
          siren: req.param('siren'),
          turnover: req.param('turnover') ? req.param('turnover').replace(/ /g, '') : undefined,
          companyName: req.param('companyName'),
          localContent: req.param('contenuLocaux')
            ? req.param('contenuLocaux').replace(/ /g, '')
            : undefined,
          localArea: req.param('surfaceLocaux')
            ? req.param('surfaceLocaux').replace(/ /g, '')
            : undefined,
          legalForm: req.param('legalForm'),
          startActivityDate: req.param('startActivityDate'),
          activity: req.param('activite'),
          franchise: req.param('franchise'),
          endStatementDate: `${req.param('endStatementDateMonth')}-${req.param(
            'endStatementDateDay',
          )}`,
          brokerFee: req.param('brokerFee'),
          alarm: req.param('alarm') === 'oui' ? true : false,
          approvedAlarm:
            req.param('alarm') === 'oui'
              ? req.param('approvedAlarm') === 'oui'
                ? true
                : false
              : false,
          bdmSup: req.param('bdmSup') === 'oui' ? true : false,
          bdm: req.param('bdmSup') === 'oui' ? req.param('bdm') : undefined,
          user: userFound.id,
        }).fetch();
      } else {
        customerFound = await Customer.update(
          { user: userFound.id },
          {
            siren: req.param('siren'),
            turnover: req.param('turnover') ? req.param('turnover').replace(/ /g, '') : undefined,
            companyName: req.param('companyName'),
            localContent: req.param('contenuLocaux')
              ? req.param('contenuLocaux').replace(/ /g, '')
              : undefined,
            localArea: req.param('surfaceLocaux')
              ? req.param('surfaceLocaux').replace(/ /g, '')
              : undefined,
            legalForm: req.param('legalForm'),
            startActivityDate: req.param('startActivityDate'),
            activity: req.param('activite'),
            franchise: req.param('franchise'),
            endStatementDate: `${req.param('endStatementDateMonth')}-${req.param(
              'endStatementDateDay',
            )}`,
            brokerFee: req.param('brokerFee'),
            alarm: req.param('alarm') === 'oui' ? true : false,
            approvedAlarm:
              req.param('alarm') === 'oui'
                ? req.param('approvedAlarm') === 'oui'
                  ? true
                  : false
                : false,
            bdmSup: req.param('bdmSup') === 'oui' ? true : false,
            bdm: req.param('bdmSup') === 'oui' ? req.param('bdm') : undefined,
          },
        ).fetch();
      }
    } else {
      leaderFound = await Leader.findOne({ user: userFound.id });
      if (!leaderFound) {
        leaderFound = await Leader.create({
          siren: req.param('siren'),
          companyName: req.param('companyName'),
          legalForm: req.param('legalForm'),
          startActivityDate: req.param('startActivityDate'),
          leaderBirthdate: req.param('leaderBirthdate'),
          formula: req.param('formula'),
          conjointName: req.param('haveConjoint') === 'oui' ? req.param('conjointName') : '',
          conjointLastname:
            req.param('haveConjoint') === 'oui' ? req.param('conjointLastname') : '',
          conjointBirthdate:
            req.param('haveConjoint') === 'oui' ? req.param('conjointBirthdate') : '',
          childrenNumber: req.param('childrenNumber'),
          haveConjoint: req.param('haveConjoint') === 'oui' ? true : false,
          haveChildren: req.param('haveChildren') === 'oui' ? true : false,
          indivAccident: req.param('indivAccident') === 'oui' ? true : false,
          indivAccidentChoix:
            req.param('indivAccident') === 'oui' ? req.param('indivAccidentChoix') : undefined,
          indivEnfants: req.param('indivEnfants') === 'oui' ? true : false,
          rapatriement: req.param('rapatriement') === 'oui' ? true : false,
          rapatriementChoix:
            req.param('rapatriement') === 'oui' ? req.param('rapatriementChoix') : undefined,
          turnover: req.param('turnover') ? req.param('turnover').replace(/ /g, '') : undefined,
          chiffreAffaireCours: req.param('chiffreAffaireCours')
            ? req.param('chiffreAffaireCours').replace(/ /g, '')
            : undefined,
          chiffreAffairePrev: req.param('chiffreAffairePrev')
            ? req.param('chiffreAffairePrev').replace(/ /g, '')
            : undefined,
          totalActifAnnee: req.param('totalActifAnnee')
            ? req.param('totalActifAnnee').replace(/ /g, '')
            : undefined,
          totalActifCours: req.param('totalActifCours')
            ? req.param('totalActifCours').replace(/ /g, '')
            : undefined,
          totalActifPrev: req.param('totalActifPrev')
            ? req.param('totalActifPrev').replace(/ /g, '')
            : undefined,
          capitauxAnnee: req.param('capitauxAnnee')
            ? req.param('capitauxAnnee').replace(/ /g, '')
            : undefined,
          capitauxCours: req.param('capitauxCours')
            ? req.param('capitauxCours').replace(/ /g, '')
            : undefined,
          capitauxPrev: req.param('capitauxPrev')
            ? req.param('capitauxPrev').replace(/ /g, '')
            : undefined,
          totalAnnee: req.param('totalAnnee')
            ? req.param('totalAnnee').replace(/ /g, '')
            : undefined,
          totalCours: req.param('totalCours')
            ? req.param('totalCours').replace(/ /g, '')
            : undefined,
          totalPrev: req.param('totalPrev') ? req.param('totalPrev').replace(/ /g, '') : undefined,
          capitauxPropres: req.param('capitauxPropres') === 'oui' ? true : false,
          resultatNet: req.param('resultatNet') === 'oui' ? true : false,
          user: userFound.id,
        }).fetch();
      } else {
        leaderFound = await Leader.update(
          { user: userFound.id },
          {
            siren: req.param('siren'),
            companyName: req.param('companyName'),
            legalForm: req.param('legalForm'),
            startActivityDate: req.param('startActivityDate'),
            leaderBirthdate: req.param('leaderBirthdate'),
            formula: req.param('formula'),
            conjointName: req.param('haveConjoint') === 'oui' ? req.param('conjointName') : '',
            conjointLastname:
              req.param('haveConjoint') === 'oui' ? req.param('conjointLastname') : '',
            conjointBirthdate:
              req.param('haveConjoint') === 'oui' ? req.param('conjointBirthdate') : '',
            childrenNumber: req.param('childrenNumber'),
            haveConjoint: req.param('haveConjoint') === 'oui' ? true : false,
            haveChildren: req.param('haveChildren') === 'oui' ? true : false,
            indivAccident: req.param('indivAccident') === 'oui' ? true : false,
            indivAccidentChoix:
              req.param('indivAccident') === 'oui' ? req.param('indivAccidentChoix') : undefined,
            indivEnfants: req.param('indivEnfants') === 'oui' ? true : false,
            rapatriement: req.param('rapatriement') === 'oui' ? true : false,
            rapatriementChoix:
              req.param('rapatriement') === 'oui' ? req.param('rapatriementChoix') : undefined,
            turnover: req.param('turnover') ? req.param('turnover').replace(/ /g, '') : undefined,
            chiffreAffaireCours: req.param('chiffreAffaireCours')
              ? req.param('chiffreAffaireCours').replace(/ /g, '')
              : undefined,
            chiffreAffairePrev: req.param('chiffreAffairePrev')
              ? req.param('chiffreAffairePrev').replace(/ /g, '')
              : undefined,
            totalActifAnnee: req.param('totalActifAnnee')
              ? req.param('totalActifAnnee').replace(/ /g, '')
              : undefined,
            totalActifCours: req.param('totalActifCours')
              ? req.param('totalActifCours').replace(/ /g, '')
              : undefined,
            totalActifPrev: req.param('totalActifPrev')
              ? req.param('totalActifPrev').replace(/ /g, '')
              : undefined,
            capitauxAnnee: req.param('capitauxAnnee')
              ? req.param('capitauxAnnee').replace(/ /g, '')
              : undefined,
            capitauxCours: req.param('capitauxCours')
              ? req.param('capitauxCours').replace(/ /g, '')
              : undefined,
            capitauxPrev: req.param('capitauxPrev')
              ? req.param('capitauxPrev').replace(/ /g, '')
              : undefined,
            totalAnnee: req.param('totalAnnee')
              ? req.param('totalAnnee').replace(/ /g, '')
              : undefined,
            totalCours: req.param('totalCours')
              ? req.param('totalCours').replace(/ /g, '')
              : undefined,
            totalPrev: req.param('totalPrev')
              ? req.param('totalPrev').replace(/ /g, '')
              : undefined,
            capitauxPropres: req.param('capitauxPropres') === 'oui' ? true : false,
            resultatNet: req.param('resultatNet') === 'oui' ? true : false,
          },
        ).fetch();
        leaderFound = leaderFound[0];
      }
      if (req.param('haveChildren') === 'oui' && req.param('ChildrenBirthdates').length > 0) {
        for (let i = 0; i < req.param('ChildrenBirthdates').length; i++) {
          await Children.create({
            birthdate: req.param('ChildrenBirthdates')[i],
            name: JSON.parse(`${JSON.stringify(req.param('ChildrenName'))}`)[i],
            lastname: JSON.parse(`${JSON.stringify(req.param('ChildrenLastname'))}`)[i],
            leader: leaderFound.id,
          });
        }
      } else if (req.param('haveChildren') === 'non') {
        await Children.destroy({ leader: leaderFound.id });
      }
    }
    let addressFound = await Address.findOne({ user: userFound.id });
    if (!addressFound) {
      addressFound = await Address.create({
        address: req.param('address'),
        postalCode: req.param('postalCode'),
        city: req.param('city'),
        user: userFound.id,
      }).fetch();
    } else {
      addressFound = await Address.update(
        { user: userFound.id },
        {
          address: req.param('address'),
          postalCode: req.param('postalCode'),
          city: req.param('city'),
        },
      ).fetch();
    }
    let paymentFound = await PaymentMethod.findOne({ user: userFound.id });
    if (!paymentFound) {
      paymentFound = await PaymentMethod.create({
        BIC: req.param('bic').toUpperCase(),
        IBAN: req.param('iban').toUpperCase(),
        holder: req.param('holder'),
        exactTitle: req.param('exactTitle'),
        pickingDate: 5,
        user: userFound.id,
      }).fetch();
    } else {
      paymentFound = await PaymentMethod.update(
        { user: userFound.id },
        {
          BIC: req.param('bic').toUpperCase(),
          IBAN: req.param('iban').toUpperCase(),
          holder: req.param('holder'),
          pickingDate: 5,
          exactTitle: req.param('exactTitle'),
        },
      ).fetch();
    }
    const addedContract = await Contract.create({
      contractPlace: 'adistance',
      creationDate: moment().format('YYYY-MM-DD hh:mm:ss'),
      effectDate: req.param('effectDate'),
      deadlineDate: Utils.calculateDeadline(
        parseInt(req.param('fraction')),
        req.param('effectDate'),
      ).deadlineDate,
      afterDeadlineDate: Utils.calculateDeadline(
        parseInt(req.param('fraction')),
        req.param('effectDate'),
      ).afterDeadlineDate,
      firstPaymentType: 'sepa',
      user: userFound.id,
      commercial: req.user.id,
    }).fetch();
    const addedWarranty = await Warranty.create({
      status: true,
      contract: addedContract.id,
    }).fetch();
    req.session.createdUser = userFound;
    const user = await User.findOne({ id: userFound.id })
      .populate('contract')
      .populate('customer')
      .populate('payment')
      .populate('firstPaymentCb')
      .populate('leader')
      .populate('adhesion')
      .populate('address');
    const addedQuotation = await Quotation.create({
      fraction: req.param('fraction'),
      tarifType: req.param('activite'),
      period: Utils.calculatePeriod(parseInt(req.param('fraction'))),
      ttc: parseFloat(req.param('fractQuotation')).toFixed(2),
      tax: Utils.getContractTaxFromOffer(
        req.param('offer'),
        user,
        true,
        req.param('fraction'),
        addedContract,
      ),
      net:
        parseFloat(req.param('fractQuotation')).toFixed(2) -
        Utils.getContractTaxFromOffer(
          req.param('offer'),
          user,
          true,
          req.param('fraction'),
          addedContract,
        ),
      offerType: req.param('offer'),
      brokerFee: req.param('brokerFee'),
      cashQuotation: parseFloat(req.param('cashQuotation')).toFixed(2),
      contract: addedContract.id,
    }).fetch();
    if (leaderFound) {
      user.leader = await Leader.find({ id: leaderFound.id }).populate('children');
    }
    req.session.offerType = req.param('offer');
    req.session.currentContractId = addedContract.id;
    await Utils.signDocumentProcess(
      { addedContract, addedWarranty, addedQuotation },
      user,
      res,
      true,
      { user: user },
      req.user,
    );
  },
};
