/**
 * AuthController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const passport = require('passport');

module.exports = {
  // login action
  login(req, res) {
    passport.authenticate('local', (err, user, info) => {
      if (_.isUndefined(req.param('password')) || _.isUndefined(req.param('login'))) {
        info.message = 'Email ou mot de passe manquant';
      }
      if (err || !user) {
        return res.status(400).json({
          message: info.message,
          success: false,
          user,
        });
      }
      req.logIn(user, async err => {
        if (err) {
          return res.send(err);
        }
        await AdhesionTmp.update(
          { user: user.id },
          { lastVisitedPage: '', continueLaterShowPopup: true },
        );
        if (req.session.email === user.email && req.session.type) {
          await Utils.formInfosSubmitted(req);
        }
        return res.status(200).json({
          message: info.message,
          success: true,
          user,
        });
      });
    })(req, res);
  },

  //logout action
  logout(req, res) {
    req.logout();
    return res.redirect('back');
  },

  //authentication action
  authenticated(req, res) {
    res
      .status(200)
      .json(
        req.user && req.user.email !== 'deleted@vetoptim.org'
          ? { user: req.user }
          : { authenticated: false },
      );
  },
};
