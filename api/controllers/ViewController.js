/**
 * ViewController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  index(req, res) {
    return res.view('pages/index');
  },

  pageIntrouvable(req, res) {
    return res.view('pages/erreur-page');
  },

  commercialError(req, res) {
    return res.view('pages/erreur-commercial');
  },

  userError(req, res) {
    return res.view('pages/erreur-utilisateur');
  },

  simulationError(req, res) {
    return res.view('pages/erreur-simulation');
  },

  protectionActivite(req, res) {
    return res.view('pages/zoom/protection-activite');
  },

  responsabiliteCivile(req, res) {
    return res.view('pages/zoom/responsabilite-civile');
  },

  multirisqueProfessionelles(req, res) {
    return res.view('pages/zoom/multirisque-professionelles');
  },

  MRPRC(req, res) {
    return res.view('pages/zoom/mrp-plus-rc');
  },

  mutuelleEntrepriseSante(req, res) {
    return res.view('pages/zoom/mutuelle-entreprise-sante');
  },

  responsabiliteCivileDirigeants(req, res) {
    return res.view('pages/zoom/responsabilite-civile-dirigeants');
  },

  welcome(req, res) {
    return res.view('pages/welcome');
  },

  register(req, res) {
    if (req.isAuthenticated()) {
      return res.redirect('/');
    }
    return res.view('pages/register', { success: undefined, error: undefined });
  },

  mailCheck(req, res) {
    return res.view('pages/mail-check', { type: req.param('offerType') });
  },

  processMailCheck(req, res) {
    return res.view('pages/process-mail-check', { type: req.param('offerType') });
  },

  loginCheck(req, res) {
    return res.view('pages/login-check', { type: req.param('offerType') });
  },

  subscriptionCancel(req, res) {
    return res.view('pages/simulation/subscription-cancel');
  },

  formulaChoice(req, res) {
    return res.view('pages/formula-choice');
  },

  createPage(req, res) {
    return res.view('pages/create-page');
  },

  listPages(req, res) {
    return res.view('pages/list-pages');
  },

  helpA(req, res) {
    return res.view('pages/simulation/sim6a-help');
  },

  helpB(req, res) {
    return res.view('pages/simulation/sim6b-help');
  },

  // Informations
  aPropos(req, res) {
    return res.view('pages/informations/a-propos');
  },
  contact(req, res) {
    return res.view('pages/informations/contact');
  },
  ogv(req, res) {
    return res.view('pages/informations/association');
  },
  partenariatBourgelat(req, res) {
    return res.view('pages/informations/partenariat-bourgelat');
  },
  partenariatBourgelatSend(req, res) {
    return res.view('pages/informations/partenariat-bourgelat-send');
  },
  recrutement(req, res) {
    return res.view('pages/informations/recrutement');
  },
  responsabiliteSocietale(req, res) {
    return res.view('pages/informations/responsabilite-societale');
  },
  engagements(req, res) {
    return res.view('pages/informations/nos-engagements');
  },

  // Espace Assuré
  profileJournal(req, res) {
    return res.view('pages/profile/journal');
  },
  profileInformations(req, res) {
    return res.view('pages/profile/informations');
  },
  profileInformationsSuccess(req, res) {
    return res.view('pages/profile/success-update');
  },
  profilePasswordUpdate(req, res) {
    return res.view('pages/profile/modifer-password');
  },
  profileDevis(req, res) {
    return res.view('pages/profile/devis');
  },
  profileSinistres(req, res) {
    return res.view('pages/profile/sinistres');
  },
  profileDeclareSinistre(req, res) {
    return res.view('pages/profile/declare-sinistre');
  },
  profileSinistreSuccess(req, res) {
    return res.view('pages/profile/success-sinistre');
  },
  profileContrats(req, res) {
    return res.view('pages/profile/contrats');
  },
  deleteAccountView(req, res) {
    return res.view('pages/profile/supprimer-compte');
  },
  deleteAccountCheckView(req, res) {
    return res.view('pages/profile/supprimer-check');
  },
  deleteAccountSucceed(req, res) {
    return res.view('pages/profile/supprimer-succes');
  },
  contractCreated(req, res) {
    return res.view('pages/profile/contrats-crees');
  },
  contractCreatedAdmin(req, res) {
    return res.view('pages/profile/contrats-crees');
  },
  addContract(req, res) {
    return res.view('pages/profile/ajouter-contrat');
  },
  commercialSubscriptionSuccess(req, res) {
    res.view('pages/profile/commercial-subscription-success', {
      contractReference: req.session.contractReference,
      customerReference: req.session.currentCustomerRef,
      mndReference: req.session.contractSepaMndRef,
      leaderReference: req.session.currentLeaderRef,
      offerType: req.session.offerType,
    });
    req.session.contractReference = undefined;
    req.session.currentCustomerRef = undefined;
    req.session.contractSepaMndRef = undefined;
    req.session.currentLeaderRef = undefined;
    req.session.offerType = undefined;
    return;
  },
  simulation(req, res) {
    return res.view('pages/simulation/simulation');
  },

  customQuote(req, res) {
    return res.view('pages/simulation/custom-quote');
  },

  customQuoteCheck(req, res) {
    return res.view('pages/simulation/custom-quote-check');
  },

  // Simulation Views
  async sim1(req, res) {
    if (req.isAuthenticated()) {
      await AdhesionTmp.update({ user: req.user.id }, { lastVisitedPage: 'simulation' });
    }
    var o = {};
    o.type = req.param('offerType');

    if (o.type == 'CR') {
      o.title = 'RESPONSABILITÉ CIVILE PROFESSIONNELLE DES VÉTÉRINAIRES';
      o.color = 'blue';
      o.controller = 'SimulationCRController';
    }
    if (o.type == 'PMR') {
      o.title = 'MULTIRISQUE PROFESSIONNELLE';
      o.color = 'blue';
      o.controller = 'SimulationPMRController';
    }
    if (o.type == 'PMRCR') {
      o.title = 'MULTIRISQUE + RC PRO';
      o.color = 'blue';
      o.controller = 'SimulationPMRCRController';
    }
    if (o.type == 'MD') {
      o.color = 'skyblue';
      o.title = 'Mutuelle des dirigeants non salariés';
      o.controller = 'SimulationMDController';
    }
    if (o.type == 'RCMS') {
      o.color = 'skyblue';
      o.title = 'responsabilité civile des dirigeants';
      o.controller = 'SimulationRCMSController';
    }

    return res.view('pages/simulation/sim1', o);
  },

  async customRate(req, res) {
    return res.view('pages/simulation/custom-rate', { type: req.param('offerType') });
  },

  CustomRateCheck(req, res) {
    return res.view('pages/simulation/custom-rate-check', { type: req.param('offerType') });
  },

  async menu(req, res) {
    if (req.isAuthenticated()) {
      await AdhesionTmp.update({ user: req.user.id }, { lastVisitedPage: 'menu' });
    }
    return res.view('pages/simulation/sim2-menu', { type: req.param('offerType') });
  },

  async info(req, res) {
    if (req.isAuthenticated()) {
      await AdhesionTmp.update({ user: req.user.id }, { lastVisitedPage: 'info' });
    }
    return res.view('pages/simulation/sim3-info', { type: req.param('offerType') });
  },

  async adhesionSummary(req, res) {
    if (req.isAuthenticated()) {
      await AdhesionTmp.update({ user: req.user.id }, { lastVisitedPage: 'adhesion-summary' });
    }
    return res.view('pages/simulation/sim4-adhesion', { type: req.param('offerType') });
  },

  async payment(req, res) {
    if (req.isAuthenticated()) {
      await AdhesionTmp.update({ user: req.user.id }, { lastVisitedPage: 'payment' });
    }
    return res.view('pages/simulation/sim3b-subscribe', { type: req.param('offerType') });
  },

  async terms(req, res) {
    if (req.isAuthenticated()) {
      await AdhesionTmp.update({ user: req.user.id }, { lastVisitedPage: 'terms' });
    }
    return res.view('pages/simulation/sim3c-subscribe', { type: req.param('offerType') });
  },

  async signSepa(req, res) {
    if (req.isAuthenticated()) {
      await AdhesionTmp.update({ user: req.user.id }, { lastVisitedPage: 'sign-sepa' });
    }
    return res.view('pages/simulation/sim3d-subscribe-sepa', { type: req.param('offerType') });
  },

  async signBc(req, res) {
    if (req.isAuthenticated()) {
      await AdhesionTmp.update({ user: req.user.id }, { lastVisitedPage: 'sign-bc' });
    }
    return res.view('pages/simulation/sim3d-subscribe-sepa', { type: req.param('offerType') });
  },

  async signSummary(req, res) {
    if (req.isAuthenticated()) {
      await AdhesionTmp.update({ user: req.user.id }, { lastVisitedPage: 'sign-summary' });
    }
    return res.view('pages/simulation/sim3e-subscribe', { type: req.param('offerType') });
  },

  async signCancel(req, res) {
    if (req.isAuthenticated()) {
      await AdhesionTmp.update({ user: req.user.id }, { lastVisitedPage: 'sign-cancel' });
    }
    return res.view('pages/simulation/sim3f-subscribe', { type: req.param('offerType') });
  },

  async quote(req, res) {
    if (req.isAuthenticated()) {
      await AdhesionTmp.update({ user: req.user.id }, { lastVisitedPage: 'quote' });
    }
    return res.view('pages/simulation/sim4a-quote', { type: req.param('offerType') });
  },

  async save(req, res) {
    if (req.isAuthenticated()) {
      await AdhesionTmp.update({ user: req.user.id }, { lastVisitedPage: 'save' });
    }
    return res.view('pages/simulation/sim5a-save', { type: req.param('offerType') });
  },

  async paymentBc(req, res) {
    if (req.isAuthenticated()) {
      await AdhesionTmp.update({ user: req.user.id }, { lastVisitedPage: 'payment-bc' });
    }
    return res.view('pages/simulation/sim-bc-payment', { type: req.param('offerType') });
  },

  async paymentBcCancel(req, res) {
    if (req.isAuthenticated()) {
      await AdhesionTmp.update({ user: req.user.id }, { lastVisitedPage: 'payment-bc-cancel' });
    }
    return res.view('pages/simulation/sim-bc-payment-cancel', { type: req.param('offerType') });
  },

  //BackOffice

  indexAdmin(req, res) {
    return res.view('admin/index', { layout: 'layouts/admin' });
  },
  loginAdmin(req, res) {
    return res.view('admin/login', {
      layout: 'layouts/admin',
      locals: { success: undefined, error: undefined },
    });
  },
  indexUsersAdmin(req, res) {
    return res.view('admin/users/index', { layout: 'layouts/admin' });
  },
  createUserAdmin(req, res) {
    return res.view('admin/users/create', { layout: 'layouts/admin' });
  },
  updateUserAdmin(req, res) {
    return res.view('admin/users/update', { layout: 'layouts/admin' });
  },
};
