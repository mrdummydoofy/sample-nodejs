/**
 * SepaController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const moment = require('moment');

module.exports = {
  async dailyExport(req, res) {
    // excel file export operation
    let firstPaymentDay = null;
    let recurringPaymentDay = null;

    if (moment(req.param('bankingDate')).isValid()) {
      if (moment(req.param('bankingDate')).isAfter(moment())) {
        res.status(400).json({ error: 'future dates are not allowed' });
      } else {
        // dailyExport logic
        if (moment(req.param('bankingDate')).isValid()) {
          firstPaymentDay = moment(req.param('bankingDate'))
            .subtract(1, 'days')
            .format('YYYY-MM-DD');
          recurringPaymentDay = moment(req.param('bankingDate'))
            .add(2, 'days')
            .format('YYYY-MM-DD 00:00:00');
        } else {
          firstPaymentDay = moment()
            .subtract(1, 'days')
            .format('YYYY-MM-DD');
          recurringPaymentDay = moment()
            .add(2, 'days')
            .format('YYYY-MM-DD 00:00:00');
        }

        const contractsFirstPayments = await Contract.find({
          signatureStatus: 'completed',
          contractStatus: 'active',
          isContract: true,
          isSigned: true,
          firstPaymentType: 'sepa',
        })
          .where({ signatureDate: { startsWith: firstPaymentDay } })
          .populate('user')
          .populate('quotation');

        const currentMonth = moment(recurringPaymentDay).month() + 1;

        const recurringPaymentDayDay = moment(recurringPaymentDay).date() - 1;
        const monLimit = moment(recurringPaymentDay)
          .subtract(1, 'months')
          .subtract(recurringPaymentDayDay, 'days')
          .format('YYYY-MM-DD 00:00:00');
        const triLimit = moment(recurringPaymentDay)
          .subtract(3, 'months')
          .subtract(recurringPaymentDayDay, 'days')
          .format('YYYY-MM-DD 00:00:00');
        const semLimit = moment(recurringPaymentDay)
          .subtract(6, 'months')
          .subtract(recurringPaymentDayDay, 'days')
          .format('YYYY-MM-DD 00:00:00');
        const annLimit = moment(recurringPaymentDay)
          .subtract(12, 'months')
          .subtract(recurringPaymentDayDay, 'days')
          .format('YYYY-MM-DD 00:00:00');

        let CONTRACTS_RECURRINGS_SQL = '';
        let contractsRecurrings = null;
        let totalCashQuotation = null;
        let totalQuotation = null;
        let totalTransactions = null;
        let sepaExport = null;
        let SepaTransactionBatch = [];
        let numInfo = 0;

        if (_.includes([2, 3, 5, 6, 8, 9, 11, 12], currentMonth)) {
          CONTRACTS_RECURRINGS_SQL = `
            SELECT c.*, p.pickingDate, q.* FROM contract as c LEFT JOIN user as u on c.user = u.id LEFT JOIN paymentmethod as p on u.id = p.user 
            LEFT JOIN quotation as q on q.contract = c.id
            Where signatureStatus = 'completed'
            AND contractStatus = 'active'
            And isSigned = true
            AND isContract = true
            AND firstPaymentType = 'sepa'
            AND p.pickingDate = $1
            AND ( (q.fraction = 12 AND c.effectDate <= $2)    
          )`;

          // Send it to the database.
          contractsRecurrings = await sails.sendNativeQuery(CONTRACTS_RECURRINGS_SQL, [
            moment(recurringPaymentDay).date(),
            monLimit,
            triLimit,
            semLimit,
            annLimit,
          ]);
        } else if (_.includes([4, 10], currentMonth)) {
          CONTRACTS_RECURRINGS_SQL = `
            SELECT c.*, p.pickingDate, q.* FROM contract as c LEFT JOIN user as u on c.user = u.id LEFT JOIN paymentmethod as p on u.id = p.user
            LEFT JOIN quotation as q on q.contract = c.id
            Where signatureStatus = 'completed'
            AND contractStatus = 'active'
            And isSigned = true
            AND isContract = true
            AND firstPaymentType = 'sepa'
            AND p.pickingDate = $1
            AND ( (q.fraction = 12 AND c.effectDate <= $2) 
                  OR (q.fraction = 4 AND c.effectDate <= $3)     
          )`;

          // Send it to the database.
          contractsRecurrings = await sails.sendNativeQuery(CONTRACTS_RECURRINGS_SQL, [
            moment(recurringPaymentDay).date(),
            monLimit,
            triLimit,
          ]);
        } else if (_.includes([7], currentMonth)) {
          CONTRACTS_RECURRINGS_SQL = `
            SELECT c.*, p.pickingDate, q.* FROM contract as c LEFT JOIN user as u on c.user = u.id LEFT JOIN paymentmethod as p on u.id = p.user
            LEFT JOIN quotation as q on q.contract = c.id
            Where signatureStatus = 'completed'
            AND contractStatus = 'active'
            And isSigned = true
            AND isContract = true
            AND firstPaymentType = 'sepa'
            AND p.pickingDate = $1
            AND ( (q.fraction = 12 AND c.effectDate <= $2) 
                  OR (q.fraction = 4 AND c.effectDate <= $3)
                  OR (q.fraction = 2 AND c.effectDate <= $4)     
          )`;

          // Send it to the database.
          contractsRecurrings = await sails.sendNativeQuery(CONTRACTS_RECURRINGS_SQL, [
            moment(recurringPaymentDay).date(),
            monLimit,
            triLimit,
            semLimit,
          ]);
        } else if (_.includes([1], currentMonth)) {
          CONTRACTS_RECURRINGS_SQL = `
            SELECT c.*, p.pickingDate, q.* FROM contract as c LEFT JOIN user as u on c.user = u.id LEFT JOIN paymentmethod as p on u.id = p.user
            LEFT JOIN quotation as q on q.contract = c.id
            Where signatureStatus = 'completed'
            AND contractStatus = 'active'
            And isSigned = true
            AND isContract = true
            AND firstPaymentType = 'sepa'
            AND p.pickingDate = $1
            AND ( (q.fraction = 12 AND c.effectDate <= $2) 
                  OR (q.fraction = 4 AND c.effectDate <= $3)
                  OR (q.fraction = 2 AND c.effectDate <= $4)
                  OR (q.fraction = 1 AND c.effectDate <= $5)        
          )`;

          // Send it to the database.
          contractsRecurrings = await sails.sendNativeQuery(CONTRACTS_RECURRINGS_SQL, [
            moment(recurringPaymentDay).date(),
            monLimit,
            triLimit,
            semLimit,
            annLimit,
          ]);
        } else {
          //do nothing
          console.log('Month not matching');
        }

        if (
          await SepaExport.findOne({
            filename: `prelevements_${moment(firstPaymentDay)
              .add(1, 'days')
              .format('YYYY-MM-DD')}.xml`,
          })
        ) {
          // do nothing
          if (_.isUndefined(req.param('nomail')) || req.param('nomail').toLowerCase() === 'false') {
            Mailer.sendExcelMail(firstPaymentDay);
          }
          res.status(200).json({ message: 'xml and excel files already exported' });
        } else {
          // do the database fill and generate files
          contractsRecurrings = contractsRecurrings.rows;

          totalCashQuotation = _.map(
            contractsFirstPayments,
            contract => contract.quotation[0].cashQuotation,
          );
          totalCashQuotation = _.reduce(totalCashQuotation, (acc, value) => acc + value, 0);
          totalQuotation = _.map(contractsRecurrings, contract => contract.ttc);
          totalQuotation = _.reduce(totalQuotation, (acc, value) => acc + value, 0);
          totalTransactions = totalCashQuotation + totalQuotation;

          if (totalTransactions !== 0) {
            sepaExport = await SepaExport.create({
              day: moment().format('YYYY-MM-DDThh:mm:ss'),
              totalCashQuotation: totalCashQuotation.toFixed(2),
              totalQuotation: totalQuotation.toFixed(2),
              nbCashQuotation: contractsFirstPayments.length,
              nbQuotation: contractsRecurrings.length,
              nbTransaction: contractsRecurrings.length + contractsFirstPayments.length,
              totalTransaction: totalTransactions.toFixed(2),
              messageId: `ORDVETOPTIM-COV-SDD-${moment(firstPaymentDay)
                .add(1, 'days')
                .format('YY')}${(
                '00' +
                moment(firstPaymentDay)
                  .add(1, 'days')
                  .format('DDD')
              ).slice(-3)}-001`,
              paymentInfoNew:
                contractsFirstPayments.length > 0
                  ? `VETOPTIMSEPALOT${moment(firstPaymentDay)
                      .add(1, 'days')
                      .format('YYYYMMDD')}-0${++numInfo}`
                  : null,
              paymentInfoRecurring:
                contractsRecurrings.length > 0
                  ? `VETOPTIMSEPALOT${moment(firstPaymentDay)
                      .add(1, 'days')
                      .format('YYYYMMDD')}-0${++numInfo}`
                  : null,
              filename: `prelevements_${moment(firstPaymentDay)
                .add(1, 'days')
                .format('YYYY-MM-DD')}.xml`,
            }).fetch();
          }

          for (let i = 0; i < contractsFirstPayments.length; i++) {
            const user = await User.find({ id: contractsFirstPayments[i].user.id })
              .populate('contract')
              .populate('address')
              .populate('payment');

            SepaTransactionBatch.push({
              user: contractsFirstPayments[i].user.id,
              contract: contractsFirstPayments[i].id,
              type: 'FRST',
              endToEndId: `DDVETOPTIMCOV${contractsFirstPayments[i].reference}TX0001`,
              amount: contractsFirstPayments[i].quotation[0].cashQuotation,
              sigDate: moment(contractsFirstPayments[i].signatureDate).format('YYYY-MM-DD'),
              IBAN: user[0].payment[0].IBAN,
              BIC: user[0].payment[0].BIC,
              exactTitle: user[0].payment[0].exactTitle,
              postalCode: user[0].address[0].postalCode,
              city: user[0].address[0].city,
              address: user[0].address[0].address,
              sepaExport: sepaExport.id,
            });
          }

          for (let i = 0; i < contractsRecurrings.length; i++) {
            const user = await User.find({ id: contractsRecurrings[i].user })
              .populate('contract')
              .populate('address')
              .populate('payment');
            const lastTransaction = await SepaTransaction.find({
              user: contractsRecurrings[i].user,
            })
              .limit(1)
              .select(['endToEndId'])
              .sort('endToEndId DESC');

            let txId = null;
            if (lastTransaction.length > 0 && lastTransaction[0].endToEndId !== null) {
              // generate tx reference id
              if (
                (parseInt(lastTransaction[0].endToEndId.split('TX')[1]) + 1).toString().length > 2
              ) {
                txId = (parseInt(lastTransaction[0].endToEndId.split('TX')[1]) + 1).toString();
              } else if (
                (parseInt(lastTransaction[0].endToEndId.split('TX')[1]) + 1).toString().length === 2
              ) {
                txId =
                  '00' + (parseInt(lastTransaction[0].endToEndId.split('TX')[1]) + 1).toString();
              } else {
                txId =
                  '000' + (parseInt(lastTransaction[0].endToEndId.split('TX')[1]) + 1).toString();
              }
            } else {
              txId = '0002';
            }

            SepaTransactionBatch.push({
              user: contractsRecurrings[i].user,
              contract: contractsRecurrings[i].id,
              type: 'RCUR',
              endToEndId: `DDVETOPTIMCOV${contractsRecurrings[i].reference}TX${txId}`,
              amount: contractsRecurrings[i].ttc,
              sigDate: moment(contractsRecurrings[i].signatureDate).format('YYYY-MM-DD'),
              IBAN: user[0].payment[0].IBAN,
              BIC: user[0].payment[0].BIC,
              exactTitle: user[0].payment[0].exactTitle,
              postalCode: user[0].address[0].postalCode,
              city: user[0].address[0].city,
              address: user[0].address[0].address,
              sepaExport: sepaExport.id,
            });
          }

          // chunk the SepaTransactionBatch array to 100
          SepaTransactionBatch = _.chunk(SepaTransactionBatch, 100);

          for (let i = 0; i < SepaTransactionBatch.length; i++) {
            await SepaTransaction.createEach(SepaTransactionBatch[i]);
          }

          if (contractsFirstPayments.length + contractsRecurrings.length > 0) {
            await ExcelBanking.buildReport(
              contractsFirstPayments,
              contractsRecurrings,
              recurringPaymentDay,
            );
            await sepaXmlExport.build(sepaExport.id, recurringPaymentDay);
            if (contractsFirstPayments.length > 0) {
              await csvExport.build(contractsFirstPayments, recurringPaymentDay);
            }
            // nomail param set to true
            if (
              _.isUndefined(req.param('nomail')) ||
              req.param('nomail').toLowerCase() === 'false'
            ) {
              Mailer.sendExcelMail(firstPaymentDay);
            }

            res.status(200).json({ message: 'xml and excel files exported successfully' });
          } else {
            if (
              _.isUndefined(req.param('nomail')) ||
              req.param('nomail').toLowerCase() === 'false'
            ) {
              Mailer.sendExcelEmptyMail(moment(recurringPaymentDay).format('YYYY-MM-DD'));
            }
            res.status(400).json({ error: 'no data found' });
          }
        }
      }
    } else {
      res.status(400).json({ error: 'invalid date' });
    }
  },
};
