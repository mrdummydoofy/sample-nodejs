/**
 * SimulationcommonController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const path = require('path');
const moment = require('moment');
const crypto = require('crypto');

moment.suppressDeprecationWarnings = true;

module.exports = {
  // Subscription Infos
  async subscriptionInfos(req, res) {
    if (
      !_.isUndefined(req.param('civility')) ||
      !_.isUndefined(req.param('firstName')) ||
      !_.isUndefined(req.param('lastName')) ||
      !_.isUndefined(req.param('mobile')) ||
      !_.isUndefined(req.param('siret')) ||
      !_.isUndefined(req.param('legalForm')) ||
      !_.isUndefined(req.param('startActivityDate')) ||
      !_.isUndefined(req.param('companyName')) ||
      !_.isUndefined(req.param('address')) ||
      // !_.isUndefined(req.param('orderNumber')) ||
      !_.isUndefined(req.param('structure')) ||
      !_.isUndefined(req.param('centralRef')) ||
      !_.isUndefined(req.param('individualCabinet')) ||
      !_.isUndefined(req.param('liberalCollaborator')) ||
      !_.isUndefined(req.param('email')) ||
      !_.isUndefined(req.param('postalCode')) ||
      !_.isUndefined(req.param('city')) ||
      !_.isUndefined(req.param('type')) ||
      !_.isUndefined(req.param('chosenMenu')) ||
      !_.isUndefined(req.param('endStatement')) ||
      !_.isUndefined(req.param('currentContract')) ||
      !_.isUndefined(req.param('endStatementDateMonth')) ||
      !_.isUndefined(req.param('endStatementDateDay'))
    ) {
      req.session.title = req.param('civility');
      req.session.name = req.param('firstName');
      req.session.lastname = req.param('lastName');
      req.session.phone = req.param('phone');
      req.session.mobile = req.param('mobile');
      req.session.email = req.param('email');
      req.session.siren = req.param('siren');
      req.session.legalForm = req.param('legalForm');
      req.session.startActivityDate = `${req.param('startActivityDateYear')}-${req.param(
        'startActivityDateMonth',
      )}-${req.param('startActivityDateDay')}`;
      req.session.companyName = req.param('companyName');
      req.session.structure = req.param('structure') === 'oui' ? true : false;
      req.session.centralRef = req.param('centralRef') === 'oui' ? true : false;
      req.session.centralRefName = req.param('centralRefName') ? req.param('centralRefName') : null;
      req.session.individualCabinet = req.param('individualCabinet') === 'oui' ? true : false;
      req.session.liberalCollaborator = req.param('liberalCollaborator') === 'oui' ? true : false;
      req.session.alarm = req.param('alarm') === 'oui' ? true : false;
      req.session.sinistreConnaissance = req.param('sinistreConnaissance') === 'oui' ? true : false;
      req.session.sinistreConnaissance5Years =
        req.param('sinistreConnaissance5Years') === 'oui' ? true : false;
      req.session.approvedAlarm = req.param('approvedAlarm') === 'oui' ? true : false;
      req.session.insuranceCancel = req.param('insuranceCancel') === 'oui' ? true : false;
      req.session.cancelReasonsSinister = req.param('cancelReasonsSinister')
        ? req.param('cancelReasonsSinister')
        : null;
      req.session.cancelReasonsPayment = req.param('cancelReasonsPayment')
        ? req.param('cancelReasonsPayment')
        : null;
      req.session.quotationsUpdated =
        req.param('quotationsUpdated') === 'oui'
          ? req.param('cancelReasons') === 'sinistre'
            ? false
            : true
          : false;
      req.session.endStatement = req.param('endStatement') === 'oui' ? true : false;
      req.session.endStatementDate = `${req.param('endStatementDateMonth')}-${req.param(
        'endStatementDateDay',
      )}`;
      req.session.currentContract = req.param('currentContract') === 'oui' ? true : false;
      req.session.currentContractDate = req.session.currentContract
        ? `${req.param('currentContractDateYear')}-${req.param(
            'currentContractDateMonth',
          )}-${req.param('currentContractDateDay')}`
        : null;
      req.session.currentContractCancel =
        req.param('currentContractCancel') === 'oui' ? true : false;
      req.session.franchise = req.param('franchise');
      req.session.address = req.param('address');
      req.session.postalCode = req.param('postalCode');
      req.session.city = req.param('city');
      req.session.type = req.param('type');
      req.session.chosenMenu = req.param('chosenMenu');
      req.session.moment = moment;
      req.session.animalsValue = req.param('animalsValue') === 'oui' ? true : false;
      req.session.sinisterAmountCR = req.param('sinisterAmountCR');
      req.session.sinisterDateDayCR = req.param('sinisterDateDayCR');
      req.session.sinisterDateMonthCR = req.param('sinisterDateMonthCR');
      req.session.sinisterDateYearCR = req.param('sinisterDateYearCR');
      req.session.sinisterAmountPMR = req.param('sinisterAmountPMR');
      req.session.sinisterDateDayPMR = req.param('sinisterDateDayPMR');
      req.session.sinisterDateMonthPMR = req.param('sinisterDateMonthPMR');
      req.session.sinisterDateYearPMR = req.param('sinisterDateYearPMR');
      req.session.sinisterType = req.param('sinisterType');
      req.session.bdmSup = req.param('bdmSup') === 'oui' ? true : false;
      req.session.tenant = req.param('tenant') === 'oui' ? true : false;
      req.session.bdm = req.param('bdm')
        ? req.param('bdmSup') === 'non'
          ? 'limite20000'
          : req.param('bdm')
        : 'limite20000';
      req.session.surfaceLocaux = req.param('surfaceLocaux');
      req.session.contenuLocaux = req.param('contenuLocaux');
      req.session.indivAccident = req.param('indivAccident') === 'oui' ? true : false;
      req.session.indivAccidentChoix = req.session.indivAccident
        ? req.param('indivAccidentChoix')
        : undefined;
      req.session.indivEnfants = req.param('indivEnfants') === 'oui' ? true : false;
      req.session.rapatriement = req.param('rapatriement') === 'oui' ? true : false;
      req.session.rapatriementChoix = req.session.rapatriement
        ? req.param('rapatriementChoix')
        : undefined;
      req.session.capitauxPropres = req.param('capitauxPropres') === 'oui' ? true : false;
      req.session.resultatNet = req.param('resultatNet') === 'oui' ? true : false;

      req.session.chiffreAffaireCours = req.param('chiffreAffaireCours')
        ? parseInt(
            req
              .param('chiffreAffaireCours')
              .toString()
              .replace(/\s/g, ''),
          )
        : undefined;
      req.session.chiffreAffairePrev = req.param('chiffreAffairePrev')
        ? parseInt(
            req
              .param('chiffreAffairePrev')
              .toString()
              .replace(/\s/g, ''),
          )
        : undefined;
      req.session.totalActifAnnee = req.param('totalActifAnnee')
        ? parseInt(
            req
              .param('totalActifAnnee')
              .toString()
              .replace(/\s/g, ''),
          )
        : undefined;
      req.session.totalActifCours = req.param('totalActifCours')
        ? parseInt(
            req
              .param('totalActifCours')
              .toString()
              .replace(/\s/g, ''),
          )
        : undefined;
      req.session.totalActifPrev = req.param('totalActifPrev')
        ? parseInt(
            req
              .param('totalActifPrev')
              .toString()
              .replace(/\s/g, ''),
          )
        : undefined;
      req.session.capitauxAnnee = req.param('capitauxAnnee')
        ? parseInt(
            req
              .param('capitauxAnnee')
              .toString()
              .replace(/\s/g, ''),
          )
        : undefined;
      req.session.capitauxCours = req.param('capitauxCours')
        ? parseInt(
            req
              .param('capitauxCours')
              .toString()
              .replace(/\s/g, ''),
          )
        : undefined;
      req.session.capitauxPrev = req.param('capitauxPrev')
        ? parseInt(
            req
              .param('capitauxPrev')
              .toString()
              .replace(/\s/g, ''),
          )
        : undefined;
      req.session.totalAnnee = req.param('totalAnnee')
        ? parseInt(
            req
              .param('totalAnnee')
              .toString()
              .replace(/\s/g, ''),
          )
        : undefined;
      req.session.totalCours = req.param('totalCours')
        ? parseInt(
            req
              .param('totalCours')
              .toString()
              .replace(/\s/g, ''),
          )
        : undefined;
      req.session.totalPrev = req.param('totalPrev')
        ? parseInt(
            req
              .param('totalPrev')
              .toString()
              .replace(/\s/g, ''),
          )
        : undefined;

      let redirectAdhesion = null;
      let redirectLoginCheck = null;
      let redirectMailCheck = null;

      redirectAdhesion = `adhesion-summary?offerType=${req.session.type}`;
      redirectLoginCheck = `login-check?offerType=${req.session.type}`;
      redirectMailCheck = `process-mail-check?offerType=${req.session.type}`;

      if (req.session.type === 'CR' || req.session.type === 'PMR' || req.session.type === 'PMRCR') {
        if (
          req.session.type === 'CR' &&
          ((req.param('insuranceCancel') === 'oui' &&
            req.param('cancelReasonsPayment') === 'non paiement' &&
            req.param('quotationsUpdated') === 'non') ||
            req.param('animalsValue') === 'oui' ||
            (req.param('sinistreConnaissance5Years') === 'oui' &&
              parseInt(req.param('sinisterNumbersCR')) >= 1))
        ) {
          if (req.isAuthenticated()) {
            await Utils.formInfosSubmitted(req);
          }
          return res.status(200).json({ redirect: 'subscription-cancel', target: undefined });
        } else if (
          req.session.type === 'PMRCR' &&
          (req.param('animalsValue') === 'oui' ||
            (req.param('sinistreConnaissance') === 'oui' &&
              (Object.keys(req.param('sinisterType'))
                .map(e => req.param('sinisterType')[e])
                .find(e => e === 'vol') ||
                Object.keys(req.param('sinisterType')).map(
                  e => req.param('sinisterType')[e] === 'incendies',
                ).length > 1 ||
                Object.keys(req.param('sinisterType')).map(
                  e => req.param('sinisterType')[e] === 'dégâts des eaux',
                ).length > 1 ||
                (Object.keys(req.param('sinisterType')).map(
                  e => req.param('sinisterType')[e] === 'dégâts des eaux',
                ).length > 1 &&
                  Object.keys(req.param('sinisterType')).map(
                    e => req.param('sinisterType')[e] === 'incendies',
                  ).length > 1))) ||
            ((req.param('insuranceCancel') === 'oui' &&
              req.param('cancelReasonsPayment') === 'non paiement' &&
              req.param('quotationsUpdated') === 'non') ||
              (req.param('sinistreConnaissance5Years') === 'oui' &&
                parseInt(req.param('sinisterNumbersCR')) >= 1)) ||
            parseInt(
              req
                .param('surfaceLocaux')
                .toString()
                .replace(/\s/g, ''),
            ) > 1000 ||
            parseInt(
              req
                .param('contenuLocaux')
                .toString()
                .replace(/\s/g, ''),
            ) > 400000 ||
            (req.param('bdmSup') === 'oui' && req.param('bdm') === 'reserveSiege'))
        ) {
          if (req.isAuthenticated()) {
            await Utils.formInfosSubmitted(req);
          }
          return res.status(200).json({ redirect: 'subscription-cancel', target: undefined });
        } else if (
          req.session.type === 'PMR' &&
          ((req.param('sinistreConnaissance') === 'oui' &&
            (Object.keys(req.param('sinisterType'))
              .map(e => req.param('sinisterType')[e])
              .find(e => e === 'vol') ||
              Object.keys(req.param('sinisterType')).map(
                e => req.param('sinisterType')[e] === 'incendies',
              ).length > 1 ||
              Object.keys(req.param('sinisterType')).map(
                e => req.param('sinisterType')[e] === 'dégâts des eaux',
              ).length > 1 ||
              (Object.keys(req.param('sinisterType')).map(
                e => req.param('sinisterType')[e] === 'dégâts des eaux',
              ).length > 1 &&
                Object.keys(req.param('sinisterType')).map(
                  e => req.param('sinisterType')[e] === 'incendies',
                ).length > 1))) ||
            (req.param('insuranceCancel') === 'oui' &&
              req.param('cancelReasonsPayment') === 'non paiement' &&
              req.param('quotationsUpdated') === 'non') ||
            parseInt(
              req
                .param('surfaceLocaux')
                .toString()
                .replace(/\s/g, ''),
            ) > 1000 ||
            parseInt(
              req
                .param('contenuLocaux')
                .toString()
                .replace(/\s/g, ''),
            ) > 400000 ||
            (req.param('bdmSup') === 'oui' && req.param('bdm') === 'reserveSiege'))
        ) {
          if (req.isAuthenticated()) {
            await Utils.formInfosSubmitted(req);
          }
          return res.status(200).json({ redirect: 'subscription-cancel', target: undefined });
        }

        if (
          !req.session.currentContract ||
          (req.session.currentContract &&
            req.session.currentContractCancel &&
            req.param('currentContractDateMinus2Months')) ||
          (req.session.currentContract && req.param('currentContractDateMinus2Months') === false)
        ) {
          if (!req.isAuthenticated()) {
            const currentUser = await User.findOne({ email: req.param('email') });

            if (currentUser) {
              return res.status(200).json({ redirect: redirectLoginCheck, target: undefined });
            } else {
              const generatedPassword = crypto.randomBytes(5).toString('hex');
              req.session.password = generatedPassword;

              // account creation
              const createdUser = await User.create({
                login: req.param('email'),
                password: generatedPassword,
                email: req.param('email'),
                mobile: req.param('mobile'),
                title: req.param('civility'),
                name: req.param('firstName'),
                lastname: req.param('lastName'),
                phone: req.param('phone'),
              }).fetch();

              Mailer.sendProcessActivationMail(createdUser, generatedPassword);
              return res.status(200).json({ redirect: redirectMailCheck, target: undefined });
            }
          } else {
            await Utils.formInfosSubmitted(req);
            return res.status(200).json({ redirect: redirectAdhesion, target: undefined });
          }
        }

        await Utils.formInfosSubmitted(req);
        let updatedData = await User.findOne({ id: req.user.id })
          .populate('customer')
          .populate('adhesion');
        updatedData.adhesion = await AdhesionTmp.find({ user: req.user.id })
          .populate('CRSinisterStatement')
          .populate('PMRSinisterStatement');
        Mailer.sendHelpMail({ email: req.session.email }, req.user, undefined);
        Mailer.sendSupportMail(updatedData, moment, req);
        return res.status(200).json({ redirect: 'subscription-cancel', target: undefined });
      } else if (req.isAuthenticated() && req.session.type === 'COMMON') {
        await Utils.formInfosSubmitted(req);
        return res.status(200).json({ redirect: 'informations-success', target: undefined });
      } else if (req.session.type === 'MD' || req.session.type === 'RCMS') {
        if (!req.isAuthenticated()) {
          if (req.session.type === 'RCMS') {
            if (
              req.param('capitauxPropres') === 'non' ||
              req.param('resultatNet') === 'non' ||
              req.param('legalForm') === 'SARL' ||
              req.param('legalForm') === 'EI' ||
              req.param('legalForm') === 'SASU' ||
              req.param('legalForm') === 'SCOP' ||
              req.param('legalForm') === 'SNC' ||
              req.param('legalForm') === 'EARL' ||
              req.param('legalForm') === 'EIRL' ||
              req.param('legalForm') === 'GAEC' ||
              req.param('legalForm') === 'GEIE' ||
              req.param('legalForm') === 'GIE' ||
              req.param('legalForm') === 'SC' ||
              req.param('legalForm') === 'SCA' ||
              req.param('legalForm') === 'SCI' ||
              req.param('legalForm') === 'SCIC' ||
              req.param('legalForm') === 'SCM' ||
              req.param('legalForm') === 'SCP' ||
              req.param('legalForm') === 'SCS' ||
              req.param('legalForm') === 'SEL' ||
              req.param('legalForm') === 'SEM' ||
              req.param('legalForm') === 'SEML' ||
              req.param('legalForm') === 'SEP' ||
              req.param('legalForm') === 'SICA'
            ) {
              Mailer.sendSupportMail(undefined, moment, req);
              return res.status(200).json({ redirect: 'subscription-cancel', target: undefined });
            }
          }
          const currentUser = await User.findOne({ email: req.param('email') });
          if (currentUser) {
            return res.status(200).json({ redirect: redirectLoginCheck, target: undefined });
          } else {
            const generatedPassword = crypto.randomBytes(5).toString('hex');
            req.session.password = generatedPassword;

            // account creation
            const createdUser = await User.create({
              login: req.param('email'),
              password: generatedPassword,
              email: req.param('email'),
              mobile: req.param('mobile'),
              title: req.param('civility'),
              name: req.param('firstName'),
              lastname: req.param('lastName'),
              phone: req.param('phone'),
            }).fetch();

            Mailer.sendProcessActivationMail(createdUser, generatedPassword);
            return res.status(200).json({ redirect: redirectMailCheck, target: undefined });
          }
        } else {
          if (req.session.type === 'RCMS') {
            if (
              req.param('capitauxPropres') === 'non' ||
              req.param('resultatNet') === 'non' ||
              req.param('legalForm') === 'SARL' ||
              req.param('legalForm') === 'EI' ||
              req.param('legalForm') === 'SASU' ||
              req.param('legalForm') === 'SCOP' ||
              req.param('legalForm') === 'SNC' ||
              req.param('legalForm') === 'EARL' ||
              req.param('legalForm') === 'EIRL' ||
              req.param('legalForm') === 'GAEC' ||
              req.param('legalForm') === 'GEIE' ||
              req.param('legalForm') === 'GIE' ||
              req.param('legalForm') === 'SC' ||
              req.param('legalForm') === 'SCA' ||
              req.param('legalForm') === 'SCI' ||
              req.param('legalForm') === 'SCIC' ||
              req.param('legalForm') === 'SCM' ||
              req.param('legalForm') === 'SCP' ||
              req.param('legalForm') === 'SCS' ||
              req.param('legalForm') === 'SEL' ||
              req.param('legalForm') === 'SEM' ||
              req.param('legalForm') === 'SEML' ||
              req.param('legalForm') === 'SEP' ||
              req.param('legalForm') === 'SICA'
            ) {
              await Utils.formInfosSubmitted(req);
              let blockedUser = await User.findOne({ id: req.user.id }).populate('leader');
              Mailer.sendSupportMail(blockedUser, moment, req);
              return res.status(200).json({ redirect: 'subscription-cancel', target: undefined });
            }
          }
          await Utils.formInfosSubmitted(req);
          return res.status(200).json({ redirect: redirectAdhesion, target: undefined });
        }
      }
    } else {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
  },

  // update adhesion infos
  async updateAdhesion(req, res) {
    // params validation control
    if (
      !_.isUndefined(req.param('fraction')) ||
      !_.isUndefined(req.param('effectDate')) ||
      !_.isUndefined(req.param('deadlineDate')) ||
      !_.isUndefined(req.param('afterDeadlineDate')) ||
      !_.isUndefined(req.param('chosenQuotation')) ||
      !_.isUndefined(req.param('cashQuotation')) ||
      !_.isUndefined(req.param('franchise'))
    ) {
      const adhesion = await AdhesionTmp.findOne({
        user: req.user.id,
        offerType: req.param('offerType'),
      });

      if (adhesion) {
        await AdhesionTmp.update(
          { user: req.user.id, offerType: req.param('offerType') },
          {
            offerType: req.param('offerType'),
            fraction: req.param('fraction'),
            effectDate: req.param('effectDate'),
            deadlineDate: req.param('deadlineDate'),
            afterDeadlineDate: req.param('afterDeadlineDate'),
            chosenQuotation: req.param('chosenQuotation'),
            cashQuotation: req.param('cashQuotation'),
            turnover: req.param('chiffreAffaires'),
          },
        );
      } else {
        await AdhesionTmp.create({
          offerType: req.param('offerType'),
          fraction: req.param('fraction'),
          effectDate: req.param('effectDate'),
          deadlineDate: req.param('deadlineDate'),
          afterDeadlineDate: req.param('afterDeadlineDate'),
          chosenQuotation: req.param('chosenQuotation'),
          cashQuotation: req.param('cashQuotation'),
          turnover: req.param('chiffreAffaires'),
          user: req.user.id,
        });
      }

      await Customer.update({ user: req.user.id }, { franchise: req.param('franchise') });
      await Leader.update({ user: req.user.id }, { formula: req.param('formula') });

      return res.status(200).json({ message: 'mise à jour des informations avec succès' });
    } else {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
  },

  // payment informations store
  async paymentInfos(req, res) {
    // validation control
    if (
      !_.isUndefined(req.param('paymentDay')) ||
      !_.isUndefined(req.param('bic')) ||
      !_.isUndefined(req.param('iban')) ||
      !_.isUndefined(req.param('holder')) ||
      !_.isUndefined(req.param('exactTitle')) ||
      !_.isUndefined(req.param('from')) ||
      !_.isUndefined(req.param('paymentMethod'))
    ) {
      const allowedFileExtensions = ['.pdf', '.jpg', '.jpeg', '.png'];
      const offerType = Utils.getOfferTypeFr(req.param('from'));

      //validate iban
      if (!(await Utils.validateIBAN(req.param('iban')))) {
        return res.status(400).json({ error: 'IBAN invalide' });
      }

      if (req.param('updateRibFile')) {
        const ext = path.extname(req.file('ribFile')._files[0].stream.filename);
        const ribImg = `ribimg_${req.user.id}_${req.user.name}_${
          req.user.lastname
        }_${moment().format('YYYY-MM-DD_HH:mm:ss')}${ext}`;

        if (
          allowedFileExtensions.some(el => _.includes(ext.toLowerCase(), el)) &&
          req.file('ribFile')._files[0].stream.byteCount < 4000000
        ) {
          req.file('ribFile').upload(
            {
              dirname: path.join(__dirname, '..', '..', '.permanent', 'ribFiles'),
              saveAs: ribImg,
              maxBytes: 4000000,
            },
            async (err, uploadedFiles) => {
              // eslint-disable-line
              if (err) {
                return res.status(400).json({ error: err });
              }
              await Utils.saveOrUpdatePaymentMethod(req, res, ribImg, offerType);
            },
          );
        } else {
          return res.status(400).json({
            error:
              'Les extensions de fichier autorisées sont PDF, JPEG et PNG avec une taille max de 4 Mo',
          });
        }
      } else if (req.param('ribImg')) {
        await Utils.saveOrUpdatePaymentMethod(req, res, req.param('ribImg'), offerType);
      } else {
        const ext = path.extname(req.file('ribFile')._files[0].stream.filename);
        const ribImg = `ribimg_${req.user.id}_${req.user.name}_${
          req.user.lastname
        }_${moment().format('YYYY-MM-DD_HH:mm:ss')}${ext}`;

        if (
          allowedFileExtensions.some(el => _.includes(ext.toLowerCase(), el)) &&
          req.file('ribFile')._files[0].stream.byteCount < 4000000
        ) {
          req.file('ribFile').upload(
            {
              dirname: path.join(__dirname, '..', '..', '.permanent', 'ribFiles'),
              saveAs: ribImg,
              maxBytes: 4000000,
            },
            async (err, uploadedFiles) => {
              // eslint-disable-line
              if (err) {
                return res.status(400).json({ error: err });
              }
              await Utils.saveOrUpdatePaymentMethod(req, res, ribImg, offerType);
            },
          );
        } else {
          return res.status(400).json({
            error:
              'Les extensions de fichier autorisées sont PDF, JPEG et PNG avec une taille max de 4 Mo',
          });
        }
      }
    } else {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
  },

  // test pdf contract
  async testContractPdf(req, res) {
    const template = path.join(__dirname, '..', '..', 'views', 'pdf', 'contractTest.ejs');
    const pdfPath = path.join(__dirname, '..', '..', '.tmp', `testContract.pdf`);

    await PdfExport.generate(template, {}, pdfPath);
    const data = await require('util').promisify(require('fs').readFile)(pdfPath);
    res.contentType('application/pdf');
    return res.send(data);

    if (req.user.adhesion.length > 0 && !_.isUndefined(req.param('type'))) {
      const currentAdhesionByType = req.user.adhesion.filter(
        adhesion => adhesion.offerType === req.param('type'),
      );
      let template = null;
      if (req.param('type') === 'RC') {
        template = path.join(__dirname, '..', '..', 'views', 'pdf', 'vetoSEPA.ejs');
      } else if (req.param('type') === 'MRP') {
        template = path.join(__dirname, '..', '..', 'views', 'pdf', 'vetoSEPA.ejs');
      } else if (req.param('type') === 'MRPRC') {
        template = path.join(__dirname, '..', '..', 'views', 'pdf', 'vetoSEPA.ejs');
      }

      const pdfPath = path.join(
        __dirname,
        '..',
        '..',
        '.permanent',
        'rates',
        `vetoSEPA-${currentAdhesionByType[0].id}.pdf`,
      );

      await PdfExport.generate(
        template,
        {
          adhesion: currentAdhesionByType[0],
          user: req.user,
          moment: moment,
          period: Utils.calculatePeriod,
          currency: require('currency-formatter'),
          Warranty: Warranty,
          Quotation: Quotation,
          indiceFFB: '988,1 €',
          contractRef: 'xxxxxx',
          customerRef: 'xxxxxx',
          tauxChiffreAffaires: 0.055,
        },
        pdfPath,
      );

      const data = await require('util').promisify(require('fs').readFile)(pdfPath);
      res.contentType('application/pdf');
      return res.send(data);
    } else {
      return res.status(400).json({ error: 'Aucune simulation trouvée' });
    }
  },

  // send rate mail
  async rateEmail(req, res) {
    if (req.user.adhesion.length > 0 && !_.isUndefined(req.param('type'))) {
      let template = null;
      let adviceTemplate = null;
      let filePostfix = null;
      let currentAdhesionByType = null;
      let advicepdfFile = null;
      let pdfFile = null;

      currentAdhesionByType = req.user.adhesion.filter(
        adhesion => adhesion.offerType === req.param('type'),
      );

      if (req.param('type') === 'MRP') {
        template = path.join(__dirname, '..', '..', 'views', 'pdf', 'devisMRP.ejs');
        adviceTemplate = path.join(__dirname, '..', '..', 'views', 'pdf', 'devoirConseilMRP.ejs');
        filePostfix = 'MRP-';
      } else if (req.param('type') === 'MRP + RC') {
        template = path.join(__dirname, '..', '..', 'views', 'pdf', 'devisMRPRC.ejs');
        adviceTemplate = path.join(__dirname, '..', '..', 'views', 'pdf', 'devoirConseilMRPRC.ejs');
        filePostfix = 'MRPRC-';
      } else if (req.param('type') === 'RC') {
        template = path.join(__dirname, '..', '..', 'views', 'pdf', 'devisRC.ejs');
        adviceTemplate = path.join(__dirname, '..', '..', 'views', 'pdf', 'devoirConseilRC.ejs');
        filePostfix = 'RC-';
      } else if (req.param('type') === 'MD') {
        filePostfix = 'MD-';
      } else if (req.param('type') === 'RCMS') {
        template = path.join(__dirname, '..', '..', 'views', 'pdf', 'devisRCMS.ejs');
        filePostfix = 'RCMS-';
      }

      filePostfix =
        filePostfix +
        moment()
          .unix()
          .toString();

      const pdfPath = path.join(
        __dirname,
        '..',
        '..',
        '.permanent',
        'rates',
        `devis-vetoptim-${req.user.lastname.replace(/ /g, '-')}-${req.user.name.replace(
          / /g,
          '-',
        )}-${filePostfix}${currentAdhesionByType[0].id}.pdf`,
      );

      const advicePdfPath = path.join(
        __dirname,
        '..',
        '..',
        '.permanent',
        'rates',
        `conseil-vetoptim-${req.user.lastname.replace(/ /g, '-')}-${req.user.name.replace(
          / /g,
          '-',
        )}-${filePostfix}${currentAdhesionByType[0].id}.pdf`,
      );

      await Utils.generateRateReference(currentAdhesionByType[0]);
      currentAdhesionByType = await AdhesionTmp.find({ id: currentAdhesionByType[0].id })
        .populate('CRSinisterStatement')
        .populate('PMRSinisterStatement');

      const currentCustomerRef = req.user.adhesion.filter(
        adhesion => adhesion.customerReference !== 'pas encore disponible',
      );

      if (currentCustomerRef.length > 0) {
        currentAdhesionByType[0].customerReference = currentCustomerRef[0].customerReference;
      }

      if (req.param('type') !== 'MD' && req.param('type') !== 'RCMS') {
        pdfFile = await PdfExport.generate(
          template,
          {
            adhesion: currentAdhesionByType[0],
            user: req.user,
            moment: moment,
            period: Utils.calculatePeriod,
            currency: require('currency-formatter'),
            Warranty: Warranty,
            Quotation: Quotation,
            indiceFFB: '988,1 €',
            tauxChiffreAffaires: 0.055,
          },
          pdfPath,
        );

        advicepdfFile = await PdfExport.generate(
          adviceTemplate,
          {
            adhesion: currentAdhesionByType[0],
            user: req.user,
            moment: moment,
            period: Utils.calculatePeriod,
            currency: require('currency-formatter'),
            Warranty: Warranty,
            Quotation: Quotation,
            indiceFFB: '988,1 €',
            tauxChiffreAffaires: 0.055,
          },
          advicePdfPath,
        );
      } else if (req.param('type') === 'MD') {
        await Utils.generateMDRate(
          req,
          currentAdhesionByType,
          `devis-vetoptim-${req.user.lastname.replace(/ /g, '-')}-${req.user.name.replace(
            / /g,
            '-',
          )}-${filePostfix}${currentAdhesionByType[0].id}.pdf`,
        );
        pdfFile = await require('util').promisify(require('fs').readFile)(pdfPath);
      } else if (req.param('type') === 'RCMS') {
        await Utils.generateRCMSRate(
          req,
          currentAdhesionByType,
          `devis-vetoptim-${req.user.lastname.replace(/ /g, '-')}-${req.user.name.replace(
            / /g,
            '-',
          )}-${filePostfix}${currentAdhesionByType[0].id}.pdf`,
        );
        pdfFile = await require('util').promisify(require('fs').readFile)(pdfPath);
      }

      await AdhesionTmp.update(
        { user: req.user.id, offerType: req.param('type') },
        {
          rateFileName: `devis-vetoptim-${req.user.lastname.replace(
            / /g,
            '-',
          )}-${req.user.name.replace(/ /g, '-')}-${filePostfix}${currentAdhesionByType[0].id}.pdf`,
          adviceFileName:
            req.param('type') !== 'MD' && req.param('type') !== 'RCMS'
              ? `conseil-vetoptim-${req.user.lastname.replace(/ /g, '-')}-${req.user.name.replace(
                  / /g,
                  '-',
                )}-${filePostfix}${currentAdhesionByType[0].id}.pdf`
              : undefined,
        },
      );

      Mailer.sendQuotationMail(
        req.user,
        pdfFile,
        `devis-vetoptim-${req.user.lastname.replace(/ /g, '-')}-${req.user.name.replace(
          / /g,
          '-',
        )}-${filePostfix}${currentAdhesionByType[0].id}.pdf`,
        req.param('type') !== 'MD' && req.param('type') !== 'RCMS' ? advicepdfFile : undefined,
        req.param('type') !== 'MD' && req.param('type') !== 'RCMS'
          ? `conseil-vetoptim-${req.user.lastname.replace(/ /g, '-')}-${req.user.name.replace(
              / /g,
              '-',
            )}-${filePostfix}${currentAdhesionByType[0].id}.pdf`
          : undefined,
        req.param('type'),
      );
      return res.status(200).json({
        rateFileName: `devis-vetoptim-${req.user.lastname.replace(
          / /g,
          '-',
        )}-${req.user.name.replace(/ /g, '-')}-${filePostfix}${currentAdhesionByType[0].id}.pdf`,
      });
    } else {
      return res.status(400).json({ error: 'Aucune simulation trouvée' });
    }
  },

  // send continue later mail
  async continueEmail(req, res) {
    if (req.user.adhesion.length > 0) {
      const adhesion = await AdhesionTmp.update(
        { user: req.user.id, offerType: req.param('offerType') },
        { continueLater: true },
      ).fetch();
      Mailer.sendContinueMail(req.user, adhesion[0]);
      return res.status(200).json({ message: 'Email envoyé avec succès' });
    } else {
      return res.status(400).json({ error: 'Aucune simulation trouvée' });
    }
  },

  // set continue later to false in adhesiontmp
  async toggleContinueLaterOnce(req, res) {
    if (req.user.adhesion.length > 0) {
      if (!_.isUndefined(req.param('continueLater')) || req.param('continueLater') === false) {
        await AdhesionTmp.update(
          { user: req.user.id, offerType: req.param('offerType') },
          { continueLater: false },
        );
        return res.status(200).json({ message: 'Demande sauvegardée avec succès' });
      } else {
        return res.status(400).json({ error: 'Action interdite' });
      }
    } else {
      return res.status(400).json({ error: 'Aucune simulation trouvée' });
    }
  },

  // set show popup continue later to false in adhesiontmp
  async toggleContinueLaterOncePopup(req, res) {
    if (req.user.adhesion.length > 0) {
      if (
        !_.isUndefined(req.param('continueLaterShowPopup')) ||
        req.param('continueLaterShowPopup') === false
      ) {
        await AdhesionTmp.update(
          { user: req.user.id, offerType: req.param('offerType') },
          { continueLaterShowPopup: false },
        );
        return res.status(200).json({ message: 'Popup modifié avec succès' });
      } else {
        return res.status(400).json({ error: 'Action interdite' });
      }
    } else {
      return res.status(400).json({ error: 'Aucune simulation trouvée' });
    }
  },

  //save help request
  async saveHelp(req, res) {
    let offerType = null;
    if (
      !_.isUndefined(req.param('phone')) ||
      !_.isUndefined(req.param('timing')) ||
      !_.isUndefined(req.param('title')) ||
      !_.isUndefined(req.param('name')) ||
      !_.isUndefined(req.param('lastname')) ||
      !_.isUndefined(req.param('email'))
    ) {
      let help = null;
      if (req.param('offerType') === 'CR') {
        offerType = 'RC';
      }
      if (req.param('offerType') === 'PMR') {
        offerType = 'MRP';
      }
      if (req.param('offerType') === 'CRPMR') {
        offerType = 'RC + MRP';
      }
      if (req.param('offerType') === 'PMRCR') {
        offerType = 'MRP + RC';
      }
      if (req.param('offerType') === 'MD') {
        offerType = 'MD';
      }
      if (req.param('offerType') === 'RCMS') {
        offerType = 'RCMS';
      }
      if (req.param('timing') === 'immediately') {
        help = await Help.create({
          phone: req.param('phone'),
          date: moment().format('YYYY-MM-DD'),
          hour: moment().format('HH:mm'),
          choice: req.param('timing'),
          title: req.param('title'),
          name: req.param('name'),
          lastname: req.param('lastname'),
          email: req.param('email'),
          offerType: offerType,
          user: req.isAuthenticated() ? req.user.id : undefined,
        }).fetch();
        Mailer.sendHelpMail(
          req.isAuthenticated()
            ? req.user
            : {
                name: req.param('name'),
                email: req.param('email'),
                lastname: req.param('lastname'),
              },
          help,
        );
        return res.status(200).json({ message: "demande d'aide enregistrée avec succès" });
      } else {
        if (
          !_.isUndefined(req.param('phone')) ||
          !_.isUndefined(req.param('requestDate')) ||
          !_.isUndefined(req.param('hour')) ||
          !_.isUndefined(req.param('title')) ||
          !_.isUndefined(req.param('name')) ||
          !_.isUndefined(req.param('lastname')) ||
          !_.isUndefined(req.param('email'))
        ) {
          if (req.param('offerType') === 'CR') {
            offerType = 'RC';
          }
          if (req.param('offerType') === 'PMR') {
            offerType = 'MRP';
          }
          if (req.param('offerType') === 'CRPMR') {
            offerType = 'RC + MRP';
          }
          if (req.param('offerType') === 'PMRCR') {
            offerType = 'MRP + RC';
          }
          if (req.param('offerType') === 'MD') {
            offerType = 'MD';
          }
          if (req.param('offerType') === 'RCMS') {
            offerType = 'RCMS';
          }
          help = await Help.create({
            phone: req.param('phone'),
            date: moment(new Date(req.param('requestDate'))).format('YYYY-MM-DD'),
            hour: req.param('hour'),
            choice: req.param('timing'),
            title: req.param('title'),
            name: req.param('name'),
            lastname: req.param('lastname'),
            email: req.param('email'),
            offerType: offerType,
            user: req.isAuthenticated() ? req.user.id : undefined,
          }).fetch();
          Mailer.sendHelpMail(
            req.isAuthenticated()
              ? req.user
              : {
                  name: req.param('name'),
                  email: req.param('email'),
                  lastname: req.param('lastname'),
                },
            help,
          );
          return res.status(200).json({ message: "demande d'aide enregistrée avec succès" });
        } else {
          return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
        }
      }
    } else {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
  },

  // save custom rate request
  async saveCustomRate(req, res) {
    if (
      // _.isUndefined(req.param('turnover')) ||
      _.isUndefined(req.param('firstname')) ||
      _.isUndefined(req.param('lastname')) ||
      _.isUndefined(req.param('phone')) ||
      _.isUndefined(req.param('email'))
    ) {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }

    await CustomRate.create({
      // turnover: req.param('turnover'),
      firstname: req.param('firstname'),
      lastname: req.param('lastname'),
      phone: req.param('phone'),
      email: req.param('email'),
      user: req.isAuthenticated() ? req.user.id : undefined,
    });

    return res
      .status(200)
      .json({ message: 'demande de devis personnalisé enregistrée avec succès' });
  },

  // get city name
  async getCityName(req, res) {
    if (_.isUndefined(req.param('postalCode'))) {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
    const city = await Cities.find({ postalCode: req.param('postalCode') });
    if (city.length === 0) {
      return res.status(400).json({ error: 'Code postale introuvable' });
    }
    return res.status(200).json({ message: city });
  },

  // get siren cooredinates
  async getSirenCoordinates(req, res) {
    if (_.isUndefined(req.param('siren'))) {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
    const siren = await Siren.find({ siren: req.param('siren') }).limit(1);
    if (siren.length === 0) {
      return res.status(400).json({ error: 'Siren introuvable' });
    }
    return res.status(200).json({ message: siren[0] });
  },

  // download rate
  async downloadRate(req, res) {
    if (_.isUndefined(req.param('filename'))) {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
    const adhesion = await AdhesionTmp.findOne({
      user: req.user.id,
      rateFileName: req.param('filename'),
    });
    if (adhesion) {
      res.download(path.join(__dirname, '..', '..', '.permanent', 'rates', req.param('filename')));
    } else {
      res.serverError('Devis Introuvable');
    }
  },

  // download contract
  async downloadContract(req, res) {
    if (_.isUndefined(req.param('filename'))) {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
    const contract = await Contract.findOne({
      user: req.param('user') !== 'undefined' ? req.param('user') : req.user.id,
      contractFileName: req.param('filename'),
    });
    if (contract) {
      res.download(
        path.join(__dirname, '..', '..', '.permanent', 'contracts', req.param('filename')),
      );
    } else {
      res.serverError('Contrat Introuvable');
    }
  },

  // download rate
  async downloadRibImg(req, res) {
    if (_.isUndefined(req.param('filename'))) {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
    const payment = await PaymentMethod.findOne({
      user: req.user.id,
      ribImg: req.param('filename'),
    });
    if (payment) {
      res.download(
        path.join(__dirname, '..', '..', '.permanent', 'ribFiles', req.param('filename')),
      );
    } else {
      res.serverError('Devis Introuvable');
    }
  },

  // save contact
  async saveContact(req, res) {
    if (
      _.isUndefined(req.param('customer')) ||
      _.isUndefined(req.param('firstname')) ||
      _.isUndefined(req.param('lastname')) ||
      _.isUndefined(req.param('email')) ||
      _.isUndefined(req.param('mobile')) ||
      _.isUndefined(req.param('objet')) ||
      _.isUndefined(req.param('message'))
    ) {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }

    await Contact.create({
      customer: req.param('customer'),
      firstname: req.param('firstname'),
      lastname: req.param('lastname'),
      phone: req.param('phone'),
      email: req.param('email'),
      mobile: req.param('mobile'),
      message: req.param('message'),
      objet: req.param('objet'),
      user: req.isAuthenticated() ? req.user.id : undefined,
    });

    return res.status(200).json({ message: 'Demande de contact enregistrée avec succès' });
  },

  // save partnership
  async savePartnership(req, res) {
    if (
      _.isUndefined(req.param('firstname')) ||
      _.isUndefined(req.param('lastname')) ||
      _.isUndefined(req.param('email')) ||
      _.isUndefined(req.param('city')) ||
      _.isUndefined(req.param('phone')) ||
      _.isUndefined(req.param('day')) ||
      _.isUndefined(req.param('fromHour')) ||
      _.isUndefined(req.param('toHour'))
    ) {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }

    const data = await Partnership.create({
      firstname: req.param('firstname'),
      lastname: req.param('lastname'),
      phone: req.param('phone'),
      city: req.param('city'),
      email: req.param('email'),
      day: req.param('day'),
      fromHour: req.param('fromHour'),
      toHour: req.param('toHour'),
    }).fetch();

    Mailer.sendPartnershipMail(data, false);
    Mailer.sendPartnershipMail(data, true);

    return res.status(200).json({ message: 'Demande de partenariat enregistrée avec succès' });
  },

  // save sinister
  async saveSinister(req, res) {
    if (
      _.isUndefined(req.param('reportingDate')) ||
      _.isUndefined(req.param('provisionAmount')) ||
      _.isUndefined(req.param('regulationAmount')) ||
      _.isUndefined(req.param('status')) ||
      _.isUndefined(req.param('franchise')) ||
      _.isUndefined(req.param('statementScan')) ||
      _.isUndefined(req.param('challengeScan'))
    ) {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }

    await Sinister.create({
      reportingDate: moment(new Date(req.param('reportingDate'))).format('YYYY-MM-DD'),
      provisionAmount: req.param('provisionAmount'),
      regulationAmount: req.param('regulationAmount'),
      status: req.param('status'),
      franchise: req.param('franchise'),
      statementScan: req.param('statementScan'),
      challengeScan: req.param('challengeScan'),
      user: req.user.id,
      contract: req.param('contract') ? req.param('contract') : undefined,
    }).fetch();

    return res.status(200).json({ redirect: 'success-sinistre', target: undefined });
  },

  async savePaymentMethod(req, res) {
    if (_.isUndefined(req.param('paymentMethod')) || _.isUndefined(req.param('offerType'))) {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
    await AdhesionTmp.update(
      { user: req.user.id, offerType: req.param('offerType') },
      {
        firstPaymentType: req.param('paymentMethod'),
      },
    );
    return res.status(200).json({ message: 'Type de payment enregistré avec succès' });
  },

  async saveCustomQuote(req, res) {
    if (
      _.isUndefined(req.param('offer')) ||
      _.isUndefined(req.param('firstname')) ||
      _.isUndefined(req.param('lastname')) ||
      _.isUndefined(req.param('requestEmail')) ||
      _.isUndefined(req.param('phone'))
    ) {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
    await CustomQuote.create({
      offer: req.param('offer'),
      name: req.param('firstname'),
      lastname: req.param('lastname'),
      telephone: req.param('requestEmail'),
      email: req.param('phone'),
    });
    return res.status(200).json({ message: 'devis personnalisé enregistré avec succès' });
  },
};
