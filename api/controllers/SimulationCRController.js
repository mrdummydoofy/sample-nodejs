/**
 * SimulationController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  // Civil Responsability first form post
  async postCr(req, res) {
    // test params validity
    if (
      !_.isUndefined(req.param('chiffreAffaires')) ||
      !_.isUndefined(req.param('activite')) ||
      !_.isUndefined(req.param('type'))
    ) {
      // do not permit turnover > 1500000 euros
      if (parseInt(req.param('chiffreAffaires')) <= 1500000) {
        // test existance of customer
        Customer.findOrCreate(
          { user: req.user.id },
          {
            turnover: parseInt(req.param('chiffreAffaires')),
            activity: req.param('activite'),
            type: req.param('type'),
            user: req.user.id,
          },
        ).exec(async (err, customer, wasCreated) => {
          if (err) {
            return res.serverError(err);
          }
          if (wasCreated) {
            return res.status(200).json({ message: 'Informations crées avec succès' });
          } else {
            await Customer.update(
              { user: req.user.id },
              {
                turnover: parseInt(req.param('chiffreAffaires')),
                activity: req.param('activite'),
                type: req.param('type'),
              },
            );
            return res.status(200).json({ message: 'Mise à jour des informations avec succès' });
          }
        });
      } else {
        return res
          .status(400)
          .json({ error: "Chiffre d'affaires HT ne doit pas dépasser 1500000 euros" });
      }
    } else {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
  },

  // Quotation of Civil Responsability(CR)
  async crQuotation(req, res) {
    // test params validity
    if (!_.isUndefined(req.param('chiffreAffaires')) || !_.isUndefined(req.param('activite'))) {
      if (req.param('activite') === 'urbain') {
        res.status(200).json({
          crUrbanQuotation: Quotation.crUrbanQuotation(parseInt(req.param('chiffreAffaires')))
            .crUrbanQuotation,
        });
      } else {
        res.status(200).json({
          crRuralMixteQuotation: Quotation.crRuralMixteQuotation(
            parseInt(req.param('chiffreAffaires')),
          ).crRuralMixteQuotation,
        });
      }

      // params are invalid
    } else {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
  },

  // Tax of urban CR
  crUrbanTax(req, res) {
    // test params validity
    if (
      !_.isUndefined(req.param('chiffreAffaires')) ||
      !_.isUndefined(req.param('fraction')) ||
      !_.isUndefined(req.param('effectDate'))
    ) {
      res.status(200).json({
        fractQuotation: Quotation.rcUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).fractQuotation,
        cashQuotation: Quotation.rcUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).cashQuotation,
        totalTax: Quotation.rcUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).TOTAL_TAX,
        totalTaxSem: Quotation.rcUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).TOTAL_TAX_SEM,
        totalTaxTrim: Quotation.rcUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).TOTAL_TAX_TRIM,
        totalTaxMens: Quotation.rcUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).TOTAL_TAX_MENS,
        fraisCourtier: Quotation.rcUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).FRAIS_COURTIER,
        fraisCourtierSem: Quotation.rcUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).FRAIS_COURTIER_SEM,
        fraisCourtierTrim: Quotation.rcUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).FRAIS_COURTIER_TRIM,
        fraisCourtierMens: Quotation.rcUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).FRAIS_COURTIER_MENS,
        totalTaxFraisContr: Quotation.rcUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).TOTAL_TAX_FRAIS_CONTR,
        totalTaxFraisContrSem: Quotation.rcUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).TOTAL_TAX_FRAIS_CONTR_SEM,
        totalTaxFraisContrTrim: Quotation.rcUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).TOTAL_TAX_FRAIS_CONTR_TRIM,
        totalTaxFraisContrMens: Quotation.rcUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).TOTAL_TAX_FRAIS_CONTR_MENS,
        totalHT: Quotation.rcUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).TOTAL_COT_HT,
        totalHTSem: Quotation.rcUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).TOTAL_COT_HT_SEM,
        totalHTTrim: Quotation.rcUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).TOTAL_COT_HT_TRIM,
        totalHTMens: Quotation.rcUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).TOTAL_COT_HT_MENS,
      });
      // params are invalid
    } else {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
  },

  // Tax of rural or mixed CR
  crRuralTax(req, res) {
    // test params validity
    if (
      !_.isUndefined(req.param('chiffreAffaires')) ||
      !_.isUndefined(req.param('fraction')) ||
      !_.isUndefined(req.param('franchise')) ||
      !_.isUndefined(req.param('effectDate'))
    ) {
      res.status(200).json({
        fractQuotation: Quotation.rcRuralCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).fractQuotation,
        cashQuotation: Quotation.rcRuralCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).cashQuotation,
        totalTax: Quotation.rcRuralCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).TOTAL_TAX,
        totalTaxSem: Quotation.rcRuralCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).TOTAL_TAX_SEM,
        totalTaxTrim: Quotation.rcRuralCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).TOTAL_TAX_TRIM,
        totalTaxMens: Quotation.rcRuralCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).TOTAL_TAX_MENS,
        fraisCourtier: Quotation.rcRuralCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).FRAIS_COURTIER,
        fraisCourtierSem: Quotation.rcRuralCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).FRAIS_COURTIER_SEM,
        fraisCourtierTrim: Quotation.rcRuralCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).FRAIS_COURTIER_TRIM,
        fraisCourtierMens: Quotation.rcRuralCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).FRAIS_COURTIER_MENS,
        totalTaxFraisContr: Quotation.rcRuralCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).TOTAL_TAX_FRAIS_CONTR,
        totalTaxFraisContrSem: Quotation.rcRuralCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).TOTAL_TAX_FRAIS_CONTR_SEM,
        totalTaxFraisContrTrim: Quotation.rcRuralCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).TOTAL_TAX_FRAIS_CONTR_TRIM,
        totalTaxFraisContrMens: Quotation.rcRuralCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).TOTAL_TAX_FRAIS_CONTR_MENS,
        totalHT: Quotation.rcRuralCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).TOTAL_COT_HT,
        totalHTSem: Quotation.rcRuralCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).TOTAL_COT_HT_SEM,
        totalHTTrim: Quotation.rcRuralCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).TOTAL_COT_HT_TRIM,
        totalHTMens: Quotation.rcRuralCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          undefined,
          req.param('effectDate'),
        ).TOTAL_COT_HT_MENS,
      });
      // params are invalid
    } else {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
  },
};
