/**
 * SimulationPMRCRControllerController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  // Professional Multi Risk first form post
  async postPmrCr(req, res) {
    // test params validity
    if (
      !_.isUndefined(req.param('chiffreAffaires')) ||
      !_.isUndefined(req.param('activite')) ||
      !_.isUndefined(req.param('type'))
    ) {
      // do not permit turnover > 1500000 euros
      if (parseInt(req.param('chiffreAffaires')) <= 1500000) {
        // test existance of customer
        Customer.findOrCreate(
          { user: req.user.id },
          {
            turnover: parseInt(req.param('chiffreAffaires')),
            activity: req.param('activite'),
            type: req.param('type'),
            user: req.user.id,
          },
        ).exec(async (err, customer, wasCreated) => {
          if (err) {
            return res.serverError(err);
          }
          if (wasCreated) {
            return res.status(200).json({ message: 'Informations crées avec succès' });
          } else {
            await Customer.update(
              { user: req.user.id },
              {
                turnover: parseInt(req.param('chiffreAffaires')),
                activity: req.param('activite'),
                type: req.param('type'),
              },
            );
            return res.status(200).json({ message: 'Mise à jour des informations avec succès' });
          }
        });
      } else {
        return res.status(400).json({
          error:
            'Informations ne correspondent pas aux critères minimales pour faire une simualation',
        });
      }
    } else {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
  },
  // Quotation of Professional Multi Risk(PMR + CR) in the simulation
  PmrCrQuotationPromo(req, res) {
    // test params validity
    if (!_.isUndefined(req.param('chiffreAffaires')) || !_.isUndefined(req.param('activite'))) {
      // if the activity is urban
      if (req.param('activite') === 'urbain') {
        res.status(200).json({
          mrpRcUrbanQuotation: Quotation.mrpRcUrbanQuotation(parseInt(req.param('chiffreAffaires')))
            .mrpRcUrbanQuotationPromo,
        });
      } // if the activity is rural or mixed
      else {
        res.status(200).json({
          mrpRcRuralMixteQuotation: Quotation.mrpRcRuralMixteQuotation(
            parseInt(req.param('chiffreAffaires')),
          ).mrpRcRuralMixteQuotationPromo,
        });
      }
      // params are invalid
    } else {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
  },

  // Quotation of Professional Multi Risk(PMR + CR)
  async PmrCrQuotation(req, res) {
    // test params validity
    if (!_.isUndefined(req.param('chiffreAffaires')) || !_.isUndefined(req.param('activite'))) {
      // with franchise 300
      if (req.param('activite') === 'urbain') {
        res.status(200).json({
          mrpRcUrbanQuotation: Quotation.mrpRcUrbanQuotation(parseInt(req.param('chiffreAffaires')))
            .mrpRcUrbanQuotation,
        });
      } else {
        res.status(200).json({
          mrpRcRuralMixteQuotation: Quotation.mrpRcRuralMixteQuotation(
            parseInt(req.param('chiffreAffaires')),
          ).mrpRcRuralMixteQuotation,
        });
      }
      // params are invalid
    } else {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
  },

  // Warraties of Professional Multi Risk(PMR + CR)
  PmrCrWarranty(req, res) {
    // test params validity
    if (!_.isUndefined(req.param('chiffreAffaires')) || !_.isUndefined(req.param('bdm'))) {
      // test existance of customer
      res.status(200).json({
        perteRecettes: Warranty.mrpWarranties(
          parseInt(req.param('chiffreAffaires')),
          req.param('bdm'),
        ).perteRecettes,
        perteValeurVenale: Warranty.mrpWarranties(
          parseInt(req.param('chiffreAffaires')),
          req.param('bdm'),
        ).perteValeurVenale,
        incendieReconstitution: Warranty.mrpWarranties(
          parseInt(req.param('chiffreAffaires')),
          req.param('bdm'),
        ).incendieReconstitution,
        tempeteReconstitution: Warranty.mrpWarranties(
          parseInt(req.param('chiffreAffaires')),
          req.param('bdm'),
        ).tempeteReconstitution,
        vandalismeReconstitution: Warranty.mrpWarranties(
          parseInt(req.param('chiffreAffaires')),
          req.param('bdm'),
        ).vandalismeReconstitution,
        brisMachine: Warranty.mrpWarranties(
          parseInt(req.param('chiffreAffaires')),
          req.param('bdm'),
        ).brisMachine,
        fraisSupplementaire: Warranty.mrpWarranties(
          parseInt(req.param('chiffreAffaires')),
          req.param('bdm'),
        ).fraisSupplementaire,
      });
      // params are invalid
    } else {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
  },

  // Tax of urban (PMR + CR)
  pmrCrUrbanTax(req, res) {
    // test params validity
    if (
      !_.isUndefined(req.param('chiffreAffaires')) ||
      !_.isUndefined(req.param('fraction')) ||
      !_.isUndefined(req.param('franchise')) ||
      !_.isUndefined(req.param('effectDate')) ||
      !_.isUndefined(req.param('alarm')) ||
      !_.isUndefined(req.param('bdm'))
    ) {
      res.status(200).json({
        mrpRcUrbanQuotation: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).mrpRcUrbanQuotation,
        fractQuotation: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).fractQuotation,
        cashQuotation: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).cashQuotation,
        contrAttentat: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).CONTRB_ATT,
        contrAttentatSem: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).CONTRB_ATT_SEM,
        contrAttentatTrim: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).CONTRB_ATT_TRIM,
        contrAttentatMens: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).CONTRB_ATT_MENS,
        totalTaxFraisContr: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_TAX_FRAIS_CONTR,
        totalTaxFraisContrSem: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_TAX_FRAIS_CONTR_SEM,
        totalTaxFraisContrTrim: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_TAX_FRAIS_CONTR_TRIM,
        totalTaxFraisContrMens: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_TAX_FRAIS_CONTR_MENS,
        totalTax: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOT_TAXES,
        totalTaxSem: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_TAX_SEM,
        totalTaxTrim: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_TAX_TRIM,
        totalTaxMens: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_TAX_MENS,
        fraisCourtier: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).FRAIS_COURTIER,
        fraisCourtierSem: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).FRAIS_COURTIER_SEM,
        fraisCourtierTrim: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).FRAIS_COURTIER_TRIM,
        fraisCourtierMens: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).FRAIS_COURTIER_MENS,
        fraisAssistance: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).COT_ASSISTANCE_TTC,
        TaxINCENDIE: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_INCENDIE,
        TaxTEMPETE: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_TEMPETE,
        TaxVANDALISME: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_VANDALISME,
        TaxDELECT: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_DELECT,
        TaxDDE: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_DDE,
        TaxBDG: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_BDG,
        TaxVOL: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_VOL,
        TaxPE: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_PE,
        TotalHT: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
        ).TOT_HT,
        TotalHTMens: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_COT_HT_MENS,
        TotalHTSem: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_COT_HT_SEM,
        TotalHTTrim: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_COT_HT_TRIM,
        TaxATT: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_ATT,
        RedVolHT: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_VOL_HT,
        RedATT: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_ATT,
        RedCATNAT: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_CATNAT,
        RedTAX: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_TAX,
        RedVOLTTC: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_VOL_TTC,
        RedVOLTTCSEM: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_VOL_TTC_SEM,
        RedVOLTTCTRIM: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_VOL_TTC_TRIM,
        RedVOLTTCMENS: Quotation.MrpCrUrbanCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_VOL_TTC_MENS,
      });
      // params are invalid
    } else {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
  },

  // Tax of rural or mixed (PMR + CR)
  pmrCrRuralMixedTax(req, res) {
    // test params validity
    if (
      !_.isUndefined(req.param('chiffreAffaires')) ||
      !_.isUndefined(req.param('fraction')) ||
      !_.isUndefined(req.param('franchise')) ||
      !_.isUndefined(req.param('effectDate')) ||
      !_.isUndefined(req.param('alarm')) ||
      !_.isUndefined(req.param('bdm'))
    ) {
      res.status(200).json({
        fractQuotation: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).fractQuotation,
        cashQuotation: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).cashQuotation,
        contrAttentat: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).CONTRB_ATT,
        contrAttentatSem: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).CONTRB_ATT_SEM,
        contrAttentatTrim: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).CONTRB_ATT_TRIM,
        contrAttentatMens: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).CONTRB_ATT_MENS,
        totalTaxFraisContr: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_TAX_FRAIS_CONTR,
        totalTaxFraisContrSem: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_TAX_FRAIS_CONTR_SEM,
        totalTaxFraisContrTrim: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_TAX_FRAIS_CONTR_TRIM,
        totalTaxFraisContrMens: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_TAX_FRAIS_CONTR_MENS,
        totalTax: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOT_TAXES,
        totalTaxSem: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_TAX_SEM,
        totalTaxTrim: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_TAX_TRIM,
        totalTaxMens: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_TAX_MENS,
        fraisCourtier: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).FRAIS_COURTIER,
        fraisCourtierSem: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).FRAIS_COURTIER_SEM,
        fraisCourtierTrim: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).FRAIS_COURTIER_TRIM,
        fraisCourtierMens: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).FRAIS_COURTIER_MENS,
        fraisAssistance: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).COT_ASSISTANCE_TTC,
        TaxINCENDIE: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_INCENDIE,
        TaxTEMPETE: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_TEMPETE,
        TaxVANDALISME: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_VANDALISME,
        TaxDELECT: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_DELECT,
        TaxDDE: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
        ).TAX_DDE,
        TaxBDG: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_BDG,
        TaxVOL: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_VOL,
        TaxPE: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_PE,
        TotalHT: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOT_HT,
        TotalHTMens: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_COT_HT_MENS,
        TotalHTSem: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_COT_HT_SEM,
        TotalHTTrim: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TOTAL_COT_HT_TRIM,
        TaxATT: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).TAX_ATT,
        RedVolHT: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_VOL_HT,
        RedATT: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_ATT,
        RedCATNAT: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_CATNAT,
        RedTAX: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_TAX,
        RedVOLTTC: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_VOL_TTC,
        RedVOLTTCSEM: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_VOL_TTC_SEM,
        RedVOLTTCTRIM: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_VOL_TTC_TRIM,
        RedVOLTTCMENS: Quotation.MrpCrRuralMixedCalculateTAX(
          parseInt(req.param('chiffreAffaires')),
          parseInt(req.param('fraction')),
          parseInt(req.param('franchise')),
          undefined,
          req.param('effectDate'),
          undefined,
          req.param('alarm') === 'oui' ? true : false,
          req.param('bdm'),
        ).RED_VOL_TTC_MENS,
      });
      // params are invalid
    } else {
      return res.status(400).json({ error: 'Veuillez saisir les informations requises' });
    }
  },
};
